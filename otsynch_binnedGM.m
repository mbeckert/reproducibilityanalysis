load('f:\desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted.mat','OT');
data = OT; clear OT

Window = sort([-0.1 0.1]);
binWidth = 0.001;
binNum = length(Window(1):binWidth:Window(2))-2;
norminc = 0:0.1:1;

dur = 0.15;
pattern = 'frozen.|b.|FF.';                   % These are to remove
pattern2 = '.';                   % These are to keep

D=[];

for date=1:size(data,2)
    for site = 1:length(data{date})
        list=combnk(2:length(data{date}{site}),2);
        for pair = 1:size(list,1)
            
            disp(['date: ' num2str(date) '; site: ' num2str(site) '; pair: ' num2str(pair)]);
            
            unit1 = data{date}{site}{list(pair,1)};
            unit2 = data{date}{site}{list(pair,2)};
            filetypes1 = fieldnames(unit1);
            filetypes2 = fieldnames(unit2);
            filetypes = intersect(filetypes1,filetypes2);
            idx = regexpi(filetypes,pattern);
            idx = cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            idx = regexpi(filetypes,pattern2);
            idx = ~cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            
            clear filetypes1 filetypes2 idx
            
            for file = 1:length(filetypes)
                
                gm = sqrt(unit1.(filetypes{file}).spikes_count(:) .* unit2.(filetypes{file}).spikes_count(:));
                gm = gm/max(gm);
                [~,~,idx] = histcounts(gm,norminc);
                
                CCG = zeros(length(norminc),binNum);
                shiftCCG = zeros(length(norminc),binNum);
                
                for i = 1:length(norminc)-1
                    trials = find(idx == i);
                    for t = 1:length(trials)
                        if ~isempty(unit1.(filetypes{file}).spikes_times{trials(t)})
                            for spike = 1:length(unit1.(filetypes{file}).spikes_times{trials(t)})
                                times = unit1.(filetypes{file}).spikes_times{trials(t)}(spike) - unit2.(filetypes{file}).spikes_times{trials(t)};
                                his=hist(times,Window(1):binWidth:Window(2)); %% Here is where you can change binwidth
                                CCG(i,:)=CCG(i,:)+his(2:end-1);
                                if trials(t)==length(unit1.(filetypes{file}).spikes_count(:))
                                    times = unit1.(filetypes{file}).spikes_times{trials(t)}(spike) - unit2.(filetypes{file}).spikes_times{1};
                                else
                                    times = unit1.(filetypes{file}).spikes_times{trials(t)}(spike) - unit2.(filetypes{file}).spikes_times{trials(t)+1};
                                end
                                his=hist(times,Window(1):binWidth:Window(2));
                                shiftCCG(i,:)=shiftCCG(i,:)+his(2:end-1);
                            end
                        end
                    end % for "t"
                    CCG(i,:) = CCG(i,:)/length(trials);
                    shiftCCG(i,:) = shiftCCG(i,:)/length(trials);
                end
                
                clear his spike times
                
                holder = cell(1,13);
                holder{1} = [date,site,list(pair,1),list(pair,2)];
                for i = 2:10
                    holder{i} = nan(1,length(norminc)-1);
                end
                for i = 11:13
                    holder{i}=cell(1,length(norminc)-1);
                end
                
                %                 m = mean(unit1.(filetypes{file}).spikes_count,2);
                %                 b = mean(unit1.(filetypes{file}).base_count(:));
                %                 s = std(unit1.(filetypes{file}).base_count(:));
                %                 c = b+2*s;
                %                 chck1 = m >= c;
                %                 m = mean(unit2.(filetypes{file}).spikes_count,2);
                %                 b = mean(unit2.(filetypes{file}).base_count(:));
                %                 s = std(unit2.(filetypes{file}).base_count(:));
                %                 c = b+2*s;
                %                 chck2 = m >= c;
                %                 check2 = chck1 | chck2;
                %                 holder{7} = check2;
                
                for i = 1:length(norminc)-1
                    
                    trials = find(idx == i);
                    
                    rate1=mean(unit1.([filetypes{file}]).spikes_count(trials))/dur;
                    rate2=mean(unit2.([filetypes{file}]).spikes_count(trials))/dur;
                    
                    SmCCG=(smooth(CCG(i,:))/sqrt(rate1*rate2))/binNum;
                    SmShiftCCG=(smooth(shiftCCG(i,:))/sqrt(rate1*rate2))/binNum;
                    
                    SmCCGCorrected=SmCCG-SmShiftCCG;
                    
                    flankS=std([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                    flankM=mean([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                    
                    PEAK=SmCCGCorrected(round(binNum/2));
                    check1 = PEAK>flankM+5*flankS;
                    positive=(SmCCGCorrected>=(PEAK/2));
                    pos=diff(positive);
                    less=pos(1:round(binNum/2));
                    more=pos(round(binNum/2):end);
                    Low=find(less==1,1,'last');
                    High=find(more==-1,1,'first');
                    Low=length(less)-Low;
                    bandPEAK=High+Low;
                    
                    if isempty(bandPEAK)
                        bandPEAK=nan;
                        sumband=nan;
                    else
                        sumband=sum(SmCCGCorrected(length(less)-Low:length(less)+High))/bandPEAK;
                    end
                    
                    holder{2}(1,i)=PEAK;
                    holder{3}(1,i)=rate1;
                    holder{4}(1,i)=rate2;
                    holder{5}(1,i)=check1;
                    %                     holder{6}(1,i)=check2;
                    %                     holder{7}(1,i)=check;
                    holder{8}(1,i)=bandPEAK;
                    holder{9}(1,i)=sum(SmCCGCorrected);
                    holder{10}(1,i)=sumband;
                    holder{11}{1,i}=SmCCG;
                    holder{12}{1,i}=SmShiftCCG;
                    holder{13}{1,i}=SmCCGCorrected;
                    
                    clear positive pos less more Low High bandPEAK
                    
                end     % for "i" the second time
                
                if isfield(D,([filetypes{file}]))
                    for i = 1:10
                        if size(D.(filetypes{file}){i},2) > size(holder{i},2)
                            padding = nan(1,size(D.(filetypes{file}){i},2)-size(holder{i},2));
                            D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i},padding];
                        elseif size(D.(filetypes{file}){i},2) < size(holder{i},2)
                            padding = nan(size(D.(filetypes{file}){i},1),size(holder{i},2)-size(D.(filetypes{file}){i},2));
                            D.([filetypes{file}]){i}=[D.([filetypes{file}]){i},padding;holder{i}];
                        else
                            D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i}];
                        end
                    end
                    for i = 11:13
                        if size(D.(filetypes{file}){i},2) > size(holder{i},2)
                            padding = cell(1,size(D.(filetypes{file}){i},2)-size(holder{i},2));
                            D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i},padding];
                        elseif size(D.(filetypes{file}){i},2) < size(holder{i},2)
                            padding = cell(size(D.(filetypes{file}){i},1),size(holder{i},2)-size(D.(filetypes{file}){i},2));
                            D.([filetypes{file}]){i}=[D.([filetypes{file}]){i},padding;holder{i}];
                        else
                            D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i}];
                        end
                    end
                else
                    for i = 1:length(holder)
                        D.([filetypes{file}]){i}=holder{i};
                    end
                end
                
                clear holder depvar rate* i PEAK check SmCCG curves CORR trial flank* sumband SmShiftCCG shiftCCG SmCCGCorrected CCG
                
            end         % for "file"
            
            clear filetypes
            
        end             % for "pair"
    end                 % for "site"
end                     % for "date"

ot_synch = D;

clearvars -except ot_synch

cd('F:\Desktop\WorkingFolder')

save ot_synch

%% plot CCGs for inspection

load('ot_synch.mat')
data = ot_synch;

filetypes = fieldnames(data);
file = 3;
x = 0.1:0.1:1;

gm = sqrt(data.(filetypes{file}){3} .* data.(filetypes{file}){4});
ccg = data.(filetypes{file}){11};
shift = data.(filetypes{file}){12};
corrected = data.(filetypes{file}){13};
s = data.(filetypes{file}){2};

for u = 1:size(gm,1)
    figure(1)
    subplot(3,size(gm,2),1:size(gm,2))
    plot(x,gm(u,:),'k','LineWidth',2)
    subplot(3,size(gm,2),size(gm,2)+1:size(gm,2)*2)
    plot(x,s(u,:),'g','LineWidth',1)
    for i = 1:size(gm,2)
        subplot(3,size(gm,2),size(gm,2)*2+i)
        plot(1:199,corrected{u,i},'k','LineWidth',2)
        hold on
        plot(1:199,ccg{u,i},'--g','LineWidth',1)
        plot(1:199,shift{u,i},'--r','LineWidth',1)
        hold off
        title(num2str(s(u,i)))
        axis square
    end
    pause
end

%% run correlation analysis

load('ot_synch.mat')
filetypes = fieldnames(ot_synch);

corr_cell = cell(1,length(filetypes));
corr_full = cell(1,length(filetypes));
val = cell(1,length(filetypes));

for file = 1:length(filetypes)
    data = ot_synch.(filetypes{file});
    corr_cell{file} = nan(size(data{1},1),3);
    corr_full{file} = nan(3,2);
    val{file} = cell(1,4);
    figure(file)
    
    for met = 1:3
        switch met
            case 1
                m = 2;
            case 2
                m = 8;
            case 3
                m = 10;
        end
        
        gm = sqrt(data{3}.*data{4});
        d = data{m};
        
        for i = 1:size(gm,1)
            gm(i,:) = normalization(gm(i,:),1);
            d(i,:) = normalization(d(i,:),1);
            if sum(isfinite(d(i,:))) > 2
                corr_cell{file}(i,met)= corr(gm(i,:)',d(i,:)','rows','pairwise');
                corr_cell{file}(i,met) = 0.5*log((1+corr_cell{file}(i,met))/(1-corr_cell{file}(i,met)));       % z-transformation
            end
        end
        val{file}{1} = gm(:);
        val{file}{met+1} = d(:);
        [corr_full{file}(met,1),corr_full{file}(met,2)] = corr(gm(:),d(:),'rows','pairwise');
        subplot(3,1,met)
        scatter(gm(:),d(:),2,'k')
        
    end
    
    val{file} = cell2mat(val{file});
    
end

corr_full = cell2mat(corr_full);
clear filetypes file data m gm i met d