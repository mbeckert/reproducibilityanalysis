load('f:\desktop\WorkingFolder\synchony\ot_synch_ff_del.mat')

win = 5;
% norm = 1;

check = ot_synch.ff{6};

gm = sqrt(ot_synch.ff{3} .* ot_synch.ff{4});
GM = gm;
% gm = normalization(gm, norm);
% gm(~check) = nan;
gm = norm01(gm);

v = 13;

synch = cellfun(@(x) ...
    sum( x(round(length(x)/2) - win : round(length(x)/2) + win)) / (win*2+1), ...
    ot_synch.ff{v});
synch = norm01(synch);
synch(~check) = nan;
% synch(synch < 0) = 0;

pref = nan(size(gm, 1), 1);
ineff = nan(size(gm, 1), 1);

[~, idxp] = max(gm, [], 2);

for i = 1:size(gm, 1)
%     [~, list] = sort(abs(gm(i, :) - (max(gm(i, :)) / 10)));
    [~, list] = sort(gm(i, :));
    for h = 1:length(list)
        if ~isnan(synch(i, list(h)))
            ineff(i) = synch(i, list(h));
            break
        end
    end
    pref(i) = synch(i, idxp(i));
end

figure(v)
subplot(2, 2, 1)
ml = min([pref; ineff]);
mx = max([pref; ineff]);
scatter(pref, ineff, 10, 'fill', 'k')
axis([ml, mx, ml, mx])
hold on
plot([0, mx], [0, mx], '-r')
axis square
xticks([0, 0.5, 1])
yticks([0, 0.5, 1])
xlabel('Normalized Preferred Direction')
ylabel('Normalized Ineffective Direction')

subplot(2, 2, 2)
spec = pref - ineff;
boxplot2_points(1, {spec}, 0.75)
xlim([0 2])
axis square
xticks([])
ylabel('Difference Preferred and Ineffective')

data = [sum(spec > 0), sum(spec == 0), sum(spec < 0)];

subplot(2, 2, 3)
bar(fliplr(data))
xticklabels({'Ineffective', 'Equal', 'Preferred'})
ylabel('Count')

% subplot(2, 2, 3)
% scatter(max(GM, [], 2), spec)
% % %
% subplot(2, 2, 4)
% x = [ones(size(ineff, 1), 1), ones(size(pref, 1), 1) * 2];
% % y = [synch(:, 1), synch(:, end)];
% y = [ineff, pref];
% 
% hold on
% for i = 1:length(x)
%     plot(x(i, :), y(i, :), 'r')
% end
%%
spec = synch(:, end) - synch(:, 1);
data = [sum(spec > 0), sum(spec == 0), sum(spec < 0)];



