% Make sure you are using the correct data.

% Run the individual distance analysis scripts, e.g.
% "varyingDistance_allmetrics"

% Then compile and apply the correction to all data using
% "distance_applycorrection_alldata"

% The distance correction is generated using
% "testing_distanceVfiringrate_allmetrics"

%% Load data and compute the correlation between fr and distance
% for each individual pair or neuron
% Put data into corr_ind for plotting and analysis

load('f:\desktop\WorkingFolder\distance\distance_corrected_data.mat')

corr_ind = cell(1, 3);

met = {'euc', 'vp', 'es'};

for d = 1:3
    switch d
        case 1
            data = icls;
        case 2
            data = ot;
        case 3
            data = special;
    end
    
    corr_ind{d} = cell(size(data, 1), 3);
    
    for t = 1:size(data, 1)
        
        corr_ind{d}{t, 1} = nan(size(data{t}.rf, 1), 1);
        corr_ind{d}{t, 2} = nan(size(data{t}.rf, 1), 1);
        corr_ind{d}{t, 3} = nan(size(data{t}.rf, 1), 1);
        
        for u = 1:size(data{t}.rf, 1)
            %%%%%
            % Use this check to remove data with very low firing rates
            %             if max(data{t}.rf{u}) > 1
            for m = 1:length(met)
                corr_ind{d}{t, m}(u) = corr(data{t}.rf{u}, data{t}.(met{m}){u}, 'rows', 'pairwise');
            end
            %             else
            %                 corr_ind{d}{t, m}(u) = nan;
            %             end
        end
        
    end
    
end

clear d data t u m met


%% This is an alternative correction and correlation 
% where we subtract the expected correlation rather than the expected distance values

load_distance_correction
clear key

load('f:\desktop\WorkingFolder\distance\distance_notcorrected_data.mat')

corr_ind = cell(1, 3);

met = {'euc', 'vp', 'es'};

for d = 1:3
    switch d
        case 1
            data = icls;
        case 2
            data = ot;
        case 3
            data = special;
    end
    
    corr_ind{d} = cell(size(data, 1), 3);
    
    for t = 1:size(data, 1)
        
        corr_ind{d}{t, 1} = nan(size(data{t}.rf, 1), 1);
        corr_ind{d}{t, 2} = nan(size(data{t}.rf, 1), 1);
        corr_ind{d}{t, 3} = nan(size(data{t}.rf, 1), 1);
        
        model = cell(3, 1);
        
        for u = 1:size(data{t}.rf, 1)

                model{1} = cor_euc(data{t}.rf{u});
                model{2} = cor_vp(data{t}.rf{u});
                model{3} = cor_es(data{t}.rf{u});
                
                expect = nan(3, 1);
                observ = nan(3, 1);
                
            for m = 1:length(met)
                expect(m) = corr(data{t}.rf{u}, model{m}, 'rows', 'pairwise');
                observ(m) = corr(data{t}.rf{u}, data{t}.(met{m}){u}, 'rows', 'pairwise');
                corr_ind{d}{t, m}(u) = observ(m) - expect(m);
            end

        end
        
    end
    
end

clear d data t u m met model expect observ cor_es cor_euc cor_vp cfun

%% Here we just compare the slope of the linear regressions
% where we subtract the expected correlation rather than the expected distance values

load_distance_correction
clear key

load('f:\desktop\WorkingFolder\distance\distance_corrected_data.mat')

corr_ind = cell(1, 3);

met = {'euc', 'vp', 'es'};

for d = 1:3
    switch d
        case 1
            data = icls;
        case 2
            data = ot;
        case 3
            data = special;
    end
    
    corr_ind{d} = cell(size(data, 1), 3);
    
    for t = 1:size(data, 1)
        
        corr_ind{d}{t, 1} = nan(size(data{t}.rf, 1), 1);
        corr_ind{d}{t, 2} = nan(size(data{t}.rf, 1), 1);
        corr_ind{d}{t, 3} = nan(size(data{t}.rf, 1), 1);
        
        model = cell(3, 1);
        
        for u = 1:size(data{t}.rf, 1)

                model{1} = cor_euc(data{t}.rf{u});
                model{2} = cor_vp(data{t}.rf{u});
                model{3} = cor_es(data{t}.rf{u});
                
                expect = nan(3, 2);
                observ = nan(3, 2);
                
                for m = 1:length(met)
%                     if max(data{t}.rf{u}) > 1
                        expect(m, :) = polyfit(data{t}.rf{u}, model{m}, 1);
                        observ(m, :) = polyfit(data{t}.rf{u}, data{t}.(met{m}){u}, 1);
                        corr_ind{d}{t, m}(u) = observ(m, 1) - expect(m, 1);
%                     else
%                         corr_ind{d}{t, m}(u) = nan;
%                     end
                end
                
        end
        
    end
    
end

clear d data t u m met model expect observ cor_es cor_euc cor_vp cfun

%% boxplots of individual correlations (grouped by stimulus type)

fignames = {'icls', 'ot', 'special'};
names = cell(1, 3);
names{1} = {'icls itd', 'icls ild', 'icls az', 'icls el'};
names{2} = {'ot itd (unfrozen)', 'ot ild (unfrozen)', 'ot az', 'ot el', 'ot di (frozen)', 'ot array'};
names{3} = {'ot acrossspeakers', 'ot betweenneurons'};

for d = 1:size(corr_ind, 2)
    
    figure('Name', fignames{d}, 'NumberTitle', 'off')
    
    optmin = find_minimal_subplot_dim(corr_ind{d}(:, 1));
    subidx = reshape(1:size(corr_ind{d}, 1), optmin(2), optmin(1));
    
    for t = 1:size(corr_ind{d}, 1)
        
        subplot(size(subidx, 1), size(subidx, 2), subidx(t))
        boxplot2_points(1:3, corr_ind{d}(t, :), 0.75)
        xlim([0 4])
        xticks(1:3)
        xticklabels({'euc', 'vp', 'es'})
        ylabel('correlation')
        title(names{d}{t})
        axis square
        
    end
end

clear d optmin subidx t fignames names

%% boxplots for all correlations grouped by metric (More informative)

fignames = {'euc', 'vp', 'es'};
names = cell(1, 3);
names{1} = {'icls itd', 'icls ild', 'icls az', 'icls el'};
names{2} = {'ot itd (unfrozen)', 'ot ild (unfrozen)', 'ot az', 'ot el', 'ot di (frozen)', 'ot array'};
names{3} = {'ot acrossspeakers', 'ot betweenneurons'};

for m = 1:length(fignames)
    
    data = cell(length(icls) + length(ot) + length(special), 1);
    
    for i = 1:length(icls)
        data{i} = corr_ind{1}{i, m};
    end
    for i = 1:length(ot)
        data{i + length(icls)} = corr_ind{2}{i, m};
    end
    for i = 1:length(special)
        data{i + length(icls) + length(ot)} = corr_ind{3}{i, m};
    end
    
    figure('Name', fignames{m}, 'NumberTitle', 'off')
    boxplot2_points(1:length(icls) + length(ot) + length(special), data, 0.75)
    xticks(1:length(icls) + length(ot) + length(special))
    xticklabels(horzcat(names{:}))
    xtickangle(45)
    ylabel('correlation')
    axis square
    
end

clear fignames names m data i

%% scatter of all data

fignames = {'icls', 'ot', 'special'};

norm = 0;

for d = 1:3
    switch d
        case 1
            data = icls;
        case 2
            data = ot;
        case 3
            data = special;
    end
    
    figure('Name', fignames{d}, 'NumberTitle', 'off')
    optmin = find_minimal_subplot_dim(data);
    subidx = reshape(1:size(data, 1), optmin(2), optmin(1));
    
    for t = 1:size(data, 1)
        
        %%%%%
        % use this if you want to remove data with very low firing rate
        %         for i = 1:size(data{t}.rf, 1)
        %             if max(data{t}.rf{i}) < 1
        %                 data{t}.euc{i} = nan;
        %                 data{t}.vp{i} = nan;
        %                 data{t}.es{i} = nan;
        %                 data{t}.rf{i} = nan;
        %             end
        %         end
        %%%%%
        
        if norm
            data{t}.vp = cellfun(@(x) norm01(x, 2), data{t}.vp, 'UniformOutput', 0);
            data{t}.rf = cellfun(@(x) norm01(x, 2), data{t}.rf, 'UniformOutput', 0);
        end
        
        col = distinguishable_colors(size(data{t}.rf, 1));
        
        subplot(size(subidx, 1), size(subidx, 2), subidx(t))
        hold on
        
        for u = 1:size(data{t}.rf, 1)
            scatter(data{t}.rf{u}, data{t}.vp{u}, 5, col(u, :), 'fill')
        end
        
        [r, p] = corr(cell2mat(data{t}.rf), cell2mat(data{t}.vp), 'rows', 'pairwise');
        title([num2str(r) ' ---- ' num2str(p)])
        axis square
        
    end
    
end

clear d data t u m met fignames names optmin p r subidx norm

%% Some extra plots

load('f:\desktop\WorkingFolder\distance\distance_corrected_data.mat', 'ot')
data = ot; clear ot;
%% boxplots of firing rate of each cell and its overall correlation

bins = 0:0.1:11;
mx = 0;

names = {'ot itd (unfrozen)', 'ot ild (unfrozen)', 'ot az', 'ot el', 'ot di (frozen)', 'ot array'};

figure('Name', 'correlation v firing rate', 'NumberTitle', 'off')
optmin = find_minimal_subplot_dim(data);
subidx = reshape(1:size(data, 1), optmin(2), optmin(1));

for t = 1:size(data, 1)
    x = cellfun(@mean, data{t}.rf);
    if max(x) > mx
        mx = max(x);
    end
    y = corr_ind{2}{t, 2};
    [~, ~, idx] = histcounts(x, bins);
    
    pl = cell(length(bins), 1);
    
    for i = 1:length(bins)
        pl{i} = y(idx == i);
    end
    
    subplot(size(subidx, 1), size(subidx, 2), subidx(t))
    boxplot2_points(bins(1:end-1), pl, 0.1)
    xlim([-1 11])
    xlabel('firing rate')
    ylabel('correlation')
    title(names{t})
end

clear t x y i bins names pl

%% for visual inspection of each individual neuron

t = 1;

for u = 1:length(data{t}.rf)
    scatter(data{t}.rf{u}, data{t}.vp{u}, 5, 'fill')
    title(num2str(u))
    pause
end

