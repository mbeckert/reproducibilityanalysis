%% load data and apply the correction for firing rate

cd('f:\desktop\WorkingFolder')

load_distance_correction

disE = cell(6, 1);
disVP = cell(6, 1);
disES = cell(6, 1);
fr = cell(6, 1);

for t = 1:6
    switch t
        case 1
            load('f:\desktop\WorkingFolder\distance\data\iclsITD_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
        case 2
            load('f:\desktop\WorkingFolder\distance\data\iclsILD_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
        case 3
            load('f:\desktop\WorkingFolder\distance\data\iclsAZ_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
        case 4
            load('f:\desktop\WorkingFolder\distance\data\iclsEL_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
        case 5
            load('f:\desktop\WorkingFolder\distance\data\otAZ_distance_unsorted.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
        case 6
            load('f:\desktop\WorkingFolder\distance\data\otEL_distance_unsorted.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
    end
    
    es = cell(size(distanceES));
    euc = cell(size(distanceEuc));
    vp = cell(size(distanceVP));
    
    for u = 1:length(Mean)
        es{u} = nan(size(distanceES{u}));
        euc{u} = nan(size(distanceEuc{u}));
        vp{u} = nan(size(distanceVP{u}));
        for i = 1:size(Mean{u}, 1)
            
            es{u}(i) = distanceES{u}(i) - cor_es(Mean{u}(i));
            euc{u}(i) = distanceEuc{u}(i) - cor_euc(Mean{u}(i));
            vp{u}(i) = distanceVP{u}(i) - cor_vp(Mean{u}(i));
            
        end
    end
    
    disE{t} = euc;
    disVP{t} = vp;
    disES{t} = es;
    fr{t} = Mean;
    
end

clear distanceEuc distanceVP distanceES Mean t u i idx es euc vp

% % plotting raw
stats_raw = cell(6, 3);

col = distinguishable_colors(max(cellfun(@length, fr)));

names = {'Euclidean Distance', 'VP Cost', 'Event Sync'};
fignames = {'ICls&OTff - Euc', 'ICls&OTff - VP', 'ICls&OTff - ES'};
rectype = {'ICls ITD', 'ICls ILD', 'ICls Az', 'ICls El', 'OT Az', 'OT El'};

for m = 1:3
    switch m
        case 1
            data = disE;
            k = key(:, 3);
        case 2
            data = disVP;
            k = key(:, 2);
        case 3
            data = disES;
            k = key(:, 4);
    end
    figure('Name',fignames{m},'NumberTitle','off')
    for t = 1:6
        subplot(3, 2, t)
        hold on
        for i = 1:size(data{t},1)
            scatter(fr{t}{i}, data{t}{i}, 3, col(i, :), 'fill')
        end
        [lin, rob, stats_raw{t, m}] = regression_compare_OrdRob(cell2mat(fr{t}), cell2mat(data{t}), 0);
        X = cell2mat(fr{t});
        plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
        plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
        axis square
%         title(['r = ' num2str(stats_raw{t, m}.coeffcorr(2)) '; p = ' num2str(stats_raw{t, m}.p(1))])
%         R = corr(cell2mat(data{t}), rob(1) + rob(2) * X) ^ 2;
%         title(num2str(R))
        title(rectype{t})
        if t == 5 || t == 6
            xlabel('spike count')
        end
        
        if mod(t, 2) == 1
            ylabel(names{m})
        end
    end
    
end

% % plot normalized data

stats_norm = cell(6, 3);
col = distinguishable_colors(max(cellfun(@length, fr)));

names = {'Euclidean Distance', 'VP Cost', 'Event Sync'};
fignames = {'ICls&OTff - NORM Euc', 'ICls&OTff - NORM VP', 'ICls&OTff - NORMES'};
rectype = {'ICls ITD', 'ICls ILD', 'ICls Az', 'ICls El', 'OT Az', 'OT El'};

for i = 1:length(fr)
    fr{i} = cellfun(@(x) norm01(x, 2), fr{i}, 'UniformOutput', 0);
end

for m = 1:3
    switch m
        case 1
            data = disE;
            k = key(:, 3);
        case 2
            data = disVP;
            k = key(:, 2);
        case 3
            data = disES;
            k = key(:, 4);
    end
    
    for i = 1:length(data)
        data{i} = cellfun(@(x) norm01(x, 2), data{i}, 'UniformOutput', 0);
    end
    
    figure('Name',fignames{m},'NumberTitle','off')
    for t = 1:6
        subplot(3, 2, t)
        hold on
        for i = 1:size(data{t},1)
            scatter(fr{t}{i}, data{t}{i}, 3, col(i, :), 'fill')
        end
        [lin, rob, stats_norm{t, m}] = regression_compare_OrdRob(cell2mat(fr{t}), cell2mat(data{t}), 0);
        X = cell2mat(fr{t});
        plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
        plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
        axis square
%         title(['r = ' num2str(stats_norm{t, m}.coeffcorr(2)) '; p = ' num2str(stats_norm{t, m}.p(1))])
%         R = corr(cell2mat(data{t}), rob(1) + rob(2) * X) ^ 2;
%         title(num2str(R))
        title(rectype{t})
        if t == 5 || t == 6
            xlabel('spike count')
        end
        
        if mod(t, 2) == 1
            ylabel(['normalized ' names{m}])
        end
    end
    
end
