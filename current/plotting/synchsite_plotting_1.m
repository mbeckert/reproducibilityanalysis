load('f:\desktop\WorkingFolder\synchony\ot_synch_ff_del.mat')

win = 7;
norm = 1;

v = 13;

check = ot_synch.ff{6};
gm = sqrt(ot_synch.ff{3} .* ot_synch.ff{4});
% gm(~check) = nan;

synch = cellfun(@(x) ...
    sum( x(round(length(x)/2) - win : round(length(x)/2) + win)) / (win*2+1), ...
    ot_synch.ff{v});

synch(~check) = nan;
synch(isnan(synch)) = 0;
gm(isnan(synch)) = 0;

if norm
    gm = norm01(gm);
    synch = norm01(synch);
end



clear win norm v check

%%

azm = cell(size(gm, 1), 1);
azs = cell(size(gm, 1), 1);
az = cell(size(gm, 1), 1);
synm = cell(size(gm, 1), 1);
syns = cell(size(gm, 1), 1);
syn = cell(size(gm, 1), 1);

for i = 1:size(gm, 1)
    [azm{i}, azs{i}, ~] = SpRF2HorVer(gm(i, :)');
    [synm{i}, syns{i}, ~] = SpRF2HorVer(synch(i, :)');
    az{i} = gm(i, 63:83);
    syn{i} = synch(i, 63:83);
end

clear i

azm = cell2mat(azm);
synm = cell2mat(synm);
azs = cell2mat(azs);
syns = cell2mat(syns);
az = cell2mat(az);
syn = cell2mat(syn);

cd('f:\desktop\WorkingFolder')

%%


for i = 1:size(gm, 1)
    
    figure
    subplot(2, 2, 1)
    SpRFplot_square(gm(i, :)')
    j = colorbar;
    ylabel(j,'GM FR')
    %     caxis([0 1])
    subplot(2, 2, 2)
    SpRFplot_square(synch(i, :)')
    j = colorbar;
    ylabel(j,'Synchrony')
    %     caxis([0 1])
    
    subplot(2, 2, 3)
    errorbar(-100:10:100, azm(i, :), azs(i, :))
    xlabel('azimuth')
    ylabel('mean firing rate')
    
    subplot(2, 2, 4)
    errorbar(-100:10:100, synm(i, :), syns(i, :))
    xlim([-100 100])
    xlabel('azimuth')
    ylabel('mean synchrony')
    
    saveas(gcf, ['synch_SpRF_' num2str(i)], 'png')
    close all
    
end

clear i j

%%

for i = 1:size(gm, 1)
    
    figure
    subplot(2, 2, 1)
    SpRFplot_square(gm(i, :)')
    j = colorbar;
    ylabel(j,'GM FR')
    %     caxis([0 1])
    subplot(2, 2, 2)
    SpRFplot_square(synch(i, :)')
    j = colorbar;
    ylabel(j,'Synchrony')
    %     caxis([0 1])
    
    subplot(2, 2, 3:4)
    yyaxis left
    plot(-100:10:100, az(i, :))
    xlabel('azimuth')
    ylabel('mean firing rate')

    yyaxis right
    plot(-100:10:100, syn(i, :))
    xlim([-100 100])
    xlabel('azimuth')
    ylabel('mean synchrony')
    
    saveas(gcf, ['synch_SpRF_zero_' num2str(i)], 'png')
    close all
    
end

