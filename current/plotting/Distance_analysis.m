fol = {'f:\desktop\WorkingFolder\distance\altcorrections\' ,...
    'f:\desktop\WorkingFolder\distance\varyingQ\'};

q = 64;

cd('f:\desktop\WorkingFolder')

disE = cell(6, 1);
disVP = cell(6, 1);
disES = cell(6, 1);
fr = cell(6, 1);

for t = 1:6
%     switch t
%         case 1
%             load('f:\desktop\WorkingFolder\distance\data\iclsITD_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
%         case 2
%             load('f:\desktop\WorkingFolder\distance\data\iclsILD_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
%         case 3
%             load('f:\desktop\WorkingFolder\distance\data\iclsAZ_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
%         case 4
%             load('f:\desktop\WorkingFolder\distance\data\iclsEL_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
%         case 5
%             load('f:\desktop\WorkingFolder\distance\data\otAZ_distance_unsorted.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
%         case 6
%             load('f:\desktop\WorkingFolder\distance\data\otEL_distance_unsorted.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
%     end
switch t
    case 1
        load([fol{2} 'iclsITD_distance_' num2str(q) '.mat'], 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
    case 2
        load([fol{2} 'iclsILD_distance_' num2str(q) '.mat'], 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
    case 3
        load([fol{2} 'iclsAZ_distance_' num2str(q) '.mat'], 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
    case 4
        load([fol{2} 'iclsEL_distance_' num2str(q) '.mat'], 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
    case 5
        load([fol{2} 'otAZ_distance_' num2str(q) '.mat'], 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
    case 6
        load([fol{2} 'otEL_distance_' num2str(q) '.mat'], 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
end
    
    disE{t} = distanceEuc;
    disVP{t} = distanceVP;
    disES{t} = distanceES;
    fr{t} = Mean;
    
end

clear distanceEuc distanceVP distanceES Mean t

% load('f:\desktop\WorkingFolder\distance\data\distance_correction.mat', 'key')
% load([fol{1} 'distance_correction_c04_q_' num2str(q) '.mat'], 'key', 'cfun')

figure(2)

for t = 1:6
    
    if t == 1 || t == 2
        load([fol{1} 'distance_correction_c02_q_' num2str(q) '.mat'], 'key', 'cfun')
    elseif t == 3 || t == 4
        load([fol{1} 'distance_correction_c04_q_' num2str(q) '.mat'], 'key', 'cfun')
    elseif t == 5 || t == 6
        load([fol{1} 'distance_correction_c01_q_' num2str(q) '.mat'], 'key', 'cfun')
    end
    d = cell2mat(disVP{t});
    f = cell2mat(fr{t});
    
    subplot(2, 3, t)
    scatter(f, d, 'fill')
    xlabel('firing rate')
    ylabel('VP cost')
    [r, p] = corr(f, d, 'rows', 'pairwise');
    title([num2str(r) ' ---- ' num2str(p)])
    hold on 
    plot(key(:, 1), key(:, 2), 'r')
    axis([min(f) max(f)+max(f)*0.1 min(d) max(d)+max(d)*0.1])
    
end

% figure(1)
% for t = 1:6
%     
%     d = cell2mat(disE{t});
%     f = cell2mat(fr{t});
%     
%     subplot(2, 3, t)
%     scatter(f, d, 'fill')
%     xlabel('firing rate')
%     ylabel('euclidean distance')
%     [r, p] = corr(f, d, 'rows', 'pairwise');
%     title([num2str(r) ' ---- ' num2str(p)])
%     hold on 
%     plot(key(:, 1), key(:, 3), 'r')
%     axis([min(f) max(f)+max(f)*0.1 min(d) max(d)+max(d)*0.1])
%     
% end
% clear t d f

% figure(3)
% 
% for t = 1:6
%     
%     d = cell2mat(disES{t});
%     f = cell2mat(fr{t});
%     
%     subplot(2, 3, t)
%     scatter(f, d, 'fill')
%     xlabel('firing rate')
%     ylabel('Quiroga Event Synchronization')
%     [r, p] = corr(f, d, 'rows', 'pairwise');
%     title([num2str(r) ' ---- ' num2str(p)])
%     hold on 
%     plot(key(:, 1), key(:, 4), 'r')
%     axis([min(f) max(f)+max(f)*0.1 min(d) max(d)+max(d)*0.1])
%     
% end
