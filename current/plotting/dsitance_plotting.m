%% Load data and apply a correction to the distance values

load('f:\desktop\WorkingFolder\distance\data\distance_ot_betweenspeakers_noonset.mat')
load_distance_correction

es = cell(size(distanceES));
euc = cell(size(distanceEuc));
vp = cell(size(distanceVP));

for u = 1:length(Mean)
    es{u} = nan(size(distanceES{u}));
    euc{u} = nan(size(distanceEuc{u}));
    vp{u} = nan(size(distanceVP{u}));
    for i = 1:size(Mean{u}, 2)
        
        es{u}(i) = distanceES{u}(i) - cor_es(Mean{u}(i));
        euc{u}(i) = distanceEuc{u}(i) - cor_euc(Mean{u}(i));
        vp{u}(i) = distanceVP{u}(i) - cor_vp(Mean{u}(i));
        
    end
end

clear u i idx

% % plot
stats = cell(3, 2);

col = distinguishable_colors(size(Mean, 1));
met = {'euc', 'vp', 'es'};

figure('Name','OTff - between speakers','NumberTitle','off')

for t = 1:3
    switch t
        case 1
            data = euc;
        case 2
            data = vp;
        case 3
            data = es;
    end
    
    subplot(2, 3, t)
    hold on
    for i = 1:size(Mean,1)
        scatter(Mean{i}, data{i}, 3, col(i, :), 'fill')
    end
    [lin, rob, stats{t, 1}] = regression_compare_OrdRob(cell2mat(Mean')', cell2mat(data')', 0);
    X = cell2mat(Mean');
    plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
    plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
    axis square
    %         title(['r = ' num2str(stats_raw{t, m}.coeffcorr(2)) '; p = ' num2str(stats_raw{t, m}.p(1))])
    R = corr(cell2mat(data(:)')', rob(1) + rob(2) * X', 'rows', 'pairwise') ^ 2;
    title(num2str(R))
    xlabel('spike count')
    ylabel(met{t})
end

% % plot normalized data

col = distinguishable_colors(size(Mean, 1));

E = cellfun(@norm01, euc, 'UniformOutput', 0);
V = cellfun(@norm01, vp, 'UniformOutput', 0);
S = cellfun(@norm01, es, 'UniformOutput', 0);
M = cellfun(@norm01, Mean, 'UniformOutput', 0);

for t = 1:3
    switch t
        case 1
            data = E;
        case 2
            data = V;
        case 3
            data = S;
    end
    
    subplot(2, 3, t+3)
    hold on
    for i = 1:size(M,1)
        scatter(M{i}, data{i}, 3, col(i, :), 'fill')
    end
    [lin, rob, stats{t, 1}] = regression_compare_OrdRob(cell2mat(M')', cell2mat(data')', 0);
    X = cell2mat(M');
    plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
    plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
    axis square
    %         title(['r = ' num2str(stats_raw{t, m}.coeffcorr(2)) '; p = ' num2str(stats_raw{t, m}.p(1))])
    R = corr(cell2mat(data(:)')', rob(1) + rob(2) * X', 'rows', 'pairwise') ^ 2;
    title(num2str(R))
    xlabel('NORM spike count')
    ylabel(['NORM ' met{t}])
    
end

% % Use this to plot the SpRF of the distance metrics

% for i = 1:length(Mean)
%
%     subplot(1,4,1)
%     SpRFplot_square(Mean{i}')
%     title('firing rate')
%     subplot(1,4,2)
%     SpRFplot_square(euc{i}')
%     title('euclidean distance')
%     subplot(1,4,3)
%     SpRFplot_square(vp{i}')
%     title('vp cost')
%     subplot(1,4,4)
%     SpRFplot_square(es{i}')
%     title('event synchronization')
%
%     % saveas(gcf,['OT_dist_' num2str(i)],'png')
%
% end

% % Now load data for the between neuron analysis

load('f:\desktop\WorkingFolder\distance\data\distance_betweenneurons_del_noonset.mat')
load('f:\desktop\WorkingFolder\distance\data\distance_correction.mat', 'key')

euc = cell(length(VP.FFf1), 1);
fr1 = cell(length(VP.FFf1), 1);
fr2 = cell(length(VP.FFf1), 1);
vp = cell(length(VP.FFf1), 1);
es = cell(length(VP.FFf1), 1);

for p = 1:length(VP.FFf1)
    euc{p} = [EUC.FFf1{p} , EUC.FFf2{p}];
    es{p} = [ES.FFf1{p} , ES.FFf2{p}];
    fr1{p} = [FR1.FFf1{p} , FR1.FFf2{p}];
    fr2{p} = [FR2.FFf1{p} , FR2.FFf2{p}];
    vp{p} = [VP.FFf1{p} , VP.FFf2{p}];
end

clear EUC ES FR1 FR2 VP p

euc = cellfun(@(x) mean(x, 2), euc, 'UniformOutput', 0);
es = cellfun(@(x) mean(x, 2), es, 'UniformOutput', 0);
fr1 = cellfun(@(x) mean(x, 2), fr1, 'UniformOutput', 0);
fr2 = cellfun(@(x) mean(x, 2), fr2, 'UniformOutput', 0);
vp = cellfun(@(x) mean(x, 2), vp, 'UniformOutput', 0);

gm = cellfun(@(x1, x2) sqrt( x1 .* x2 ), fr1, fr2, 'UniformOutput', 0);
rm = cellfun(@(x1, x2) ( x1 + x2 ) / 2, fr1, fr2, 'UniformOutput', 0);

clear fr1 fr2

load_distance_correction

for u = 1:length(rm)
    for i = 1:size(rm{u}, 1)
        
        es{u}(i) = es{u}(i) - cor_es(rm{u}(i));
        euc{u}(i) = euc{u}(i) - cor_euc(rm{u}(i));
        vp{u}(i) = vp{u}(i) - cor_vp(rm{u}(i));
        
    end
end

clear i key u
% % Plot raw data

stats = cell(3, 2);

col = distinguishable_colors(size(rm, 1));
met = {'euc', 'vp', 'es'};

figure('Name','OTff - between neurons','NumberTitle','off')

for t = 1:3
    switch t
        case 1
            data = euc;
        case 2
            data = vp;
        case 3
            data = es;
    end
    
    subplot(2, 3, t)
    hold on
    for i = 1:size(rm,1)
        scatter(rm{i}, data{i}, 3, col(i, :), 'fill')
    end
    [lin, rob, stats{t, 1}] = regression_compare_OrdRob(cell2mat(rm), cell2mat(data), 0);
    X = cell2mat(rm);
    plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
    plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
    axis square
    %         title(['r = ' num2str(stats_raw{t, m}.coeffcorr(2)) '; p = ' num2str(stats_raw{t, m}.p(1))])
    R = corr(cell2mat(data), rob(1) + rob(2) * X, 'rows', 'pairwise') ^ 2;
    title(num2str(R))
    xlabel('spike count')
    ylabel(met{t})
    
end

% % plot normalized data

a = cellfun(@(x) norm01(x, 2), es, 'UniformOutput', 0);
z = cellfun(@(x) norm01(x, 2), euc, 'UniformOutput', 0);
y = cellfun(@(x) norm01(x, 2), vp, 'UniformOutput', 0);
x = cellfun(@(x) norm01(x, 2), rm, 'UniformOutput', 0);

for t = 1:3
    switch t
        case 1
            data = z;
        case 2
            data = y;
        case 3
            data = a;
    end
    
    subplot(2, 3, t+3)
    hold on
    for i = 1:size(x,1)
        scatter(x{i}, data{i}, 3, col(i, :), 'fill')
    end
    [lin, rob, stats{t, 1}] = regression_compare_OrdRob(cell2mat(x), cell2mat(data), 0);
    X = cell2mat(x);
    plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
    plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
    axis square
    %         title(['r = ' num2str(stats_raw{t, m}.coeffcorr(2)) '; p = ' num2str(stats_raw{t, m}.p(1))])
    R = corr(cell2mat(data), rob(1) + rob(2) * X, 'rows', 'pairwise') ^ 2;
    title(num2str(R))
    xlabel('NORM spike count')
    ylabel(['NORM ' met{t}])
end