%% load data and apply the correction for firing rate

cd('f:\desktop\WorkingFolder')

load_distance_correction

disE = cell(6, 1);
disVP = cell(6, 1);
disES = cell(6, 1);
fr = cell(6, 1);

for t = 1:6
    switch t
        case 1
            load('f:\desktop\WorkingFolder\distance\iclsITD_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
        case 2
            load('f:\desktop\WorkingFolder\distance\iclsILD_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
        case 3
            load('f:\desktop\WorkingFolder\distance\iclsAZ_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
        case 4
            load('f:\desktop\WorkingFolder\distance\iclsEL_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
        case 5
            load('f:\desktop\WorkingFolder\distance\otAZ_distance_del_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
        case 6
            load('f:\desktop\WorkingFolder\distance\otEL_distance_del_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
    end
    
    es = cell(size(distanceES));
    euc = cell(size(distanceEuc));
    vp = cell(size(distanceVP));
    
    for u = 1:length(Mean)
        es{u} = nan(size(distanceES{u}));
        euc{u} = nan(size(distanceEuc{u}));
        vp{u} = nan(size(distanceVP{u}));
        for i = 1:size(Mean{u}, 1)
            
            es{u}(i) = distanceES{u}(i) - cor_es(Mean{u}(i));
            euc{u}(i) = distanceEuc{u}(i) - cor_euc(Mean{u}(i));
            vp{u}(i) = distanceVP{u}(i) - cor_vp(Mean{u}(i));
            
        end
    end
    
    disE{t} = euc;
    disVP{t} = vp;
    disES{t} = es;
    fr{t} = Mean;
    
end

clear distanceEuc distanceVP distanceES Mean t u i idx es euc vp

% % calculate the correlations for each unit and do boxplots

r = cell(size(fr));
names = {'icls itd', 'icls ild', 'icls az', 'icls el', 'ot az', 'ot el'};
figure('Name', 'icls&otff - distance - ind', 'NumberTitle', 'off')

for t = 1:6
    
    n_units = size(fr{t}, 1);
    
    r{t} = cell(1, 3);
    r{t}{1} = nan(n_units, 1);
    r{t}{2} = nan(n_units, 1);
    r{t}{3} = nan(n_units, 1);
    
    for i = 1:n_units
        r{t}{1}(i) = corr(fr{t}{i}, disE{t}{i}, 'rows', 'pairwise');
        r{t}{2}(i) = corr(fr{t}{i}, disVP{t}{i}, 'rows', 'pairwise');
        r{t}{3}(i) = corr(fr{t}{i}, disES{t}{i}, 'rows', 'pairwise');
    end
    
    subplot(3, 2, t)
    boxplot2_points(1:3, r{t}, 0.75)
    xlim([0 4])
    xticks(1:3)
    xticklabels({'euc', 'vp', 'es'})
    ylabel('correlation')
    title(names{t})
    
end

% % load distance - dichotic ot frozen

load_distance_correction
idx = reshape(1:6, 3, 2)';
type = {'OTdi - neuron - frozen', 'OTdi - neuron - itd', 'OTdi - neuron - ild'};
met = {'euc', 'vp', 'es'};

es = cell(1, 3);
vp = cell(1, 3);
euc = cell(1, 3);
fr = cell(1, 3);

for t = 1:3
    switch t
        case 1
            load('f:\desktop\WorkingFolder\distance\distance_OTdichotic_froz_noonset.mat')
            clear FROZ
        case 2
            load('f:\desktop\WorkingFolder\distance\distance_OTdichotic_itd_noonset.mat')
            clear ITD
        case 3
            load('f:\desktop\WorkingFolder\distance\distance_OTdichotic_ild_noonset.mat')
            clear ILD
    end
    
    % Apply the correction
    es{t} = cell(size(es_c));
    euc{t} = cell(size(euc_c));
    vp{t} = cell(size(vp_c));
    fr{t} = rm_c;
    
    for u = 1:length(rm_c)
        es{t}{u} = nan(size(es_c{u}));
        euc{t}{u} = nan(size(euc_c{u}));
        vp{t}{u} = nan(size(vp_c{u}));
        for s = 1:size(rm_c{u}, 1)
            
            es{t}{u}(s) = es_c{u}(s) - cor_es(rm_c{u}(s));
            euc{t}{u}(s) = euc_c{u}(s) - cor_euc(rm_c{u}(s));
            vp{t}{u}(s) = vp_c{u}(s) - cor_vp(rm_c{u}(s));
            
        end
    end
    
end

% % plotting

figure('Name','OT - dichotic - indiv','NumberTitle','off')

names = {'di froz', 'itd', 'ild'};

for t = 1:3
    
n_units = size(fr{t}, 1);

r{t} = cell(1, 3);
r{t}{1} = nan(n_units, 1);
r{t}{2} = nan(n_units, 1);
r{t}{3} = nan(n_units, 1);

for i = 1:n_units
    r{t}{1}(i) = corr(fr{t}{i}, euc{t}{i}, 'rows', 'pairwise');
    r{t}{2}(i) = corr(fr{t}{i}, vp{t}{i}, 'rows', 'pairwise');
    r{t}{3}(i) = corr(fr{t}{i}, es{t}{i}, 'rows', 'pairwise');
end

subplot(1, 3, t)
boxplot2_points(1:3, r{t}, 0.75)
xlim([0 4])
xticks(1:3)
xticklabels({'euc', 'vp', 'es'})
ylabel('correlation')
title(names{t})
axis square

end

clear names t n_units i

% %

load('f:\desktop\WorkingFolder\distance\arrayITD_distance_noonset.mat')
load_distance_correction

idx = cellfun(@(x) length(size(x)) ~= 2, rf);
es(idx) = []; euc(idx) = []; vp(idx) = []; rf(idx) = [];
idx = cellfun(@isempty, rf);
es(idx) = []; euc(idx) = []; vp(idx) = []; rf(idx) = [];
clear idx

for u = 1:length(rf)
    for i = 1:size(rf{u}, 2)
        
        es{u}(i) = es{u}(i) - cor_es(rf{u}(i));
        euc{u}(i) = euc{u}(i) - cor_euc(rf{u}(i));
        vp{u}(i) = vp{u}(i) - cor_vp(rf{u}(i));
        
    end
end

% %

figure('Name','OT - Array - indiv','NumberTitle','off')
n_units = size(rf, 1);

r = cell(1, 3);

r{1} = nan(n_units, 1);
r{2} = nan(n_units, 1);
r{3} = nan(n_units, 1);

for i = 1:n_units
    r{1}(i) = corr(rf{i}', euc{i}, 'rows', 'pairwise');
    r{2}(i) = corr(rf{i}', vp{i}, 'rows', 'pairwise');
    r{3}(i) = corr(rf{i}', es{i}, 'rows', 'pairwise');
end

boxplot2_points(1:3, r, 0.75)
xlim([0 4])
xticks(1:3)
xticklabels({'euc', 'vp', 'es'})
ylabel('correlation')
title('array')
axis square