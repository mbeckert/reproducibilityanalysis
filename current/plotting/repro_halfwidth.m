fol = 'f:\desktop\WorkingFolder\working_reprod\frompython\';
region = 'ot';
bins = {'50micro', '100micro', '1ms', '2ms', '3ms', '5ms', '10ms'};
% bins = {'50micro'};

figure

for b = 1:length(bins)
    load([fol 'repro_' region '_curve_' bins{b} '.mat'])
    x = (-0.1:cw:0.1)* 1000;
    x = x(2:end-1);
    
    neu = unique(neuron);
    cur = nan(length(neu), size(curve, 2));
    
    for n = 1:length(neu)
        ix = neuron == neu(n);
        [~, m] = max(fr(ix));
        cur(n, :) = (curve(m, :) - fr(m) ^ 2) * cw / fr(m);
    end
    
    clear curve depvar fr full_fr ix m n neu neuron
    
%     cur = norm01(cur);
    data = nanmean(cur);
%     s = smooth(data, 10);
    s = data * 1000;
    center = round(length(s)/2);
    floor = nanmean(s(s < 0));
%     floor = 0;
    half = (s(center) - floor) / 2;
    over = (s - floor) > half;
    
    subplot(1, length(bins), b)
    ax = gca;
    ax.FontSize = 12;
    plot(x, s, 'k')
    hold on
    plot([min(x) max(x)], [floor floor], 'b')
    plot([min(x) max(x)], [half+floor half+floor], 'b--')
    if b == 1
        ylabel(['Reproducibility' newline '(coincidences/spike)' newline '(x 10^-^3)'], 'FontSize', 18)
    end
%     yyaxis right
%     plot(x, over, 'r')
    axis square
    xlim([-0.05 0.05] * 1000)
    xticks(-50:25:50)
    xlabel('Time (ms)', 'FontSize', 18)

    time = (find(over(center:end) == 0, 1, 'first') * 2 - 1) * cw;
    title(num2str(time * 1000))
%     title(bins(b))
end

