fol = {'f:\desktop\WorkingFolder\distance\altcorrections\' ,...
    'f:\desktop\WorkingFolder\distance\varyingQ\', ...
    'f:\desktop\WorkingFolder\distance\varyingQ\backup_wrongMean\'};

q = 100;
f = 2;

icls = cell(3, 1);

for t = 1:size(icls, 1)
    switch t
        case 1
            load([fol{f} 'iclsITD_distance_' num2str(q) '.mat'], 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
            load([fol{1} 'distance_correction_0_p4_c020_q_' num2str(q) '.mat'], 'key', 'cfun')
        case 2
            load([fol{f} 'iclsILD_distance_' num2str(q) '.mat'], 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
            load([fol{1} 'distance_correction_0_p4_c020_q_' num2str(q) '.mat'], 'key', 'cfun')
        case 3
            load([fol{f} 'iclsFF_distance_450_' num2str(q) '.mat'], 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
            load([fol{1} 'distance_correction_0_p4_c045_q_' num2str(q) '.mat'], 'key', 'cfun')
    end
    
%     cor_vp = @(x) cfun{1}(1) * x + cfun{1}(2);
%     cor_euc = @(x) cfun{2}(1) * x + cfun{2}(2);
%     cor_es = @(x) cfun{3}(1) * x + cfun{3}(2);
%     cor_vp = @(x) cfun{1}(1) * x .^ 3 + cfun{1}(2) * x .^ 2 + cfun{1}(3) * x + cfun{1}(4);
%     cor_euc = @(x) cfun{2}(1) * x .^ 3 + cfun{2}(2) * x .^ 2 + cfun{2}(3) * x + cfun{2}(4);
%     cor_es = @(x) cfun{3}(1) * x .^ 3 + cfun{3}(2) * x .^ 2 + cfun{3}(3) * x + cfun{3}(4);
    cor_vp = @(x) cfun{1}(1) * x .^ 4 + cfun{1}(2) * x .^ 3 + cfun{1}(3) * x .^ 2 + cfun{1}(4) * x + cfun{1}(5);
    cor_euc = @(x) cfun{2}(1) * x .^ 4 + cfun{2}(2) * x .^ 3 + cfun{2}(3) * x .^ 2 + cfun{2}(4) * x + cfun{1}(5);
    cor_es = @(x) cfun{3}(1) * x .^ 4 + cfun{3}(2) * x .^ 3 + cfun{3}(3) * x .^ 2 + cfun{3}(4) * x + cfun{1}(5);

    es = cell(size(distanceES));
    euc = cell(size(distanceEuc));
    vp = cell(size(distanceVP));
    x = cell(size(Mean));
    
    for u = 1:length(Mean)
        es{u} = nan(size(distanceES{u}));
        euc{u} = nan(size(distanceEuc{u}));
        vp{u} = nan(size(distanceVP{u}));
        x{u} = nan(size(Mean{u}));
        for i = 1:size(Mean{u}, 1)
            
            es{u}(i) = distanceES{u}(i);% - cor_es(Mean{u}(i));
            euc{u}(i) = distanceEuc{u}(i);% - cor_euc(Mean{u}(i));
            vp{u}(i) = distanceVP{u}(i);% - cor_vp(Mean{u}(i));
            x{u}(i) = cor_vp(Mean{u}(i));
        end
    end
    
    icls{t}.euc = euc;
    icls{t}.vp = vp;
    icls{t}.es = es;
    icls{t}.rf = Mean;
    icls{t}.x = x;
    
end

clear distanceEuc distanceVP distanceES Mean t u i idx es euc vp key cfun cor* fol x

% %

for i = 1:length(icls)

e = icls{i};

indcorr = cellfun(@(x, y) corr(x, y, 'rows', 'pairwise'), e.rf, e.vp);

figure

subplot(1, 5, 1:4)
scatter(cell2mat(e.rf), cell2mat(e.vp), 10, 'fill', 'k')
hold on
scatter(cell2mat(e.rf), cell2mat(e.x), 10, 'fill', 'r')
[r, p] = corr(cell2mat(e.rf), cell2mat(e.vp), 'rows', 'pairwise');
title([num2str(r) ' ----- ' num2str(p)])
xlabel('mean spike counts')
ylabel('corrected distance')

subplot(1, 5, 5)
boxplot2_points(1, {indcorr}, 1)
xticks([])
ylabel('correlation coefficient')
xlim([0 2])
ylim([-1 1])

end

