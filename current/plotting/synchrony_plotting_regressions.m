stats = cell(4, 2);

%% free field and dichotic
for sy = 1:3
    ctypes = {'ccg', 'sh', 'cor'};
    subidx = reshape(1:8, 2, 4)';
    figure('Name',['OT - synchrony - ' ctypes{sy}],'NumberTitle','off')
    names = {'FF', 'ILD', 'ITD', 'di froz'};
    
    for t = 1:4
        switch t
            case 1
                load('f:\desktop\WorkingFolder\distance\data\ot_synch_ff_shift_noonset_form.mat')
            case 2
                load('f:\desktop\WorkingFolder\distance\data\ot_synch_di_shift_noonset_ild_form.mat')
            case 3
                load('f:\desktop\WorkingFolder\distance\data\ot_synch_di_shift_noonset_itd_form.mat')
            case 4
                load('f:\desktop\WorkingFolder\distance\data\ot_synch_di_shift_noonset_froz_form.mat')
        end
        
        switch sy
            case 1
                data = ccg;
            case 2
                data = sh;
            case 3
                data = cor;
        end
        col = distinguishable_colors(size(GM, 1));
        
        subplot(4, 2, subidx(t, 1))
        hold on
        for i = 1:size(GM, 1)
            scatter(GM(i, :), data(i, :), 3, col(i, :), 'fill')
        end
        [lin, rob, stats{t, 1}] = regression_compare_OrdRob(GM(:), data(:), 0);
        X = GM(:);
        plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
        plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
        axis square
        R = corr(data(:), rob(1) + rob(2) * X, 'rows', 'pairwise') ^ 2;
        title(num2str(R))
        xlabel('gm firing rate')
        ylabel(names{t})
        
        GM = norm01(GM);
        data = norm01(data);
        
        subplot(4, 2, subidx(t, 2))
        hold on
        for i = 1:size(GM, 1)
            scatter(GM(i, :), data(i, :), 3, col(i, :), 'fill')
        end
        [lin, rob, stats{1, 2}] = regression_compare_OrdRob(GM(:), data(:), 0);
        X = GM(:);
        plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
        plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
        axis square
        R = corr(data(:), rob(1) + rob(2) * X, 'rows', 'pairwise') ^ 2;
        title(num2str(R))
        xlabel('norm gm firing rate')
        ylabel('norm data ccg')
        
    end
    
end

%% plot correlations of each individual

names = {'ITD', 'ILD', 'FF', 'di froz'};
r = cell(1, 4);

figure('Name', 'OT - indiv synch', 'NumberTitle', 'off')
optmin = find_minimal_subplot_dim(1:4);
subidx = reshape(1:4, optmin(2), optmin(1));

for t = 1:4
    switch t
        case 1
            load('f:\desktop\WorkingFolder\distance\data\ot_synch_di_shift_noonset_itd_form.mat')
        case 2
            load('f:\desktop\WorkingFolder\distance\data\ot_synch_di_shift_noonset_ild_form.mat')
        case 3
            load('f:\desktop\WorkingFolder\distance\data\ot_synch_ff_shift_noonset_form.mat')
        case 4
            load('f:\desktop\WorkingFolder\distance\data\ot_synch_di_shift_noonset_froz_form.mat')
    end
    
    r{t} = cell(1, 3);
    r{t}{1} = nan(size(GM, 1), 1);
    r{t}{2} = nan(size(GM, 1), 1);
    r{t}{3} = nan(size(GM, 1), 1);
    
    for i = 1:size(GM, 1)
        [r{t}{1}(i), ~] = corr(GM(i, :)', ccg(i, :)', 'rows', 'pairwise');
        [r{t}{2}(i), ~] = corr(GM(i, :)', sh(i, :)', 'rows', 'pairwise');
        [r{t}{3}(i), ~] = corr(GM(i, :)', cor(i, :)', 'rows', 'pairwise');
    end
    
    subplot(size(subidx, 1), size(subidx, 2), subidx(t))
    boxplot2_points(1:3, r{t}, 0.75)
    xlim([0 4])
    xticks(1:3)
    xticklabels({'ccg', 'sh', 'cor'})
    ylabel('synchrony')
    title(names{t})
    
end

clear names t i ccg sh cor GM CCG SH COR idx optmin subidx
