load('f:\desktop\WorkingFolder\distance\arrayITD_distance_noonset.mat')
load_distance_correction

idx = cellfun(@(x) length(size(x)) ~= 2, rf);
es(idx) = []; euc(idx) = []; vp(idx) = []; rf(idx) = [];
idx = cellfun(@isempty, rf);
es(idx) = []; euc(idx) = []; vp(idx) = []; rf(idx) = [];
clear idx

for u = 1:length(rf)
    for i = 1:size(rf{u}, 2)
        
        es{u}(i) = es{u}(i) - cor_es(rf{u}(i));
        euc{u}(i) = euc{u}(i) - cor_euc(rf{u}(i));
        vp{u}(i) = vp{u}(i) - cor_vp(rf{u}(i));
        
    end
end

figure('Name','OT - Array','NumberTitle','off')

stats = cell(3, 2);
met = {'euc', 'vp', 'es'};
col = distinguishable_colors(size(rf, 1));

for m = 1:3
    switch m
        case 1
            data = euc;
        case 2
            data = vp;
        case 3
            data = es;
    end
    subplot(2, 3, m)
    hold on
    for i = 1:size(data,1)
        scatter(rf{i}, data{i}, 3, col(i, :), 'fill')
    end
    [lin, rob, stats{m, 1}] = regression_compare_OrdRob(cell2mat(rf')', cell2mat(data), 0);
    X = cell2mat(rf')';
    plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
    plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
    axis square
    %         title(['r = ' num2str(stats_raw{t, m}.coeffcorr(2)) '; p = ' num2str(stats_raw{t, m}.p(1))])
    R = corr(cell2mat(data), rob(1) + rob(2) * X) ^ 2;
    title(num2str(R))
    xlabel('spike count')
    ylabel(met{m})
end

es = cellfun(@(x) norm01(x, 2), es, 'UniformOutput', 0);
euc = cellfun(@(x) norm01(x, 2), euc, 'UniformOutput', 0);
vp = cellfun(@(x) norm01(x, 2), vp, 'UniformOutput', 0);
rf = cellfun(@(x) norm01(x, 1), rf, 'UniformOutput', 0);

for m = 1:3
    switch m
        case 1
            data = euc;
        case 2
            data = vp;
        case 3
            data = es;
    end
    subplot(2, 3, m+3)
    hold on
    for i = 1:size(data,1)
        scatter(rf{i}, data{i}, 3, col(i, :), 'fill')
    end
    [lin, rob, stats{m, 2}] = regression_compare_OrdRob(cell2mat(rf')', cell2mat(data), 0);
    X = cell2mat(rf')';
    plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
    plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
    axis square
    %         title(['r = ' num2str(stats_raw{t, m}.coeffcorr(2)) '; p = ' num2str(stats_raw{t, m}.p(1))])
    R = corr(cell2mat(data), rob(1) + rob(2) * X) ^ 2;
    title(num2str(R))
    xlabel('NORM spike count')
    ylabel(['NORM ' met{m}])
end