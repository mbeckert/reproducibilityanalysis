load('f:\desktop\WorkingFolder\synchony\ot_synch_ff_del_fr.mat')

win = 10;
norm = 0;

v = 13;

check = ot_synch.ff{6};
gm = sqrt(ot_synch.ff{3} .* ot_synch.ff{4});
% gm(~check) = nan;

synch = cellfun(@(x) ...
    sum( x(round(length(x)/2) - win : round(length(x)/2) + win)) / (win*2+1), ...
    ot_synch.ff{v});

synch(~check) = nan;
synch(isnan(synch)) = 0;
gm(isnan(synch)) = 0;

if norm
    gm = norm01(gm);
    synch = norm01(synch);
end

clear win norm v

% %

ot_synch.ff{2}(isnan(ot_synch.ff{2})) = 0;
ot_synch.ff{3}(isnan(ot_synch.ff{3})) = 0;
key = ot_synch.ff{1};

sites = unique(key(:, 1:2), 'rows');

curve_site = zeros(size(sites, 1), 144);
curve_syn = zeros(size(sites, 1), 144);

for s = 1:size(sites, 1)
    
    idx = find(sum(key(:, 1:2) == sites(s, :), 2) == 2);
    units = unique(key(idx, 3:4));
    
    for u = 1:length(units)
        [r, c] = find(key(idx, 3:4) == units(u), 1, 'first');
        curve_site(s, :) = curve_site(s, :) + ot_synch.ff{1 + c}(idx(r), :);
    end
    
    curve_site(s, :) = curve_site(s, :) / length(units);
    
    clear idx units u r c
    
end

for s = 1:size(sites, 1)
    idx = sum(key(:, 1:2) == sites(s, :), 2) == 2;
    curve_syn(s, :) = nanmean(synch(idx, :));
end

clear s idx

azm = cell(size(curve_site, 1), 1);
azs = cell(size(curve_site, 1), 1);
az = cell(size(curve_site, 1), 1);
synm = cell(size(curve_syn, 1), 1);
syns = cell(size(curve_syn, 1), 1);
syn = cell(size(curve_syn, 1), 1);

for i = 1:size(curve_site, 1)
    [azm{i}, azs{i}, ~] = SpRF2HorVer(curve_site(i, :)');
    [synm{i}, syns{i}, ~] = SpRF2HorVer(curve_syn(i, :)');
    az{i} = curve_site(i, 63:83);
    syn{i} = curve_syn(i, 63:83);
end

clear i

azm = cell2mat(azm);
synm = cell2mat(synm);
azs = cell2mat(azs);
syns = cell2mat(syns);
az = cell2mat(az);
syn = cell2mat(syn);

cd('f:\desktop\WorkingFolder')

%% 

for i = 1:size(curve_site, 1)
    
    figure
    subplot(3, 2, 1)
    SpRFplot_square(curve_site(i, :)')
    j = colorbar;
    ylabel(j,'Mean site firing rate')
    %     caxis([0 1])
    subplot(3, 2, 2)
    SpRFplot_square(curve_syn(i, :)')
    j = colorbar;
    ylabel(j,'Mean site synchrony')
    %     caxis([0 1])
   
    subplot(3, 2, 3)
    errorbar(-100:10:100, azm(i, :), azs(i, :))
    xlabel('azimuth')
    ylabel('mean firing rate')
    axis square
    
    subplot(3, 2, 4)
    errorbar(-100:10:100, synm(i, :), syns(i, :))
    xlim([-100 100])
    xlabel('azimuth')
    ylabel('mean synchrony')
    axis square
    
    subplot(3, 2, 5:6)
    yyaxis left
    plot(-100:10:100, az(i, :))
    xlabel('azimuth')
    ylabel('mean firing rate')

    yyaxis right
    plot(-100:10:100, syn(i, :))
    xlim([-100 100])
    xlabel('azimuth')
    ylabel('mean synchrony')
    axis square
    
    
    saveas(gcf, ['sitesynch_SpRF_cor_' num2str(i)], 'png')
    close all
    
    
end

clear i j