% Load data and reorganize into a structure

type.rec = {'tet', 'near', 'dis'};
type.data = {'base', 'evoked'};
type.syn = {'ccg', 'sh', 'cor'};
key_base = {'_base', ''};
n_max = 0;
for tr = 1:length(type.rec)
    
    output.(type.rec{tr}) = cell(1,2);
    
    for td = 1:length(type.data)
        if tr == 1
            load(['f:\desktop\WorkingFolder\synchony\ot_synch_ff' key_base{td} '.mat'], 'ccg', 'sh', 'cor', 'GM')
            
            gm = GM;
            
            CCG = cell(size(ccg,1),1);
            SH = cell(size(sh,1),1);
            COR = cell(size(cor,1),1);
            GM = cell(size(gm,1),1);
            
            for p = 1:size(ccg,1)
                CCG{p} = ccg(p, :);
                SH{p} = sh(p, :);
                COR{p} = cor(p, :);
                GM{p} = gm(p, :);
            end
            
            output.(type.rec{tr}){td} = [GM, CCG, SH, COR];
            
            if n_max < size(GM, 1)
                n_max = size(GM, 1);
            end
            
        else
            load('f:\desktop\WorkingFolder\working_array\arraydata_spikes.mat','pairdata')
            if tr == 2
                idx = pairdata.diffitd < 60;
            elseif tr == 3
                idx = pairdata.diffitd >= 100;
            end
            
            ccg_a = pairdata.syn_frozen.synch_ccg(idx,td);
            sh_a = pairdata.syn_frozen.synch_sh(idx,td);
            cor_a = pairdata.syn_frozen.synch_cor(idx,td);
            gm_a = pairdata.syn_frozen.gm(idx,td);
            
            for i = 1:size(gm_a,1)
                [~,idx] = max(max(gm_a{i}));
                ccg_a{i} = ccg_a{i}(:,idx);
                sh_a{i} = sh_a{i}(:,idx);
                cor_a{i} = cor_a{i}(:,idx);
                gm_a{i} = gm_a{i}(:,idx);
            end
            
            gm_a = cellfun(@transpose, gm_a, 'UniformOutput', 0);
            ccg_a = cellfun(@transpose, ccg_a, 'UniformOutput', 0);
            sh_a = cellfun(@transpose, sh_a, 'UniformOutput', 0);
            cor_a = cellfun(@transpose, cor_a, 'UniformOutput', 0);
            
            output.(type.rec{tr}){td} = [gm_a, ccg_a, sh_a, cor_a];
            
            if n_max < size(gm_a, 1)
                n_max = size(gm_a, 1);
            end
            
        end
        
    end
    
end
colmap = distinguishable_colors(n_max);
clear key_base tr td i gm ccg sh cor gm_a ccg_a sh_a cor_a idx GM CCG COR
clear SH p n_max pairdata

%% Do regressions and correlations (plots figures 1:3)

R = cell(3, 2);
for tr = 1:length(type.rec)
    for td = 1:length(type.data)
        
        R{tr, td} = cell(1, 3);
        figure(tr)
        
        for ts = 1:length(type.syn)
            
            R{tr, td}{ts} = nan(size(output.(type.rec{tr}){td},1), 1);
            
            
            
            subplot(2,3,ts + (td * 3 - 3))
            hold on
            if ts == 2 && td == 1
                title(type.rec{tr})
            end
            if td == 2
                xlabel('geometric mean firing rate')
            end
            ylabel(type.syn{ts})
            
            for i = 1:size(output.(type.rec{tr}){td},1)
                
                a = output.(type.rec{tr}){td}{i, 1};
                b = output.(type.rec{tr}){td}{i, ts+1};
                
                if ~isempty(a)
                    
                    R{tr, td}{ts}(i) = corr(a', b', 'rows', 'pairwise');
                    
                    idx = isnan(a);
                    a(idx) = []; b(idx) = [];
                    idx = isnan(b);
                    a(idx) = []; b(idx) = [];
                    p = polyfit(a, b, 1);
                    v = polyval(p,a);
                    scatter(a, v, 5, colmap(i,:), 'fill')
                    
                end
            end
            axis square
            
        end
        
    end
end
clear tr td ts i a b idx p v

%% normalize data and plot (figures 4:6)

output_norm = output;
for tr = 1:length(type.rec)
    for td = 1:length(type.data)
%         output_norm.(type.rec{tr}){td} = cellfun(@norm01, output_norm.(type.rec{tr}){td}, 'UniformOutput', 0);
    output_norm.(type.rec{tr}){td} = cellfun(@(x) normalization(x, 0), output_norm.(type.rec{tr}){td}, 'UniformOutput', 0);
    end
end
for tr = 1:length(type.rec)
    figure(tr + 3)
    for td = 1:length(type.data)
        for ts = 1:length(type.syn)
            
            subplot(2, 3, ts + (td * length(type.syn) - length(type.syn)))
            axis square
            hold on
            
            if ts == 2 && td == 1
                title(type.rec{tr})
            end
            if td == 2
                xlabel('Normalized Geometric Mean Firing Rate')
            end
            
            ylabel(type.syn{ts})
            
            for p = 1:size(output_norm.(type.rec{tr}){td},1)
                scatter(output_norm.(type.rec{tr}){td}{p,1}, output_norm.(type.rec{tr}){td}{p,ts + 1}, 5, colmap(p,:), 'fill')
            end
        end
    end
end
clear tr td colmap ts p

%% Extract single synchrony values for each condition
% Here using the maximum geometric firing rate during stimulation, that
% index is pulled out for spontaneous and evoked, and for each synch curve type

M_synch = cell(3, 2);
for tr = 1:length(type.rec)
    [~, m_idx] = cellfun(@(x) max(x), output.(type.rec{tr}){2}(:,1), 'UniformOutput', 0);
    for td = 1:length(type.data)
        M_synch{tr, td} = cell(1, size(output.(type.rec{tr}){td},2));
        for ts = 1:size(output.(type.rec{tr}){td},2)
            M_synch{tr, td}{ts} = nan(size(output.(type.rec{tr}){td},1),1);
            for p = 1:size(output.(type.rec{tr}){td},1)
                if ~isempty(m_idx{p})
                    M_synch{tr, td}{ts}(p) = output.(type.rec{tr}){td}{p,ts}(m_idx{p});
                end
            end
        end
    end
end
clear tr m_idx td p ts

%% run stats:
%%%%%%%%% Correlations
% comparison across synch types for each recording type
statdataR.AcrossSynch = cell(3, 2);
for tr = 1:length(type.rec)
    for td = 1:length(type.data)
        figure(10)
        statdataR.AcrossSynch{tr, td} = CorrelationAnalysis_stats(R{tr, td},{'synch'});
    end
end
clear tr td

% comparison baseline vs evoked of each recording type
statdataR.BvE = cell(3, 1);
for tr = 1:length(type.rec)
    statdataR.BvE{tr} = cell(1, 3);
    for ts = 1:length(type.syn)
        figure(10)
        statdataR.BvE{tr}{ts} = CorrelationAnalysis_stats({R{tr, 1}{ts},R{tr, 2}{ts}},{'synch'});
    end
end
clear tr ts

% comparison curve types across recording type
statdataR.AcrossTypes = cell(1, 2);
for td = 1:length(type.data)
    statdataR.AcrossTypes{td} = cell(1, 3);
    for ts = 1:length(type.syn)
        figure(10)
        statdataR.AcrossTypes{td}{ts} = CorrelationAnalysis_stats({R{1, td}{ts}, R{2, td}{ts}, R{3, td}{ts}},{'synch'});
    end
end
clear td ts

%%%%%%%% Max synch
% comparison across synch types for each recording type
statdataM.AcrossSynch = cell(3, 2);
for tr = 1:3
    for td = 1:2
        figure(10)
        statdataM.AcrossSynch{tr, td} = CorrelationAnalysis_stats(M_synch{tr, td}(2:end),{'synch'});
    end
end
clear tr td

% comparison baseline vs evoked of each recording type
statdataM.BvE = cell(3, 1);
for tr = 1:length(type.rec)
    statdataM.BvE{tr} = cell(1, 3);
    for ts = 1:length(type.syn)
        figure(10)
        statdataM.BvE{tr}{ts} = CorrelationAnalysis_stats({M_synch{tr, 1}{ts}(2:end),M_synch{tr, 2}{ts}(2:end)},{'synch'});
    end
end
clear tr ts

% comparison curve types across recording type
statdataM.AcrossTypes = cell(1, 2);
for td = 1:length(type.data)
    statdataM.AcrossTypes{td} = cell(1, 3);
    for ts = 1:length(type.syn)
        figure(10)
        statdataM.AcrossTypes{td}{ts} = CorrelationAnalysis_stats({M_synch{1, td}{ts}(2:end), M_synch{2, td}{ts}(2:end), M_synch{3, td}{ts}(2:end)},{'synch'});
    end
end
clear td ts

%%

type.stat = {'AcrossSynch', 'BvE', 'AcrossTypes'};

for stattype = 1:2
    switch stattype
        case 1
            sdata = statdataR;
            data = R;
            label = 'correlation coefficient';
        case 2
            sdata = statdataM;
            data = cellfun(@(x) x(2:end), M_synch, 'UniformOutput', 0);
            label = 'max synchony';
    end

        
figure(6 + stattype)
for td = 1:length(type.data)
    for tr = 1:length(type.rec)
        
        subplot(2,3,tr + (td * length(type.rec) - length(type.rec)))
        pdata = data{tr, td};
        boxplot2_points(1:length(type.syn),pdata)
        axis square
        xlim([0 length(type.syn) + 1])
        xticks(1:3)
        xticklabels(type.syn)
        if td == 1
            title(type.rec{tr})
        end
        if tr == 1
            ylabel(label)
        end
        
        ypoint = ylim;
        yinc = (ypoint(2) - ypoint(1)) / 20;
        ypoint = ypoint(2);
        
        for p = 1: length(type.syn)
            if sdata.AcrossSynch{tr,td}.stats.kruskalmult{1}(p, 6) < 0.05
                plot(statdataM.AcrossSynch{tr,td}.stats.kruskalmult{1}(p,1:2), [ypoint, ypoint], 'r')
            end
            ypoint = ypoint + yinc;
        end
    end
end

clear td tr ypoint yinc p

end

for stattype = 1:2
    switch stattype
        case 1
            sdata = statdataR;
            data = R;
            label = 'correlation coefficient';
        case 2
            sdata = statdataM;
            data = cellfun(@(x) x(2:end), M_synch, 'UniformOutput', 0);
            label = 'max synchony';
    end

        
figure(8 + stattype)
for tr = 1:length(type.rec)
    for ts = 1:length(type.syn)
        subplot(length(type.rec), length(type.syn), ts + (tr * length(type.syn) - length(type.syn)))
        pdata = {data{tr, 1}{ts},data{tr, 2}{ts}};
        boxplot2_points(1:length(pdata),pdata)
        axis square
        xlim([0 length(pdata) + 1])
        xticks(1:length(pdata))
        xticklabels({'spontaneous', 'evoked'})
        if tr == 1
            title(type.syn{ts})
        end
        if ts == 1
            ylabel(type.rec{tr})
        end
        
        ypoint = ylim;
        ypoint = ypoint(2);
        
        if sdata.BvE{tr}{ts}.stats.kruskalmult{1}(1, 6) < 0.05
            plot(sdata.BvE{tr}{ts}.stats.kruskalmult{1}(1,1:2), [ypoint, ypoint], 'r')
        end
    end
end

clear td tr ts ypoint yinc p

end

for stattype = 1:2
    switch stattype
        case 1
            sdata = statdataR;
            data = R;
            label = 'correlation coefficient';
        case 2
            sdata = statdataM;
            data = cellfun(@(x) x(2:end), M_synch, 'UniformOutput', 0);
            label = 'max synchony';
    end

        
figure(10 + stattype)
for td = 1:length(type.data)
    for ts = 1:length(type.syn)
        
        subplot(2,3,ts + (td * length(type.syn) - length(type.syn)))
        pdata = {data{1, td}{ts}, data{2, td}{ts}, data{3, td}{ts}};
        boxplot2_points(1:length(type.syn),pdata)
        axis square
        xlim([0 length(type.rec) + 1])
        xticks(1:length(type.rec))
        xticklabels(type.rec)
        if td == 1
            title(type.syn{ts})
        end
        if ts == 1
            ylabel(type.data{td})
        end
        
        ypoint = ylim;
        yinc = (ypoint(2) - ypoint(1)) / 20;
        ypoint = ypoint(2);
        
        for p = 1: length(type.rec)
            if sdata.AcrossTypes{td}{ts}.stats.kruskalmult{1}(p, 6) < 0.05
                plot(sdata.AcrossTypes{td}{ts}.stats.kruskalmult{1}(p,1:2), [ypoint, ypoint], 'r')
            end
            ypoint = ypoint + yinc;
        end
    end
end

clear td tr ypoint yinc p

end