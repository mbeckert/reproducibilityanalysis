% load('f:\desktop\WorkingFolder\distance\distance_OTdichotic_itd.mat')
% data = ITD;
%%
% load('f:\desktop\WorkingFolder\distance\distance_OTdichotic_ild.mat')
% data = ILD;

%% plotting rasters across depvar (remember the ITD and ILD are unfrozen stimuli)

for c = 1:length(data)
    
    y = cell(size(data{c}));
    for i = 1:size(y, 1)
        y(i, :) = get_spike_y(data{c}(i, :));
    end
    
    [optmin] = find_minimal_subplot_dim(1:size(y, 1));
    
    for i = 1:size(y, 1)
        subplot(optmin(1), optmin(2), i)
        scatter(cell2mat(data{c}(i,:)'), cell2mat(y(i,:)'), 5, 'fill')
        axis([0.05 0.2 0 20])
    end
    
    pause
    
end

%% within

load_distance_correction
idx = reshape(1:6, 3, 2)';
type = {'OTdi - neuron - frozen', 'OTdi - neuron - itd', 'OTdi - neuron - ild'};
met = {'euc', 'vp', 'es'};

for i = 1:3
    switch i
        case 1
            load('f:\desktop\WorkingFolder\distance\data\distance_OTdichotic_froz_noonset.mat')
            clear FROZ
        case 2
            load('f:\desktop\WorkingFolder\distance\data\distance_OTdichotic_itd_noonset.mat')
            clear ITD
        case 3
            load('f:\desktop\WorkingFolder\distance\data\distance_OTdichotic_ild_noonset.mat')
            clear ILD
    end
    
    % Apply the correction
    figure('Name',type{i},'NumberTitle','off')
    es = cell(size(es_c));
    euc = cell(size(euc_c));
    vp = cell(size(vp_c));
    
    for u = 1:length(rm_c)
        es{u} = nan(size(es_c{u}));
        euc{u} = nan(size(euc_c{u}));
        vp{u} = nan(size(vp_c{u}));
        for s = 1:size(rm_c{u}, 1)
            
            es{u}(s) = es_c{u}(s) - cor_es(rm_c{u}(s));
            euc{u}(s) = euc_c{u}(s) - cor_euc(rm_c{u}(s));
            vp{u}(s) = vp_c{u}(s) - cor_vp(rm_c{u}(s));
            
        end
    end
    
    rf = rm_c;
    
    for m = 1:3
        switch m
            case 1
                data = euc;
            case 2
                data = vp;
            case 3
                data = es;
        end
        
        col = distinguishable_colors(size(data, 1));
        
        subplot(2, 3, idx(1, m))
        hold on
        for s = 1:size(data,1)
            scatter(rf{s}, data{s}, 3, col(s, :), 'fill')
        end
        [lin, rob, ~] = regression_compare_OrdRob(cell2mat(rf), cell2mat(data), 0);
        X = cell2mat(rf);
        plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
        plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
        axis square
        %         title(['r = ' num2str(stats_raw{t, m}.coeffcorr(2)) '; p = ' num2str(stats_raw{t, m}.p(1))])
        R = corr(cell2mat(data), rob(1) + rob(2) * X, 'rows', 'pairwise') ^ 2;
        title(num2str(R))
        xlabel('spike count')
        ylabel(met{m})
        
        
        
        % now normalize and plot again
        data = cellfun(@(x) norm01(x, 2), data, 'UniformOutput', 0);
        RF = cellfun(@(x) norm01(x, 2), rf, 'UniformOutput', 0);
        
        subplot(2, 3, idx(2, m))
        hold on
        for s = 1:size(data,1)
            scatter(RF{s}, data{s}, 3, col(s, :), 'fill')
        end
        [lin, rob, ~] = regression_compare_OrdRob(cell2mat(RF), cell2mat(data), 0);
        X = cell2mat(RF);
        plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
        plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
        axis square
        %         title(['r = ' num2str(stats_raw{t, m}.coeffcorr(2)) '; p = ' num2str(stats_raw{t, m}.p(1))])
        R = corr(cell2mat(data), rob(1) + rob(2) * X, 'rows', 'pairwise') ^ 2;
        title(num2str(R))
        xlabel('NORM spike count')
        ylabel(['NORM ' met{m}])
        
        
        
        
    end
    
end



%% site

load_distance_correction
idx = reshape(1:6, 3, 2)';
type = {'OTdi - site - frozen', 'OTdi - site - itd', 'OTdi - site - ild'};
met = {'euc', 'vp', 'es'};

for i = 1:3
    switch i
        case 1
            load('f:\desktop\WorkingFolder\distance\data\distance_OTdichotic_froz_noonset.mat')
            clear FROZ
        case 2
            load('f:\desktop\WorkingFolder\distance\data\distance_OTdichotic_itd_noonset.mat')
            clear ITD
        case 3
            load('f:\desktop\WorkingFolder\distance\data\distance_OTdichotic_ild_noonset.mat')
            clear ILD
    end
    
    euc = cell(size(euc_s));
    euc(cellfun(@isempty, euc_s)) = [];
    for L = 1:length(euc)
        euc{L} = vertcat(euc_s{L}{:});
    end
    
    es = cell(size(es_s));
    es(cellfun(@isempty, es_s)) = [];
    for L = 1:length(es)
        es{L} = vertcat(es_s{L}{:});
    end
    
    vp = cell(size(vp_s));
    vp(cellfun(@isempty, vp_s)) = [];
    for L = 1:length(vp)
        vp{L} = vertcat(vp_s{L}{:});
    end
    
    rm = cell(size(rm_s));
    rm(cellfun(@isempty, rm_s)) = [];
    for L = 1:length(rm)
        rm{L} = vertcat(rm_s{L}{:});
    end
    
    rf = rm;
    
    figure('Name',type{i},'NumberTitle','off')
    
    for m = 1:3
        switch m
            case 1
                data = euc;
            case 2
                data = vp;
            case 3
                data = es;
        end
        
        col = distinguishable_colors(size(data, 1));
        
        subplot(2, 3, idx(1, m))
        hold on
        for s = 1:size(data,1)
            scatter(rf{s}, data{s}, 3, col(s, :), 'fill')
        end
        [lin, rob, ~] = regression_compare_OrdRob(cell2mat(rf), cell2mat(data), 0);
        X = cell2mat(rf);
        plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
        plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
        axis square
        %         title(['r = ' num2str(stats_raw{t, m}.coeffcorr(2)) '; p = ' num2str(stats_raw{t, m}.p(1))])
        R = corr(cell2mat(data), rob(1) + rob(2) * X, 'rows', 'pairwise') ^ 2;
        title(num2str(R))
        xlabel('spike count')
        ylabel(met{m})
        
        
        
        % now normalize and plot again
        data = cellfun(@(x) norm01(x, 2), data, 'UniformOutput', 0);
        RF = cellfun(@(x) norm01(x, 2), rf, 'UniformOutput', 0);
        
        subplot(2, 3, idx(2, m))
        hold on
        for s = 1:size(data,1)
            scatter(RF{s}, data{s}, 3, col(s, :), 'fill')
        end
        [lin, rob, ~] = regression_compare_OrdRob(cell2mat(RF), cell2mat(data), 0);
        X = cell2mat(RF);
        plot(X, lin(1) + lin(2) * X, 'r', 'LineWidth', 2)
        plot(X, rob(1) + rob(2) * X, 'b', 'LineWidth', 2)
        axis square
        %         title(['r = ' num2str(stats_raw{t, m}.coeffcorr(2)) '; p = ' num2str(stats_raw{t, m}.p(1))])
        R = corr(cell2mat(data), rob(1) + rob(2) * X, 'rows', 'pairwise') ^ 2;
        title(num2str(R))
        xlabel('NORM spike count')
        ylabel(['NORM ' met{m}])
        
    end
    
end
    
