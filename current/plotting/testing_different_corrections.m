load('f:\desktop\WorkingFolder\distance\altcorrections\distance_correction_0_p5_c010_q_10.mat')
load('f:\desktop\WorkingFolder\distance\varyingQ\otAZ_distance_10.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
E = distanceEuc; V = distanceVP; S = distanceES; M = Mean;
clear distanceEuc distanceVP distanceES Mean

cor_vp = @(x) cfun{1}(1) * x .^ 5 + cfun{1}(2) * x .^ 4 + cfun{1}(3) * x .^ 3 + cfun{1}(4) * x .^ 2 + cfun{1}(5) * x + cfun{1}(6);
cor_euc = @(x) cfun{2}(1) * x .^ 5 + cfun{2}(2) * x .^ 4 + cfun{2}(3) * x .^ 3 + cfun{2}(4) * x .^ 2 + cfun{2}(5) * x + cfun{2}(6);
cor_es = @(x) cfun{3}(1) * x .^ 5 + cfun{3}(2) * x .^ 4 + cfun{3}(3) * x .^ 3 + cfun{3}(4) * x .^ 2 + cfun{3}(5) * x + cfun{3}(6);
% cor_vp = @(x) cfun{1}(1) * x .^ 4 ...
%     + cfun{1}(2) * x .^ 3 ...
%     + cfun{1}(3) * x .^ 2 ...
%     + cfun{1}(4) * x ...
%     + cfun{1}(5);
% cor_euc = @(x) cfun{2}(1) * x .^ 4 + cfun{2}(2) * x .^ 3 + cfun{2}(3) * x .^ 2 + cfun{2}(4) * x + cfun{2}(5);
% cor_es = @(x) cfun{3}(1) * x .^ 4 + cfun{3}(2) * x .^ 3 + cfun{3}(3) * x .^ 2 + cfun{3}(4) * x + cfun{3}(5);

% Apply the correction

for u = 1:length(M)
    for s = 1:size(M{u}, 1)
        
        S{u}(s) = S{u}(s) - cor_es(M{u}(s));
        E{u}(s) = E{u}(s) - cor_euc(M{u}(s));
        V{u}(s) = V{u}(s) - cor_vp(M{u}(s));
        
    end
end

figure
boxplot2_points(cellfun(@nanmedian, cellplot(1:50,1)), cellplot(1:50,2), 1);
hold on
plot(key(1:50, 1), cfun{1}(1) * key(1:50, 1) .^ 5 ...
    + cfun{1}(2) * key(1:50, 1) .^ 4 ...
    + cfun{1}(3) * key(1:50, 1) .^ 3 ...
    + cfun{1}(4) * key(1:50, 1) .^ 2 ...
    + cfun{1}(5) * key(1:50, 1) ...
    + cfun{1}(6), 'r', 'LineWidth', 3)
% plot(key(1:50, 1), cfun{1}(1) * key(1:50, 1) .^ 4 ...
%     + cfun{1}(2) * key(1:50, 1) .^ 3 ...
%     + cfun{1}(3) * key(1:50, 1) .^ 2 ...
%     + cfun{1}(4) * key(1:50, 1) ...
%     + cfun{1}(5), 'r', 'LineWidth', 3)

xlim([0 51])
xlabel('Geometric Mean Firing Rate')
ylabel('Distance (cost)')
axis square

norm = 1;

indcorr = cellfun(@(a, b) corr(a, b, 'rows', 'pairwise'), M, V);

if norm
    x = cellfun(@(x) norm01(x, 2), M, 'UniformOutput', 0);
    y = cellfun(@(x) norm01(x, 2), V, 'UniformOutput', 0);
    x = -cell2mat(x);
    y = -cell2mat(y);
else
    x = cell2mat(M);
    y = cell2mat(V);
end

figure
subplot(1, 10, 1:7)
scatterheatmapplot(x, y, {'firing rate', 'distance'}, 0);
subplot(1, 10, 9:10)
boxplot2_points(1, {indcorr}, 0.75)
xticks([])
xlim([0 2])
ylim([-1 1])
ylabel('correlation coefficient')