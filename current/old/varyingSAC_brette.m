% addpath('C:\Users\mbeck\Desktop\Data\Analysis\ReproducibilityPaper\working')

for type = 1:3
    switch type
        case 1
%             cd('f:\desktop\Data\ICls\Data\RawData\FF')
            cd('Z:\Michael Beckert\data\ICls\Data\RawData\FF')
        case 2
%             cd('f:\desktop\Data\ICls\Data\RawData\Dichotic\ILD')
            cd('Z:\Michael Beckert\data\ICls\Data\RawData\Dichotic\ILD')
        case 3
%             cd('f:\desktop\Data\ICls\Data\RawData\Dichotic\ITD')
            cd('Z:\Michael Beckert\data\ICls\Data\RawData\Dichotic\ITD')
    end
    
    FILES = ls;
    FILES = FILES(3:end,:);
    
    Index = cell(1,length(FILES));
    HIST = cell(1,length(FILES));
    CURVE = cell(1,length(FILES));
    PEAK = cell(1,length(FILES));
    HW = cell(1,length(FILES));
    Mean = cell(1,length(FILES));
    SACcurve = cell(1,length(FILES));
    SACnorm = cell(1,length(FILES));
    SPtimes = cell(1,length(FILES));
    
    bins = 0:0.001:0.3;
    bin = 0.001;
    maxWidth = 10;
    
    for file=1:size(FILES,1)
        
        load(FILES(file,:))
        disp(num2str(file))
        
        if type >1
%             data=cell(size(curvedata.spike_times,1),size(curvedata.spike_times,2));
%             for jj=1:size(curvedata.spike_times,2)
%                 if isfield(curvedata,'depvar')
%                     [Indexer,ind]=sortrows(curvedata.depvar(:,jj));
%                 end
%                 if isfield(curvedata,'depvars')
%                     [Indexer,ind]=sortrows(curvedata.depvars(:,jj));
%                 end
%                 data(:,jj)=curvedata.spike_times(ind,jj);
%             end
%             Index{file}=Indexer';
%             TrialData = data;
            TrialData = curvedata.spike_times;
        else
            Index{file}=Indexer;
        end
        SPtimes{file} = TrialData;

        HIST{file} = cell(1, size(TrialData,1));
        CURVE{file} = nan(1, size(TrialData,1));
        Mean{file} = nan(1, size(TrialData,1));
        SACcurve{file} = cell(1, size(TrialData,1));
        SACnorm{file} = cell(1, size(TrialData,1));
        HW{file} = nan(1, size(TrialData,1));
        PEAK{file} = nan(1, size(TrialData,1));
        
        for con = 1:size(TrialData,1)
%             [HIST{file}{con},CURVE{file}(con),Mean{file}(con),SACcurve{file}{con},SACnorm{file}{con},HW{file}(con),PEAK{file}(con)] = ...
%                 reproducibility_brettenorm(TrialData(con,:),bins,bin,maxWidth);
            [HIST{file}{con},CURVE{file}(con),Mean{file}(con),SACcurve{file}{con},SACnorm{file}{con},HW{file}(con),PEAK{file}(con)] = ...
                reproducibility_jorisnorm(TrialData(con,:),bins,bin,maxWidth);
        end
    end
    
%     cd('f:\desktop\Data\Analysis\ReproducibilityPaper\working')
    cd('f:\desktop\WorkingFolder')
    
    switch type
        case 1
            save iclsFF_joris_update_1 CURVE Mean SACcurve SACnorm HIST Index HW PEAK SPtimes
        case 2
            save iclsILD_joris_update_1 CURVE Mean SACcurve SACnorm HIST Index HW PEAK SPtimes
        case 3
            save iclsITD_joris_update_1 CURVE Mean SACcurve SACnorm HIST Index HW PEAK SPtimes
    end
%     switch type
%         case 1
%             save iclsFF_brette_update_1 CURVE Mean SACcurve SACnorm HIST Index HW PEAK SPtimes
%         case 2
%             save iclsILD_brette_update_1 CURVE Mean SACcurve SACnorm HIST Index HW PEAK SPtimes
%         case 3
%             save iclsITD_brette_update_1 CURVE Mean SACcurve SACnorm HIST Index HW PEAK SPtimes
%     end
    
end
