% cd('f:\desktop\Data\Analysis\Data')
cd('Z:\Michael Beckert\data\Analysis\Data')
load('FreeFieldRawDATA_notsidecorrected_FFffomitted.mat','OT')
% cd('f:\desktop\Data\Analysis\ReproducibilityPaper\working')

data = OT;
clear OT

bins = 0:0.001:0.15;
bin = 0.001;
maxWidth = 10;

HIST = cell(1,200);
CURVE = cell(1,200);
HW = cell(1,200);
PEAK = cell(1,200);
Mean = cell(1,200);
SACcurve = cell(1,200);
SACnorm = cell(1,200);
SPtimes = cell(1,200);
anID = cell(1,200);

count = 1;

for date = 1:length(data)
    for site = 1:length(data{date})
        for unit = 2:length(data{date}{site})

            d = data{date}{site}{unit};
            disp(['Date - ' num2str(date) '; Site - ' num2str(site) '; Unit - ' num2str(unit)]);
            filetypes = fieldnames(d);
            idx = regexpi(filetypes,'FFf.');
            filetypes = filetypes(~cellfun(@isempty,idx));
            
            CURVE{count}=cell([length(filetypes) 1]);
            HW{count}=cell([length(filetypes) 1]);
            PEAK{count}=cell([length(filetypes) 1]);
            Mean{count}=cell([length(filetypes) 1]);
            SACcurve{count}=cell([length(filetypes) 1]);
            SACnorm{count}=cell([length(filetypes) 1]);
            HIST{count}=cell([length(filetypes) 1]);
            SPtimes{count}=cell(length(filetypes),1);
            anID{count} = data{date}{site}{1}(1:3);
            check = 0;
            
            for file=1:length(filetypes)
                TrialData = d.(filetypes{file}).spikes_times;
                TrialData = cellfun(@(x) x*1000, TrialData, 'UniformOutput',0);
                SPtimes{count}{file} = TrialData;
                
                HIST{count}{file} = cell(1, size(TrialData,1));
                CURVE{count}{file} = nan(1, size(TrialData,1));
                Mean{count}{file} = nan(1, size(TrialData,1));
                SACcurve{count}{file} = cell(1, size(TrialData,1));
                SACnorm{count}{file} = cell(1, size(TrialData,1));
                HW{count}{file} = nan(1, size(TrialData,1));
                PEAK{count}{file} = nan(1, size(TrialData,1));
        
                for con = 1:size(TrialData,1)
%                   [HIST{count}{file}{con},CURVE{count}{file}(con),Mean{count}{file}(con),SACcurve{count}{file}{con},SACnorm{count}{file}{con},HW{count}{file}(con),PEAK{count}{file}(con)] = ... 
%                       reproducibility_jorisnorm(TrialData(con,:),bins,bin,maxWidth);
                    [HIST{count}{file}{con},CURVE{count}{file}(con),Mean{count}{file}(con),SACcurve{count}{file}{con},SACnorm{count}{file}{con},HW{count}{file}(con),PEAK{count}{file}(con)] = ... 
                        reproducibility_brettenorm(TrialData(con,:),bins,bin,maxWidth);
                end
                check = check + sum(sum(Mean{count}{file}));
            end
            
            if check > 0
                count = count + 1;
            end
            
        end
    end
end

L = cellfun(@isempty,CURVE);
CURVE(L)=[];
PEAK(L)=[];
HW(L)=[];
Mean(L)=[];
SACcurve(L)=[];
SACnorm(L)=[];
HIST(L)=[];
SPtimes(L)=[];
anID(L) = [];

for i = 1:length(CURVE)
    CURVE{i} = cell2mat(CURVE{i});
    HW{i} = cell2mat(HW{i});
    Mean{i} = cell2mat(Mean{i});
    PEAK{i} = cell2mat(PEAK{i});
end

CURVE = cellfun(@(x) nanmean(x,1),CURVE,'UniformOutput',0);
HW = cellfun(@(x) nanmean(x,1),HW,'UniformOutput',0);
Mean = cellfun(@(x) nanmean(x,1),Mean,'UniformOutput',0);
PEAK = cellfun(@(x) nanmean(x,1),PEAK,'UniformOutput',0);

clearvars -except Index CURVE Mean SACcurve SACnorm HIST HW PEAK SPtimes anID

% cd('f:\desktop\Data\Analysis\ReproducibilityPaper\working')
cd('f:\desktop\WorkingFolder')

save otFF_brette_update_1
% save otFF_joris_update_1