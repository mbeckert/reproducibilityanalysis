bins = 0:0.001:0.5;

for type = 2:3
    switch type
        case 1
%             cd('f:\desktop\Data\ICls\Data\RawData\FF')
            cd('Z:\Michael Beckert\data\ICls\Data\RawData\FF')
        case 2
%             cd('f:\desktop\Data\ICls\Data\RawData\Dichotic\ILD')
            cd('Z:\Michael Beckert\data\ICls\Data\RawData\Dichotic\ILD')
        case 3
%             cd('f:\desktop\Data\ICls\Data\RawData\Dichotic\ITD')
            cd('Z:\Michael Beckert\data\ICls\Data\RawData\Dichotic\ITD')
    end
    
    FILES = ls;
    FILES = FILES(3:end,:);
    
    distanceEuc = cell(size(FILES, 1), 1);
    distanceVP = cell(size(FILES, 1), 1);
    Index = cell(size(FILES, 1), 1);
    Mean = cell(size(FILES, 1), 1);
    
    for file=1:size(FILES,1)
        
        if type >1
            load(FILES(file,:), 'curvedata')
            TrialData = curvedata.spike_times;
            clear curvedata
        else
            load(FILES(file,:), 'TrialData', 'Indexer')
            Index{file}=Indexer;
        end

        Mean{file} = nanmean(cellfun(@length, TrialData), 2);   
        
        TrialData = cellfun(@(x) x/1000, TrialData, 'UniformOutput', 0);
        trialdata = TrialData;
        TrialData = cellfun(@(x) hist(x, bins), TrialData, 'UniformOutput', 0);
        
        distanceEuc{file} = nan(size(TrialData, 1), 1);
        distanceVP{file} = nan(size(TrialData, 1), 1);
        for u = 1:size(TrialData, 1)
            disp(['type = ' num2str(type) ', file = ' num2str(file) ', unit = ' num2str(u)])
            tic
            holder = nan(size(TrialData, 2));
            holder2 = nan(size(TrialData, 2));
            idx = ones(size(TrialData, 2));
            idx = tril(idx, -1);
            
            for t1 = 1:size(TrialData, 2)
                for t2 = 1:size(TrialData, 2)
                    if idx(t1, t2) == 1
                        holder(t1, t2) = sqrt( sum( (TrialData{u, t1} - TrialData{u, t2}) .^2 ) );
                        holder2(t1, t2) = VPdistance_reich(trialdata{u, t1}', trialdata{u, t2}', 10);
                    end
                end
            end
            idx = find(idx == 1);
            distanceEuc{file}(u) = nanmean(holder(idx));
            distanceVP{file}(u) = nanmean(holder2(idx));
            toc
        end
        
        clear TrialData trialdata u t1 t2 holder idx Indexer holder2
        
    end
    
    cd('f:\desktop\WorkingFolder')
    
    switch type
        case 1
            save iclsFF_distance distanceEuc distanceVP Index Mean
            for t = 1:2
            load('iclsFF_distance.mat')
                for i = 1:length(Index)
                    idx = find(Index{i}(t,:) == mode(Index{i}(t,:)));
                    distanceEuc{i} = distanceEuc{i}(idx);
                    distanceVP{i} = distanceVP{i}(idx);
                    Index{i} = Index{i}(:,idx);
                    Mean{i} = Mean{i}(idx);
                end
                clear i idx
                switch t
                    case 1 
                        save iclsAZ_distance distanceEuc distanceVP Index Mean
                    case 2
                        save iclsEL_distance distanceEuc distanceVP Index Mean
                end
            end
            clear t
        case 2
            save iclsILD_distance distanceEuc distanceVP Index Mean
        case 3
            save iclsITD_distance distanceEuc distanceVP Index Mean
    end
    
end

clear type bins FILES file