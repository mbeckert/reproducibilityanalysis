load('f:\desktop\WorkingFolder\synchony\ot_synch_ff_del.mat')

win = 1;
norm = 1;
% pos = 0;

check = ot_synch.ff{6};

gm = sqrt(ot_synch.ff{3} .* ot_synch.ff{4});
% gm = (ot_synch.ff{3} + ot_synch.ff{4}) / 2;

% for i = 1:size(gm, 1)
%     cut = max(gm(i, :)) * 0.05;
%     gm(i, gm(i, :) < cut) = nan;
% end

% gm = normalization(gm, norm);
% gm(~check) = nan;
gm = norm01(gm);


for v = 13
    
    % synch = ot_synch.ff{9};
    synch = cellfun(@(x) ...
        sum( x(round(length(x)/2) - win : round(length(x)/2) + win)) / (win*2+1), ...
        ot_synch.ff{v});
    synch(~check) = nan;
    
%     if pos
%         idx = synch >= 0;
%     else
%         idx = synch < 0;
%     end
%     
%     synch(~idx) = nan;
%     synch = normalization(abs(synch), norm);
%         synch = normalization(synch, norm);
        synch = norm01(synch);
    
    ind = diag(corr(gm', synch', 'rows', 'pairwise'));
    
    for plt = 1:3
        figure((plt * 10) + (v - 10))
        
        switch plt
            case 1
                subplot(1, 10, 1:7)
                hold on
                %%%% scatter plot with background heatmap and ordinary regression
                scatterheatmapplot(gm(:), synch(:), {'norm gm fr', 'norm synch'}, norm);
                subplot(1, 10, 9:10)
                boxplot2_points(1, {ind}, 0.75)
                xticks(1)
                xticklabels([])
                ylabel('correlation coefficient')
                xlim([0 2])
                
            case 2
                subplot(1, 10, 1:7)
                hold on
                %%%% scatter of individual neurons without any regression line
                col = distinguishable_colors(size(gm, 1));
                for i = 1:size(gm, 1)
                    scatter(gm(i, :), synch(i, :), 10, col(i, :), 'fill')
                end
                xlabel('normalized gm fr')
                ylabel('normalized synch')
                
                subplot(1, 10, 9:10)
                boxplot2_points(1, {ind}, 0.75)
                xticks(1)
                xticklabels([])
                ylabel('correlation coefficient')
                xlim([0 2])
                
            case 3
                %%%% scatter plot of pooled data and both ordinary and robust regressions
                [ord, rob, stats] = regression_compare_OrdRob(gm(:), synch(:), 1);
        end
    end
end