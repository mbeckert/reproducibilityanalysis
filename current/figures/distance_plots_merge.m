% First use "apply_distance_correction" To all the data
% load('f:\desktop\WorkingFolder\distance\distance_corrected_data_fullcorrect_merge.mat')
load('f:\desktop\WorkingFolder\distance\distance_corrected_data_100_omit_merge.mat')
%%
norm = 1;
d = cell(3, 2);
indcorr = cell(3, 1);
dis = cell(3, 1);

e = data;

d{1, 1} = e{1}.rf;
d{1, 2} = e{1}.vp;
d{2, 1} = e{2}.rf;
d{2, 2} = e{2}.vp;
d{3, 1} = e{3}.rf;
d{3, 2} = e{3}.vp;

for i = 1:size(d, 1)
    
    indcorr{i} = cellfun(@(a, b) corr(a, b, 'rows', 'pairwise'), d{i, 1}, d{i, 2});
    dis{i} = cellfun(@(x, y) y(find(x == max(x), 1, 'first')), d{i, 1}, d{i, 2});
    
    if norm
        x = cellfun(@(x) norm01(x, 2), d{i, 1}, 'UniformOutput', 0);
        y = cellfun(@(x) norm01(x, 2), d{i, 2}, 'UniformOutput', 0);
        x = -cell2mat(x);
        y = -cell2mat(y);
    else
        x = cell2mat(d{i, 1});
        y = cell2mat(d{i, 2});
    end
    
    figure(str2double(num2str(i)))
    subplot(1, 10, 1:7)
    scatterheatmapplot(x, y, {'firing rate', 'distance'}, 0);
    subplot(1, 10, 9:10)
    boxplot2_points(1, indcorr(i), 0.75)
    xticks([])
    xlim([0 2])
    ylim([-1 1])
    ylabel('correlation coefficient')
    
end

figure(25)
dis = {dis{1}, dis{2}, dis{3}};
boxplot2_points(1:length(dis), dis, 0.75)
xticks(1:length(dis))
xticklabels({'ICls di', 'ICls ff', 'OT'})
xlim([0 4])
ylabel('Peak Corrected Distance')