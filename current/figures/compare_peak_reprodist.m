distance_plots_merge
repro_plots_single
close all

groups = {'ICcl dichotic', 'ICcl free field', 'OT (1ms)', 'OT (10ms)'};

subplot(2, 1, 1)
boxplot2_points(1:4, rep, 0.75)
ax = gca;
ax.FontSize = 12;
xlim([0 5])
ylim([-0.05 0.4])
xticks(1:4)
xticklabels(groups)
xtickangle(90)
ylabel(['Peak Reproducibility' newline '(normalized coincidence/spike)'], 'FontSize', 18)

subplot(2, 1, 2)
boxplot2_points([1 2 3.5], dis, 0.75)
ax = gca;
ax.FontSize = 12;
xlim([0 5])
ylim([-15 5])
xticks([])
ylabel(['Peak Distance' newline '(corrected cost)'], 'FontSize', 18)

