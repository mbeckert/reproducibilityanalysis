load('K:\WorkingFolder\synchony\ot_synch_ff_del_fr_200_cor.mat')

win = 10;

check = ot_synch.ff{6};

gm = sqrt(ot_synch.ff{3} .* ot_synch.ff{4});
GM = gm;
gm = norm01(gm);

v = 11;

synch = cellfun(@(x) ...
    sum( x(round(length(x)/2) - win : round(length(x)/2) + win)) / (win*2+1), ...
    ot_synch.ff{v});
%synch = norm01(synch);
synch(~check) = nan;


a = synch;

v = 12;

synch = cellfun(@(x) ...
    sum( x(round(length(x)/2) - win : round(length(x)/2) + win)) / (win*2+1), ...
    ot_synch.ff{v});
%synch = norm01(synch);
synch(~check) = nan;

b = synch;

v = 13;

synch = cellfun(@(x) ...
    sum( x(round(length(x)/2) - win : round(length(x)/2) + win)) / (win*2+1), ...
    ot_synch.ff{v});
%synch = norm01(synch);

synch(~check) = nan;

c = synch;

%%

A = nan(size(a, 1), 1);
B = nan(size(b, 1), 1);

for i = 1:size(a, 1)
    
    [~, ix] = sort(gm(i, :));
    ix = fliplr(ix);
    
    for h = 1:length(ix)
        if ~isnan(a(i, ix(h))) && ~isnan(b(i, ix(h)))
            A(i) = a(i, ix(h));
            B(i) = b(i, ix(h));
        end
    end
end
            
scatter(A, B)
%%

dif = nan(size(a, 1), 1);

for i = 1:size(a, 1)
    [~, ix] = sort(gm(i, :));
    ix = fliplr(ix);
    for h = 1:length(ix)
        if ~isnan(a(i, ix(h))) && ~isnan(b(i, ix(h)))
%             if isnan(a(i, ix(h)))
%                 a(i, ix(h)) = 0;
%             end
%             if isnan(b(i, ix(h)))
%                 b(i, ix(h)) = 0;
%             end

%             dif(i) = b(i, ix(h)) / a(i, ix(h)); % ratio
%             dif(i) = 1 - (a(i, ix(h)) - b(i, ix(h))) / a(i, ix(h)); % percent change
%             dif(i) = (a(i, ix(h)) / b(i, ix(h))) + a(i, ix(h)); 
%             dif(i) = (a(i, ix(h)) - c(i, ix(h))) / a(i, ix(h)); % percent remaining after correction
           dif(i) = a(i, ix(h)) - b(i, ix(h));
           
break
        end
    end
end

% dif(dif == Inf) = nan;
nanmean(dif)
nanstd(dif)

%%

dif = nan(size(a, 1), 2);

for i = 1:size(a, 1)
    [~, ix] = sort(gm(i, :));
    ix = fliplr(ix);
    for h = 1:length(ix)
        if ~isnan(a(i, ix(h))) && ~isnan(b(i, ix(h)))
            dif(i, 1) = a(i, ix(h));
            dif(i, 2) = b(i, ix(h));
           break
        end
    end
end
%%
% These are the pairs that have no significant response
thesenans = isnan(nanmean(a, 2));
% Get a total count to calculate the proportions
total = length(dif) - sum(thesenans);
% If a dif is nan it is either because one of the original values was 0
% (not possible due to the selection criteria in above section) or two
% zeros were divided by each other. In this case the reproducibility
% explains nothing, so set it to 0 for computing the mean contribution
dif(isnan(dif)) = 0;
dif(thesenans) = nan;

fullexplain = sum(dif >= 1);
noneexplain = sum(dif == 0);
partexplain = sum(dif > 0 & dif < 1);

m = nanmean(dif(dif < 1 & dif > 0));
s = nanstd(dif(dif < 1 & dif > 0));


%%

dif(dif >= 1) = 1;
m = nanmean(dif);
s = nanstd(dif);


