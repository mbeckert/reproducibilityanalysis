tic
figure
ax = gca;
ax.FontSize = 12;

for t = 1:2
    switch t
        case 1
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_itd_curve_50micro.mat')
            col = 'b';
            yyaxis left
        case 2
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_ot_curve_50micro.mat')
            col = 'r';
            yyaxis right
    end
    
%     mw_time = linspace(0.00001, 0.05, 50);
    mw_time = round(logspace(-4, -1.3, 50), 5);
%     mw_time = round(logspace(-4, -1, 30), 5);
%     mw_time = [0.02 0.04 0.08 0.1 0.2 0.4 0.8 1 2 4 8 10 20 40] / 1000;
    repro = cell(size(mw_time));
    REP = cell(size(mw_time));
    
    neu = unique(neuron);
    center = round(size(curve, 2) / 2);
%     nf = double(reps) * (double(reps) - 1) * (fr .^ 2) * dur;
    
    for mw = 1:length(mw_time)
        
        win = round(mw_time(mw) / cw);
        winsac = curve(:, center - win + 1:center + win - 1);
        f = repmat(fr' .^2, 1, size(winsac, 2));
%         repro{mw} = sum(winsac - f, 2) * cw ./ fr';
        repro{mw} = sum(winsac - f, 2) ./ (mw_time(mw) * fr');
%         repro{mw} = sum(winsac, 2) ./ (nf * mw_time(mw))';

        REP{mw} = cell(length(neu), 1);
        for n = 1:length(neu)
            idx = neuron == neu(n);
            REP{mw}{n} = repro{mw}(idx)';
        end
    end
    
    FR = cell(length(neu), 1);
    for n = 1:length(neu)
        idx = neuron == neu(n);
        FR{n} = fr(idx);
    end
    
    clear center mw f win winsac curve fr neuron depvar fr full_fr ...
        idx n neu repro
    
    [~, idx] = cellfun(@max, FR);
    rep = cell(size(REP));
    for i = 1:length(REP)
        rep{i} = nan(size(REP{i}));
        for n = 1:length(REP{i})
            rep{i}(n) = REP{i}{n}(idx(n));
        end
    end
    clear i n idx
    
%     rep = cell(size(REP));
%     for i = 1:length(REP)
%         rep{i} = cellfun(@max, REP{i});
%     end
    
    quartile_error_plot(rep, 1:length(rep), col)
    xticks(1:length(mw_time))
    xticklabels(mw_time * 1000 * 2)
    xtickangle(90)
    % set(gca, 'XScale', 'log')
    xlim([1 length(rep)])
    xlabel('Coincidence Window (ms)', 'FontSize', 18)
    ylabel(['Reproducibility' newline '(coincidence/spike)'], 'FontSize', 18)
%     axes = axis;
%     ylim([0 axes(end)])
    
end

toc


