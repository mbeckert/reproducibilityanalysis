hw = cell(4, 1);
HW = cell(4, 1);
FR = cell(4, 1);

for i = 1:4
    switch i
        case 1
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_itd_curve_100micro.mat')
            smo = 3;
        case 2
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_ild_curve_100micro.mat')
            smo = 3;
        case 3
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_ff_curve_100micro.mat')
            smo = 3;
        case 4
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_ot_curve_100micro.mat')
            smo = 80;
    end
    
    f = repmat(fr' .^2, 1, size(curve, 2));
    repro = (curve - f) * cw ./ fr';
    center = round(size(repro, 2)/2);
    
    hw{i} = nan(size(repro, 1), 1);
    
    for n = 1:size(repro, 1)
        repro(n, :) = smooth(repro(n, :), smo);
        floor = nanmean(repro(n, repro(n,:) < 0));
        half = (repro(n, center) - floor) / 2;
        over = (repro(n, :) - floor) > half;
        h = (find(over(center:end) == 0, 1, 'first') * 2 - 1) * cw;
        if ~isempty(h)
            hw{i}(n) = h;
        else
            hw{i}(n) = nan;
        end
    end

    neu = unique(neuron);
    FR{i} = cell(length(neu), 1);
    HW{i} = cell(length(neu), 1);
    
for n = 1:length(neu)
    idx = neuron == neu(n);
    HW{i}{n} = hw{i}(idx)';
    FR{i}{n} = fr(idx);
end  
    
end

% Merge ITD and ILD data for each neuron
FR{1} = cellfun(@(a, b) [a, b], FR{1}, FR{2}, 'UniformOutput', 0);
FR{2} = FR{3};
FR{3} = FR{4};
FR(4) = [];
HW{1} = cellfun(@(a, b) [a, b], HW{1}, HW{2}, 'UniformOutput', 0);
HW{2} = HW{3};
HW{3} = HW{4};
HW(4) = [];

clear smo center curve cw depvar dur f floor fr full_fr h half hw i idx n neu neuron over repro reps

%%
hw = cell(size(HW));
for i = 1:length(hw)
%     rep{i} = cellfun(@nanmax, REP{i});
    hw{i} = cellfun(@(x, y) y(find(x == max(x), 1, 'first')), FR{i}, HW{i}, 'UniformOutput', 0);
    hw{i} = cell2mat(hw{i}')';
end

figure(8)
boxplot2_points_logy(1:length(hw), hw, 0.75)
    ax = gca;
    ax.FontSize = 12;
xticks(1:length(hw))
xticklabels({'ICls di', 'ICls ff', 'OT'})
xlim([0 4])
ylabel('Maximum Imprecision', 'fontsize', 16)
clear i
%%
indcorr = cell(length(FR), 1);
norm = 1;

for i = 1:length(FR)
    indcorr{i} = cellfun(@(a, b) corr(a', b', 'rows', 'pairwise'), FR{i}, HW{i});
    
    if norm    
        FR{i} = cellfun(@(x) norm01(x), FR{i}, 'UniformOutput', 0);
        HW{i} = cellfun(@(x) norm01(x), HW{i}, 'UniformOutput', 0);
    end
    
    figure(i)
    subplot(1, 10, 1:7)
    scatterheatmapplot(cell2mat(FR{i}')', cell2mat(HW{i}')', ...
        {'firing rate', 'imprecision'}, norm);
    subplot(1, 10, 9:10)
    boxplot2_points(1, indcorr(i), 0.75)
    xticks([])
    xlim([0 2])
    ylim([-1 1])
    ylabel('correlation coefficient')
    
end

clear i norm



