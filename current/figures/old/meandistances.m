load('f:\desktop\WorkingFolder\distance\distance_corrected_data_long.mat')

otaz = ot{3}.vp;
gm = num2cell(cellfun(@(x) find(x == max(x), 1, 'first'), ot{3}.rf));
otaz = cellfun(@(x, i) x(i), otaz, gm);
oagm = cellfun(@max, ot{3}.rf);

otel = ot{4}.vp;
gm = num2cell(cellfun(@(x) find(x == max(x), 1, 'first'), ot{4}.rf));
otel = cellfun(@(x, i) x(i), otel, gm);
oegm = cellfun(@max, ot{4}.rf);

st = special{1}.vp;
gm = num2cell(cellfun(@(x) find(x == max(x), 1, 'first'), special{1}.rf));
st = cellfun(@(x, i) x(i), st, gm);
sgm = cellfun(@max, special{1}.rf);

icaz = icls{3}.vp;
gm = num2cell(cellfun(@(x) find(x == max(x), 1, 'first'), icls{3}.rf));
icaz = cellfun(@(x, i) x(i), icaz, gm);
iagm = cellfun(@max, icls{3}.rf);

icel = icls{4}.vp;
gm = num2cell(cellfun(@(x) find(x == max(x), 1, 'first'), icls{4}.rf));
icel = cellfun(@(x, i) x(i), icel, gm);
iegm = cellfun(@max, icls{3}.rf);

clear icls ot special gm

x = {iagm, iegm, oagm, oegm, sgm};
data = {icaz, icel, otaz, otel, st};


load('f:\desktop\WorkingFolder\distance\mod_dist_ot.mat')
load('f:\desktop\WorkingFolder\distance\data\distance_correction_c_01_10_long.mat', 'key', 'cfun')
% cor_vp = @(x) cfun{1}(1) * x .^ 3 + cfun{1}(2) * x .^ 2 + cfun{1}(3) * x + cfun{1}(4);
cor_vp = @(x) cfun{1}(1) * x + cfun{1}(2);
for u = 1:size(mod_dist, 1)
    for s = 1:size(mod_dist{u})
        if cor_vp(Mean{u}(s)) >= 0
            mod_dist{u}(s) = mod_dist{u}(s) - cor_vp(Mean{u}(s));
        end
    end
end

ot = cellfun(@max, mod_dist);

load('f:\desktop\WorkingFolder\distance\iclsAZ_mod.mat')
load('f:\desktop\WorkingFolder\distance\data\distance_correction_c_04_10_long.mat', 'key', 'cfun')
cor_vp = @(x) cfun{1}(1) * x + cfun{1}(2);
for u = 1:size(mod_dist, 1)
    for s = 1:size(mod_dist{u})
        if cor_vp(Mean{u}(s)) >= 0
            mod_dist{u}(s) = mod_dist{u}(s) - cor_vp(Mean{u}(s));
        end
    end
end

iclsaz = cellfun(@max, mod_dist);

load('f:\desktop\WorkingFolder\distance\iclsEL_mod.mat')
load('f:\desktop\WorkingFolder\distance\data\distance_correction_c_04_10_long.mat', 'key', 'cfun')
cor_vp = @(x) cfun{1}(1) * x + cfun{1}(2);
for u = 1:size(mod_dist, 1)
    for s = 1:size(mod_dist{u})
        if cor_vp(Mean{u}(s)) >= 0
            mod_dist{u}(s) = mod_dist{u}(s) - cor_vp(Mean{u}(s));
        end
    end
end

iclsel = cellfun(@max, mod_dist);

load('f:\desktop\WorkingFolder\distance\data\distance_betweenneurons_mod.mat')
load('f:\desktop\WorkingFolder\distance\data\distance_correction_c_01_10_long.mat', 'key', 'cfun')
cor_vp = @(x) cfun{1}(1) * x + cfun{1}(2);

mod_dist = cellfun(@(x, y) mean([x, y], 2), VP.FFf1, VP.FFf2, 'UniformOutput', 0);
gm1 = cellfun(@(x, y) sqrt(x .* y), FR1.FFf1, FR2.FFf1, 'UniformOutput', 0);
gm2 = cellfun(@(x, y) sqrt(x .* y), FR1.FFf2, FR2.FFf2, 'UniformOutput', 0);
Mean = cellfun(@(x, y) mean([x, y], 2), gm1, gm2, 'UniformOutput', 0);

for u = 1:size(mod_dist, 1)
    for s = 1:size(mod_dist{u})
        if cor_vp(Mean{u}(s)) >= 0
            mod_dist{u}(s) = mod_dist{u}(s) - cor_vp(Mean{u}(s));
        end
    end
end

bet = cell2mat(cellfun(@max, mod_dist, 'UniformOutput', 0));

rdata = {iclsaz, iclsel, ot, ot, bet};


boxplot2_points(1:5, data, 0.75);
xticks(1:5)
xticklabels({'icls az', 'icls el', 'ot az', 'ot el', 'between neurons'})
xtickangle(45)
ylabel('corrected distance')
title('average distance per condition')
xlim([0 6])

hold on

boxplot2_points(1:5, rdata, 0.75);