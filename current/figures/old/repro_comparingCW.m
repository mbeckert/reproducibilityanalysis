region = 'itd';
names = {'10micro', '50micro', '100micro', '500micro', '1ms', '2ms', '3ms', '5ms', '10ms', '20ms', '50ms'};

FR = cell(length(names), 1);
REP = cell(length(names), 1);
coinwin = nan(size(REP));

for i = 1:length(coinwin)
    load(['f:\desktop\WorkingFolder\working_reprod\frompython\repro_' region '_curve_' names{i} '.mat'])
    
    center = round(size(curve, 2) / 2);
    winsac = curve(:, center);
    f = repmat(fr' .^2, 1, size(winsac, 2));
    
%     repro = sum(winsac - f, 2) * cw ./ fr';
    
    nf = double(reps) * double(reps - 1) * f * cw * (dur - 0.05);
    repro = winsac ./ nf;
    
    neu = unique(neuron);
    FR{i} = cell(length(neu), 1);
    REP{i} = cell(length(neu), 1);
    coinwin(i) = cw;
    
for n = 1:length(neu)
    idx = neuron == neu(n);
    REP{i}{n} = repro(idx)';
    FR{i}{n} = fr(idx);
end  
    
end

FR = FR{1};

clear i center winsac d repro neu n curve cw depvar f fr full_fr idx neuron region dur reps

%%

[~, idx] = cellfun(@max, FR);
rep = cell(size(REP));
for i = 1:length(REP)
    rep{i} = nan(size(REP{i}));
    for n = 1:length(REP{i})
        rep{i}(n) = REP{i}{n}(idx(n));
    end
end
clear i n idx

boxplot2_points(1:length(rep), rep, 0.75)
xticks(1:length(coinwin))
xticklabels(coinwin * 1000)