cd('f:\desktop\WorkingFolder')

disVP = cell(6, 1);
fr = cell(6, 1);

for t = 1:6
    switch t
        case 1
            load('f:\desktop\WorkingFolder\distance\data\iclsITD_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
            load('f:\desktop\WorkingFolder\distance\data\distance_correction_c_02_10_long.mat', 'key')
        case 2
            load('f:\desktop\WorkingFolder\distance\data\iclsILD_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
            load('f:\desktop\WorkingFolder\distance\data\distance_correction_c_02_10_long.mat', 'key')
        case 3
            load('f:\desktop\WorkingFolder\distance\data\iclsAZ_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
            load('f:\desktop\WorkingFolder\distance\data\distance_correction_c_04_10_long.mat', 'key')
        case 4
            load('f:\desktop\WorkingFolder\distance\data\iclsEL_distance_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
            load('f:\desktop\WorkingFolder\distance\data\distance_correction_c_04_10_long.mat', 'key')
        case 5
            load('f:\desktop\WorkingFolder\distance\data\otAZ_distance_del_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
            load('f:\desktop\WorkingFolder\distance\data\distance_correction_c_01_10_long.mat', 'key')
        case 6
            load('f:\desktop\WorkingFolder\distance\data\otEL_distance_del_noonset.mat', 'distanceEuc', 'distanceVP', 'distanceES', 'Mean')
            load('f:\desktop\WorkingFolder\distance\data\distance_correction_c_01_10_long.mat', 'key')
    end
    
    disVP{t} = distanceVP;
    fr{t} = Mean;
    
    clear distanceEuc distanceVP distanceES Mean
    
    figure(10)
    
    d = cell2mat(disVP{t});
    f = cell2mat(fr{t});
    
    subplot(2, 3, t)
    scatter(f, d, 'fill')
    xlabel('firing rate')
    ylabel('VP cost')
    [r, p] = corr(f, d, 'rows', 'pairwise');
    title([num2str(r) ' ---- ' num2str(p)])
    hold on
    plot(key(:, 1), key(:, 2), 'r')
    axis([min(f) max(f)+max(f)*0.1 min(d) max(d)+max(d)*0.1])
    
end