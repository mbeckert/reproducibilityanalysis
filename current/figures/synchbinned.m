load('K:\WorkingFolder\synchony\ot_synch_ff_del_fr.mat')

win = 10;
norm = 0;
bins = 0:0.1:1;

for v = 11:13
    
    check = ot_synch.ff{6};
    gm = sqrt(ot_synch.ff{3} .* ot_synch.ff{4});
    % gm(~check) = nan;
%     gm = norm01(gm);
    
    synch = ot_synch.ff{v};
    %     synch(~check) = {nan(199, 1)};
    
    gm_bin = nan(size(gm, 1), length(bins) - 1);
    syn_bin = cell(size(synch, 1), length(bins) - 1);
    
    for r = 1:size(gm_bin, 1)
        [~, ~, idx] = histcounts(gm(r, :), bins);
        for c = 1:size(gm_bin, 2)
            gm_bin(r, c) = nanmean(gm(r, idx == c));
            syn_bin{r, c} = nansum(cell2mat(synch(r, idx == c)), 2) / sum(idx == c);
            if isempty(syn_bin{r, c})
                syn_bin{r, c} = nan(199, 1);
            end
        end
    end
    
    synch = cellfun(@(x) ...
        sum( x(round(length(x)/2) - win : round(length(x)/2) + win)) / (win*2+1), ...
        syn_bin);
    
    if norm
        synch = norm01(synch);
    end
    
    gm = gm_bin;
    
    ind = diag(corr(gm', synch', 'rows', 'pairwise'));
    
    figure(100 + (v - 10))
    
    subplot(1, 10, 1:7)
    hold on
       
    a = gm(:);
    b = synch(:);
    
    cmpl = flipud(gray);
    counting = {'0.1','0.2','0.3','0.4','0.5','0.6','0.7','0.8','0.9','1'};
    [mat,~,~] = hist2D([a,b], [0,1,11], [0,1,11]);
    imagesc(flipud(mat))
    xticks(1:10)
    yticks(1:10)
    xticklabels(counting)
    yticklabels(counting)
    colorbar
    
    idx = isnan(a);
    a(idx) = []; b(idx) = [];
    idx = isnan(b);
    a(idx) = []; b(idx) = [];
    p = polyfit(a * 10, b * 10, 1);
    x = polyval(p,a * 10);
    plot(a * 10, x, 'b', 'LineWidth', 3)
    
    % We can still add the axis labels in either condition
    xlabel('Normalized Geometric Mean Firing Rate')
    ylabel('Normalized Synchrony')
    
    % Now create a second plot overlaying the first histogram
    ax1 = gca;
    axis square
    ax1_pos = ax1.Position;
    ax2 = axes('Position',ax1_pos,...
        'XAxisLocation','top',...
        'YAxisLocation','right',...
        'Color','none');
    colormap(cmpl)
    hold on
    
    % And plot the scatter into this new axes
    boxplot2_points(1:size(synch, 2), num2cell(synch, 1), 0.75)

    xticks([])
    yticks([])
    axis square
    
    % Run a quick Pearson's correlation to see the relationship and
    % significance, put it as the title for easy visualization
    [cc, pp] = corr(a,b,'rows','pairwise');
    title([num2str(cc) ' ---- ' num2str(pp)])
    
    subplot(1, 10, 9:10)
    boxplot2_points(1, {ind}, 0.75)
    xticks(1)
    xticklabels([])
    ylabel('correlation coefficient')
    xlim([0 2])
    
end