FR = cell(5, 1);
REP = cell(5, 1);

% Load the curves and pull out the integral here

for i = 1:5
    switch i
        case 1
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_itd_curve_100micro.mat')
            mw_time = 0.0005;
        case 2
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_ild_curve_100micro.mat')
            mw_time = 0.0005;
        case 3
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_ff_curve_100micro.mat')
            mw_time = 0.0005;
        case 4
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_ot_curve_100micro.mat')
            mw_time = 0.0005;
        case 5
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_ot_curve_100micro.mat')
            mw_time = 0.005;
    end
    
    center = round(size(curve, 2) / 2);
    win = round(mw_time / cw);
    winsac = curve(:, center - win + 1:center + win - 1);
    f = repmat(fr' .^2, 1, size(winsac, 2));
    repro = sum(winsac - f, 2) * cw ./ fr';
    
    neu = unique(neuron);
    FR{i} = cell(length(neu), 1);
    REP{i} = cell(length(neu), 1);
    
for n = 1:length(neu)
    idx = neuron == neu(n);
    REP{i}{n} = repro(idx)';
    FR{i}{n} = fr(idx);
end  
    
end

clear i center win winsac f repro neu n idx mw_time curve cw neuron fr depvar ex_ix e full_fr

% Merge ITD and ILD data for each neuron
FR{1} = cellfun(@(a, b) [a, b], FR{1}, FR{2}, 'UniformOutput', 0);
FR{2} = FR{3};
FR{3} = FR{4};
FR(4) = [];
REP{1} = cellfun(@(a, b) [a, b], REP{1}, REP{2}, 'UniformOutput', 0);
REP{2} = REP{3};
REP{3} = REP{4};
REP(4) = [];

clear d
%%
rep = cell(size(REP));
for i = 1:length(rep)
    rep{i} = cellfun(@(x, y) y(find(x == max(x), 1, 'first')), FR{i}, REP{i}, 'UniformOutput', 0);
    rep{i} = cell2mat(rep{i}')';
end

figure(8)
boxplot2_points(1:length(rep), rep, 0.75)
xticks(1:length(rep))
xticklabels({'ICls di', 'ICls ff', 'OT (2ms)', 'OT (18)'})
xlim([0 5])
ylabel('Peak Reproducibility')
clear i
%%
indcorr = cell(length(FR), 1);
norm = 1;

for i = 1:length(FR)
    indcorr{i} = cellfun(@(a, b) corr(a', b', 'rows', 'pairwise'), FR{i}, REP{i});
    
%     FR{i} = cellfun(@(x) normalization(x, norm), FR{i}, 'UniformOutput', 0);
%     REP{i} = cellfun(@(x) normalization(x, norm), REP{i}, 'UniformOutput', 0);
    if norm    
    FR{i} = cellfun(@(x) norm01(x), FR{i}, 'UniformOutput', 0);
    REP{i} = cellfun(@(x) norm01(x), REP{i}, 'UniformOutput', 0);
    end
    
    figure(i)
    subplot(1, 10, 1:7)
    scatterheatmapplot(cell2mat(FR{i}')', cell2mat(REP{i}')', ...
        {'firing rate', 'reproducibility'}, norm);
    subplot(1, 10, 9:10)
    boxplot2_points(1, indcorr(i), 0.75)
    xticks([])
    xlim([0 2])
    ylim([-1 1])
    ylabel('correlation coefficient')
    
end

clear i norm

