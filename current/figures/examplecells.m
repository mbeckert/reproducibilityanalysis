load('f:\desktop\Data\Analysis\SpeakerIndex.mat','SpeakerIndex')
load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_ot_curve_base_100micro.mat')
xx_n = -0.05:cw:0.05;
xx_n(1) = []; xx_n(end) = [];
f = repmat(fr' .^2, 1, size(curve, 2));
curve_n = (curve - f) * cw ./ fr';
load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_ot_curve_2ms.mat')
xx = -0.1:cw:0.1;
xx(1) = []; xx(end) = [];
f = repmat(fr' .^2, 1, size(curve, 2));
curve = (curve - f) * cw ./ fr';
ID_counting_ot2python
load('f:\desktop\WorkingFolder\distance\data\FreeField_OT_wDelay','OT')
data = OT; clear OT

% %

% If you know the date, site, and unit numbers and want the python index
d = 4;
s = 3;
u = 5;
[~, py, ~] = intersect(out(:, 2:4), [d,s,u], 'rows');
py = out(py, 1);

% If you know the python index and want the date, site, and unit numbers
% py = 10;
% d = out(out(:, 1) == py, 2);
% s = out(out(:, 1) == py, 3);
% u = out(out(:, 1) == py, 4);

neu = neuron == py;
curve = curve(neu, :);
curve_n = curve_n(neu, :);

sp = [56 54 53];
curve = curve(sp, :);
curve_n = curve_n(sp, :);

colors = [0, 0, 1; ...
    0.9100, 0.4100, 0.1700; ...
    1, 0, 0];

figure('units', 'inch', 'position', [0, 0, 8, 14])
subplot(6, length(s), 1:length(s)*2)
SpRFplot_square(mean(data{d}{s}{u}.FFf1.spikes_count, 2) / 0.15)
ax = gca;
ax.FontSize = 12;
title('Spatial Receptive Field', 'FontSize', 18)
j = colorbar;
ylabel(j,'Firing Rate (Hz)')
xticks(-100:20:100)
yticks(-80:20:80)
hold on

for i = 1:length(sp)
    scatter(SpeakerIndex(sp(i), 1), SpeakerIndex(sp(i), 2), 200, colors(i, :), 'fill', 'MarkerEdgeColor', 'w')
end

for i = 1:length(sp)
    spikes = data{d}{s}{u}.FFf1.spikes_times(sp(i), :);
    y = cell2mat(get_spike_y(spikes)');
    spikes = cell2mat(spikes');
    
    subplot(6, length(sp), length(sp)*2 + i)
    scatter(spikes, y, 10, colors(i, :), 'fill')
    ax = gca;
    ax.FontSize = 12;
    yticks(0:5:20)
    xlim([0.05, 0.2])
    xticks(0.05:0.05:0.2)
    xticklabels(0:50:150)
    xlabel('Time (ms)', 'FontSize', 16)
    if i == 1
        ylabel('Repetitions', 'FontSize', 16)
    end
    
    subplot(6, length(sp), length(sp)*3 + i)
    [nn, edges] = histcounts(spikes, 0.05:0.005:0.2);
    nn = nn / 0.005 / 20;
    edges = edges(1:end - 1) + 0.0025;
    bar(edges, nn, 'FaceColor', colors(i, :), 'EdgeColor', colors(i, :))
    ax = gca;
    ax.FontSize = 12;
    ylim([0 80])
    yticks(0:20:80)
    xlim([0.05, 0.2])
    xticks(0.05:0.05:0.2)
    xticklabels(0:50:150)
    xlabel('Time (ms)', 'FontSize', 16)
    if i == 1
        ylabel('Firing Rate (Hz)', 'FontSize', 16)
    end
    
    subplot(6, length(sp), length(sp)*4 + i)
    plot(xx_n, curve_n(i, :), 'Color', colors(i, :))
    ax = gca;
    ax.FontSize = 12;
    xlim([-0.05 0.05])
    ylim([-0.001 0.015])
    xticks(-0.05:0.025:0.05)
    xticklabels(-50:25:50)
    xlabel('Lag (ms)', 'FontSize', 16)
    if i == 1
        ylabel('Reproducibility', 'FontSize', 16)
    end
    
    subplot(6, length(sp), length(sp)*5 + i)
    plot(xx, curve(i, :), 'Color', colors(i, :))
    ax = gca;
    ax.FontSize = 12;
    xlim([-0.05 0.05])
    ylim([-0.03 0.1])
    xticks(-0.05:0.025:0.05)
    xticklabels(-50:25:50)
    xlabel('Lag (ms)', 'FontSize', 16)
    if i == 1
        ylabel('Reproducibility', 'FontSize', 16)
    end
    
end
    
%%
cd('Z:\Michael Beckert\data\ICls\Data\RawData\FF')

FILES = ls;
FILES = FILES(3:end,:);

d = cell(size(FILES, 1), 1);
IND = cell(size(FILES, 1), 1);

for file = 1:size(FILES, 1)
    load(FILES(file,:), 'TrialData', 'Indexer')
     
    TrialData = cellfun(@(x) x/1000, TrialData, 'UniformOutput', 0);
    d{file} = cellfun(@transpose, TrialData, 'UniformOutput', 0);
    IND{file} = Indexer;
    
end

clear TrialData
load('f:\desktop\Data\Analysis\SpeakerIndex.mat','SpeakerIndex')
cd('f:\desktop\WorkingFolder')

% %

n = 7;
sp = [2, 4, 9];

load('repro_ff_curve_base_50micro.mat')
xx = -0.05:cw:0.05;
xx(1) = []; xx(end) = [];
f = repmat(fr' .^2, 1, size(curve, 2));
curve = (curve - f) * cw ./ fr';

% Check the records on the amount of rotation the owl was moved.
f_rotate = [30, 0, 10, 30, 20, 20, 20];
f_ix = [5, 29, 34, 40, 46, 53, 55];
f_dir = {'Z:\Michael Beckert\data\ICls\Feb11-2015\006-2015-0211-02-FF.mat';
    'Z:\Michael Beckert\data\ICls\Feb9-2015\021-2015-0209-02-FF.mat';
    'Z:\Michael Beckert\data\ICls\Feb17-2015\021-2015-0217-02-FF.mat';
    'Z:\Michael Beckert\data\ICls\Feb26-2015\021-2015-0226-02-FF.mat';
    'Z:\Michael Beckert\data\ICls\Feb13-2015\023-2015-0213-03-FF.mat';
    'Z:\Michael Beckert\data\ICls\Feb23-2015\023-2015-0223-02-FF.mat';
    'Z:\Michael Beckert\data\ICls\Feb23-2015\023-2015-0223-03-FF.mat'};

% sp = 1:size(d{f_ix(n)}, 1);
idx = neuron == (f_ix(n) - 1);
curve = curve(idx, :);

load(f_dir{n})
% c = cellfun(@(x) sum(x > 100), TIMES);
c = cellfun(@length, TIMES);

colors = [0, 0, 1; ...
    0.9100, 0.4100, 0.1700; ...
    1, 0, 0];
  
figure('units', 'inch', 'position', [0, 0, 8, 12])
hold on

subplot(5, length(sp), 1:length(sp) * 2)
SpRFplot_square(mean(c, 2) /0.5)
ax = gca;
ax.FontSize = 12;
j = colorbar;
ylabel(j,'Firing Rate (Hz)')
xticks(-100:20:100)
yticks(-80:20:80)
title('Spatial Receptive Field', 'FontSize', 18)
% caxis([0 20])
hold on
% colors = distinguishable_colors(size(d{f_ix(n)}, 1));

for i = 1:length(sp)
    scatter(IND{f_ix(n)}(1, sp(i)) + f_rotate(n), IND{f_ix(n)}(2, sp(i)), 200, colors(i, :), 'fill', 'MarkerEdgeColor', 'w')
end 

for i = 1:length(sp)
    spikes = d{f_ix(n)}(sp(i), :);
    y = cell2mat(get_spike_y(spikes)');
    spikes = cell2mat(spikes');
    
    subplot(5, length(sp), length(sp)*2 + i)
    scatter(spikes, y, 1.5, colors(i, :), 'fill')
    ax = gca;
    ax.FontSize = 12;
    xlim([0, 0.45])
    xticks([0, 0.05:0.1:0.45])
    xticklabels([0, 50:100:450])
    ylim([0 500])
    yticks(0:100:500)
    xlabel('Time (ms)', 'FontSize', 16)
    if i == 1
        ylabel('Repetitions', 'FontSize', 16)
    end
%     if i == 2
%         title('Raster', 'FontSize', 20)
%     end
    
    subplot(5, length(sp), length(sp)*3 + i)
    [nn, edges] = histcounts(spikes, 0:0.005:0.45);
    nn = nn / 0.005 / 500;
    edges = edges(1:end - 1) + 0.0025;
    bar(edges, nn, 'FaceColor', colors(i, :), 'EdgeColor', colors(i, :))
    ax = gca;
    ax.FontSize = 12;
    xlim([0, 0.45])
    xticks([0, 0.05:0.1:0.45])
    xticklabels([0, 50:100:450])
    ylim([0 300])
    yticks(0:50:300)
    yticklabels(0:50:300)
    xlabel('Time (ms)', 'Fontsize', 16)
    if i == 1
        ylabel('Firing Rate (Hz)', 'FontSize', 16)
    end
%     if i == 2
%         title('Peristimulus Time Histogram', 'FontSize', 20)
%     end
    
    subplot(5, length(sp), length(sp)*4 + i)
    plot(xx, curve(sp(i), :), 'Color', colors(i, :))
    ax = gca;
    ax.FontSize = 12;
    xlim([-0.02 0.02])
    ylim([-0.0003 0.0015])
    xticks(-0.02:0.01:0.02)
    xticklabels(-20:10:20)
    xlabel('Lag (ms)', 'FontSize', 16)
    if i == 1
        ylabel('Reproducibility', 'FontSize', 16)
    end
%     if i == 2
%         title('Shuffled Autocorrelogram', 'FontSize', 20)
%     end
end

%%
cd('Z:\Michael Beckert\data\ICls\Data\RawData\Dichotic\ILD\')

FILES = ls;
FILES = FILES(3:end,:);

d = cell(size(FILES, 1), 1);
IND = cell(size(FILES, 1), 1);

for file = 1:size(FILES, 1)
    load(FILES(file,:), 'curvedata', 'curvesettings')
     
    TrialData = cellfun(@(x) x/1000, curvedata.spike_times, 'UniformOutput', 0);
    d{file} = cellfun(@transpose, TrialData, 'UniformOutput', 0);
    IND{file} = curvesettings.curve.ILDrange;
    
end

clear TrialData curvedata curvesettings file
load('f:\desktop\Data\Analysis\SpeakerIndex.mat','SpeakerIndex')
cd('f:\desktop\WorkingFolder')
% %

figure('units', 'inch', 'position', [0, 0, 8, 12])
n = 6;

colors = [0, 0, 1; ...
    0.9100, 0.4100, 0.1700; ...
    1, 0, 0];

load('repro_ild_curve_base_50micro.mat')

xx = -0.05:cw:0.05;
xx(1) = []; xx(end) = [];
f = repmat(fr' .^2, 1, size(curve, 2));
curve = (curve - f) * cw ./ fr';

% sp = 1:size(d{n}, 1);
sp = 1:3;
idx = neuron == (n - 1);
curve = curve(idx, :);

load('Z:\Michael Beckert\data\ICls\Mar2-2015\006-2015-0302-02-itd.mat')
subplot(5, 6, [1 3 7 9])
errorbar(curvesettings.curve.ITDrange, mean(curvedata.spike_counts / 0.15, 2), std(curvedata.spike_counts / 0.15, [], 2))
xlim([min(curvesettings.curve.ITDrange), max(curvesettings.curve.ITDrange)])
ax = gca;
ax.FontSize = 12;
xlabel('ITD (�s)')
ylabel('Firing Rate (Hz)')
title('ITD tuning curve', 'FontSize', 18)
axis square

load('Z:\Michael Beckert\data\ICls\Mar2-2015\006-2015-0302-02-ild.mat')
subplot(5, 6, [4 6 10 12])
errorbar(curvesettings.curve.ILDrange, mean(curvedata.spike_counts / 0.15, 2), std(curvedata.spike_counts / 0.15, [], 2))
xlim([min(curvesettings.curve.ILDrange), max(curvesettings.curve.ILDrange)])
ax = gca;
ax.FontSize = 12;
a = axis;
xlabel('ILD (dB)')
title('ILD tuning curve', 'FontSize', 18)
axis square

hold on
% colors = distinguishable_colors(size(d{n}, 1));

for i = 1:length(sp)
    scatter(IND{n}(1, sp(i)), a(4), 200, colors(i, :), 'fill', 'MarkerEdgeColor', 'w')
end 

for i = 1:length(sp)
    spikes = d{n}(sp(i), :);
    y = cell2mat(get_spike_y(spikes)');
    spikes = cell2mat(spikes');
    
    subplot(5, 6, [12 + i*2-1, 12 + i*2])
    scatter(spikes, y, 2, colors(i, :), 'fill')
    ax = gca;
    ax.FontSize = 12;
    xlim([0.05, 0.3])
    xticks([0.05, 0.1:0.05:0.3])
    xticklabels([0, 50:50:250])
    ylim([0 100])
    yticks(0:20:100)
    xlabel('Time (ms)', 'FontSize', 16)
    if i == 1
        ylabel('Repetitions', 'FontSize', 16)
    end
    
    subplot(5, 6, [18 + i*2-1, 18 + i*2])
    [nn, edges] = histcounts(spikes, 0.05:0.005:0.3);
    nn = nn / 0.005 / 100;
    edges = edges(1:end - 1) + 0.0025;
    bar(edges, nn, 'FaceColor', colors(i, :), 'EdgeColor', colors(i, :))
    ax = gca;
    ax.FontSize = 12;
    xlim([0.05, 0.3])
    xticks([0.05, 0.1:0.05:0.3])
    xticklabels([0, 50:50:250])
    ylim([0 500])
    yticks(0:100:500)

    xlabel('Time (ms)', 'FontSize', 16)
    if i == 1
        ylabel('Firing Rate (Hz)', 'FontSize', 16)
    end
    
    subplot(5, 6, [24 + i*2-1, 24 + i*2])
    plot(xx, curve(sp(i), :), 'Color', colors(i, :))
    ax = gca;
    ax.FontSize = 12;
    xlim([-0.02 0.02])
    xticks(-0.02:0.01:0.02)
    xticklabels(-20:10:20)
    ylim([-0.005 0.025])
    yticks(0:0.005:0.025)
    xlabel('Lag (ms)', 'FontSize', 16)
    if i == 1
        ylabel('Reproducibility', 'FontSize', 16)
    end
end