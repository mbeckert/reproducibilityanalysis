load('f:\desktop\WorkingFolder\distance\altcorrections\distance_correction_1_p4_c040_q_100.mat')

%%

boxplot2_points(cellfun(@nanmedian, cellplot(1:50,1)), cellplot(1:50,2), 1);
hold on
plot(key(1:50, 1), cfun{1}(1) * key(1:50, 1) .^ 4 ...
    + cfun{1}(2) * key(1:50, 1) .^ 3 ...
    + cfun{1}(3) * key(1:50, 1) .^ 2 ...
    + cfun{1}(4) * key(1:50, 1) ...
    + cfun{1}(5), 'r', 'LineWidth', 3)
xlim([0 51])
xlabel('Geometric Mean Firing Rate')
ylabel('Distance (cost)')
axis square