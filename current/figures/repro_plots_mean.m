% Old incorrect data
% for i = 1:4
%     switch i
%         case 1
%             load('f:\desktop\WorkingFolder\working_reprod\repro_itd.mat')
%         case 2
%             load('f:\desktop\WorkingFolder\working_reprod\repro_ild.mat')
%         case 3
%             load('f:\desktop\WorkingFolder\working_reprod\repro_ff.mat')
%         case 4
%             load('f:\desktop\WorkingFolder\working_reprod\repro_ot.mat')
%     end
%     
%     FR{i} = fr;
%     REP{i} = r_brette;
% end
% 
% FR{1} = FR{1}';
% REP{1} = REP{1}';
% FR{3} = FR{3}';
% REP{3} = REP{3}';
% FR{4} = FR{4}';
% REP{4} = REP{4}';
% 
% FR{2} = num2cell(FR{2}, 2);
% REP{2} = num2cell(REP{2}, 2);

% Corrected data from python export, usable but not easily changed
% for i = 1:4
%     switch i
%         case 1
% %             load('f:\desktop\WorkingFolder\working_reprod\repro_itd_10_again.mat')
%             load('repro_itd_05.mat')
%         case 2
% %             load('f:\desktop\WorkingFolder\working_reprod\repro_ild_10_again.mat')
%             load('repro_ild_05.mat')
%         case 3
% %             load('f:\desktop\WorkingFolder\working_reprod\repro_ff_10_again.mat')
%             load('repro_ff_05.mat')
%         case 4
% %             load('f:\desktop\WorkingFolder\working_reprod\repro_ot_10_again.mat')
%             load('repro_ot_0.mat')
%     end
%     
%     FR{i} = fr;
%     REP{i} = r_brette;
% end

% clear i fr r_brette r_joris sac

% Merge ITD and ILD data for each neuron
% FR{1} = cellfun(@(a, b) [a, b], FR{1}, FR{2}, 'UniformOutput', 0);
% FR{2} = FR{3};
% FR{3} = FR{4};
% FR(4) = [];
% REP{1} = cellfun(@(a, b) [a, b], REP{1}, REP{2}, 'UniformOutput', 0);
% REP{2} = REP{3};
% REP{3} = REP{4};
% REP(4) = [];

FR = cell(6, 1);
REP = cell(6, 1);

% Load the curves and pull out the integral here
% mw_time = 0.01; % in seconds

for i = 1:4
    switch i
        case 1
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_itd_curve_50micro.mat')
            mw_time = 0.0001;
        case 2
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_ild_curve_50micro.mat')
            mw_time = 0.0001;
        case 3
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_az_curve_50micro.mat')
            mw_time = 0.0001;
        case 4
            load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_el_curve_50micro.mat')
            mw_time = 0.0001;
%         case 5
%             load('repro_otaz_curve_1ms.mat')
%         case 6
%             load('repro_otel_curve_1ms.mat')
    end
    
%     ex_ix = fr > 2;
%     for e = 1:size(curve, 1)
%         if ex_ix(e) == 0
%             curve(e, :) = nan(1, size(curve, 2));
%         end
%     end
    
    center = round(size(curve, 2) / 2);
    win = round(mw_time / cw);
    winsac = curve(:, center - win + 1:center + win - 1);
%     winsac = curve(:, center);
    f = repmat(fr' .^2, 1, size(winsac, 2));
    repro = mean(winsac - f, 2) * cw ./ fr';
    
    neu = unique(neuron);
    FR{i} = cell(length(neu), 1);
    REP{i} = cell(length(neu), 1);
    
for n = 1:length(neu)
    idx = neuron == neu(n);
    REP{i}{n} = repro(idx)';
    FR{i}{n} = fr(idx);
end  
    
end

clear i center win winsac f repro neu n idx mw_time curve cw neuron fr depvar ex_ix e full_fr

d = load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_otaz_curve_3ms_mean.mat');
FR{5} = d.FR;
REP{5} = d.REP;
d = load('f:\desktop\WorkingFolder\working_reprod\frompython\repro_otel_curve_3ms_mean.mat');
FR{6} = d.FR;
REP{6} = d.REP;

clear d
%%
rep = cell(size(REP));
for i = 1:length(rep)
%     rep{i} = cellfun(@nanmax, REP{i});
    rep{i} = cellfun(@(x, y) y(find(x == max(x), 1, 'first')), FR{i}, REP{i}, 'UniformOutput', 0);
    rep{i} = cell2mat(rep{i}')';
end

figure(8)
boxplot2_points(1:length(rep), rep, 0.75)
xticks(1:length(rep))
% xticklabels({'ICls itd', 'ICls ild', 'ICls ff', 'OT'})
% xlim([0 5])
xticklabels({'ICls itd', 'ICls ild', 'ICls az', 'ICls el', 'OT az', 'OT el'})
xlim([0 7])
ylabel('Maximum Reproducibility')
clear i
%%
indcorr = cell(length(FR), 1);
norm = 1;

for i = 1:length(FR)
    indcorr{i} = cellfun(@(a, b) corr(a', b', 'rows', 'pairwise'), FR{i}, REP{i});
    
%     FR{i} = cellfun(@(x) normalization(x, norm), FR{i}, 'UniformOutput', 0);
%     REP{i} = cellfun(@(x) normalization(x, norm), REP{i}, 'UniformOutput', 0);
    if norm    
    FR{i} = cellfun(@(x) norm01(x), FR{i}, 'UniformOutput', 0);
    REP{i} = cellfun(@(x) norm01(x), REP{i}, 'UniformOutput', 0);
    end
    
    figure(i)
    subplot(1, 10, 1:7)
    scatterheatmapplot(cell2mat(FR{i}')', cell2mat(REP{i}')', ...
        {'firing rate', 'reproducibility'}, norm);
    subplot(1, 10, 9:10)
    boxplot2_points(1, indcorr(i), 0.75)
    xticks([])
    xlim([0 2])
    ylim([-1 1])
    ylabel('correlation coefficient')
    
end

clear i norm

