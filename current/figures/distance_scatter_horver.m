% First use "apply_distance_correction" To all the data
% load('f:\desktop\WorkingFolder\distance\distance_corrected_data_fullcorrect.mat')
% load('f:\desktop\WorkingFolder\distance\distance_corrected_data_20.mat')
load('f:\desktop\WorkingFolder\distance\distance_corrected_data_100_omit.mat')
%%
norm = 1;
d = cell(4, 2);
indcorr = cell(2, 4);
dis = cell(2, 4);

for h = 1:2
    switch h
        case 1
            e = icls;
        case 2
            e = ot;
    end
    
    if h == 1
        d{1, 1} = e{1}.rf;
        d{1, 2} = e{1}.vp;
        d{2, 1} = e{2}.rf;
        d{2, 2} = e{2}.vp;
    end
    
    d{3, 1} = e{3}.rf;
    d{3, 2} = e{3}.vp;
    d{4, 1} = e{4}.rf;
    d{4, 2} = e{4}.vp;
    
    for i = 1:size(d, 1)
        
        indcorr{h, i} = cellfun(@(a, b) corr(a, b, 'rows', 'pairwise'), d{i, 1}, d{i, 2});
%         dis{h, i} = cellfun(@min, d{i, 2});
        dis{h, i} = cellfun(@(x, y) y(find(x == max(x), 1, 'first')), d{i, 1}, d{i, 2});
    
        if norm
%             x = cellfun(@abs, d{i, 1}, 'UniformOutput', 0);
%             y = cellfun(@abs, d{i, 2}, 'UniformOutput', 0);
            x = cellfun(@(x) norm01(x, 2), d{i, 1}, 'UniformOutput', 0);
            y = cellfun(@(x) norm01(x, 2), d{i, 2}, 'UniformOutput', 0);
            x = -cell2mat(x);
            y = -cell2mat(y);
        else
            x = cell2mat(d{i, 1});
            y = cell2mat(d{i, 2});
        end
  
        figure(str2double([num2str(h), num2str(i)]))
        subplot(1, 10, 1:7)
        scatterheatmapplot(x, y, {'firing rate', 'distance'}, 0);
        subplot(1, 10, 9:10)
        boxplot2_points(1, indcorr(h, i), 0.75)
        xticks([])
        xlim([0 2])
        ylim([-1 1])
        ylabel('correlation coefficient')
        
    end
    
end

figure(25)
dis = {dis{1, 1}, dis{1, 2}, dis{1, 3}, dis{1, 4}, dis{2, 3}, dis{2, 4}};
boxplot2_points(1:length(dis), dis, 0.75)
xticks(1:length(dis))
xticklabels({'ICls itd', 'ICls ild', 'ICls az', 'ICls el', 'OT - az', 'OT - el'})
xlim([0 7])
ylabel('Minimum Corrected Distance')