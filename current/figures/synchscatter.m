cd('f:\desktop\WorkingFolder\distance\data')
load('FreeField_OT_wDelay','OT')
% load('FreeField_OT_wDelay_unsorted','OT')
data = OT; clear OT
cd('F:\Desktop\WorkingFolder')

date = 4;
site = 3;
unit1 = 4;
unit2 = 5;

u1 = data{date}{site}{unit1}.FFf1.spikes_times;
u2 = data{date}{site}{unit2}.FFf1.spikes_times;

%%

sp = 53;

x1 = cell2mat(u1(sp, :)');
x2 = cell2mat(u2(sp, :)');

y1 = cell2mat(get_spike_y(u1(sp, :)'));
y2 = cell2mat(get_spike_y(u2(sp, :)'));

scatter(x1, y1, 50, 'fill', 'sb')
hold on
scatter(x2, y2, 50, 'fill', 'sr')
xlim([0.05 0.2])
ylabel('reps')
xlabel('time (s)')

%%


