%% Run this then "reformatsynch"
load('FreeField_OT_wDelay','OT')
data = OT; clear OT

Window = sort([-0.1 0.1]);
% binWidth = 0.005; int = 5;
binWidth = 0.001; int = 10;
binNum = length(Window(1):binWidth:Window(2))-2;
norminc = 0:0.1:1;

dur = 0.15;
start = 0.1;
pattern = 'b.|FF.';                   % These are to remove
pattern2 = 'frozen.|i.';                   % These are to keep
f = 'ff';

D=[];

for d = 1:length(data)
    for s = 1:length(data{d})
        list = combnk(2:length(data{d}{s}),2);
        for p = 1:size(list,1)
            
            disp(['date: ' num2str(d) '; site: ' num2str(s) '; unit1: ' num2str(list(p,1)) '; unit2: ' num2str(list(p,2))]);
            
            unit1 = data{d}{s}{list(p,1)};
            unit2 = data{d}{s}{list(p,2)};
            filetypes1 = fieldnames(unit1);
            filetypes2 = fieldnames(unit2);
            filetypes = intersect(filetypes1,filetypes2);
            idx = regexpi(filetypes,pattern);
            idx = cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            idx = regexpi(filetypes,pattern2);
            idx = ~cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            
            clear filetypes1 filetypes2 idx chck* check
            
            if ~isempty(filetypes)
                for f = 1:length(filetypes)
                    st_u1 = unit1.(filetypes{f}).spikes_times;
                    st_u2 = unit2.(filetypes{f}).spikes_times;
                    sc_u1 = unit1.(filetypes{f}).spikes_count;
                    sc_u2 = unit2.(filetypes{f}).spikes_count;
                    
                        for i = 1:length(st_u1(:))
                            st_u1{i}(st_u1{i} < start) = [];
                            st_u2{i}(st_u2{i} < start) = [];
                        end
    
                    [depvar, reps] = size(sc_u1);
                    if depvar == 1 || reps == 1
                        reps = depvar;
                        depvar = 1;
                        st_u1 = st_u1';
                        st_u2 = st_u2';
                        sc_u1 = sc_u1';
                        sc_u2 = sc_u2';
                    end
                    
                    CCG = zeros(depvar,binNum);
                    shiftCCG = zeros(depvar,binNum);
                    
                    for t = 1:depvar
                        for r = 1:reps
                            if ~isempty(st_u1{t,r})
                                for spike = 1:length(st_u1{t,r})
                                    times = st_u1{t,r}(spike) - st_u2{t,r};
                                    his=hist(times,Window(1):binWidth:Window(2)); %% Here is where you can change binwidth
                                    CCG(t,:)=CCG(t,:)+his(2:end-1);
                                    
                                    % regular shift
                                    if t==size(sc_u1,1)
                                        times = st_u1{t,r}(spike) - st_u2{1};
                                    else
                                        times = st_u1{t,r}(spike) - st_u2{t+1,r};
                                    end
                                    
                                    his=hist(times,Window(1):binWidth:Window(2));
                                    shiftCCG(t,:)=shiftCCG(t,:)+his(2:end-1);
                                end
                            end
                        end
                    end % for "t"
                    clear his spike times
                    
                    holder = cell(1,13);
                    holder{1} = [d,s,list(p,1),list(p,2)];
                    for i = 2:10
                        holder{i} = nan(1,depvar);
                    end
                    for i = 11:13
                        holder{i}=cell(1,depvar);
                    end
                    
                    for t = 1:depvar
                        
                        rate1=mean(sc_u1(t,:)/dur);
                        rate2=mean(sc_u2(t,:)/dur);
                        
                        SmCCG=(smooth(CCG(t,:))/sqrt(rate1*rate2))/binNum;
                        SmShiftCCG=(smooth(shiftCCG(t,:))/sqrt(rate1*rate2))/binNum;
                        
                        SmCCGCorrected=SmCCG-SmShiftCCG;
                        
                        flankS=std([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                        flankM=mean([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                        center = SmCCGCorrected(round(binNum/2) - int:round(binNum/2) + int);
                        
                        PEAK=SmCCGCorrected(round(binNum/2));
                        %                     check1 = max(center)>flankM+5*flankS;
                        check1 = PEAK>flankM+5*flankS;
                        positive=(SmCCGCorrected>=(PEAK/2));
                        pos=diff(positive);
                        less=pos(1:round(binNum/2));
                        more=pos(round(binNum/2):end);
                        Low=find(less==1,1,'last');
                        High=find(more==-1,1,'first');
                        Low=length(less)-Low;
                        bandPEAK=High+Low;
                        
                        if isempty(bandPEAK)
                            bandPEAK=nan;
                            sumband=nan;
                        else
                            sumband=sum(SmCCGCorrected(length(less)-Low:length(less)+High))/bandPEAK;
                        end
                        
                        holder{2}(1,t)=PEAK;
                        holder{3}(1,t)=rate1;
                        holder{4}(1,t)=rate2;
                        holder{5}(1,t)=check1;
%                           holder{6}(1,t)=check2;
%                           holder{7}(1,i)=check;
                        holder{8}(1,t)=bandPEAK;
                        holder{9}(1,t)=sum(center);
                        holder{10}(1,t)=sumband;
                        holder{11}{1,t}=SmCCG;
                        holder{12}{1,t}=SmShiftCCG;
                        holder{13}{1,t}=SmCCGCorrected;
                        
                        clear positive pos less more Low High bandPEAK sens1 sens2
                        
                    end     % for "i" the second time
                    
                    if isfield(D,filetypes{f})
                        for i = 1:10
                            if size(D.(filetypes{f}){i},2) > size(holder{i},2)
                                padding = nan(1,size(D.(filetypes{f}){i},2)-size(holder{i},2));
                                D.(filetypes{f}){i}=[D.(filetypes{f}){i};holder{i},padding];
                            elseif size(D.(filetypes{f}){i},2) < size(holder{i},2)
                                padding = nan(size(D.(filetypes{f}){i},1),size(holder{i},2)-size(D.(filetypes{f}){i},2));
                                D.(filetypes{f}){i}=[D.(filetypes{f}){i},padding;holder{i}];
                            else
                                D.(filetypes{f}){i}=[D.(filetypes{f}){i};holder{i}];
                            end
                        end
                        for i = 11:13
                            if size(D.(filetypes{f}){i},2) > size(holder{i},2)
                                padding = cell(1,size(D.(filetypes{f}){i},2)-size(holder{i},2));
                                D.(filetypes{f}){i}=[D.(filetypes{f}){i};holder{i},padding];
                            elseif size(D.(filetypes{f}){i},2) < size(holder{i},2)
                                padding = cell(size(D.(filetypes{f}){i},1),size(holder{i},2)-size(D.(filetypes{f}){i},2));
                                D.(filetypes{f}){i}=[D.(filetypes{f}){i},padding;holder{i}];
                            else
                                D.(filetypes{f}){i}=[D.(filetypes{f}){i};holder{i}];
                            end
                        end
                    else
                        for i = 1:length(holder)
                            D.(filetypes{f}){i}=holder{i};
                        end
                    end
                    
                    clear holder depvar rate* i PEAK check SmCCG curves CORR trial flank* sumband SmShiftCCG shiftCCG SmCCGCorrected CCG
                    
                end         % for "file"
            end
            clear filetypes
            
        end             % for "pair"
    end                 % for "site"
end                     % for "date"

ot_synch_di = D;

clearvars -except ot_synch_di

cd('F:\Desktop\WorkingFolder')
save ot_synch_di_shift
