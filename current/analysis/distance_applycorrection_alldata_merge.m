cd('f:\desktop\WorkingFolder')

data = cell(4, 1);

for t = 1:size(data, 1)
    switch t
        case 1
            load('f:\desktop\WorkingFolder\distance\altcorrections\distance_correction_0_p4_c020_q_100.mat', 'key', 'cfun')
            load('f:\desktop\WorkingFolder\distance\varyingQ\iclsITD_distance_100_omit.mat', 'distanceVP', 'Mean')
        case 2
            load('f:\desktop\WorkingFolder\distance\altcorrections\distance_correction_0_p4_c020_q_100.mat', 'key', 'cfun')
            load('f:\desktop\WorkingFolder\distance\varyingQ\iclsILD_distance_100_omit.mat', 'distanceVP', 'Mean')
        case 3
            load('f:\desktop\WorkingFolder\distance\altcorrections\distance_correction_0_p4_c040_q_100.mat', 'key', 'cfun')
            load('f:\desktop\WorkingFolder\distance\varyingQ\iclsFF_distance_100_omit.mat', 'distanceVP', 'Mean')
        case 4
            load('f:\desktop\WorkingFolder\distance\altcorrections\distance_correction_0_p4_c010_q_100.mat', 'key', 'cfun')
            load('f:\desktop\WorkingFolder\distance\varyingQ\otFF_distance_100_omit.mat', 'distanceVP', 'Mean')
            distanceVP = cellfun(@transpose, distanceVP, 'UniformOutput', 0);
            Mean = cellfun(@transpose, Mean, 'UniformOutput', 0);
    end
    
    cor_vp = @(x) cfun{1}(1) * x .^ 4 + cfun{1}(2) * x .^ 3 + cfun{1}(3) * x .^ 2 + cfun{1}(4) * x + cfun{1}(5);
    vp = cell(size(distanceVP));
    
    for u = 1:length(Mean)
        vp{u} = nan(size(distanceVP{u}));
        for i = 1:size(Mean{u}, 1)
            vp{u}(i) = distanceVP{u}(i) - cor_vp(Mean{u}(i));
        end
    end
    
    data{t}.vp = vp;
    data{t}.rf = Mean;
    
end

clear distanceVP Mean t u i idx es euc vp cfun cor_vp key M V

data{1}.vp = cellfun(@(x, y) [x; y], data{1}.vp, data{2}.vp, 'UniformOutput', 0);
data{1}.rf = cellfun(@(x, y) [x; y], data{1}.rf, data{2}.rf, 'UniformOutput', 0);
data{2} = data{3};
data{3} = data{4};
data(4) = [];