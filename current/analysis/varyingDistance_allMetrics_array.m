load('f:\desktop\WorkingFolder\working_array\arraydata_spikes.mat', 'unitdata')

q = 10;
start = 0.1;

euc = cell(size(unitdata));
vp = cell(size(unitdata));
es = cell(size(unitdata));
rf = cell(size(unitdata));

for u = 1:length(unitdata)
    disp(num2str(u))
    tic
    times = unitdata{u}.frozentimes;
    
    if length(times) > 1
        for i = 1:length(times(:))
            times{i}(times{i} < start) = [];
        end
        euc{u} = nan(size(times, 2), 1);
        vp{u} = nan(size(times, 2), 1);
        es{u} = nan(size(times, 2), 1);
        rf{u} = nanmean(cellfun(@length, times));
        
        idx = ones(size(times, 1));
        idx = tril(idx, -1);
        idx2 = find(idx == 1);
        for st = 1:size(times, 2)
            holder = nan(size(times, 1));
            holder2 = nan(size(times, 1));
            holder3 = nan(size(times, 1));
            for t1 = 1:size(times, 1)
                for t2 = 1:size(times, 1)
                    if idx(t1, t2) == 1
                        holder(t1, t2) = euclidean_distance(times{t1, st}, times{t2, st}, ...
                            start:0.001:unitdata{u}.frozenparams.del + unitdata{u}.frozenparams.dur);
                        holder2(t1, t2) = VPdistance(times{t1, st}, times{t2, st}, q);
                        [holder3(t1, t2), ~] = Event_Sync(times{t1, st}, times{t2, st});
                    end
                end
            end
            euc{u}(st) = nanmean(holder(idx2));
            vp{u}(st) = nanmean(holder2(idx2));
            es{u}(st) = nanmean(holder3(idx2));
        end
    end
    toc
end

clear u idx idx2 st holder* t1 t2 times unitdata q start

save arrayITD_distance