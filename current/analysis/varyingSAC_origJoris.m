folders = {'Z:\Michael Beckert\data\ICls\Data\RawData\Dichotic\ITD'; ...
    'Z:\Michael Beckert\data\ICls\Data\RawData\Dichotic\ILD'; ...
    'Z:\Michael Beckert\data\ICls\Data\RawData\FF'};
start = 100;
durs = [250, 250, 450];
binWidth = 0.5;
maxBin = 50;

i_rate = cell(3, 1);
i_fr = cell(3, 1);
i_sac = cell(3, 1);
i_nf = cell(3, 1);

for f = 1:length(folders)
    
    disp(['folder ' num2str(f)])
    
    cd(folders{f})
    files = dir('*.mat');
    i_rate{f} = cell(length(files), 1);
    i_fr{f} = cell(length(files), 1);
    i_sac{f} = cell(length(files), 1);
    i_nf{f} = cell(length(files), 1);
    TrialData = cell(length(files), 1);
    if f < 3
        for i = 1:length(files)
            load(files(i).name, 'curvedata')
            TrialData{i} = curvedata.spike_times;
        end
        clear curvedata i files
    elseif f == 3
        for i = 1:length(files)
            a = load(files(i).name, 'TrialData');
            TrialData{i} = a.TrialData;
        end
        clear a i files
    end
    
    
    for i = 1:length(TrialData)
        disp(['f = ' num2str(i)])
        i_rate{f}{i} = mean(cellfun(@length, TrialData{i}), 2) / (durs(f) / 1000 - 0.05);
        i_fr{f}{i} = nan(size(TrialData{i}, 1), 1);
        i_sac{f}{i} = cell(size(TrialData{i}, 1), 1);
        i_nf{f}{i} = nan(size(TrialData{i}, 1), 1);
        for d = 1:size(TrialData{i}, 1)
            [i_sac{f}{i}{d}, i_nf{f}{i}(d), i_fr{f}{i}(d)] = computeSAC_origjoris(TrialData{i}(d, :)', [start durs(f)+50], binWidth, maxBin);
        end
    end
    
    clear TrialData i d
    
end

clear folders f durs start maxBin
% %
cd('f:\desktop\WorkingFolder\distance\data')
load('FreeField_OT_wDelay','OT')
data = OT; clear OT
cd('F:\Desktop\WorkingFolder')

o_rate = cell(200, 2);
o_sac = cell(200, 2);
o_fr = cell(200, 2);
o_nf = cell(200, 2);

% binWidth = 0.05;
maxBin = 50;
start = 100;
durs = 150;
count = 1;

for d = 1:size(data, 1)
    for s = 1:size(data{d}, 1)
        for u = 2:size(data{d}{s}, 1)
            disp(['d = ' num2str(d) '; s = ' num2str(s) '; u = ' num2str(u)])
            neu = data{d}{s}{u};
            check = 0;
            for f = 1:2
                if isfield(neu, ['FFf' num2str(f)])
                    check = 1;
                    sp = cellfun(@(x) x' * 1000, neu.(['FFf' num2str(f)]).spikes_times, 'UniformOutput', 0);
                    o_rate{count, f} = mean(cellfun(@length, sp), 2) / (durs / 1000 - 0.05);
                    o_fr{count, f} = nan(size(sp, 1), 1);
                    o_sac{count, f} = cell(size(sp, 1), 1);
                    o_nf{count, f} = nan(size(sp, 1), 1);
                    for i = 1:size(sp, 1)
                        [o_sac{count, f}{i}, o_nf{count, f}(i), o_fr{count, f}(i)] = computeSAC_origjoris(sp(i, :)', [start durs+50], binWidth, maxBin);
                    end
                end
            end
            if check
                count = count + 1;
            end
        end
    end
end

o_sac(count:end, :) = [];
o_fr(count:end, :) = [];
o_nf(count:end, :) = [];
o_rate(count:end, :) = [];

clear durs count d s u neu check f sp i data start maxBin

% %
save(['repro_joris_all_' pad(num2str(binWidth * 100), 4, 'left', '0')])

%%
load('repro_joris_all_0200.mat')





%%

% binWidth = 0.05; % Needs to be the same as the SAC bin size from 'computeSAC'
timewin = 0; % in milliseconds
binWin = int16(round(timewin / binWidth));

i_rep = cell(size(i_sac));

for t = 1:size(i_sac, 1)
    i_rep{t} = cell(size(i_sac{t}));
    for n = 1:size(i_sac{t}, 1)
        i_rep{t}{n} = nan(size(i_sac{t}{n}));
        for s = 1:size(i_sac{t}{n}, 1)
            sac = i_sac{t}{n}{s} / i_nf{t}{n}(s);
            i_rep{t}{n}(s) = sum(sac(int16(ceil(length(sac)/2)) - binWin: int16(ceil(length(sac)/2)) + binWin));
        end
    end
end
clear sac t n s

o_rep = cell(size(o_sac));

for n = 1:size(o_sac, 1)
    for f = 1:size(o_sac, 2)
        o_rep{n, f} = nan(size(o_sac{n, f}));
        for s = 1:size(o_sac{n, f}, 1)
            sac = o_sac{n, f}{s} / o_nf{n, f}(s);
            o_rep{n, f}(s) = sum(sac(int16(ceil(length(sac)/2)) - binWin: int16(ceil(length(sac)/2)) + binWin));
        end
    end
end
clear sac n f s binWin timewin 

o_rep(cellfun(@isempty, o_rep)) = {ones(144, 1)};
o_fr(cellfun(@isempty, o_fr)) = {ones(144, 1)};
o_rate(cellfun(@isempty, o_rate)) = {ones(144, 1)};

i_rep{4} = cellfun(@(x, y) x .* y, o_rep(:, 1), o_rep(:, 2), 'UniformOutput', 0);
i_fr{4} = cellfun(@(x, y) x .* y, o_fr(:, 1), o_fr(:, 2), 'UniformOutput', 0);
i_rate{4} = cellfun(@(x, y) x .* y, o_rate(:, 1), o_rate(:, 2), 'UniformOutput', 0);

%%

p = cell(4, 1);
for i = 1:4
    p{i} = cellfun(@max, i_rep{i});
end
boxplot2_points(1:4, p, 0.75)
xticks(1:4)
xticklabels({'icls itd', 'icls ild', 'icls ff', 'ot'})
ylabel('maximum reproducibility')
clear i

cellfun(@nanmedian, p)
%%

norm = 1;

for i = 1:4
    
    if norm
        x = cellfun(@(x) norm01(x, 2), i_rate{i}, 'UniformOutput', 0);
        y = cellfun(@(x) norm01(x, 2), i_rep{i}, 'UniformOutput', 0);
    else
        x = i_rate{i};
        y = i_rep{i};
    end
    indcorr = cellfun(@(x, y) corr(x, y, 'rows', 'pairwise'), i_rate{i}, i_rep{i});
    
    x = cell2mat(x);
    y = cell2mat(y);
    
    figure(i)
    subplot(1, 4, 1:3)
    scatter(x, y, 5, 'k', 'fill')
    [r, c] = corr(x, y, 'rows', 'pairwise');
    title([num2str(r) ' ---- ' num2str(c)])
    xlabel('firing rate')
    ylabel('reproducibility')
    
    subplot(1, 4, 4)
    boxplot2_points(1, {indcorr}, 0.75)
    xticks([])
    xlim([0 2])
    ylabel('correlation coefficient')
    ylim([-1 1])
end

clear i x y r c