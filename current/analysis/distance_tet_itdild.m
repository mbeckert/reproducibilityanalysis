load('FreeField_OT_wDelay','OT')
data = OT; clear OT

pattern = '';                   % These are to remove
% pattern2 = 'it.';                   % These are to keep
pattern2 = 'il.';

% %

ID = cell(200, 1);
DATA = cell(200, 1);

for d = 1:length(data)
    for s = 1:length(data{d})
        for u = 2:length(data{d}{s})
            
            disp(['date: ' num2str(d) '; site: ' num2str(s) '; unit: ' num2str(u)]);
            
            tic
            
            unit = data{d}{s}{u};
            
            filetypes = fieldnames(unit);
            idx = regexpi(filetypes,pattern);
            idx = cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            idx = regexpi(filetypes,pattern2);
            idx = ~cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            
            clear idx
            
            if ~isempty(filetypes)
                
                L = find(cellfun(@isempty, ID), 1, 'first');
                
                DATA{L} = cell(size(filetypes));
                ID{L} = [d, s, u];
                
                for f = 1:length(filetypes)
                    DATA{L}{f} = unit.(filetypes{f}).spikes_times;
                end
                
                DATA{L} = vertcat(DATA{L}{:});
                
            end
            
        end
    end
end

L = find(cellfun(@isempty, ID), 1, 'first');
DATA(L:end) = [];
ID(L:end) = [];
ID = cell2mat(ID);

clear d s u unit filetypes L f pattern pattern2 data

% % calculate distance within cell

dur = 0.2;
del = 0.05;
start = 0.1;

euc_c = cell(size(ID, 1), 1);
vp_c = cell(size(ID, 1), 1);
es_c = cell(size(ID, 1), 1);
rm_c = cell(size(ID, 1), 1);

for u = 1:length(DATA)
    
    euc_c{u} = nan(size(DATA{u}, 1), 1);
    vp_c{u} = nan(size(DATA{u}, 1), 1);
    es_c{u} = nan(size(DATA{u}, 1), 1);
    rm_c{u} = nan(size(DATA{u}, 1), 1);
    
    for i = 1:length(DATA{u}(:))
        DATA{u}{i}(DATA{u}{i} < start) = [];
    end
    
    for st = 1:size(DATA{u}, 1)
        disp(['u = ' num2str(u) '; st = ' num2str(st)])
        
        
        
        tic
        holder1 = nan(size(DATA{u}(st, :), 2));
        holder2 = nan(size(DATA{u}(st, :), 2));
        holder3 = nan(size(DATA{u}(st, :), 2));
        
        idx = ones(size(DATA{u}(st, :), 2));
        idx = tril(idx, -1);
        
        for t1 = 1:size(DATA{u}, 2)
            for t2 = 1:size(DATA{u}, 2)
                if idx(t1, t2) == 1
                    holder1(t1, t2) = euclidean_distance(DATA{u}{st, t1}, DATA{u}{st, t2}, start:0.001:del + dur);
                    holder2(t1, t2) = VPdistance(DATA{u}{st, t1}, DATA{u}{st, t2}, 10);
                    [holder3(t1, t2), ~] = Event_Sync(DATA{u}{st, t1}, DATA{u}{st, t2});
                end
            end
        end
        
        idx = find(idx == 1);
        
        euc_c{u}(st) = nanmean(holder1(idx));
        vp_c{u}(st) = nanmean(holder2(idx));
        es_c{u}(st) = nanmean(holder3(idx));
        rm_c{u}(st) = nanmean(cellfun(@length, DATA{u}(st,:)));
        
        toc
    end
end

clear u idx t1 t2 holder* dur

% % calculate distance between nearby neurons

dur = 0.2;
del = 0.05;
start = 0.1;

[sites, ~, idx] = unique(ID(:,1:2), 'rows');

euc_s = cell(size(sites, 1), 1);
vp_s = cell(size(sites, 1), 1);
es_s = cell(size(sites, 1), 1);
rm_s = cell(size(sites, 1), 1);

for u = 1:size(sites, 1)
    
    disp(num2str(u))
    
    tic
    
    s = find(idx == u);
    list = combnk(s, 2);
    
    euc_s{u} = cell(size(list, 1), 1);
    vp_s{u} = cell(size(list, 1), 1);
    es_s{u} = cell(size(list, 1), 1);
    rm_s{u} = cell(size(list, 1), 1);
    
    for p = 1:size(list, 1)
        
        euc_s{u}{p} = nan(size(DATA{list(p, 1)}, 1), 1);
        vp_s{u}{p} = nan(size(DATA{list(p, 1)}, 1), 1);
        es_s{u}{p} = nan(size(DATA{list(p, 1)}, 1), 1);
        rm_s{u}{p} = nan(size(DATA{list(p, 1)}, 1), 1);
        
        for st = 1:size(DATA{list(p, 1)}, 1)
            
            holder1 = nan(size(DATA{list(p, 1)}, 1), size(DATA{list(p, 2)}, 1));
            holder2 = nan(size(DATA{list(p, 1)}, 1), size(DATA{list(p, 2)}, 1));
            holder3 = nan(size(DATA{list(p, 1)}, 1), size(DATA{list(p, 2)}, 1));
            
            for t1 = 1:size(DATA{list(p, 1)}, 2)
                for t2 = 1:size(DATA{list(p, 2)}, 2)
                    holder1(t1, t2) = euclidean_distance(DATA{list(p, 1)}{st, t1}, DATA{list(p, 2)}{st, t2}, start:0.001:del + dur);
                    holder2(t1, t2) = VPdistance_reich(DATA{list(p, 1)}{st, t1}, DATA{list(p, 2)}{st, t2}, 10);
                    holder3(t1, t2) = Event_Sync(DATA{list(p, 1)}{st, t1}, DATA{list(p, 2)}{st, t2});
                end
            end
            
            euc_s{u}{p}(st) = nanmean(holder1(:));
            vp_s{u}{p}(st) = nanmean(holder2(:));
            es_s{u}{p}(st) = nanmean(holder3(:));
            rm_s{u}{p}(st) = nanmean([cellfun(@length, DATA{list(p, 1)}(st, :)), cellfun(@length, DATA{list(p, 2)}(st, :))]);
            
        end
        
    end
    
    toc
    
end

clear u sites idx s list p t1 t2 holder* st

% %

% ITD = DATA; clear DATA;
ILD = DATA; clear DATA;