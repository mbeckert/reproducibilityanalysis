load('Z:\Pena Lab\Data\Michael\Old Data\Lou''s Data\Bjorn\saccomparison.mat')

AN = vertcat(ansac(:).sacPeak);
IC = vertcat(icsac(:).sacPeak);
NL = vertcat(nlsac(:).sacPeak);

clear ansac icsac nlsac

load('Z:\Pena Lab\Data\Louisa\FinalBackup\VLVp\NA_LLDp_SACcomp\MaxILD\SACpeakdata2.mat')
LLDp_max = vertcat(data(:).sacpeak);
load('Z:\Pena Lab\Data\Louisa\FinalBackup\VLVp\NA_LLDp_SACcomp\MinILD\SACpeakdata2.mat')
LLDp_min = vertcat(data(:).sacpeak);

load('Z:\Pena Lab\Data\Michael\Old Data\Lou''s Data\NA\SACSTRFmatched\SACpeakdata.mat')
NA = vertcat(data(:).sacpeak);

clear data

load('f:\desktop\WorkingFolder\working_reprod\repro_joris_analyzed.mat', 'p')

ICls_di = p{2};
ICls_ff = p{3};
OT = p{4};

clear p

medians = [ nanmedian(AN); ...
    nanmedian(NA); ...
    nanmedian(NL); ...
    nanmedian(IC); ...
    nanmedian(LLDp_min); ...
    nanmedian(LLDp_max); ...
    nanmedian(ICls_di); ...
    nanmedian(ICls_ff); ...
    nanmedian(OT)];