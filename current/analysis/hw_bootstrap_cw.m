fol = 'K:\WorkingFolder\repro_bootstrap';
region = 'itd';
cd(fol)
files = ls;
files = files(3:end, :);
bins = unique(files(:, 12:16), 'rows');
iter = unique(files(:, 19:21), 'rows');

c = 1; it = 1;
load(['repro_' region '_c' bins(c, :) '_r' iter(it, :) '.mat'])

neu = unique(neuron);
hw = cell(length(neu), 1);
for n = 1:length(neu)
    hw{n} = nan(size(bins, 1), size(iter, 1));
end

clear c curve cw depvar dur files fr full_fr it n neuron reps
%%

for c = 1:size(bins, 1)
    tic
    for it = 1:size(iter, 1)

        disp(['repro_' region '_c' bins(c, :) '_r' iter(it, :) '.mat'])
        load([fol '\repro_' region '_c' bins(c, :) '_r' iter(it, :) '.mat'])
        
        center = round(size(curve, 2)/2);
        
        f = repmat(fr' .^2, 1, size(curve, 2));
        repro = (curve - f) * cw ./ fr';
        
        for n = 1:length(neu)
            ix = find(neuron == neu(n));
            ix = intersect(ix, find(fr == max(fr(ix)), 1, 'first'));
            floor = nanmean(repro(ix, repro(ix,:) < 0));
            half = (repro(ix, center) - floor) / 2;
            over = (repro(ix, :) - floor) > half;
            h = (find(over(center:end) == 0, 1, 'first') * 2 - 1) * cw;
            if ~isempty(h)
                hw{n}(c, it) = h;
            else
                hw{n}(c, it) = nan;
            end
        end
    end
    toc
end

clear c center curve cw depvar dur f floor fr full_fr h half it n neuron over repro reps neu ix fol
%%

data = hw{1};

m = repmat(nanmean(data, 2), 1, size(data, 2));

e = sqrt( (data - m) .^ 2 );
e = mat2cell(e, ones(size(e, 1), 1));

%%

boxplot2_points(1:length(e), e, 0.75);
