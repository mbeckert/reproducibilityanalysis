fol = 'f:\desktop\WorkingFolder\working_reprod\bootstrap_cw';
region = 'ff';
cd(fol)
files = ls;
files = files(3:end, :);
files = files(sum(files(:, 7:8) == region(1:2), 2) == 2, :);

if strcmp(region, 'ot') || strcmp(region, 'ff')
    bins = unique(files(:, 11:15), 'rows');
    iter = unique(files(:, 18:20), 'rows');
elseif strcmp(region, 'itd') || strcmp(region, 'ild')
    bins = unique(files(:, 12:16), 'rows');
    iter = unique(files(:, 19:21), 'rows');
end

c = 1; it = 1;
load(['repro_' region '_c' bins(c, :) '_r' iter(it, :) '.mat'])

neu = unique(neuron);
rp = cell(length(neu), 1);
for n = 1:length(neu)
    rp{n} = nan(size(bins, 1), size(iter, 1));
end

clear c curve cw depvar dur files fr full_fr it n neuron reps

cd('f:\desktop\WorkingFolder')

for c = 1:size(bins, 1)
    tic
    for it = 1:size(iter, 1)
        
        disp(['repro_' region '_c' bins(c, :) '_r' iter(it, :) '.mat'])
        load([fol '\repro_' region '_c' bins(c, :) '_r' iter(it, :) '.mat'])
        
        center = round(size(curve, 2)/2);
        
        f = repmat(fr' .^2, 1, size(curve, 2));
        repro = (curve - f) * cw ./ fr';
        if size(neu, 2) == size(neuron, 2) || strcmp(region, 'itd')
            for n = 1:length(neu)
                ix = find(neuron == neu(n));
                ix = intersect(ix, find(fr == max(fr(ix)), 1, 'first'));
                if ~isempty(ix)
                    rp{n}(c, it) = repro(ix, center);
                else
                    rp{n}(c, it) = nan;
                end

            end
        end
    end
    toc
end

clear c center curve cw depvar dur f floor fr full_fr h half it n neuron over repro reps neu ix fol
%% check the variance, root mean squared deviation, or fano factor of the peak

figure

for h = 1:3
    switch h
        case 1
            load('repro_bootstrap_peak_itd.mat')
        case 2
            load('repro_bootstrap_peak_ff.mat')
        case 3
            load('repro_bootstrap_peak_ot.mat')
    end
    
    subplot(1, 3, h)
    
    data = cell(size(rp));
    for i = 1:length(rp)
        d = rp{i};
        
        m = repmat(nanmean(d, 2), 1, size(d, 2));
        data{i} = nanmedian(sqrt( (d - m) .^ 2 ), 2);

%         data{i} = nanvar(d, [], 2) ./ nanmean(d, 2);

    end
    clear i d m
    
    data = cell2mat(cellfun(@transpose, data, 'UniformOutput', 0));
    data = mat2cell(data', ones(size(data, 2), 1));
    boxplot2_points(1:length(data), data, 0.75);
end

clear h data