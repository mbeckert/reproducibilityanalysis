% load('f:\desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted.mat','OT');
load('FreeField_OT_wDelay','OT')
data = OT; clear OT

distanceES = cell(200,1);
distanceEuc = cell(200,1);
distanceVP = cell(200,1);
Mean = cell(200,1);
anID = cell(200,1);
ID = cell(200, 1);

q = 10;
dur = 0.15;
del = 0.05;
start = 0.1;

count = 1;

for date = 1:length(data)
    for site = 1:length(data{date})
        for unit = 2:length(data{date}{site})
            
            d = data{date}{site}{unit};
            disp(['Date - ' num2str(date) '; Site - ' num2str(site) '; Unit - ' num2str(unit)]);
            
            tic
            
            filetypes = fieldnames(d);
            idx = regexpi(filetypes,'FFf.');
            filetypes = filetypes(~cellfun(@isempty,idx));
            
            distanceEuc{count}=cell([length(filetypes) 1]);
            distanceVP{count}=cell([length(filetypes) 1]);
            distanceES{count}=cell([length(filetypes) 1]);
            Mean{count}=cell([length(filetypes) 1]);
            anID{count} = data{date}{site}{1}(1:3);
            check = 0;
            
            for file=1:length(filetypes)
                TrialData = d.(filetypes{file}).spikes_times;
                
                for i = 1:length(TrialData(:))
                    TrialData{i}(TrialData{i} < start) = [];
                end
                
                distanceEuc{count}{file} = nan(1, size(TrialData,1));
                distanceVP{count}{file} = nan(1, size(TrialData,1));
                distanceES{count}{file} = nan(1, size(TrialData,1));
                Mean{count}{file} = nanmean(cellfun(@length, TrialData), 2)';
                ID{count} = [date, site, unit];
                
                best = find(Mean{count}{file} == max(Mean{count}{file}), 1, 'first');
                
                for sp = 1:size(TrialData, 1)
                    idx = ones(size(TrialData, 2));
                    idx = tril(idx, -1);
                    holder = nan(size(TrialData, 2));
                    holder2 = nan(size(TrialData, 2));
                    holder3 = nan(size(TrialData, 2));
                    for t1 = 1:size(TrialData, 2)
                        for t2 = 1:size(TrialData, 2)
                            if idx(t1, t2) == 1
                                holder(t1, t2) = euclidean_distance(TrialData{best, t1}, TrialData{sp, t2}, start:0.001:del + dur);
                                holder2(t1, t2) = VPdistance(TrialData{best, t1}, TrialData{sp, t2}, q);
                                [holder3(t1, t2), ~] = Event_Sync(TrialData{best, t1}, TrialData{sp, t2});
                            end
                        end
                    end
                    idx = find(idx == 1);
                    distanceEuc{count}{file}(sp) = nanmean(holder(idx));
                    distanceVP{count}{file}(sp) = nanmean(holder2(idx));
                    distanceES{count}{file}(sp) = nanmean(holder3(idx));
                end
                
                clear TrialData trialdata u t1 t2 holder holder2 holder3 idx Indexer
                check = check + sum(sum(Mean{count}{file}));
            end
            
            if check > 0
                count = count + 1;
            end
            
            toc
            
        end
    end
end

L = cellfun(@isempty,distanceES);
distanceEuc(L)=[];
distanceVP(L)=[];
distanceES(L)=[];
Mean(L)=[];
anID(L) = [];
ID(L) = [];

for i = 1:length(distanceES)
    distanceEuc{i} = cell2mat(distanceEuc{i});
    distanceVP{i} = cell2mat(distanceVP{i});
    distanceES{i} = cell2mat(distanceES{i});
    Mean{i} = cell2mat(Mean{i});
end

distanceEuc = cellfun(@(x) nanmean(x,1),distanceEuc,'UniformOutput',0);
distanceVP = cellfun(@(x) nanmean(x,1),distanceVP,'UniformOutput',0);
distanceES = cellfun(@(x) nanmean(x,1),distanceES,'UniformOutput',0);
Mean = cellfun(@(x) nanmean(x,1),Mean,'UniformOutput',0);

clearvars -except Index distanceES distanceEuc distanceVP Mean ID

save distance_ot_betweenspeakers

%% This plotting is still usable, but see "distance_plotting" for an updated version

load('f:\desktop\WorkingFolder\distance\distance_ot_betweenspeakers_del.mat')
load('f:\desktop\WorkingFolder\distance\distance_correction.mat', 'key')

E = cellfun(@norm01, distanceEuc, 'UniformOutput', 0);
V = cellfun(@norm01, distanceVP, 'UniformOutput', 0);
S = cellfun(@norm01, distanceES, 'UniformOutput', 0);
M = cellfun(@norm01, Mean, 'UniformOutput', 0);

figure(10)
subplot(1, 3, 1)
hold on
scatter(cell2mat(Mean'), cell2mat(distanceEuc'), 5, 'fill')
plot(key(:, 1), key(:, 3), 'r')
xlabel('mean spike count')
ylabel('euclidean distance')
axis([0 max(cell2mat(Mean')) + 1 0 max(cell2mat(distanceEuc')) + 1])
axis square

subplot(1, 3, 2)
hold on
scatter(cell2mat(Mean'), cell2mat(distanceVP'), 5, 'fill')
plot(key(:, 1), key(:, 2), 'r')
xlabel('mean spike count')
ylabel('vp cost')
axis([0 max(cell2mat(Mean')) + 1 0 max(cell2mat(distanceVP')) + 1])
axis square

subplot(1, 3, 3)
hold on
scatter(cell2mat(Mean'), cell2mat(distanceES'), 5, 'fill')
plot(key(:, 1), key(:, 4), 'r')
xlabel('mean spike count')
ylabel('quiroga event synchronization')
axis([0 max(cell2mat(Mean')) + 1 0 max(cell2mat(distanceES')) + 1])
axis square

figure(11)
subplot(1, 3, 1)
scatter(cell2mat(M'), cell2mat(E'), 5, 'fill')
xlabel('normalized mean spike count')
ylabel('normalized euclidean distance')
axis square

subplot(1, 3, 2)
scatter(cell2mat(M'), cell2mat(V'), 5, 'fill')
xlabel('normalized mean spike count')
ylabel('normalized vp cost')
axis square

subplot(1, 3, 3)
scatter(cell2mat(M'), cell2mat(S'), 5, 'fill')
xlabel('normalized mean spike count')
ylabel('normalized quiroga event synchronization')
axis square



