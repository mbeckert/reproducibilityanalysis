% load('f:\desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted.mat','OT');
cd('f:\desktop\WorkingFolder\distance\data')
load('FreeField_OT_wDelay','OT')
data = OT; clear OT
cd('F:\Desktop\WorkingFolder')

dur = 0.20;
del = 0.05;
start = 0.1;

bins = start:0.001:del + dur;
pattern = 'frozen.|b.';                   % These are to remove
pattern2 = 'FFf.';                   % These are to keep

VP = []; EUC = []; ES = []; FR1 = []; FR2 = [];
count = 1;

for date = 1:length(data)
    for site = 1:length(data{date})
        list = combnk(2:length(data{date}{site}),2);
        for pair = 1:size(list,1)
            
            disp(['date: ' num2str(date) '; site: ' num2str(site) '; pair: ' num2str(pair)]);
            
            tic
            
            unit1 = data{date}{site}{list(pair,1)};
            unit2 = data{date}{site}{list(pair,2)};
            filetypes1 = fieldnames(unit1);
            filetypes2 = fieldnames(unit2);
            filetypes = intersect(filetypes1,filetypes2);
            idx = regexpi(filetypes,pattern);
            idx = cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            idx = regexpi(filetypes,pattern2);
            idx = ~cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            
            clear filetypes1 filetypes2 idx
            
            for file = 1:length(filetypes)
                
                spikes1 = unit1.(filetypes{file}).spikes_times;
                spikes2 = unit2.(filetypes{file}).spikes_times;
                
                
                for i = 1:length(spikes1(:))
                    spikes1{i}(spikes1{i} < start) = [];
                    spikes2{i}(spikes2{i} < start) = [];
                end
                
                %%%%%% This section will introduce random spikes for
                %%%%%% statistical comparison. Comment these out to
                %%%%%% calculation of real data
%                 sp = cell(size(spikes1));
%                 for R = 1:length(sp(:))
%                     sp{R} = sort((del + dur - start) .* rand(length(spikes1{R}), 1) + start);
%                 end
%                 spikes1 = sp;
%                 sp = cell(size(spikes2));
%                 for R = 1:length(sp(:))
%                     sp{R} = sort((del + dur - start) .* rand(length(spikes2{R}), 1) + start);
%                 end
%                 spikes2 = sp;
                %%%%%%
                %%%%%%
                
                
                fr1 = cellfun(@length, spikes1);
                fr2 = cellfun(@length, spikes2);
                
                vp = nan( size(spikes1) );
                euc = nan( size(spikes1) );
                es = nan( size(spikes1) );

                for s = 1:size(spikes1, 1)
                    for r = 1:size(spikes1, 2)
                        euc(s, r) = euclidean_distance(spikes1{s, r}, spikes2{s, r}, start:0.001:del + dur);
                        vp(s, r) = VPdistance_reich(spikes1{s, r}, spikes2{s, r}, 10);
                        es(s, r) = Event_Sync(spikes1{s, r}, spikes2{s, r});
                    end
                end
                        
                EUC.(filetypes{file}){count, 1} = euc;
                ES.(filetypes{file}){count, 1} = es;
                VP.(filetypes{file}){count, 1} = vp;
                
                FR1.(filetypes{file}){count, 1} = fr1;
                FR2.(filetypes{file}){count, 1} = fr2;
                
            end         % for "file"
            
            count = count + 1;
            clear filetypes
            toc
            
        end             % for "pair"
    end                 % for "site"
end                     % for "date"

clearvars -except VP FR1 FR2 EUC ES

% save distance_betweenneurons_mod
%% PLOTTING
% This section has been copied to "distance_plotting" use this script to
% easily plot all results

load('f:\desktop\WorkingFolder\distance\distance_betweenneurons_del.mat')
load('f:\desktop\WorkingFolder\distance\distance_correction.mat', 'key')

euc = cell(length(VP.FFf1), 1);
fr1 = cell(length(VP.FFf1), 1);
fr2 = cell(length(VP.FFf1), 1);
vp = cell(length(VP.FFf1), 1);
es = cell(length(VP.FFf1), 1);

for p = 1:length(VP.FFf1)
    euc{p} = [EUC.FFf1{p} , EUC.FFf2{p}];
    es{p} = [ES.FFf1{p} , ES.FFf2{p}];
    fr1{p} = [FR1.FFf1{p} , FR1.FFf2{p}];
    fr2{p} = [FR2.FFf1{p} , FR2.FFf2{p}];
    vp{p} = [VP.FFf1{p} , VP.FFf2{p}];
end

clear EUC ES FR1 FR2 VP p

euc = cellfun(@(x) mean(x, 2), euc, 'UniformOutput', 0);
es = cellfun(@(x) mean(x, 2), es, 'UniformOutput', 0);
fr1 = cellfun(@(x) mean(x, 2), fr1, 'UniformOutput', 0);
fr2 = cellfun(@(x) mean(x, 2), fr2, 'UniformOutput', 0);
vp = cellfun(@(x) mean(x, 2), vp, 'UniformOutput', 0);

gm = cellfun(@(x1, x2) sqrt( x1 .* x2 ), fr1, fr2, 'UniformOutput', 0);
rm = cellfun(@(x1, x2) ( x1 + x2 ) / 2, fr1, fr2, 'UniformOutput', 0);

clear fr1 fr2
%%

figure(13)
subplot(1, 3, 1)
scatter(cell2mat(rm), cell2mat(euc), 10, 'fill')
xlabel('mean spike count')
ylabel('euclidean distance')
hold on
plot(key(:, 1), key(:, 3), 'r')
axis square

subplot(1, 3, 2)
scatter(cell2mat(rm), cell2mat(vp), 10, 'fill')
xlabel('mean spike count')
ylabel('vp cost')
hold on
plot(key(:, 1), key(:, 2), 'r')
axis square

subplot(1, 3, 3)
scatter(cell2mat(rm), cell2mat(es), 10, 'fill')
xlabel('mean spike count')
ylabel('quiroga event synchronization')
hold on
plot(key(:, 1), key(:, 4), 'r')
axis square

%%

a = cellfun(@(x) norm01(x, 2), es, 'UniformOutput', 0);
z = cellfun(@(x) norm01(x, 2), euc, 'UniformOutput', 0);
y = cellfun(@(x) norm01(x, 2), vp, 'UniformOutput', 0);
x = cellfun(@(x) norm01(x, 2), rm, 'UniformOutput', 0);

figure(14)
subplot(1, 3, 1)
scatter(cell2mat(x), cell2mat(z), 5, 'fill')
xlabel('mean spike count')
ylabel('euclidean distance')
[r,p] = corr(cell2mat(x), cell2mat(z), 'rows', 'pairwise');
title([num2str(r) ' ----- ' num2str(p)])
subplot(1, 3, 2)
scatter(cell2mat(x), cell2mat(y), 5, 'fill')
xlabel('mean spike count')
[r,p] = corr(cell2mat(x), cell2mat(y), 'rows', 'pairwise');
title([num2str(r) ' ----- ' num2str(p)])
ylabel('vp cost')
subplot(1, 3, 3)
scatter(cell2mat(x), cell2mat(a), 5, 'fill')
xlabel('mean spike count')
ylabel('quiroga event synchronization')
[r,p] = corr(cell2mat(x), cell2mat(a), 'rows', 'pairwise');
title([num2str(r) ' ----- ' num2str(p)])

clear a z y x r p