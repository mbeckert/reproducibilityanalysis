fol = 'f:\desktop\WorkingFolder\working_reprod\frompython\';
region = 'ot';
bins = {'50micro', '100micro', '1ms', '2ms', '3ms', '5ms', '10ms'};

time = cell(size(bins));
actual = cell(size(bins));

rep = 500;

for b = 1:length(bins)
    tic
    disp([fol 'repro_' region '_curve_' bins{b} '.mat'])
    load([fol 'repro_' region '_curve_' bins{b} '.mat'])
    
    neu = unique(neuron);
    cur = nan(length(neu), size(curve, 2));
    
    for n = 1:length(neu)
        ix = neuron == neu(n);
        [~, m] = max(fr(ix));
        cur(n, :) = (curve(m, :) - fr(m) ^ 2) * cw / fr(m);
    end
    
    clear curve depvar fr full_fr ix m n neu neuron
    
    center = round(size(cur, 2)/2);
    
    s = nanmean(cur);
    floor = nanmean(s(s < 0));
    half = (s(center) - floor) / 2;
    over = (s - floor) > half;
    
    actual{b} = (find(over(center:end) == 0, 1, 'first') * 2 - 1) * cw;

    time{b} = nan(rep, 1);
    
    for r = 1:rep
        disp(num2str(r))
        idx = randi(size(cur, 1), round(size(cur, 1) * 0.3), 1);
        s = nanmean(cur(idx, :));
        
        floor = nanmean(s(s < 0));
        half = (s(center) - floor) / 2;
        over = (s - floor) > half;
        
        if sum(over == 0) > 0
            time{b}(r) = (find(over(center:end) == 0, 1, 'first') * 2 - 1) * cw;
        end
    end
    toc
end

clear b neu cur ix m center idx s floor half over cw dur fol r reps

rmse = cellfun(@(x, y) sqrt(nanmean((x - y) .^ 2)), time, actual);
vari = cellfun(@(x) var(x, 'omitnan'), time);

boxplot2_points(1:length(time), time, 0.75)
subplot(1, 2, 1)
plot(rmse)
subplot(1, 2, 2)
plot(vari)