figure
fol = 'f:\desktop\WorkingFolder\working_reprod\frompython\';
region = {'itd', 'ff', 'ot'};
name = '100micro';
bins = round(linspace(1, 140, 140));

time = cell(size(bins));
% actual = cell(size(bins));

rep = 500;

for rg = 1:length(region)
    
load([fol 'repro_' region{rg} '_curve_' name '.mat'])
neu = unique(neuron);
cur = nan(length(neu), size(curve, 2));

for n = 1:length(neu)
    ix = neuron == neu(n);
    [~, m] = max(fr(ix));
    cur(n, :) = (curve(m, :) - fr(m) ^ 2) * cw / fr(m);
end

clear curve depvar fr full_fr ix m n neu neuron

center = round(size(cur, 2)/2);
    
for b = 1:length(bins)
    tic
    disp(num2str(bins(b)))
    
%     s = smooth(nanmean(cur), bins(b));
% 
%     floor = nanmean(s(s < 0));
%     half = (s(center) - floor) / 2;
%     over = (s - floor) > half;
%     
%     actual{b} = (find(over(center:end) == 0, 1, 'first') * 2 - 1) * cw;

    time{b} = nan(rep, 1);
    
    for r = 1:rep
        idx = randi(size(cur, 1), round(size(cur, 1) * 0.5), 1);
        s = smooth(nanmean(cur(idx, :)), bins(b));
        
        floor = nanmean(s(s < 0));
        half = (s(center) - floor) / 2;
        over = (s - floor) > half;
        
        if sum(over == 0) > 0
            time{b}(r) = (find(over(center:end) == 0, 1, 'first') * 2 - 1) * cw;
        end
    end
    toc
end

clear b neu cur ix m center idx s floor half over cw dur r reps

% rmse = cellfun(@(x, y) sqrt(nanmean((x - y) .^ 2)), time, actual);
vari = cellfun(@(x) var(x, 'omitnan'), time);

subplot(2, 3, rg)
boxplot2_points(bins, time, 1)
ax = gca;
ax.FontSize = 12;
title(region{rg})
if rg == 1
    ylabel('Half-width (s)', 'fontsize', 16)
end
xlim([0 max(bins)])
ylim([0 0.06])
subplot(2, 3, length(region) + rg)
ax = gca;
ax.FontSize = 12;
plot(bins, vari)
xlabel('Smoothing Bin Number', 'fontsize', 16)
ylabel('Variance (half-width)', 'fontsize', 16)
xlim([0 max(bins)])

end