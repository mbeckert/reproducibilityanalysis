load('FreeField_OT_wDelay','OT')
data = OT; clear OT

% %

pattern = '';                   % These are to remove
pattern2 = 'frozen.';                   % These are to keep

ID = cell(200, 1);
DATA = cell(200, 1);

for d = 1:length(data)
    for s = 1:length(data{d})
        for u = 2:length(data{d}{s})
            
            disp(['date: ' num2str(d) '; site: ' num2str(s) '; unit: ' num2str(u)]);
            
            tic
            
            unit = data{d}{s}{u};
            
            filetypes = fieldnames(unit);
            idx = regexpi(filetypes,pattern);
            idx = cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            idx = regexpi(filetypes,pattern2);
            idx = ~cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            
            clear idx
            
            if ~isempty(filetypes)
                
                L = find(cellfun(@isempty, ID), 1, 'first');
                
                DATA{L} = cell(size(filetypes));
                ID{L} = [d, s, u];
                
                for f = 1:length(filetypes)
                    DATA{L}{f} = unit.(filetypes{f}).spikes_times';
                end
                
                %                 DATA{L} = vertcat(DATA{L}{:});
                
            end
            
        end
    end
end

L = find(cellfun(@isempty, ID), 1, 'first');
DATA(L:end) = [];
ID(L:end) = [];
ID = cell2mat(ID);

clear d s u unit filetypes L f pattern pattern2 data

% % calculate distance within cell

dur = 0.35;
del = 0.05;
start = 0.1;

euc_c = cell(size(ID, 1), 1);
vp_c = cell(size(ID, 1), 1);
es_c = cell(size(ID, 1), 1);
rm_c = cell(size(ID, 1), 1);

for u = 1:length(DATA)
    tic
    
    euc_c{u} = nan(length(DATA{u}), 1);
    vp_c{u} = nan(length(DATA{u}), 1);
    es_c{u} = nan(length(DATA{u}), 1);
    rm_c{u} = nan(length(DATA{u}), 1);
    
    for r = 1:length(DATA{u})
        
        for i = 1:length(DATA{u}{r}(:))
            DATA{u}{r}{i}(DATA{u}{r}{i} < start) = [];
        end
        
        disp(['unit = ' num2str(u) '; rep = ' num2str(r)])
        
        holder1 = nan(size(DATA{u}{r}, 1));
        holder2 = nan(size(DATA{u}{r}, 1));
        holder3 = nan(size(DATA{u}{r}, 1));
        
        idx = ones(size(DATA{u}{r}, 2));
        idx = tril(idx, -1);
        
        for t1 = 1:size(DATA{u}{r}, 2)
            for t2 = 1:size(DATA{u}{r}, 2)
                if idx(t1, t2) == 1
                    holder1(t1, t2) = euclidean_distance(DATA{u}{r}{t1}, DATA{u}{r}{t2}, start:0.001:del + dur);
                    holder2(t1, t2) = VPdistance(DATA{u}{r}{t1}, DATA{u}{r}{t2}, 10);
                    [holder3(t1, t2), ~] = Event_Sync(DATA{u}{r}{t1}, DATA{u}{r}{t2});
                end
            end
        end
        
        idx = find(idx == 1);
        
        euc_c{u}(r) = nanmean(holder1(idx));
        vp_c{u}(r) = nanmean(holder2(idx));
        es_c{u}(r) = nanmean(holder3(idx));
        rm_c{u}(r) = nanmean(cellfun(@length, DATA{u}{r}));
        
    end
    
    toc
    
end

clear u idx t1 t2 holder* dur r

% % calculate distance between nearby neurons

dur = 0.35;
del = 0.05;
start = 0.1;

[sites, ~, idx] = unique(ID(:,1:2), 'rows');

euc_s = cell(size(sites, 1), 1);
vp_s = cell(size(sites, 1), 1);
es_s = cell(size(sites, 1), 1);
rm_s = cell(size(sites, 1), 1);

for u = 1:size(sites, 1)
    tic
    
    s = find(idx == u);
    list = combnk(s, 2);
    
    euc_s{u} = cell(size(list, 1), 1);
    vp_s{u} = cell(size(list, 1), 1);
    es_s{u} = cell(size(list, 1), 1);
    rm_s{u} = cell(size(list, 1), 1);
    
    for p = 1:size(list, 1)
        
        euc_s{u}{p} = nan(length(DATA{list(p, 1)}), 1);
        vp_s{u}{p} = nan(length(DATA{list(p, 1)}), 1);
        es_s{u}{p} = nan(length(DATA{list(p, 1)}), 1);
        rm_s{u}{p} = nan(length(DATA{list(p, 1)}), 1);
        
        R = min([ length(DATA{list(p, 1)}), length(DATA{list(p, 2)})]);
        for r = 1:R
            
            disp(['site = ' num2str(u) '; pair = ' num2str(p) '; rep = ' num2str(r)])
            
            holder1 = nan(length(DATA{list(p, 1)}{r}), length(DATA{list(p, 2)}{r}));
            holder2 = nan(length(DATA{list(p, 1)}{r}), length(DATA{list(p, 2)}{r}));
            holder3 = nan(length(DATA{list(p, 1)}{r}), length(DATA{list(p, 2)}{r}));
            
            for t1 = 1:length(DATA{list(p, 1)}{r})
                for t2 = 1:length(DATA{list(p, 2)}{r})
                    holder1(t1, t2) = euclidean_distance(DATA{list(p, 1)}{r}{t1}, DATA{list(p, 2)}{r}{t2}, start:0.001:del + dur);
                    holder2(t1, t2) = VPdistance_reich(DATA{list(p, 1)}{r}{t1}, DATA{list(p, 2)}{r}{t2}, 10);
                    holder3(t1, t2) = Event_Sync(DATA{list(p, 1)}{r}{t1}, DATA{list(p, 2)}{r}{t2});
                end
            end
            
            euc_s{u}{p}(r) = nanmean(holder1(:));
            vp_s{u}{p}(r) = nanmean(holder2(:));
            es_s{u}{p}(r) = nanmean(holder3(:));
            rm_s{u}{p}(r) = nanmean([cellfun(@length, DATA{list(p, 1)}{r}), cellfun(@length, DATA{list(p, 2)}{r})]);
            
        end
    end
    
    toc
    
end

clear u sites idx s list p t1 t2 holder* r R

FROZ = DATA; clear DATA