fol = 'f:\desktop\WorkingFolder\working_reprod\frompython\';
region = 'ot';
bins = {'50micro', '100micro', '1ms', '2ms', '3ms', '5ms', '10ms'};
% bins = {'50micro'};

check = cell(size(bins));

for b = 1:length(bins)
    load([fol 'repro_' region '_curve_' bins{b} '.mat'])
%     x = (-0.1+cw:cw:0.1-cw)* 1000;
    
    neu = unique(neuron);
    cur = nan(length(neu), size(curve, 2));
    
    center = round(size(cur, 2)/2);
    
    for n = 1:length(neu)
        ix = neuron == neu(n);
        [~, m] = max(fr(ix));
        cur(n, :) = (curve(m, :) - fr(m) ^ 2) * cw / fr(m);
    end
    
    clear curve depvar fr full_fr ix m n neu neuron
    
    cur = nanmean(cur);
    
    flanks = round(([-0.05, -0.02, 0.02, 0.05] / cw)) + center;
    flanks = [flanks(1):flanks(2), flanks(3):flanks(4)];
    f_s = nanstd(cur(:, flanks), [], 2);
    f_m = nanmean(cur(:, flanks), 2);
    peak = cur(:, center);
    thresh = f_m + (2 * f_s);
    
    check{b} = peak > thresh;
    
end

clear b x neu cur n ix m center flanks f_s f_m peak thresh cw dur reps fol

figure('Name', region, 'NumberTitle', 'off')
pdata = nan(length(bins), 2);

for b = 1:length(bins)
    pdata(b, 1) = sum(check{b});
    pdata(b, 2) = sum(check{b} == 0);
end

bar(pdata, 'stacked')
xticklabels(bins)
ylabel('count')