fol = 'f:\desktop\WorkingFolder\working_reprod\bootstrap_cw';
region = 'ot';
cd(fol)
files = ls;
files = files(3:end, :);
files = files(sum(files(:, 7:8) == region(1:2), 2) == 2, :);

if strcmp(region, 'ot') || strcmp(region, 'ff')
    bins = unique(files(:, 11:15), 'rows');
    iter = unique(files(:, 18:20), 'rows');
elseif strcmp(region, 'itd') || strcmp(region, 'ild')
    bins = unique(files(:, 12:16), 'rows');
    iter = unique(files(:, 19:21), 'rows');
end

c = 1; it = 1;
load(['repro_' region '_c' bins(c, :) '_r' iter(it, :) '.mat'])

neu = unique(neuron);
cur = cell(length(neu), 1);
for n = 1:length(neu)
    cur{n} = cell(size(bins, 1), size(iter, 1));
end

clear c curve cw depvar dur files fr full_fr it n neuron reps

cd('f:\desktop\WorkingFolder')

for c = 1:size(bins, 1)
    tic
    for it = 1:size(iter, 1)
        
        disp(['repro_' region '_c' bins(c, :) '_r' iter(it, :) '.mat'])
        load([fol '\repro_' region '_c' bins(c, :) '_r' iter(it, :) '.mat'])
        
        f = repmat(fr' .^2, 1, size(curve, 2));
        repro = (curve - f) * cw ./ fr';
        
        if size(neu, 2) == size(neuron, 2) || strcmp(region, 'itd')
            for n = 1:length(neu)
                ix = find(neuron == neu(n));
                ix = intersect(ix, find(fr == max(fr(ix)), 1, 'first'));
                cur{n}{c, it} = repro(ix, :);
            end
        end
    end
    toc
end

clear c center curve cw depvar dur f floor fr full_fr h half it n neuron over repro reps neu ix fol

CUR = cell(size(cur));
cor = cell(size(cur));
for n = 1:size(CUR, 1)
    CUR{n} = cell(size(cur{n}, 1), 1);
    cor{n} = cell(size(cur{n}, 1), 1);
    for c = 1:size(cur{n}, 1)
        CUR{n}{c} = cell2mat(cur{n}(c, :)');
        idx = ones(size(CUR{n}{c}, 1));
        idx = logical(tril(idx, -1));
        co = corrcoef(CUR{n}{c}');
        cor{n}{c} = co(idx);
    end
end
cur = CUR;
clear CUR n c co idx

co = cell(size(cor{1}, 1), size(cor, 1));
for n = 1:size(co, 2)
    for c = 1:size(co, 1)
        co{c, n} = cor{n}{c};
    end
end

cor = cell(size(co, 1), 1);
for c = 1:size(cor, 1)
    cor{c} = cell2mat(co(c, :)');
end
clear n c co


%%
figure

c = cell(1, 3);
for h = 1:3
    switch h
        case 1
            load('repro_bootstrap_cor_itd.mat')
        case 2
            load('repro_bootstrap_cor_ff.mat')
        case 3
            load('repro_bootstrap_cor_ot.mat')
    end
    
    subplot(1, 3, h)
%     cor = cellfun(@Fisher_Z, cor, 'UniformOutput', 0);
    boxplot2_points(1:size(cor, 1), cor', 0.75)

end



