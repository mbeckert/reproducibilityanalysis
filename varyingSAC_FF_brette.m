%% These are for free field
cd('C:\Users\mbeck\Desktop\Data\ICls\Data\RawData\FF')

FILES = ls;
FILES = FILES(3:end,:);

Index = cell(1,length(FILES));
HIST = cell(1,length(FILES));
CURVE = cell(1,length(FILES));
Mean = cell(1,length(FILES));
SACcurve = cell(1,length(FILES));
SACnorm = cell(1,length(FILES));
HW = cell(1,length(FILES));

bins = 0:0.001:0.5;
bin = 0.001;
maxWidth = 100;

for file=1:size(FILES,1)
    
    load(FILES(file,:))
    disp(num2str(file))
    
    Index{file}=Indexer;
    
    TrialData = cellfun(@(x) x/1000, TrialData, 'UniformOutput', 0);
    m = cellfun(@length, TrialData, 'UniformOutput', 0);
    m = cell2mat(m);
    m = mean(m,2)/max(bins);
    psth = cellfun(@(x) hist(x,bins), TrialData, 'UniformOutput', 0);
    
    N = size(psth,2);
    
    HIST{file} = zeros(size(psth,1),length(bins));
    
    for h = 1:size(psth,1)
        for i = 1:N
            HIST{file}(h,:) = HIST{file}(h,:) + psth{h,i};
        end
    end
    
    HIST{file} = HIST{file}/N*1000;
    
    for sp = 1:size(psth,1)
        
    sac=zeros(1,maxWidth);
    PSTHnoself=(HIST{file}(sp,:)-1/(bin*N));
    PSTHnoself(PSTHnoself < 0) = 0;
    sac(1)=mean(HIST{file}(sp,:).*PSTHnoself)*N/(N-1);
    
    for i = 2:maxWidth
        sac(i)=mean(HIST{file}(sp,1:end-i+1).*HIST{file}(sp,i:end));
    end
    
    sac=[fliplr(sac(maxWidth:end)),sac];
    
    SACcurve{file}{sp} = sac/(bin/1000)/N;
    SACnorm{file}{sp} = (sac-m(sp).^2)*(bin/1000)/m(sp);

    CURVE{file}(sp) = sum(SACnorm{file}{sp});
    
    end
    
    Mean{file} = m';
    
end

clearvars -except CURVE Mean SACcurve SACnorm HIST Index HW 

cd('C:\Users\mbeck\Desktop\Data\Analysis\ReproducibilityPaper\working')

%% These are for the dichotic stimuli

cd('C:\Users\mbeck\Desktop\Data\ICls\Data\RawData\Dichotic\ILD')
% cd('C:\Users\mbeck\Desktop\Data\ICls\Data\RawData\Dichotic\ITD')

FILES = ls;
FILES = FILES(3:end,:);

Index = cell(1,length(FILES));
HIST = cell(1,length(FILES));
HistCorr = HIST;
CURVE = HIST;
Mean = HIST;
SACcurve = HIST;
SACnorm = HIST;

bins = 0:0.001:0.3;
bin = 0.1;
maxWidth = 30;

for file=1:size(FILES,1)
    
    load(FILES(file,:))
    disp(num2str(file))
    
    data=cell(size(curvedata.spike_times,1),size(curvedata.spike_times,2));
    for jj=1:size(curvedata.spike_times,2)
        if isfield(curvedata,'depvar')
            [Indexer,ind]=sortrows(curvedata.depvar(:,jj));
        end
        if isfield(curvedata,'depvars')
            [Indexer,ind]=sortrows(curvedata.depvars(:,jj));
        end
        data(:,jj)=curvedata.spike_times(ind,jj);
    end
    
    Index{file}=Indexer';
    
    TrialData = cellfun(@(x) x/1000, data, 'UniformOutput', 0);
    m = cellfun(@length, TrialData, 'UniformOutput', 0);
    m = cell2mat(m);
    m = mean(m,2)/max(bins);
    psth = cellfun(@(x) hist(x,bins), TrialData, 'UniformOutput', 0);
    
    N = size(psth,2);
    
    HIST{file} = zeros(size(psth,1),length(bins));
    
    for h = 1:size(psth,1)
        for i = 1:N
            HIST{file}(h,:) = HIST{file}(h,:) + psth{h,i};
        end
    end
    
    HIST{file} = HIST{file}/N*1000;
    
    for sp = 1:size(psth,1)
        
        sac=zeros(1,maxWidth);
        PSTHnoself=(HIST{file}(sp,:)-1/(bin*N));
        PSTHnoself(PSTHnoself < 0) = 0;
        sac(1)=mean(HIST{file}(sp,:).*PSTHnoself)*N/(N-1);
        
        for i = 2:maxWidth
            sac(i)=mean(HIST{file}(sp,1:end-i+1).*HIST{file}(sp,i:end));
        end
        
        sac=[fliplr(sac),sac];
        
        SACcurve{file}{sp} = sac/(bin/1000)/N;
        SACnorm{file}{sp} = (sac-m(sp).^2)*(bin/1000)/m(sp);
        
        CURVE{file}(sp) = sum(SACnorm{file}{sp});
        
    end
    
    Mean{file} = m';
    
end

clearvars -except CURVE Mean SACcurve SACnorm HIST Index

cd('C:\Users\mbeck\Desktop\Data\Analysis\ReproducibilityPaper\working')