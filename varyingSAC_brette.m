% addpath('C:\Users\mbeck\Desktop\Data\Analysis\ReproducibilityPaper\working')

for type = 1:3
    switch type
        case 1
            cd('f:\desktop\Data\ICls\Data\RawData\FF')
        case 2
            cd('f:\desktop\Data\ICls\Data\RawData\Dichotic\ILD')
        case 3
            cd('f:\desktop\Data\ICls\Data\RawData\Dichotic\ITD')
    end
    
    FILES = ls;
    FILES = FILES(3:end,:);
    
    Index = cell(1,length(FILES));
    HIST = cell(1,length(FILES));
    CURVE = cell(1,length(FILES));
    PEAK = cell(1,length(FILES));
    HW = cell(1,length(FILES));
    Mean = cell(1,length(FILES));
    SACcurve = cell(1,length(FILES));
    SACnorm = cell(1,length(FILES));
    SPtimes = cell(1,length(FILES));
    
    bins = 0:0.001:0.5;
    bin = 0.001;
    maxWidth = 200;
    
    for file=1:size(FILES,1)
        
        load(FILES(file,:))
        disp(num2str(file))
        
        if type >1
%             data=cell(size(curvedata.spike_times,1),size(curvedata.spike_times,2));
%             for jj=1:size(curvedata.spike_times,2)
%                 if isfield(curvedata,'depvar')
%                     [Indexer,ind]=sortrows(curvedata.depvar(:,jj));
%                 end
%                 if isfield(curvedata,'depvars')
%                     [Indexer,ind]=sortrows(curvedata.depvars(:,jj));
%                 end
%                 data(:,jj)=curvedata.spike_times(ind,jj);
%             end
%             Index{file}=Indexer';
%             TrialData = data;
            TrialData = curvedata.spike_times;
        else
            Index{file}=Indexer;
        end
        SPtimes{file} = TrialData;
        [HIST{file},CURVE{file},Mean{file},SACcurve{file},SACnorm{file},HW{file},PEAK{file}] = reproducibility(TrialData,bins,bin,maxWidth);
    end
    
%     cd('f:\desktop\Data\Analysis\ReproducibilityPaper\working')
    cd('f:\desktop\WorkingFolder')
    
    switch type
        case 1
            save iclsFF_brette_9 CURVE Mean SACcurve SACnorm HIST Index HW PEAK SPtimes
        case 2
            save iclsILD_brette_9 CURVE Mean SACcurve SACnorm HIST Index HW PEAK SPtimes
        case 3
            save iclsITD_brette_9 CURVE Mean SACcurve SACnorm HIST Index HW PEAK SPtimes
    end
    
end
