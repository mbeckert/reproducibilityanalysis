%% plot CCGs for each neuron

load('ot_synch')
filetypes = fieldnames(ot_synch);
x = 1:199;

for file = 1

for c = 1:size(ot_synch.(filetypes{file}){11},1)
    figure(1)
    len = find(cellfun(@(x) ~isempty(x),ot_synch.(filetypes{file}){13}(c,:)),1,'last');
    subplot(2,len,[1 len])
    title(num2str(c))
    gm = sqrt(ot_synch.(filetypes{file}){3}(c,1:len) .* ot_synch.(filetypes{file}){4}(c,1:len));
    plot(1:len,gm)
    for d = 1:len
        subplot(2,len,len+d)
        plot(x,ot_synch.(filetypes{file}){12}{c,d},'--r','LineWidth',0.5);
        hold on
        plot(x,ot_synch.(filetypes{file}){11}{c,d},'-g','LineWidth',0.5);
        plot(x,ot_synch.(filetypes{file}){13}{c,d},'k','LineWidth',2);
        hold off
        title(num2str(ot_synch.(filetypes{file}){10}(c,d)))
        axis square
        axis tight
    end
    pause
end

end

%% compile synchrony data into a form that is easily pasted into GraphPad

d = Synchrony_scatter;

t = 2;
m = 2;

p = cell(1,2);

p{1} = d{2*t-1}(:,2*m-1:2*m);
p{2} = d{2*t}(:,2*m-1:2*m);

for i = 1:2
    idx1 = isnan(p{i}(:,1));
    idx2 = isnan(p{i}(:,2));
    idx = idx1 .* idx2;
    idx = logical(idx);
    p{i}(idx,:) = [];
end

out = nan(sum(cellfun(@length,p)),3);
out(:,1) = [p{1}(:,1);p{2}(:,1)];
out(:,2) = [p{1}(:,2);nan(length(p{2}),1)];
out(:,3) = [nan(length(p{1}),1);p{2}(:,2)];

clear d t m i idx* p