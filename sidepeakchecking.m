load('f:\desktop\Data\Analysis\ReproducibilityPaper\working\iclsILD_brette_5.mat','CURVE','Index','Mean');

idx = zeros(size(Mean));

for n = 1:length(CURVE)
    plot(Index{n},Mean{n});
    i = input('side? - ');
    if isempty(i)
        idx(n) = 0;
    else
        idx(n) = i;
    end
end

clear n i

%%

side = nan(2,size(Mean,2));
main = cell(2,size(Mean,2));

for n = 1:length(idx)
    m = normalization(Mean{n},0);
    r = normalization(CURVE{n},0);
    if idx(n) > 0
        i = idx(n);
        side(1,n) = m(i);
        side(2,n) = r(i);
        i = 1:length(Mean{n});
        i(idx(n)) = [];
        main{1,n} = m(i);
        main{2,n} = r(i);
    else
        main{1,n} = m;
        main{2,n} = r;
    end
end

side = side';
main = cell2mat(main)';

clear i n m r

%%

for n = 1:length(CURVE)
    m = normalization(Mean{n},1);
    r = normalization(CURVE{n},1);
    plot(Index{n},m);
    hold on
    plot(Index{n},r);
    hold off
    title(num2str(n))
    pause
end