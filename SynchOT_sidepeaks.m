cd('C:\Users\mbeck\Desktop\Data\Analysis\ReproducibilityPaper\working')
load SynchOT_checksidepeaks

data = Azimuth;

bounds = nan(size(data{1},1),2);
%%

u = find(isnan(bounds(:,1)),1,'first');

while u <= size(data,1)
    plot(data{u}(1,:))
    grid on
    grid minor
    title([num2str(d) ' ' num2str(u)])
    tmp = input('bounds = ');
    if isempty(tmp)
        u = u - 1;
    else
        bounds(u,:) = tmp;
        u = u + 1;
    end
end

%%

in = cell(size(data));
out = cell(size(data));

for i = 1:size(data,1)
    idx = bounds(i,1):bounds(i,2);
    in{i} = data{i}(:,idx);
    check = 1:length(data{i});
    check = setdiff(check,idx);
    out{i} = data{i}(:,check);
end

in = cell2mat(in')';
out = cell2mat(out')';