clear all; close all;

combinedanalysis
CORR_FULL = corr_full;
CORR_CELL = corr_cell;
SCATTER = cellfun(@transpose,m_output,'UniformOutput',0);
clearvars -except CORR_FULL CORR_CELL SCATTER

mutualinformation_repsync_combineddim
MuIn = MI;
clearvars -except CORR_FULL CORR_CELL SCATTER MuIn

otsynch_ff_shiftVccg_binned
CORR_FULL = [CORR_FULL; corr_full'];
CORR_CELL{4} = corr_cell(:,1);
CORR_CELL{5} = corr_cell(:,2);
CORR_CELL{6} = corr_cell(:,3);
SCATTER{4} = [output{1}(:),output{2}(:)];

SCATTER{5} = [output{1}(:),output{3}(:)];
SCATTER{6} = [output{1}(:),output{4}(:)];
clearvars -except CORR_FULL CORR_CELL SCATTER MuIn %rat

mutualinformation_repsync_otsynch_shiftVccgBinned
MuIn = [MuIn; MI'];
clearvars -except CORR_FULL CORR_CELL SCATTER MuIn %rat

close all

%% plotting
rangex = [0,1,11];
rangey = [0,1,11];
cmap = flipud(gray(64));

for m = 1:length(SCATTER)
    figure(m)
    data = SCATTER{m};
    [mat,~,~] = hist2D(data,rangex,rangey);
    imagesc(mat)
    colormap(cmap)
    colorbar
    axis square
end

clear rangex rangey cmap data mat m