% get synchrony values from array recordings

cd('f:\desktop\WorkingFolder')
load('arraydata_spikes', 'pairdata')

synch = pairdata.syn_frozen;

index = nan(length(synch.gm),2);

for i = 1:length(index)
    if ~isempty(synch.gm{i,2})
        [~,index(i,1)] = max(max(synch.gm{i,2},[],2));
        [~,index(i,2)] = max(max(synch.gm{i,2},[],1));
    end
end

syn = cell(3,1);

for i = 1:length(index)
    if ~isempty(synch.gm{i,2})
        syn{1}(i,1) = synch.gm{i,1}(index(i,1),index(i,2));
        syn{1}(i,2) = synch.gm{i,2}(index(i,1),index(i,2));
        syn{2}(i,1) = synch.synch_ccg{i,1}(index(i,1),index(i,2));
        syn{2}(i,2) = synch.synch_ccg{i,2}(index(i,1),index(i,2));
        syn{3}(i,1) = synch.synch_sh{i,1}(index(i,1),index(i,2));
        syn{3}(i,2) = synch.synch_sh{i,2}(index(i,1),index(i,2));
        syn{4}(i,1) = synch.synch_cor{i,1}(index(i,1),index(i,2));
        syn{4}(i,2) = synch.synch_cor{i,2}(index(i,1),index(i,2));
    end
end

distant = cell(2,1);
nearby = cell(2,1);

for i = 1:2
    
    distant{i}{1,1} = syn{1}(pairdata.diffitd >= 100, i);
    distant{i}{1,2} = syn{2}(pairdata.diffitd >= 100, i);
    distant{i}{1,3} = syn{3}(pairdata.diffitd >= 100, i);
    distant{i}{1,4} = syn{4}(pairdata.diffitd >= 100, i);
    
    nearby{i}{1,1} = syn{1}(pairdata.diffitd < 60, i);
    nearby{i}{1,2} = syn{2}(pairdata.diffitd < 60, i);
    nearby{i}{1,3} = syn{3}(pairdata.diffitd < 60, i);
    nearby{i}{1,4} = syn{4}(pairdata.diffitd < 60, i);
    
    distant{i} = cell2mat(distant{i});
    nearby{i} = cell2mat(nearby{i});
    
end

clear i syn synch index pairdata

%% get synchrony values for tetrode recordings

tetro = cell(2,1);
files = {'f:\desktop\WorkingFolder\synchony\ot_synch_ff_base.mat'; ...
    'f:\desktop\WorkingFolder\synchony\ot_synch_ff.mat'};


for f = 1:2
    
    load(files{f}, 'ccg', 'sh', 'cor', 'GM')
    
    [~, index] = max(GM, [], 2);
    
    syn = cell(3,1);
    
    for i = 1:length(index)
        syn{1}(i,1) = GM(i,index(i));
        syn{2}(i,1) = ccg(i,index(i));
        syn{3}(i,1) = sh(i,index(i));
        syn{4}(i,1) = cor(i,index(i));
    end
    
    tetro{f}{1,1} = syn{1};
    tetro{f}{1,2} = syn{2};
    tetro{f}{1,3} = syn{3};
    tetro{f}{1,4} = syn{4};
    
    tetro{f} = cell2mat(tetro{f});
    
    clear i syn index ccg sh cor GM
    
end

clear files f

%% find maximum values across all data groups for consistent axis values in plotting

data = {tetro, nearby, distant};

mx = zeros(1,3);
my = zeros(1,3);

for d = 1:3
for b = 1:2
    tmp = nanmax(data{d}{b});
    if tmp(1) > mx(d)
        mx(d) = tmp(1);
    end
    if nanmax(tmp(2:end)) > my(d)
        my(d) = nanmax(tmp(2:end));
    end
end
end

mx = ceil(mx/10) * 10;
my = ceil(my*100) / 100;

clear data d b tmp

%% plot data

data = {tetro, nearby, distant};
titles = {'ccg','sh','cor'};
ftitles = {'tetrode', 'array - nearby', 'array - distant'};


for d = 1:3
    figure('Name',ftitles{d},'NumberTitle','off')
    for b = 1:2
        for t = 1:3
            subplot(2, 3, t + (3 * b) - 3)
            scatter(data{d}{b}(:,1), data{d}{b}(:,t+1), 5, 'fill')
            if b == 1 
                title(titles{t}) 
            end
            if b == 2
                xlabel('geometric firing rate')
            end
            if t == 1
                ylabel('synchrony')
            end
%             axis([0, mx(d), 0, my(d)])
            axis square
        end
    end
end

clear data titles ftitles d t b

%% bar plots

dis = {distant{1}(:,2), distant{2}(:,2), ...
        distant{1}(:,3), distant{2}(:,3), ...
        distant{1}(:,4), distant{2}(:,4)};
    
ner = {nearby{1}(:,2), nearby{2}(:,2), ...
        nearby{1}(:,3), nearby{2}(:,3), ...
        nearby{1}(:,4), nearby{2}(:,4)};
    
tet = {tetro{1}(:,2), tetro{2}(:,2), ...
        tetro{1}(:,3), tetro{2}(:,3), ...
        tetro{1}(:,4), tetro{2}(:,4)};
    
figure

subplot(1,3,1)
boxplot2_points(1:6, tet)
xticks(1:6)
xticklabels({'b ccg', 'e ccg', 'b sh', 'e sh', 'b cor', 'e cor'})
xlim([0 7])
title('tetrode neurons')

subplot(1,3,2)
boxplot2_points(1:6, ner)
xticks(1:6)
xticklabels({'b ccg', 'e ccg', 'b sh', 'e sh', 'b cor', 'e cor'})
xlim([0 7])
title('nearby neurons ( < 60�s itd)')

subplot(1,3,3)
boxplot2_points(1:6, dis)
xticks(1:6)
xticklabels({'b ccg', 'e ccg', 'b sh', 'e sh', 'b cor', 'e cor'})
xlim([0 7])
title('distant neurons ( > 100�s itd)')

figure
boxplot2_points(1:6, {tet{2}, ner{2}, dis{2}, tet{4}, ner{4}, dis{4}})
xticks(1:6)
xticklabels({'ccg tetrode', 'ccg nearby', 'ccg distant', 'sh tetrode', 'sh nearby', 'sh distant'})
xlim([0 7])
ylabel('synchrony')

