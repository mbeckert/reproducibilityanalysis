%% Load data

cd('f:\desktop\WorkingFolder')
load('f:\desktop\WorkingFolder\working_array\arraydata_spikes')

%% Run through once to decide if a unit looks like it is patterned or not for a key
% Run through now or load the saved key if available (if you run it save
% the key for ease of use later)

r = nan(size(unitdata));

for u = 1:length(unitdata)
    if length(unitdata{u,1}.frozentimes) > 2
        
        y = cell(unitdata{u,1}.frozentimes);
        
        for rp = 1:size(y,1)
            for t = 1:size(y,2)
                y{rp,t} = ones(size(y{rp,t})) * rp;
            end
        end
        
        for t = 1:size(y,2)
            
            subplot(1,size(y,2),t)
            title(num2str(u))
            xx = cell2mat(unitdata{u,1}.frozentimes(:,t));
            yy = cell2mat(y(:,t));
            scatter(xx, yy, 5, 'fill')
            axis square
            
        end
        
        I = input('reproducible? (1,[0])');
        if isempty(I)
            r(u) = 0;
        else
            r(u) = I;
        end
    else
        r(u) = 0;
    end
    
end

clear u y rp t xx yy I

r = logical(r);

%% Plot relationships and correlations
% Raw scatter, scatterheatmap (for normalized data), polynomial plots of
% individual relationships, and boxplot2_points for correlation values

% if key for reproducible units is available load it here.
load('f:\desktop\WorkingFolder\arraykeys\reproducible_key.mat')

colmap = distinguishable_colors(size(unitdata,1));

rep = cell(size(unitdata));
fr = cell(size(unitdata));
co = nan(size(unitdata));

for u = 1:length(unitdata)
    rep{u} = unitdata{u}.repro(:);
    fr{u} = unitdata{u}.r_fr(:);
    co(u) = corr(fr{u}, rep{u}, 'rows', 'pairwise');
end

for type = 1:2
    if type == 1
        R = rep(r);
        F = fr(r);
        C = co(r);
    else
        R = rep(~r);
        F = fr(~r);
        C = co(~r);
    end

subplot(2,4,3 + (type * 4 - 4))
hold on
for u = 1:length(R)
    
    a = R{u};
    b = F{u};
    
    if ~isempty(a)
        
        idx = isnan(a);
        a(idx) = []; b(idx) = [];
        idx = isnan(b);
        a(idx) = []; b(idx) = [];
        p = polyfit(a, b, 1);
        v = polyval(p,a);
        scatter(a, v, 5, colmap(u,:), 'fill')
        
    end
    
end
xlabel('fr')
ylabel('reproducibility')
axis square

rep_n = cell2mat(cellfun(@(x) norm01(x,2), R, 'UniformOutput', 0));
fr_n = cell2mat(cellfun(@(x) norm01(x,2), F, 'UniformOutput', 0));

R = cell2mat(R);
F = cell2mat(F);

subplot(2,4,1 + (type * 4 - 4))
scatter(R, F, 5, 'k', 'fill')
xlabel('fr')
ylabel('reproducibility')
axis square

subplot(2,4,2 + (type * 4 - 4))
scatterheatmapplot(fr_n, rep_n, {'normalized fr', 'normalized reproducibility'})
axis square

subplot(2,4,4 + (type * 4 - 4))
boxplot2_points(1, {C})
xticks(1)
xticklabels('ot')
xlim([0 2])
ylabel('correlation coefficient')
axis square

end

clear a b u idx p v

