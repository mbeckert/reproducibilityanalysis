%% Get all of the HW to determine a common value for the integral.
cd('f:\desktop\WorkingFolder\reprod_working')

hw = cell(1,6);
cur = cell(1,6);
fr = cell(1,6);

for t = 4:6
    switch t
        case 1
            load('iclsITD_brette_9.mat', 'HW', 'CURVE', 'Mean')
        case 2
            load('iclsILD_brette_9.mat', 'HW', 'CURVE', 'Mean')
        case 3
            load('iclsAZ_brette_9.mat', 'HW', 'CURVE', 'Mean')
        case 4
            load('iclsEL_brette_9.mat', 'HW', 'CURVE', 'Mean')
        case 5
            load('otAZ_brette_9.mat', 'HW', 'CURVE', 'Mean')
        case 6
            load('otEL_brette_9.mat', 'HW', 'CURVE', 'Mean')
    end
    
    %     hw{t} = cell2mat(HW);
    %     cur{t} = cell2mat(CURVE);
    %     fr{t} = cell2mat(Mean);
    
    hw{t} = cell2mat(cellfun(@max, HW, 'UniformOutput', 0));
    cur{t} = cell2mat(cellfun(@max, CURVE, 'UniformOutput', 0));
    fr{t} = cell2mat(cellfun(@max, Mean, 'UniformOutput', 0));
    
end

clear t HW CURVE Mean

hw = cell2mat(hw)';
cur = cell2mat(cur)';
fr = cell2mat(fr)';

subplot(1,3,1)
boxplot2_points(1,{hw})
xlim([0 2])
ylabel('half-width')
subplot(1,3,2)
boxplot2_points(1,{cur})
xlim([0 2])
ylabel('reproducibility')
subplot(1,3,3)
boxplot2_points(1,{fr})
xlim([0 2])
ylabel('firing rate')

%% Simple correlation plot

cd('f:\desktop\WorkingFolder\reprod_working')

out = nan(6,1);
output = cell(6,1);
indiv = cell(6,1);

normal = 1; % normalize?
rangex = [0,1,11];
rangey = [0,1,11];

figure(1)

for t = 1:6
    switch t
        case 1
            load('iclsITD_brette_9.mat', 'Mean', 'CURVE', 'PEAK')
        case 2
            load('iclsILD_brette_9.mat', 'Mean', 'CURVE', 'PEAK')
        case 3
            load('iclsAZ_brette_9.mat', 'Mean', 'CURVE', 'PEAK')
        case 4
            load('iclsEL_brette_9.mat', 'Mean', 'CURVE', 'PEAK')
        case 5
            load('otAZ_brette_9.mat', 'Mean', 'CURVE', 'PEAK')
        case 6
            load('otEL_brette_9.mat', 'Mean', 'CURVE', 'PEAK')
    end
    
    data = CURVE; % select which metric to plot here
    output{t} = nan(size(CURVE));
    
    x = cellfun(@(x) normalization(x,normal), Mean, 'UniformOutput', 0);
    y = cellfun(@(x) normalization(x,normal), data, 'UniformOutput', 0);
    
    indiv{t} = nan(size(x));
    for u = 1:length(x)
        indiv{t}(u) = corr(x{u}',y{u}','rows','pairwise');
    end
    
    x = cell2mat(x);
    y = cell2mat(y);
    
    subplot(3,2,t)
    
    [out(t), pp] = scatterheatmapplot(x',y', {'FR','rep'});
    
    for ii = 1:length(data)
        output{t}(ii) = corr(data{ii}',Mean{ii}','rows','pairwise');
    end
    
    clear x y ii Mean CURVE PEAK  data mat
    
end

clear t normal range* cmap

figure(2)

boxplot2_points(1:6,output)
output = cellfun(@(x) 0.5 .* log((1 + x) ./ (1 - x)), output, 'UniformOutput', 0);

m = max(cellfun(@length,output));
for i = 1:length(output)
    output{i} = [output{i}'; nan(m - length(output{i}),1)];
end
output = cell2mat(output');

figure(3)
for u = 1:length(indiv)
    subplot(1,length(indiv),u)
    boxplot2_points(1,indiv(u))
    axis([0,2,-1,1])
    xticks([])
    ylabel('correlation coefficient (r)')
end

clear m i

%% plot rasters, PSTH, and SAC for example cells

cd('f:\desktop\WorkingFolder\reprod_working')

normal = 1; % normalize?
rangex = [0,1,11];
rangey = [0,1,11];

for t = 1:6
    width = 20;
    switch t
        case 1
            load('iclsITD_brette_9.mat', 'SACnorm', 'SACcurve', 'SPtimes', 'Mean', 'CURVE')
            reps = 100;
            duration = 250;
            unit = 19;
            dv = 1:4;
        case 2
            load('iclsILD_brette_9.mat', 'SACnorm', 'SACcurve', 'SPtimes', 'Mean', 'CURVE')
            reps = 100;
            duration = 250;
            unit = 19;
            dv = 1:4;
        case 3
            load('iclsAZ_brette_9.mat', 'SACnorm', 'SACcurve', 'SPtimes', 'Mean', 'CURVE')
            reps = 500;
            duration = 450;
            unit = 37;
            dv = [2,3,5,7];
        case 4
            load('iclsEL_brette_9.mat', 'SACnorm', 'SACcurve', 'SPtimes', 'Mean', 'CURVE')
            reps = 500;
            duration = 450;
            unit = 37;
            dv = 1:4;
        case 5
            load('otAZ_brette_9.mat', 'SACnorm', 'SACcurve', 'SPtimes', 'Mean', 'CURVE')
            reps = 20;
            duration = 100;
            unit = 59;
            dv = [1,4,6,7];
        case 6
            load('otEL_brette_9.mat', 'SACnorm', 'SACcurve', 'SPtimes', 'Mean', 'CURVE')
            reps = 20;
            duration = 100;
            unit = 59;
            dv = [2,7,8,9];
    end
    
    for n = unit %1:length(SPtimes) %unit
        figure('name', [num2str(t) ' --- ' num2str(n)], ...
            'units','normalized','outerposition',[0 0 1 1])
        title([num2str(t) ' --- ' num2str(n)])
%         dv = 1:size(SPtimes{n},1);
        colors = distinguishable_colors(length(dv));
        sp = SPtimes{n};
        
        m = ceil(max(cellfun(@max, SACnorm{n})) * 100) / 100;
        mm = 0;
        
        for r = 1:length(dv)
            xx = cell2mat(sp(dv(r),:));
            xx = xx - 50;
            tmp = max(histcounts(xx,0:2:duration));
            if tmp > mm
                mm = tmp;
            end
        end
        for r = 1:length(dv)
            
            cur = SACnorm{n}{dv(r)};
            
            subplot(3,length(dv),r)
            yy = cell(1,size(sp,2));
            for y = 1:length(yy)
                yy{y} = ones(1,length(sp{dv(r),y})) * y;
            end
            yy = cell2mat(yy);
            xx = cell2mat(sp(dv(r),:));
            xx = xx - 50;
            scatter(xx,yy,5,colors(r,:),'fill');
            title(['fr = ' num2str(Mean{n}(dv(r))) ' --- rep = ' num2str(CURVE{n}(dv(r)))])
            xlabel('Time (ms)')
            ylabel('Trials')
            axis([0,duration,0,reps])
            axis square
            
            subplot(3,length(dv),r+length(dv))
            histogram(xx,0:2:duration,'FaceColor', colors(r,:), 'EdgeAlpha', 0)
            axis square
            axis([0 duration 0 mm])
            xlabel('Time (ms)')
            ylabel('Counts')
            
            subplot(3,length(dv),r+(length(dv)*2))
%             plot(-floor(length(cur)/2):floor(length(cur)/2),cur, 'Color', colors(r,:))
            plot(-width:width,cur(round(length(cur)/2)-width:round(length(cur)/2)+width), 'Color', colors(r,:))            
            xlabel('Lag (ms)')
            ylabel('Coincidences/spikes')
            axis square
            ylim([0 m])
        end
        
        pause
        close all
    end
    
end
