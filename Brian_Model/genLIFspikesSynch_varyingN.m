function [Vm,fr,sp]=genLIFspikesSynch_varyingN(g, dt, gL, EL, Esyn)

%% Number of neurons

[N,Lt]=size(g);


%% Time

tt=0:dt:(Lt-1)*dt;


%% Neuron parameters

Vthres=-58.3;

Tref=1; %ms
MaxFR=1/Tref; %Maximum firing rate

Vrest=-65;
% EL=-60;
% gL=1;
% Esyn=0;


tRC=2;


dVthres=.4;
%% Initialize

inref=zeros(N,1);
spikecnts=ones(N,1);
spiketimes = zeros(N,floor(dt*Lt*MaxFR)+2); % max possible # spikes.

V=Vrest+rand(N,1);

%Record potential for plotting
Vm=ones(N,Lt)*Vrest;


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%integrate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for n=2:Lt
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %dX=zeros(Dout,1);
    itest = find(inref<=0);
    if (length(itest)) %% For those neurons out of the refractory period
 
		
 
        dV=zeros(N,1);
        dV(itest)=dt/tRC*(gL*(EL-V(itest))+g(itest,n-1).*(Esyn-V(itest)));
        V(itest)=V(itest)+dV(itest);
        
        %Record potential for plotting
        Vm(itest,n)=V(itest);
        
        i2 = find(V(itest)>Vthres);
        %i2 = find(dV(itest)>dVthres);
        if(length(i2))
            for l=1:length(i2)
                spikeIndex= itest(i2(l));
                cnt = spikecnts(spikeIndex);
                deltaT = dt*(V(spikeIndex)-Vthres)./dV(spikeIndex); % Interpolated spike time
                if deltaT>dt
                    n
                    deltaT
                    fprintf('Error: (deltaT>dt) in genLIFspikes\n');
                    fprintf('Check to see if the signal is too large for the neuronal encoding range,\n');
                    fprintf('otherwise try to reduce step size dt.\n');
                    error('.');
                end
                spiketimes(spikeIndex,cnt)= tt(n) - deltaT;
                inref(spikeIndex) = Tref+dt-deltaT; % We subtract dt in the next loop
            end
            spikecnts(itest(i2)) = spikecnts(itest(i2)) + 1;
            
            
            V(itest(i2))=Vrest;
            
            %Record potential for plotting
            Vm(itest(i2),n)=10; %Artificial height of AP
           
            
        end
    end
    
    itest = find(inref>0);  %% In refractory period
    if length(itest)
        inref(itest) = inref(itest) - dt; %Countdown
    end
    
   
    
end    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


fr=spikecnts-1;
sp=spiketimes;

%%

%{
figure(1);clf;
plot(tt,Vm)
xlabel('Time (ms)','fontsize',15)
ylabel('Membrane potential (mV)','fontsize',15)
%}
