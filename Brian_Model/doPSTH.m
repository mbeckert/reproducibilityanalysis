function [PSTH,BINS]=doPSTH(spikes,Tmin,Tmax,BINSIZE)

%doPSTH(spikes,Tmin,Tmax,BINSIZE)
%Assumes spikes is a cell array with a cell of spike times for each trial

BINS=Tmin:BINSIZE:Tmax;

Ntrial=length(spikes);

PSTH=zeros(size(BINS));

for n=1:Ntrial
    sp=spikes{n};
    PSTH=PSTH+histc(sp,BINS);
end





