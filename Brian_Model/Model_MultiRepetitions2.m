cd('f:\desktop\WorkingFolder\model_newvariables_fr')
EL=[-55,-60,-65];
gL=[0.5,1,1.5];
Esyn=[0,0.5,1];

reps = 100;
for g = 1:length(gL)
    for L = 1:length(EL)
        for S = 1:length(Esyn)
            
            results = cell(reps,2);
            
            for rep = 1:reps
                tic
                disp(num2str(rep))
                [results{rep,1},results{rep,2}] = BiophysModelReproducibility_Mike_varyingN(gL(g),EL(L),Esyn(S));
                toc
            end
            
            save(['model_' num2str(g) num2str(L) num2str(S)],'results');
        end
    end
end

clear
cd('f:\desktop\WorkingFolder')

%% perform analysis
cd('f:\desktop\WorkingFolder\model_newvariables_constant')

files = ls;
files = files(3:end,:);

vars = nan(1,3);
vars(1) = max(str2double(cellstr(files(:,7))));
vars(2) = max(str2double(cellstr(files(:,8))));
vars(3) = max(str2double(cellstr(files(:,9))));

HW = cell(vars);
SPS = cell(vars);

for f = 1:size(files,1)
    
    load(files(f,:))
    idx = str2double(cellstr(files(f,7:9)'));
    
    results(:,2) = cellfun(@transpose,results(:,2),'UniformOutput',0);
    results = cellfun(@(x) norm01(x), results, 'UniformOutput',0);
    
    figure(f)
    plot(results{1,1})
    hold on
    plot(results{1,2})
    hold off
    title(files(f,:))
    
    hw = cellfun(@(x) sum(x(50:150) > max(x)/2), results);
    HW{idx(1),idx(2),idx(3)} = hw(:,2) ./ hw(:,1);
    sps = cellfun(@(x) max(x(1:50))/max(x), results);
    SPS{idx(1),idx(2),idx(3)} = sps(:,2) ./ sps(:,1);
    
end

clear f files vars results hw sps idx

cd('F:\Desktop\WorkingFolder')

hw = cellfun(@nanmean,HW);
sps = cellfun(@nanmean,SPS);

EL=[-55,-60,-65];   % default is -60
gL=[0.5,1,1.5];     % default is 1
Esyn=[0,0.5,1];     % default is 0
