
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   restructures data organized in "ReproducibilityAnalysis" to be plotted
%   as a function of degrees azimuth/elevation or itd/ild.
%%

% variable for which analysis you want to perform
% "5" will arrange the data by the raw values of space
% "6" will arrange them relative to the best response of each neuron
type = 5;
met = 4; % 1 is int, 3 is BW, 4 is peak

val = cell(1,6);
X = cell(1,6);

for t = 1:6
    switch t
        case 1
            data = iclsAZ;
        case 2
            data = iclsEL;
        case 3
            data = iclsITD;
        case 4
            data = iclsILD;
        case 5
            data = otAZ;
        case 6
            data = otEL;
    end
    
    x = unique(abs(data(type,:)));
    
    for i = 1:length(x)
        idx = abs(data(type,:)) == x(i);
        val{t}{i} = data(met,idx);
    end
    
    X{t} = x;
end

clear t data idx i m x

for t = 1:6
    m = max(cell2mat(cellfun(@length,val{t},'UniformOutput',0)));
    for i = 1:length(val{t})
        val{t}{i} = [val{t}{i}'; nan(m-length(val{t}{i}),1)];
    end
    val{t} = cell2mat(val{t});
end

clear m t i

d = cellfun(@nanmean,val,'UniformOutput',0);
s = cellfun(@nanstd,val,'UniformOutput',0);

for t = 1:6
    figure(t)
    errorbar(d{t},s{t})
end