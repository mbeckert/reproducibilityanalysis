r_range = 5:5:100;
data = zeros(2,length(r_range));
out = zeros(2,length(r_range));
data(1,:) = r_range;

for reps = 1:100

R = 1;
while R <= length(r_range)
    check = 0;
    while check < 1
        try
            model_spikes = Model_ReproVsJitter_varying(r_range(R));
            check = 1;
        catch
            check = 0;
        end
    end
    
    if check == 1
    outREP = cell(1,size(model_spikes,1));
    outMEAN = cell(1,size(model_spikes,2));
    outHW = cell(1,size(model_spikes,1));
    
    bins = 0:0.001:0.50;
    bin = 0.001;
    maxWidth = 100;
    
    for fr = 1:size(model_spikes,1)
        for re = 1:size(model_spikes,2)
            TrialData = squeeze(model_spikes(fr,re,:))';
            [~,outREP{fr,re},outMEAN{fr,re},~,~,outHW{fr,re}] = reproducibility(TrialData,bins,bin,maxWidth);
        end
    end
    
    outREP = cell2mat(outREP);
    outHW = cell2mat(outHW);
    outMEAN = cell2mat(outMEAN);
    
    data(2,R) = outREP(1,1);
    R = R + 1;
    
    end
    
end

out = out + data;

end

out = out/reps;

clearvars -except out


