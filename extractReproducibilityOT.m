%% Load variables

load('C:\Users\PenaLab\Desktop\FreeField\Analysis\SpeakerIndex.mat','SpeakerIndex')
load('ReproducibilityOT.mat','spikecountmatrix','CURVE_all')

FR = spikecountmatrix; %#ok<USENS>
REP = CURVE_all; %#ok<USENS>
IDX = SpeakerIndex;

clear spikecountmatrix SpeakerIndex CURVE_all

%%

B=cell(size(FR));
AZ=cell(size(FR));
EL=cell(size(FR));

for unit = 1:length(FR)
    
    REP{unit}(REP{unit}==-Inf)=nan;
    d=[norm01(FR{unit},2),norm01(REP{unit},1)'];
    best = find(d(:,1)==max(d(:,1)));

    az_data = [];
    el_data = [];
    BEST = nan(1,2);
    
    for b = 1:length(best)
        a=find(IDX(:,1)==IDX(best(b),1));
        e=find(IDX(:,2)==IDX(best(b),2));
        az_data_temp = d(IDX(:,1)==IDX(best(b),1),:);
        el_data_temp = d(IDX(:,2)==IDX(best(b),2),:);
        if size(az_data_temp,1)>size(az_data,1);
            az_data = az_data_temp;
            BEST(1)= best(b);
        end
        if size(el_data_temp,1)>size(el_data,1);
            el_data = el_data_temp;
            BEST(2) = best(b);
        end
    end             % b
    
    B{unit} = BEST;
    AZ{unit} = az_data;
    EL{unit} = el_data;
    
end                 % unit

clear d az_data* el_data* BEST b best unit

%%

load('ReproducibilityOT.mat','CURVE_all','spikecountmatrix');

FR=spikecountmatrix;
REP=CURVE_all;

rep=[];
fr=[];

for unit = 1:length(FR)
    
    REP{unit}(REP{unit}==-Inf)=nan;
%     rep=[rep,norm01(REP{unit},1)];
%     fr=[fr,norm01(FR{unit},2)'];
    rep=[rep,REP{unit}];
    fr=[fr,FR{unit}'];
    
end

clear FR REP unit

d=[fr',rep'];

%% turn the data into histograms

load('ReproducibilityOT.mat','CURVE_all_AZ','CURVE_all_EL');

a=cell2mat(CURVE_all_AZ);
e=cell2mat(CURVE_all_EL);

c_a=cell(1,10);
c_e=cell(1,10);

edges=[-0.01,0.1:0.1:1];

for i = 1:length(edges)-1
    c_a{i} = a((a(:,1)>edges(i) & a(:,1)<=edges(i+1)),2);
    c_e{i} = e((e(:,1)>edges(i) & e(:,1)<=edges(i+1)),2);
end

clear a e i edges

fillto_a = max(cellfun(@length,c_a));
fillto_e = max(cellfun(@length,c_e));

for i = 1:length(c_a)
    c_a{i}=[c_a{i};nan(fillto_a-length(c_a{i}),1)];
    c_e{i}=[c_e{i};nan(fillto_e-length(c_e{i}),1)];
end

clear fillto* i
        
c_a=cell2mat(c_a);
c_e=cell2mat(c_e);
    