load('ot_synch_ff_shift.mat')
ff = ot_synch.ff;
clear ot_synch

normalize = [1,1];

GM = sqrt(ff{3} .* ff{4});
GM = normalization(GM,normalize(1));

correct = nan(size(ff{11}{1}));

chck = ff{6};
chck = chck == 0;
chck = find(chck == 1);
for i = 1:length(chck)
    ff{11}{chck(i)} = correct;
    ff{12}{chck(i)} = correct;
    ff{13}{chck(i)} = correct;
end

CCG = ff{11};
SH = ff{12};
COR = ff{13};

binNum = 199; int = 10;

ccg = nan(size(CCG));
sh = nan(size(SH));
cor = nan(size(COR));

for n = 1:size(ccg,1)
    for sp = 1:size(ccg,2)
        if ~isempty(CCG{n,sp})
            ccg(n,sp) = sum(CCG{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        end
        if ~isempty(SH{n,sp})
            sh(n,sp) = sum(SH{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        end
        if ~isempty(COR{n,sp})
            cor(n,sp) = sum(COR{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        end
    end
end

ccg = normalization(ccg,normalize(2));
sh = normalization(sh,normalize(2));
cor = normalization(cor,normalize(2));

[c,~,~] = hist2D([GM(:),ccg(:)], [0,1,11], [0,1,11]);
[s,~,~] = hist2D([GM(:),sh(:)], [0,1,11], [0,1,11]);
[r,x,y] = hist2D([GM(:),cor(:)], [0,1,11], [0,1,11]);
g = gray;
g = flipud(g);

figure(1)
imagesc(c)
colormap(g)
title(num2str(corr(GM(:),ccg(:),'rows','pairwise')))


figure(2)
imagesc(s)
colormap(g)
title(num2str(corr(GM(:),sh(:),'rows','pairwise')))

figure(3)
imagesc(r)
colormap(g)
title(num2str(corr(GM(:),cor(:),'rows','pairwise')))

save otsynch2 GM ccg sh cor

clearvars -except GM ccg sh cor

corr_pair{1} = nan(size(GM,1),1);
corr_pair{2} = nan(size(GM,1),1);
corr_pair{3} = nan(size(GM,1),1);

for i = 1:size(GM,1)
    corr_pair{1}(i) = corr(GM(i,:)',ccg(i,:)','rows','pairwise');
    corr_pair{2}(i) = corr(GM(i,:)',sh(i,:)','rows','pairwise');
    corr_pair{3}(i) = corr(GM(i,:)',cor(i,:)','rows','pairwise');
end

corr_pair = cell2mat(corr_pair);
clear i

z = @(x) 0.5*log((1+x)/(1-x));

corr_z = nan(size(corr_pair));
for i = 1:length(corr_pair(:));
    corr_z(i) = z(corr_pair(i));
end

clear z i
