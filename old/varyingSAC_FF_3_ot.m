cd('C:\Users\mbeck\Desktop\Data\Analysis\Data')
load('FreeFieldRawDATA_notsidecorrected_FFffomitted.mat','OT')
cd('C:\Users\mbeck\Desktop\Data\Analysis\ReproducibilityPaper\working')

data = OT;
clear OT

HIST = cell(1,200);
CURVE = cell(1,200);
Mean = cell(1,200);
SACcurve = cell(1,200);
SACnorm = cell(1,200);

count = 1;

for date = 1:length(data)
    for site = 1:length(data{date})
        for unit = 2:length(data{date}{site})
            
            d = data{date}{site}{unit};
            disp(['Date - ' num2str(date) '; Site - ' num2str(site) '; Unit - ' num2str(unit)]);
            filetypes = fieldnames(d);
            idx = regexpi(filetypes,'FFf.');
            filetypes = filetypes(~cellfun(@isempty,idx));
            
            CURVE{count}=zeros([length(filetypes) 144]);
            Mean{count}=zeros([length(filetypes) 144]);
            SACcurve{count}=cell([length(filetypes) 144]);
            SACnorm{count}=cell([length(filetypes) 144]);
            
            for file=1:length(filetypes)
                
                %%%%%% Plot PSTH for each Speaker then run correlation on each pair
                TrialData = d.(filetypes{file}).spikes_times;
                HIST{count} = nan(size(TrialData,1),500);
                holder=zeros([1 500]);
                for ii=1:size(TrialData,1)
                    for i=1:size(TrialData,2)
                        
                        h=hist(TrialData{ii,i},0.001:0.001:0.5);
                        holder=holder+h;
                        clear h;
                    end
                    holder(1:50)=0;
                    HIST{count}(ii,:) = holder;
                    holder=zeros([1 500]);
                end
                
                %%%%%% Actual Work
                
                PARAM=[0.05,20];
                
                binWidth=PARAM(1);
                maxBin=PARAM(2);
                
                for h=1:size(TrialData,1)
                    spikes=TrialData(h,:);
                    spikes=cellfun(@transpose,spikes,'un',0);
                    spikes=cellfun(@(x) x*1000, spikes, 'UniformOutput', false);
                    spikeWin=[50 200];
                    duration = 150;
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % Run computeSAC
                    
                    % Make sure spikeWin is sorted in the correct order.
                    spikeWin=sort(spikeWin);
                    
                    % Pre-allocate some arrays.
                    rates=zeros(size(spikes));
                    histax=-maxBin:binWidth:maxBin;
                    sac=zeros(size(histax));
                    
                    % Now the work...
                    for n=1:length(spikes)
                        
                        % For the current trial, get the spikes, filter out any outside the
                        % spike window, and then compute a mean rate for use in the normalizing
                        % factor later.
                        thisTrial=spikes{n};
                        thisTrial=thisTrial(thisTrial>spikeWin(1)&thisTrial<spikeWin(2));
                        rates(n)=length(thisTrial)./(diff(spikeWin)./1000);
                        
                        % Now, for each trial which we're NOT working on, find all the spikes
                        % in the spike window.
                        idx=1:length(spikes);idx=idx(idx~=n);
                        restSpikes=[spikes{idx}];
                        restSpikes=restSpikes(restSpikes>spikeWin(1)&...
                            restSpikes<spikeWin(2));
                        
                        % Then, for each spike in the current trial, subtract it from the times
                        % of all spikes from all other trials, bin them up, and keep track of
                        % them in the running histogram.
                        for t=1:length(thisTrial);
                            rs=restSpikes-thisTrial(t);
                            rs=rs(abs(rs)<maxBin);
                            tmp=hist(rs,histax);
                            sac=sac+tmp;
                        end
                    end
                    
                    sac=sac(2:end-1);
                    
                    % % Get SAC values as described by Joris (2006)
%                     nf=length(spikes)*(length(spikes)-1)*mean(rates).^2*...
%                         (diff(spikeWin)./1000)*(binWidth./1000);
%                     SACcurve{count}{file,h} = sac;
%                     SACnorm{count}{file,h} = sac/nf;
                    
%                     % Get SAC values as written by Bertrand Fontaine via email
%                     sac = sac/(binWidth/1000)/(duration/1000)/length(spikes)/(length(spikes)-1);
%                     SACcurve{count}{file,h} = sac;
%                     SACnorm{count}{file,h} = (sac-mean(rates).^2)*(binWidth/1000)/mean(rates);
                    
%                     % Get SAC values as purely described by Brette (2012)
%                     sac = sac/((binWidth/1000)*(duration/1000));
%                     SACcurve{file}{1,h} = sac;
%                     SACnorm{file}{1,h} = (sac-mean(rates).^2)/mean(rates);
%                     
%                     % Get SAC values through a combined method
%                     sac = sac/((binWidth/1000)*(duration/1000))/(length(spikes)*(length(spikes)-1));
%                     SACcurve{file}{1,h} = sac;
%                     SACnorm{file}{1,h} = (sac-mean(rates).^2)/mean(rates);
                    
%                     mid = round(length(sac)/2);
%                     int = round(3/binWidth);
%                     sac = sac/length(spikes)/(length(spikes)-1);
%                     CURVE{count}(file,h)=sum(sac(mid-int:int+mid)-((binWidth/1000)/mean(rates)).^2)*(binWidth/1000)/mean(rates);

%                     CURVE{count}(file,h)=sum(SACnorm{count}{file,h}(mid-int:mid+int))/((mid+int)-(mid-int));
                    
                    % % Get SAC values without normalization
                    sac = sac/(binWidth/1000)/length(spikes);
                    SACcurve{count}{1,h} = sac;
                    SACnorm{count}{1,h} = sac;

%                     mid = round(length(sac)/2);
%                     int = round(3/binWidth);
                    CURVE{count}(1,h)=sum(SACnorm{count}{1,h});

                    Mean{count}(file,h)=mean(rates);
                end
            end
            count = count + 1;
        end
    end
end

L = cellfun(@isempty,CURVE);
CURVE(L)=[];
Mean(L)=[];
SACcurve(L)=[];
SACnorm(L)=[];

clearvars -except Index CURVE Mean SACcurve SACnorm

CURVE = cellfun(@(x) mean(x,1),CURVE,'UniformOutput',0);
Mean = cellfun(@(x) mean(x,1),Mean,'UniformOutput',0);

cd('C:\Users\mbeck\Desktop\Data\Analysis\ReproducibilityPaper\working')



normMean = cellfun(@(x) x/max(x),Mean,'UniformOutput',0);
normCURVE = cellfun(@(x) x/max(x),CURVE,'UniformOutput',0);
x = [cell2mat(normMean);cell2mat(normCURVE)];
scatter(x(1,:),x(2,:))