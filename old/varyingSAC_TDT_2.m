% function [CURVE,Index,Mean,HIST,HistCorr,RasterCheck]=varyingSAC_TDT_2

% cd('C:\Users\mbeck\Desktop\Data\ICls\Data\RawData\Dichotic\ILD')
cd('C:\Users\mbeck\Desktop\Data\ICls\Data\RawData\Dichotic\ITD')

FILES = ls;
FILES = FILES(3:end,:);

Index = cell(1,length(FILES));
HIST = cell(1,length(FILES));
HistCorr = HIST;
CURVE = HIST;
Mean = HIST;
SACcurve = HIST;
SACnorm = HIST;

for file=1:length(FILES);

%% Load Data
load(FILES(file,:))
% data(:,51:100)={NaN};

%% Reorganize Data

data=cell(size(curvedata.spike_times,1),size(curvedata.spike_times,2));
for jj=1:size(curvedata.spike_times,2);
    if isfield(curvedata,'depvar');
        [Indexer,ind]=sortrows(curvedata.depvar(:,jj));
    end
    if isfield(curvedata,'depvars');
        [Indexer,ind]=sortrows(curvedata.depvars(:,jj));
    end
    data(:,jj)=curvedata.spike_times(ind,jj);
end

Indexer=Indexer';

Index{file}=Indexer;
RasterCheck{file}=zeros([1 size(data,1)]);

%% Plot Rasters of the Trials and grab spike counts

spikecountmatrix=zeros([size(data,1) size(data,2)]);

% XX=0; YY=1;
for ii=1:size(data,1);
% %     XX=XX+1;
% %     FigHandle = figure(ii);
% %     set(FigHandle, 'OuterPosition', [XX*380-379, 850-(YY*350+80-(350+(80*YY))), 380, 350]);
% %     clf;
%     figure(1)
%     hold on;
%     for hh=1:length(data);
%         if not(isempty(data{ii,hh}));
%             scatter(data{ii,hh},(ones(1,length(data{ii,hh}))*hh),8,'fill','k');
%             spikecountmatrix(ii,hh)=length(data{ii,hh});
%         end
%     end
%     axis([0 300 0 100]);
%     hold off
% %     if XX==5;
% %         XX=0;
% %         YY=YY+1;
% %     end
%     RC=input('How does the raster look? no patterning - 0; too few spikes - 1; {meets criteria - 2} ... ');
%     if isempty(RC);
%         RC=2;
%     end
RC = 2;
    RasterCheck{file}(ii)=RC;
    close all
end

% pause

holder=zeros([1 300]);
for ii=1:size(data,1);
%     figure(ii);
    for i=1:100;

        h=hist(data{ii,i},[1:300]);
        holder=holder+h;
        clear h;
    end
    holder(1:50)=0;
%     bar(holder)
    eval(['HIST{file}.spkr' num2str(ii) '=holder;']);
    holder=zeros([1 300]);
%     axis([0 300 0 80]);
end

% Number of permutations of units
list=combnk(1:size(data,1),2);

% Preparing the final output
% Col 1&2 are the units of the correlation
% Remaining Col are for each freq tested
HistCorr{file}=zeros([size(list,1) 4]);
HistCorr{file}(:,1)=list(:,1);
HistCorr{file}(:,2)=list(:,2);

% Calculate the Correlations and input them into the FINAL matrix
for k=1:size(list,1);
        eval(['[CORR,p]=corrcoef(HIST{file}.spkr' num2str(list(k,1)) '(50:end),HIST{file}.spkr' num2str(list(k,2)) '(50:end));']);
        HistCorr{file}(k,3)=CORR(1,2);
        HistCorr{file}(k,4)=p(1,2);
end

clear k holder i ii h

% pause

%% Find mean evoked firing rate

Mean{file}=mean(spikecountmatrix,2);
Mean{file}(isnan(Mean{file}(:,1)))=[];
Mean{file}=Mean{file}';


%% Actual Work

CHECK = 1;
PARAM=[0.05,20];     % these are default binWidths and maxBin respectively, see commented bit in the Run computeSAC section for some accepted values

while CHECK==1;

%     running=input(['Change Parameters? Current binWidth = ' num2str(PARAM(1)) '; maxBin = ' num2str(PARAM(2)) ': yes-1 {no-0}']);
%     if isempty(running);
%         running=0;
%     end
%     if running == 1;
%         PARAM(1)=input('Input SAC defaults: [binWidth]');
%     end
    running = 0;
CURVE{file}=zeros([1 size(data,1)]);
PLOT=cell([1 size(data,1)]);

binWidth=PARAM(1);
maxBin=PARAM(2);

for h=1:size(data,1);
    spikes=data(h,:); %#ok<COLND>
    spikeWin=[100 300];
    duration = 200;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run computeSAC

% Set some defaults.
% binWidth=0.1;
% binWidth=0.15;
% binWidth=50e-3;
% binWidth=20e-3;
% maxBin=4;
% warnopts(assignopts(who,varargin));

% Make sure spikeWin is sorted in the correct order.
spikeWin=sort(spikeWin);

% Pre-allocate some arrays.
rates=zeros(size(spikes));
histax=-maxBin:binWidth:maxBin;
sac=zeros(size(histax));

% Now the work...
for n=1:length(spikes)
    
    % For the current trial, get the spikes, filter out any outside the
    % spike window, and then compute a mean rate for use in the normalizing
    % factor later.
    thisTrial=spikes{n};
    thisTrial=thisTrial(thisTrial>spikeWin(1)&thisTrial<spikeWin(2));
    rates(n)=length(thisTrial)./(diff(spikeWin)./1000);
    
    % Now, for each trial which we're NOT working on, find all the spikes
    % in the spike window.
    idx=1:length(spikes);idx=idx(idx~=n);
    restSpikes=[spikes{idx}];
    restSpikes=restSpikes(restSpikes>spikeWin(1)&...
        restSpikes<spikeWin(2));
    
    % Then, for each spike in the current trial, subtract it from the times
    % of all spikes from all other trials, bin them up, and keep track of
    % them in the running histogram.
    for t=1:length(thisTrial);
        rs=restSpikes-thisTrial(t);
        rs=rs(abs(rs)<maxBin);
        tmp=hist(rs,histax);
        sac=sac+tmp;
    end
end

sac=sac(2:end-1);

             % % Get SAC values as described by Joris (2006)
%             nf=length(spikes)*(length(spikes)-1)*mean(rates).^2*...
%                 (diff(spikeWin)./1000)*(binWidth./1000);
%             SACcurve{file}{1,h} = sac;
%             SACnorm{file}{1,h} = sac/nf;
                        
            % % Get SAC values without normalization
            sac = sac/(binWidth/1000)/length(spikes);
            SACcurve{file}{1,h} = sac;
            SACnorm{file}{1,h} = sac;

            mid = round(length(sac)/2);
            int = round(3/binWidth);
            CURVE{file}(1,h)=sum(SACnorm{file}{1,h}(mid-int:mid+int));
            
            Mean{file}(1,h)=mean(rates);
end


% check=input('Run again? = yes-1 {no-0}');
check = 0;
if isempty(check);
    check=0;
end

if check==0;
    CHECK=0;
end

end
end

clearvars -except Index HIST HistCorr CURVE Mean SACcurve SACnorm

cd('C:\Users\mbeck\Desktop\Data\Analysis\ReproducibilityPaper\working')