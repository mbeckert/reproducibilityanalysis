%%%%%%% tetrode data
load('f:\desktop\WorkingFolder\synchony\ot_synch_ff.mat', 'ccg', 'sh', 'cor', 'GM')
reg = 1;
nNeu = size(GM,1);

%%%%%%% unsorted array data
% fol = 'f:\desktop\Data\Array\analysis\corr\';
% load([fol 'data_compile_raw_update'], 'pair_synch');
% distant = [pair_synch.rate{reg}, pair_synch.ccg{reg}, pair_synch.sh{reg}, pair_synch.cor{reg}];

%%%%%%% sorted array data
load('arraydata_spikes.mat','pairdata')
b = 2;

% change this threshold to determine what pairs are used here
idx = pairdata.diffitd < 60;

ccg_a = pairdata.syn_frozen.synch_ccg(idx,b);
sh_a = pairdata.syn_frozen.synch_sh(idx,b);
cor_a = pairdata.syn_frozen.synch_cor(idx,b);
gm_a = pairdata.syn_frozen.gm(idx,b);

for i = 1:size(gm_a,1)
    [~,idx] = max(max(gm_a{i}));
    ccg_a{i} = ccg_a{i}(:,idx);
    sh_a{i} = sh_a{i}(:,idx);
    cor_a{i} = cor_a{i}(:,idx);
    gm_a{i} = gm_a{i}(:,idx);
end

gm_a = cellfun(@transpose, gm_a, 'UniformOutput', 0);
ccg_a = cellfun(@transpose, ccg_a, 'UniformOutput', 0);
sh_a = cellfun(@transpose, sh_a, 'UniformOutput', 0);
cor_a = cellfun(@transpose, cor_a, 'UniformOutput', 0);
distant = [gm_a, ccg_a, sh_a, cor_a];

colmap = distinguishable_colors(max([nNeu,size(distant,1)]));

%% analysis and plotting

near_R = cell(1,3);
dis_R = cell(1,3);

figure(1)
for t = 1:3
    switch t
        case 1
            data = ccg;
            ti = 'ccg';
        case 2
            data = sh;
            ti = 'sh';
        case 3
            data = cor;
            ti = 'cor';
    end
    subplot(2,3,t)
    hold on
    xlabel('geometric mean firing rate')
    ylabel(ti)
    
    near_R{t} = nan(nNeu,1);
    
    for i = 1:nNeu
        
        a = GM(i,:);
        b = data(i,:);
        
        near_R{t}(i) = corr(a', b', 'rows', 'pairwise');
        
        idx = isnan(a);
        a(idx) = []; b(idx) = [];
        idx = isnan(b);
        a(idx) = []; b(idx) = [];
        p = polyfit(a, b, 1);
        v = polyval(p,a);
        scatter(a, v, 5, colmap(i,:), 'fill')
        
    end
    axis square
    
    subplot(2,3,t+3)
    hold on
    xlabel('geometric mean firing rate')
    ylabel(ti)
    
    dis_R{t} = nan(size(distant,1),1);
    
    for i = 1:size(distant,1)
        
        a = distant{i,1};
        b = distant{i,t+1};
        if ~isempty(a)
            dis_R{t}(i) = corr(a', b', 'rows', 'pairwise');
            
            idx = isnan(a);
            a(idx) = []; b(idx) = [];
            idx = isnan(b);
            a(idx) = []; b(idx) = [];
            p = polyfit(a, b, 1);
            v = polyval(p,a);
            scatter(a, v, 5, colmap(i,:), 'fill')
        end
    end
    axis square
end

dis_R = cellfun(@Fisher_Z, dis_R, 'UniformOutput', 0);
near_R = cellfun(@Fisher_Z, near_R, 'UniformOutput', 0);

figure(2)
statdata.dis = CorrelationAnalysis_stats(dis_R,{'synch'});
figure(2)
statdata.near = CorrelationAnalysis_stats(near_R,{'synch'});

figure(2)
x = 1:3;
for r = 1:2
    switch r
        case 1
            y = near_R;
            ti = 'near';
        case 2
            y = dis_R;
            ti = 'distant';
    end
    subplot(1,2,r)
    boxplot2_points(x, y)
    xlim([0 4])
    ylim([-1 1])
    xticks(1:3)
    xticklabels({'ccg', 'sh', 'cor'})
    ylabel('Correlation Coefficient')
    title(ti)
end

figure(3)

dis = distant;
dis = cellfun(@norm01, dis, 'UniformOutput', 0);

gm = cell2mat(dis(:,1)');
g = cell2mat(distant(:,1)');

for t = 2:4
    
    d = cell2mat(dis(:,t)');
    subplot(2,3,t-1)
    scatter(gm,d,5,'fill','k')
    axis square
    xlabel('normalized gm firing rate')
    ylabel('normalized synchrony')
    
    d = cell2mat(distant(:,t)');
    subplot(2,3,t+2)
    scatter(g,d,5,'fill','k')
    axis square
    xlabel('normalized gm firing rate')
    ylabel('normalized synchrony')
end

for meas = 1:2
    for t = 1:2
        switch t
            case 1
                data = statdata.near;
                ti = 'nearby';
            case 2
                data = statdata.dis;
                ti = 'distant';
        end
        var = length(data.name);
        figure(meas + 3)
        
        for v = 1:var
            switch meas
                case 1
                    st = data.stats.kruskalmult{v};
                    x = cell2mat(data.descr.quant(v,:)');
                    m = x(:,3);
                    s = ( x(:,4) - x(:,2) ) / 2;
                case 2
                    st = data.stats.anovamult{v};
                    m = data.descr.mean(v,:);
                    s = data.descr.std(v,:);
            end
            
            subplot(1,2,t)
            bar(1:3, m)
            hold on
            errorbar(1:3, m, s);
            xticks(1:3)
            xticklabels({'ccg'; 'sh'; 'cor'})
            ylabel(data.name{v})
            title(ti)
            
            ypoint = ylim;
            yinc = (ypoint(2) - ypoint(1)) / 20;
            ypoint = ypoint(2);
            
            for p = 1:size(st,1)
                if st(p,6) < 0.05
                    plot(st(p,1:2), [ypoint, ypoint], 'r')
                end
                ypoint = ypoint + yinc;
            end
            
        end
        
        clear var v st ypoint yinc data p x m s
    end
end

clear meas

%%

near_R = cell2mat(near_R);
dis_R = cell2mat(dis_R);
near_R = Finv(near_R);
dis_R = Finv(dis_R);

[~, idx] = sort(near_R(:,1));
near_R = near_R(idx,:);
[~, idx] = sort(dis_R(:,1));
dis_R = dis_R(idx,:);


for t = 1:2
    switch t
        case 1
            data = near_R;
        case 2
            data = dis_R;
    end
    
    n = size(data,1);
    
    offset = linspace(-0.5,0.5,n);
    offset = repmat(offset', 1, 3);
    
    x = repmat(1:3, n, 1);
    
    x = x + offset;
    
    figure
    hold on
    
    for i = 1:n
        plot(x(i,:), data(i,:), 'k');
    end
    
    scatter(x(:), data(:), 25, 'b')
    
    xticks(1:3)
    xticklabels({'ccg', 'sh', 'cor'});
    xlim([0, 4])
    
end