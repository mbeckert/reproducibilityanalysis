cd('C:\Users\mbeck\Desktop\Data\Analysis\Data')
load('FreeFieldRawDATA_notsidecorrected_FFffomitted.mat','OT')
cd('C:\Users\mbeck\Desktop\Data\Analysis\ReproducibilityPaper\working')

data = OT;
clear OT

% plotter = [3 10 12 15 23 44];
% plotter = [10 13 50 57];

count = 1;
for date = 1:length(data)
    for site = 1:length(data{date})
        for unit = 2:length(data{date}{site})
            d = data{date}{site}{unit};
            disp(['Date - ' num2str(date) '; Site - ' num2str(site) '; Unit - ' num2str(unit)]);
            
            if isfield(d,'FFf1')
%                 if count == 10;
                    spikecountmatrix{count}=nanmean(d.FFf1.spikes_count,2);
                    RasterCheck{count}=zeros([1 144]);
                    FR=sum(d.FFf1.spikes_count,2);
                    
                    %% Plot Rasters of the Trials
                    
                    ii = 1;
                    while ii<=size(d.FFf1.spikes_times,1)
                        if FR(ii)>2;
                            
%                             figure(1)
%                             hold on;
%                             for hh=1:size(d.FFf1.spikes_times,2);
%                                 if ~isempty(d.FFf1.spikes_times{ii,hh});
%                                     scatter(d.FFf1.spikes_times{ii,hh},(ones(1,length(d.FFf1.spikes_times{ii,hh}))*hh),8,'fill','k');
%                                 end
%                             end
%                             hold off
%                             axis([0 0.2 0 size(d.FFf1.spikes_times,2)])
%                             title(['Date - ' num2str(date) '; Site - ' num2str(site) '; Unit - ' num2str(unit) '; Speaker - ' num2str(ii)])
%                             RC=input('How does the raster look? {no patterning - 0}; meets criteria - 1; go back - 2 ... ');
                            RC = 1;
                            if RC ~= 2
                                last = ii;
                            elseif isempty(RC)
                                last = ii;
                            end
                        else
                            RC=0;
                        end
                        if isempty(RC) || RC==0;
                            RasterCheck{count}(ii)=0;
                            ii=ii+1;
                        end
                        if RC == 1;
                            RasterCheck{count}(ii)=1;
                            ii=ii+1;
                        end
                        if RC == 2;
                            ii=last;
                        end
                        
                        close all
                        
                    end
                    
                    disp('done')
                    
                    clear RC hh ii
                    
                    %% Plot PSTH for each Speaker then run correlation on each pair
                    
                    % holder=zeros([1 500]);
                    % for ii=1:size(d.spikes_times,1);
                    %     for i=1:size(d.spikes_times,2)
                    %         h=hist(d.spikes_times{ii,i},[1:500]);
                    %         holder=holder+h;
                    %         clear h;
                    %     end
                    %     holder(1:50)=0;
                    %     eval(['HIST{count}.spkr' num2str(ii) '=holder;']);
                    %     holder=zeros([1 500]);
                    % end
                    %
                    % % Number of permutations of units
                    % list=combnk(1:size(TrialData,1),2);
                    %
                    % % Preparing the final output
                    % % Col 1&2 are the units of the correlation
                    % % Remaining Col are for each freq tested
                    % HistCorr{file}=zeros([size(list,1) 4]);
                    % HistCorr{file}(:,1)=list(:,1);
                    % HistCorr{file}(:,2)=list(:,2);
                    %
                    % % Calculate the Correlations and input them into the FINAL matrix
                    % for k=1:size(list,1);
                    %         eval(['[CORR,p]=corrcoef(HIST{file}.spkr' num2str(list(k,1)) '(50:end),HIST{file}.spkr' num2str(list(k,2)) '(50:end));']);
                    %         HistCorr{file}(k,3)=CORR(1,2);
                    %         HistCorr{file}(k,4)=p(1,2);
                    % end
                    %
                    % clear k holder i ii h
                    %
                    % % pause
                    
                    %% get mean evoked FR from spikecountmatrix
                    
                    % Mean{file}=mean(spikecountmatrix,2);
                    % Mean{file}(isnan(Mean{file}(:,1)))=[];
                    % Mean{file}=Mean{file}';
                    
                    %% Actual Work
                    
                    PARAM=[0.5,20];     % these are default binWidths and maxBin respectively, see commented bit in the Run computeSAC section for some accepted values
                    
                    disp('Running SAC analysis')
                    
                    CURVE{count}=zeros([1 size(d.FFf1.spikes_times,1)]);
                    % rate=zeros([1 size(TrialData,1)]);
                    PLOT=cell([1 size(d.FFf1.spikes_times,1)]);
                    
                    binWidth=PARAM(1);
                    maxBin=PARAM(2);
                    
                    for h=1:size(d.FFf1.spikes_times,1);
                        %                         if RasterCheck{count}(h)==1
                        spikes=d.FFf1.spikes_times(h,:); %#ok<COLND>
                        spikes=cellfun(@transpose,spikes,'un',0);
                        spikes=cellfun(@(x) x*1000, spikes, 'UniformOutput', false);
                        spikeWin=[50 200];
                        duration = 150;
                        
                        % Make sure spikeWin is sorted in the correct order.
                        spikeWin=sort(spikeWin);
                        
                        % Pre-allocate some arrays.
                        rates=zeros(size(spikes));
                        histax=-maxBin:binWidth:maxBin;
                        sac=zeros(size(histax));
                        
                        % Now the work...
                        for n=1:length(spikes)
                            
                            % For the current trial, get the spikes, filter out any outside the
                            % spike window, and then compute a mean rate for use in the normalizing
                            % factor later.
                            thisTrial=spikes{n};
                            thisTrial=thisTrial(thisTrial>spikeWin(1)&thisTrial<spikeWin(2));
                            rates(n)=length(thisTrial)./(diff(spikeWin)./1000);
                            
                            % Now, for each trial which we're NOT working on, find all the spikes
                            % in the spike window.
                            idx=1:length(spikes);idx=idx(idx~=n);
                            restSpikes=[spikes{idx}];
                            restSpikes=restSpikes(restSpikes>spikeWin(1)&...
                                restSpikes<spikeWin(2));
                            
                            % Then, for each spike in the current trial, subtract it from the times
                            % of all spikes from all other trials, bin them up, and keep track of
                            % them in the running histogram.
                            for t=1:length(thisTrial);
                                rs=restSpikes-thisTrial(t);
                                rs=rs(abs(rs)<maxBin);
                                tmp=hist(rs,histax);
                                sac=sac+tmp;
                            end
                        end
                        
                        sac=sac(2:end-1);
                        
                        mid = round(length(sac)/2);
                        int = round(1/binWidth);
                        
                        % Calculate the reliability (integral of the SAC)
%                         sac = sac/length(spikes)/(length(spikes)-1);
%                         PLOT{1,h}=sac;
%                         figure(1); plot(PLOT{1,h});
%                         title(['Date - ' num2str(date) '; Site - ' num2str(site) '; Unit - ' num2str(unit) '; Speaker - ' num2str(h)])
%                         CURVE{count}(1,h)=sum(sac(mid-int:int+mid)-((binWidth/1000)/mean(rates)).^2)*(binWidth/1000)/mean(rates);
                        nf=length(spikes)*(length(spikes)-1)*mean(rates).^2*...
                        (diff(spikeWin)./1000)*(binWidth./1000);
                    SACcurve= sac;
                    SACnorm= sac/nf;
                    
%                     % Get SAC values as written by Bertrand Fontaine via email
%                     sac = sac/(binWidth/1000)/(duration/1000)/length(spikes)/(length(spikes)-1);
%                     SACcurve{count}{file,h} = sac;
%                     SACnorm{count}{file,h} = (sac-mean(rates).^2)*(binWidth/1000)/mean(rates);
                    
%                     % Get SAC values as purely described by Brette (2012)
%                     sac = sac/((binWidth/1000)*(duration/1000));
%                     SACcurve{file}{1,h} = sac;
%                     SACnorm{file}{1,h} = (sac-mean(rates).^2)/mean(rates);
%                     
%                     % Get SAC values through a combined method
%                     sac = sac/((binWidth/1000)*(duration/1000))/(length(spikes)*(length(spikes)-1));
%                     SACcurve{file}{1,h} = sac;
%                     SACnorm{file}{1,h} = (sac-mean(rates).^2)/mean(rates);

                    mid = round(length(sac)/2);
                    int = round(3/binWidth);
                    CURVE{count}(1,h)=SACnorm(mid);
                    end
                    
%                 end
                
                clear rs t restSpikes idx thisTrial sac h mid int spikeWinbinWidth PARAM maxBin tmp rates histax
                
            end
            
            count = count+1;
        end
    end
end


clear binWidth duration hh ii n PLOT date site spikeWin CHECK spikes unit d count running