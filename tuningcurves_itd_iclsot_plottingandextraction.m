% load data compiled from 'tuningcurves_itd_iclsot_datamunging'
load('spiketimes_itd_iclsot.mat')

for r = 1:2
    switch r
        case 1
            data = icls;
            titlestr = 'icls';
            div = 1000;
            off = 0.05;
        case 2
            data = ot;
            titlestr = 'ot';
            div = 1;
            off = 0;
    end
    
    output = cell(size(data,1),1);
    
    for u = 1:length(data)
        
        x = cell(size(data{u}));
        for dep = 1:size(data{u,1},1)
            for rep = 1:size(data{u,1},2)
                x{dep,rep} = ones(size(data{u,1}{dep,rep}))*rep;
            end
        end
        
        output{u} = nan(dep,3);
        output{u}(:,1) = data{u,2};
        output{u}(:,2) = mean(cell2mat(cellfun(@(x) sum(x/div < 0.05+off),data{u,1},'UniformOutput',0)),2);
        output{u}(:,3) = mean(cell2mat(cellfun(@length,data{u,1},'UniformOutput',0)),2);
        
        % % This section is for plotting
        
%         figure('units','normalized','outerposition',[0 0 1 1])
%         
%         subplot(2,size(data{u,1},1),[1,size(data{u,1},1)])
%         hold on
%         plot(output{u,1},output{u,3},'k')
%         plot(output{u,1},output{u,2},'r')
%         title([titlestr ' - ' num2str(u)])
%         axis tight
%         hold off
%         for dep = 1:size(data{u,1},1)
%             X = cell2mat(x(dep,:));
%             Y = cell2mat(data{u,1}(dep,:));
%             subplot(2,size(data{u,1},1),size(data{u,1},1)+dep)
%             scatter(Y,X,1,'k')
%             axis square
%         end
%         fig = gcf;
%         pause
%         close(fig)
        
    end         % u
    
    switch r
        case 1
            ICLS = output;
        case 2
            OT = output;
    end
    
end             % r

clear r data u dep div fig off r rep titlestr u x X Y output