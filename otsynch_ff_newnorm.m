%%
load('f:\desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted.mat','OT');
data = OT; clear OT

load('blacklist.mat')

Window = sort([-0.1 0.1]);
% binWidth = 0.005; int = 5;
binWidth = 0.001; int = 10;
binNum = length(Window(1):binWidth:Window(2))-2;
norminc = 0:0.1:1;

dur = 0.15;
pattern = 'frozen.|b.|i.';                   % These are to remove
pattern2 = '.';                   % These are to keep
f = 'ff';

D=[];

for date=1:size(data,2)
    for site = 1:length(data{date})
        list=combnk(2:length(data{date}{site}),2);
        for pair = 1:size(list,1)
            
            disp(['date: ' num2str(date) '; site: ' num2str(site) '; unit1: ' num2str(list(pair,1)) '; unit2: ' num2str(list(pair,2))]);
            
            unit1 = list(pair,1); unit2 = list(pair,2);
            chck1 = blacklist(:,1) == date;
            chck2 = blacklist(:,2) == site;
            chck3 = blacklist(:,3) == unit1;
            chck4 = blacklist(:,3) == unit2;
            check = chck1 .* chck2 .* (chck3 + chck4);
            
            if sum(check) > 0
                pattern2 = 'FFf1';  
                disp('caught')
            elseif sum(check) == 0
                pattern2 = '.';
            end
            
            unit1 = data{date}{site}{list(pair,1)};
            unit2 = data{date}{site}{list(pair,2)};
            filetypes1 = fieldnames(unit1);
            filetypes2 = fieldnames(unit2);
            filetypes = intersect(filetypes1,filetypes2);
            idx = regexpi(filetypes,pattern);
            idx = cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            idx = regexpi(filetypes,pattern2);
            idx = ~cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            
            clear filetypes1 filetypes2 idx chck* check
            
            if ~isempty(filetypes)
                st_u1 = []; st_u2 = []; sc_u1 = []; sc_u2 = []; bc_u1 = []; bc_u2 = [];
                for file = 1:length(filetypes)
                    st_u1 = [st_u1,unit1.(filetypes{file}).spikes_times];
                    st_u2 = [st_u2,unit2.(filetypes{file}).spikes_times];
                    sc_u1 = [sc_u1,unit1.(filetypes{file}).spikes_count];
                    sc_u2 = [sc_u2,unit2.(filetypes{file}).spikes_count];
                    bc_u1 = [bc_u1,unit1.(filetypes{file}).sensitive];
                    bc_u2 = [bc_u2,unit2.(filetypes{file}).sensitive];
                end
                idx = randperm(size(sc_u1,1));
%                 sh_st_u2 = st_u2(idx,:);
%                 sh_sc_u2 = [sc_u2(:,2:end),sc_u2(:,1)];
                br1 = sum(bc_u1,2);
                br2 = sum(bc_u2,2);
                br1 = br1 > 0;
                br2 = br2 > 0;
                depvar = size(sc_u1,1);
                reps = size(sc_u1,2);
                
                CCG = zeros(depvar,binNum);
                shiftCCG = zeros(depvar,binNum);
                
                for t = 1:depvar
                    for r = 1:reps
                        if ~isempty(st_u1{t,r})
                            for spike = 1:length(st_u1{t,r})
                                times = st_u1{t,r}(spike) - st_u2{t,r};
                                his=hist(times,Window(1):binWidth:Window(2)); %% Here is where you can change binwidth
                                CCG(t,:)=CCG(t,:)+his(2:end-1);
                                
                                % shuffle using the shifted trial number
%                                 if t == size(sc_u1,1)
%                                     times = st_u1{t,r}(spike) - (rand(size(st_u2{1})) * dur);
%                                 else
%                                     times = st_u1{t,r}(spike) - (rand(size(st_u2{t+1,r})) * dur);
%                                 end
                                
                                % shuffle using the same trial number
%                                 times = st_u1{t,r}(spike) - (rand(size(st_u2{t,r})) * dur);
                                
                                % use the shuffled trials made above
%                                 times = st_u1{t,r}(spike) - sh_st_u2{t,r};
                                
                                % regular shift
                                if t==size(sc_u1,1)
                                    times = st_u1{t,r}(spike) - st_u2{1};
                                else
                                    times = st_u1{t,r}(spike) - st_u2{t+1,r};
                                end
                                
                                his=hist(times,Window(1):binWidth:Window(2));
                                shiftCCG(t,:)=shiftCCG(t,:)+his(2:end-1);
                            end
                        end
                    end
                end % for "t"
                
                clear his spike times
                
                holder = cell(1,13);
                holder{1} = [date,site,list(pair,1),list(pair,2)];
                for i = 2:10
                    holder{i} = nan(1,depvar);
                end
                for i = 11:13
                    holder{i}=cell(1,depvar);
                end
                
                for t = 1:depvar

                    check2 = br1(t) .* br2(t);
                    rate1 = mean(sc_u1(t,:))/dur;
                    rate2 = mean(sc_u2(t,:))/dur;
                    
                    % approach 1
%                     norm1 = sqrt(mean(sc_u1(t,:) .* sc_u2(t,:)))/dur;
%                     norm2 = sqrt(mean(sc_u1(t,:) .* sh_sc_u2(t,:)))/dur;
                    
                    % approach 2
                    norm1 = sqrt(rate1 .* rate2);
                    norm2 = norm1;
                    
                    % appraoch 3
% %                     norm1 = sum(CCG(t,:));
% %                     norm2 = sum(shiftCCG(t,:));
                    
                    % approach 4
%                     norm1 = sum(CCG(t,round(binNum/2) - int:round(binNum/2) + int));
%                     norm2 = sum(shiftCCG(t,round(binNum/2) - int:round(binNum/2) + int));
%                     if norm1 == 0
%                         norm1 = 1;
%                     end
%                     if norm2 == 0
%                         norm2 = 1;
%                     end

                    % approach 5
%                     norm1 = 1; norm2 = 1;

%                     SmCCG=(smooth(CCG(t,:))/norm1)/binNum;
%                     SmShiftCCG=(smooth(shiftCCG(t,:))/norm2)/binNum;

                    SmCCG = (CCG(t,:)'/norm1)/binNum;
                    SmShiftCCG = (shiftCCG(t,:)'/norm2)/binNum;
                    
                    SmCCGCorrected=SmCCG-SmShiftCCG;
                    
                    flankS=std([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                    flankM=mean([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                    center = SmCCGCorrected(round(binNum/2) - int:round(binNum/2) + int);
                    
                    PEAK=SmCCGCorrected(round(binNum/2));
                    %                     check1 = max(center)>flankM+5*flankS;
                    check1 = PEAK>flankM+5*flankS;
                    positive=(SmCCGCorrected>=(PEAK/2));
                    pos=diff(positive);
                    less=pos(1:round(binNum/2));
                    more=pos(round(binNum/2):end);
                    Low=find(less==1,1,'last');
                    High=find(more==-1,1,'first');
                    Low=length(less)-Low;
                    bandPEAK=High+Low;
                    
                    if isempty(bandPEAK)
                        bandPEAK=nan;
                        sumband=nan;
                    else
                        sumband=sum(SmCCGCorrected(length(less)-Low:length(less)+High))/bandPEAK;
                    end
                    
                    holder{2}(1,t)=PEAK;
                    holder{3}(1,t)=rate1;
                    holder{4}(1,t)=rate2;
                    holder{5}(1,t)=norm1;
                    holder{6}(1,t)=check2;
                    holder{7}(1,i)=norm2;
                    holder{8}(1,t)=bandPEAK;
                    holder{9}(1,t)=sum(center);
                    holder{10}(1,t)=sumband;
                    holder{11}{1,t}=SmCCG;
                    holder{12}{1,t}=SmShiftCCG;
                    holder{13}{1,t}=SmCCGCorrected;
                    
                    clear positive pos less more Low High bandPEAK sens1 sens2
                    
                end     % for "i" the second time
                
                if isfield(D,f)
                    for i = 1:10
                        if size(D.(f){i},2) > size(holder{i},2)
                            padding = nan(1,size(D.(f){i},2)-size(holder{i},2));
                            D.(f){i}=[D.(f){i};holder{i},padding];
                        elseif size(D.(f){i},2) < size(holder{i},2)
                            padding = nan(size(D.(f){i},1),size(holder{i},2)-size(D.(f){i},2));
                            D.(f){i}=[D.(f){i},padding;holder{i}];
                        else
                            D.(f){i}=[D.(f){i};holder{i}];
                        end
                    end
                    for i = 11:13
                        if size(D.(f){i},2) > size(holder{i},2)
                            padding = cell(1,size(D.(f){i},2)-size(holder{i},2));
                            D.(f){i}=[D.(f){i};holder{i},padding];
                        elseif size(D.(f){i},2) < size(holder{i},2)
                            padding = cell(size(D.(f){i},1),size(holder{i},2)-size(D.(f){i},2));
                            D.(f){i}=[D.(f){i},padding;holder{i}];
                        else
                            D.(f){i}=[D.(f){i};holder{i}];
                        end
                    end
                else
                    for i = 1:length(holder)
                        D.(f){i}=holder{i};
                    end
                end
                
                clear holder depvar rate* i PEAK check SmCCG curves CORR trial flank* sumband SmShiftCCG shiftCCG SmCCGCorrected CCG
                
            end         % for "file"
            
            clear filetypes
            
        end             % for "pair"
    end                 % for "site"
end                     % for "date"

ot_synch = D;

clearvars -except ot_synch

cd('F:\Desktop\WorkingFolder')
save ot_synch_ff_shift_nonsmooth
