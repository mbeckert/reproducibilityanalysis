
load 021-2015-0217-03-FFFrozen2-analysis CURVE Indexer Mean

Azimuths=unique(Indexer(1,:));
Elevations=unique(Indexer(2,:));

AzSAC=zeros(1,length(Azimuths));
ElSAC=zeros(1,length(Elevations));
AzFR=zeros(1,length(Azimuths));
ElFR=zeros(1,length(Elevations));

for i=1:length(Azimuths);
    A=Indexer(1,:)==Azimuths(i);
    AzSAC(i)=mean(CURVE(A));
    AzFR(i)=mean(Mean(A));
end

for i=1:length(Elevations);
    E=Indexer(2,:)==Elevations(i);
    ElSAC(i)=mean(CURVE(E));
    ElFR(i)=mean(Mean(E));
end

figure(1)
scatter(Indexer(1,:),Indexer(2,:),CURVE*10);
xlabel('Azimuth'); ylabel('Elevation'); title('SAC');

figure(2)
scatter(Indexer(1,:),Indexer(2,:),Mean*10);
xlabel('Azimuth'); ylabel('Elevation'); title('FR');

figure(3)
scatter(ElSAC,ElFR);
xlabel('SAC');ylabel('FR');title('Elevation');

figure(4)
scatter(AzSAC,AzFR);
xlabel('SAC');ylabel('FR');title('Azimuth');

clear i A E Azimuths Elevations