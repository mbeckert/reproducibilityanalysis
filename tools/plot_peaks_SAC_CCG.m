load('f:\desktop\WorkingFolder\synchony\ot_synch_ff_del_fr.mat')
syn = ot_synch.ff{13};

ccg = cell(size(syn, 1), 1);

for i = 1:length(ccg)
    cur = cell2mat(syn(i, :));
    ccg{i} = nanmean(cur, 2);
end

clear syn i cur ot_synch

ccg = cell2mat(ccg');

plot(nanmean(ccg, 2))

%%
fol = 'f:\desktop\WorkingFolder\working_reprod\frompython\';
data = cell(4, 1);

for i = 1:4
    switch i
        case 1
            load([fol 'repro_itd_curve_50micro.mat'])
        case 2
            load([fol 'repro_ild_curve_50micro.mat'])
        case 3
            load([fol 'repro_ff_curve_50micro.mat'])
        case 4
            load([fol 'repro_ot_curve_50micro.mat'])
    end
    
    data{i} = (curve - repmat(fr', [1 size(curve, 2)]) .^ 2) * cw ./ fr'; 
    
end

clear neuron full_fr fr depvar cw curve i fol

data = cell2mat(cellfun(@nanmean, data, 'UniformOutput', 0));
% plot(data(:, 901:1099)')
plot(data')

%%
fol = 'f:\desktop\WorkingFolder\working_reprod\frompython\';
% load([fol 'repro_ot_curve_4ms.mat'])
load([fol 'repro_ot_curve_50micro.mat'])

neu = unique(neuron);
cur = nan(length(neu), size(curve, 2));

for n = 1:length(neu)
    ix = neuron == neu(n);
    [~, m] = max(fr(ix));
    cur(n, :) = (curve(m, :) - fr(m) ^ 2) * cw / fr(m);
end

clear curve cw depvar fr full_fr ix m n neu neuron

data = nanmean(cur);
plot(data')
%%
data = cell(5, 1);

for i = 1:5
    switch i
        case 1
            load('repro_itd_curve_base_50micro.mat')
        case 2
            load('repro_ild_curve_base_50micro.mat')
        case 3
            load('repro_az_curve_base_50micro.mat')
        case 4
            load('repro_el_curve_base_50micro.mat')
        case 5
            load('repro_ot_curve_base_50micro.mat')
    end
    
    curve = (curve - repmat(fr', [1 size(curve, 2)]) .^ 2) * cw ./ fr'; 
    neu = unique(neuron);
    data{i} = nan(length(neu), size(curve, 2));
    
    for n = 1:length(neu)
        ix = neuron == neu(n);
        [~, m] = max(fr(ix));
        data{i}(n, :) = curve(m, :);
    end

    
end

clear neuron full_fr fr depvar cw curve i

data = cell2mat(cellfun(@nanmean, data, 'UniformOutput', 0));
plot(data(:, 901:1099)')

%%

mw = 0.04;

for i = 1:6
    switch i
        case 1
            load('repro_ot_curve_base_50micro.mat')
        case 2
            load('repro_ot_curve_base_100micro.mat')
        case 3
            load('repro_ot_curve_1ms.mat')
        case 4
            load('repro_ot_curve_2ms.mat')
        case 5
            load('repro_ot_curve_4ms.mat')
        case 6
            load('repro_ot_curve_5ms.mat')
    end
    center = round(size(curve, 2) / 2);
    win = round(mw / cw);
    figure(i)
    curve = (curve - repmat(fr', [1 size(curve, 2)]) .^ 2) * cw ./ fr';
    plot(-mw:cw:mw, curve(:, center - win:center + win)')
    
end
