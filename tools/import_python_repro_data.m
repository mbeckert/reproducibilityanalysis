%%
% First import the data from the saved csv file and name the variable 'd'

fr = d.fr;
key = [d.neuron, d.depvar];
repro = d.strength;
clear d

% curve = curve';
% %

neu = unique(key(:, 1));

r_brette = cell(length(neu), 1);
FR = cell(length(neu), 1);
% sac = cell(length(neu), 1);

for n = 1:length(neu)
    idx = key(:, 1) == neu(n);
    r_brette{n} = repro(idx)';
    FR{n} = fr(idx)';
%     sac{n} = curve(idx, :);
end

fr = FR;

clear neu FR idx n repro key curve

