load('f:\desktop\WorkingFolder\distance\data\distance_correction_c_04_10_long.mat', 'key', 'cfun')

% cor_vp = @(x) cfun{1}(1) * x .^ 3 + cfun{1}(2) * x .^ 2 + cfun{1}(3) * x + cfun{1}(4);
% cor_euc = @(x) cfun{2}(1) * x .^ 3 + cfun{2}(2) * x .^ 2 + cfun{2}(3) * x + cfun{2}(4);
% cor_es = @(x) cfun{3}(1) * x .^ 3 + cfun{3}(2) * x .^ 2 + cfun{3}(3) * x + cfun{3}(4);

cor_vp = @(x) cfun{1}(1) * x + cfun{1}(2);
cor_euc = @(x) cfun{2}(1) * x + cfun{2}(2);
cor_es = @(x) cfun{3}(1) * x + cfun{3}(2);