load('f:\desktop\WorkingFolder\tetrodekeys\SpeakerIndex.mat','SpeakerIndex')

for t = 1:2
    
    mw_time = 0.005;
    
    load('repro_ot_curve_100micro.mat')
    
    center = round(size(curve, 2) / 2);
    win = round(mw_time / cw);
    winsac = curve(:, center - win + 1:center + win - 1);
%     winsac = curve(:, center);
    f = repmat(fr' .^2, 1, size(winsac, 2));
    repro = sum(winsac - f, 2) * cw ./ fr';
    
    neu = unique(neuron);
    FR = cell(length(neu), 1);
    REP = cell(length(neu), 1);
    
    for n = 1:length(neu)
        idx = neuron == neu(n);
        REP{n} = repro(idx)';
        FR{n} = fr(idx);
    end
    
    idx = cellfun(@sum, cellfun(@isnan, FR, 'UniformOutput', 0)) == 144;
    FR(idx) = [];
    REP(idx) = [];
    idx = cellfun(@length, FR) == 1;
    FR(idx) = [];
    REP(idx) = [];
    
    clear winsac win repro neuron neu n mw_time idx full_fr fr f depvar cw curve center
    
    for i = 1:length(FR)
        
        best = min(SpeakerIndex(FR{i} == max(FR{i}),t));
        idx = SpeakerIndex(:,t) == best;
        
        FR{i} = FR{i}(idx);
        REP{i} = REP{i}(idx);
        
        clear idx best
        
    end
    
    clear i
    
    switch t
        case 1
            save repro_otaz_curve_10ms -regexp ^(?!(t|SpeakerIndex)$).
        case 2
            save repro_otel_curve_10ms -regexp ^(?!(t|SpeakerIndex)$).
    end
    
end

clear t