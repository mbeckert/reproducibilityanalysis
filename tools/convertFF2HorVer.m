%% conversion for ICcL
cd('f:\desktop\WorkingFolder')

for t = 1:2
    
load('iclsFF_brette_update_1.mat')

for i = 1:length(Index)
    idx = find(Index{i}(t,:) == mode(Index{i}(t,:)));
    CURVE{i} = CURVE{i}(idx);
    HIST{i} = HIST{i}(idx);
    HW{i} = HW{i}(idx);
    Index{i} = Index{i}(:,idx);
    Mean{i} = Mean{i}(idx);
    PEAK{i} = PEAK{i}(idx);
    SACcurve{i} = SACcurve{i}(idx);
    SACnorm{i} = SACnorm{i}(idx);
    SPtimes{i} = SPtimes{i}(idx,:);
end

clear i idx

switch t
    case 1 
        save iclsAZ_brette_update_1 -regexp ^(?!(t)$).
    case 2
        save iclsEL_brette_update_1 -regexp ^(?!(t)$).
end

end
        
clear t

%% conversion for OT

cd('f:\desktop\WorkingFolder')
load('f:\desktop\WorkingFolder\tetrodekeys\SpeakerIndex.mat','SpeakerIndex')

for t = 1:2
load('otFF_joris_update_1.mat');

for i = 1:length(CURVE)
    
    best = min(SpeakerIndex(Mean{i} == max(Mean{i}),t));   
    idx = SpeakerIndex(:,t) == best;
    
    CURVE{i} = CURVE{i}(idx);
    HIST{i} = HIST{i}{1}(:,idx);
    HW{i} = HW{i}(idx);
    Mean{i} = Mean{i}(idx);
    PEAK{i} = PEAK{i}(idx);
    SACcurve{i} = SACcurve{i}{1}(idx);
    SACnorm{i} = SACnorm{i}{1}(idx);
    SPtimes{i} = SPtimes{i}{1}(idx,:);
    SPtimes{i} = cellfun(@transpose, SPtimes{i}, 'UniformOutput', 0);
    
    clear idx best

end

clear i

switch t
    case 1 
        save otAZ_joris_update_1 -regexp ^(?!(t|SpeakerIndex)$).
    case 2
        save otEL_joris_update_1 -regexp ^(?!(t|SpeakerIndex)$).
end

end
    
clear t 