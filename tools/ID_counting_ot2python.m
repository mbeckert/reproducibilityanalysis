load('f:\desktop\WorkingFolder\distance\data\FreeField_OT_wDelay','OT')
data = OT; clear OT

count = 0;
out = nan(200, 4);

for d = 1:length(data)
    for s = 1:length(data{d})
        for u = 2:length(data{d}{s})
            if isfield(data{d}{s}{u}, 'FFf1') || isfield(data{d}{s}{u}, 'FFf2')
                ix = find(isnan(out(:, 1)), 1, 'first');
                out(ix, :) = [count, d, s, u];
                count = count + 1;
            end
        end
    end
end

ix = find(isnan(out(:, 1)), 1, 'first');
out(ix:end, :) = [];

clear d s u ix count data