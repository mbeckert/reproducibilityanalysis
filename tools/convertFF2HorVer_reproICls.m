load('repro_ff_curve_100micro.mat')
load('ff_indexer.mat')

neu = unique(neuron);

keep = cell(length(neu), 2);

for n = 1:length(neu)
    ix = neuron == neu(n);
    keep{n, 1} = Indexer(ix, 1) == mode(Indexer(ix, 1));
    keep{n, 2} = Indexer(ix, 2) == mode(Indexer(ix, 2));
end

keep = cell2mat(keep);

clear neu n ix Indexer

load('repro_ff_curve_100micro.mat')

neuron = neuron(keep(:, 1));
% full_fr = full_fr(keep(:, 1));
fr = fr(keep(:, 1));
depvar = depvar(keep(:, 1));
curve = curve(keep(:, 1), :);

save repro_el_curve_100micro neuron fr depvar curve cw

load('repro_ff_curve_100micro.mat')

neuron = neuron(keep(:, 2));
% full_fr = full_fr(keep(:, 2));
fr = fr(keep(:, 2));
depvar = depvar(keep(:, 2));
curve = curve(keep(:, 2), :);

save repro_az_curve_100micro neuron fr depvar curve cw