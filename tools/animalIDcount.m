d = ff;
animal = {'006','021','023','032','029'};
chck = nan(length(d),length(animal));

for i = 1:length(animal)
    ty = animal{i};
    tmp = mean(ty == d,2);
    tmp(tmp ~= 1) = 0;
    chck(:,i) = tmp;
end

out = sum(chck);
