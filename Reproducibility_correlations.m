%% Run correlations for each neuron

out = cell(1,6);
met = 4;

for t = 1:6
    switch t
        case 1
            data = iclsAZ;
        case 2
            data = iclsEL;
        case 3
            data = iclsITD;
        case 4
            data = iclsILD;
        case 5
            data = otAZ;
        case 6
            data = otEL;
    end
    
    c = cellfun(@(x) corr(x(2,:)',x(met,:)','rows','complete'),data,'UniformOutput',0);
    c = cellfun(@(x) 0.5*log((1+x)/(1-x)), c, 'UniformOutput',0);
    c = c';
    chck = cellfun(@(x) sum(isfinite(x(met,:)))<3, data);
    c = cell2mat(c);
    c(chck) = nan;
    out{t} = c;
    
end

m = max(cellfun(@length,out));
for t = 1:6
    out{t} = [out{t};nan(m-length(out{t}),1)];
end
out = cell2mat(out);

clear met t c m chck

%% Run correlations of entire population

cor = nan(6,2);
val = cell(1,12);
met = 3;

for t = 1:6
    switch t
        case 1
            data = iclsAZ;
        case 2
            data = iclsEL;
        case 3
            data = iclsITD;
        case 4
            data = iclsILD;
        case 5
            data = otAZ;
        case 6
            data = otEL;
    end
    
    data = cell2mat(data);
    [cor(t,1),cor(t,2)] = corr(data(2,:)',data(met,:)','rows','pairwise');
    val{2*t-1} = data(2,:)';
    val{2*t} = data(met,:)';
    subplot(3,2,t)
    scatter(data(2,:),data(met,:),1,'k','fill')
    
end

m = max(cellfun(@length,val));
for i = 1:length(val)
   val{i} = [val{i}; nan(m - length(val{i}),1)];    
end
val = cell2mat(val);

clear met t m i
    
%% compile data so we have something similar to the synchrony output. This
% is used to check the mutual information

val = cell(1,6);

for t = 1:6
    switch t
        case 1
            data = iclsAZ;
        case 2
            data = iclsEL;
        case 3
            data = iclsITD;
        case 4
            data = iclsILD;
        case 5
            data = otAZ;
        case 6
            data = otEL;
    end
    
    data = cell2mat(data);
    data = data';
    
    val{t} = data;
    
end

clear data t