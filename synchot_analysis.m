% run "synchot" first and load that file for analysis
% also uses "SpRF2HorVer" to convert ot FF to azimuth/elevation
%  - 7/6/17 : MVB

% also see "SynchOT_correlations" as an alternative version that was made
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Data munging: convert SpRF to azimuth and elevation
% MAKE SURE LOAD AND SAVE FILES MATCH

% % load data
cd('F:\Desktop\WorkingFolder')
pathstartup
load('ot_synch.mat')

FFf1 = ot_synch.FFf1;
FFf2 = ot_synch.FFf2;
FF = cell(1,10);
FF{1} = nan(200,4);
FF{2} = nan(200,144);
FF{3} = nan(200,144);
FF{4} = nan(200,144);
FF{5} = nan(200,144);
FF{6} = nan(200,144);
FF{7} = nan(200,144);
FF{8} = nan(200,144);
FF{9} = nan(200,144);
FF{10} = nan(200,144);

count = 1;

for d = 1:max(unique([FFf1{1}(:,1);FFf2{1}(:,1)]))
    for s = 1:max(unique([FFf1{1}(:,2);FFf2{1}(:,2)]))
        for u1 = 1:max(unique([FFf1{1}(:,3);FFf2{1}(:,3)]))
            for u2 = 1:max(unique([FFf1{1}(:,4);FFf2{1}(:,4)]))
                i1 = FFf1{1}(:,1) == d;
                i2 = FFf1{1}(:,2) == s;
                i3 = FFf1{1}(:,3) == u1;
                i4 = FFf1{1}(:,4) == u2;
                idx1 = i1.*i2.*i3.*i4;
                i1 = FFf2{1}(:,1) == d;
                i2 = FFf2{1}(:,2) == s;
                i3 = FFf2{1}(:,3) == u1;
                i4 = FFf2{1}(:,4) == u2;
                idx2 = i1.*i2.*i3.*i4;
                idx1 = logical(idx1);
                idx2 = logical(idx2);
                if sum([idx1;idx2]) > 0
                    FF{1}(count,:) = [d,s,u1,u2];
                    for m = 2:10
                        D = [FFf1{m}(idx1,:);FFf2{m}(idx2,:)];
                        FF{m}(count,:) = nanmean(D,1);
                    end
                    count = count + 1;
                end
            end
        end
    end
end

L = find(isnan(FF{1}(:,1)),1,'first');
for m = 1:10
    FF{m}(L:end,:) = [];
end

ot_synch.FF = FF;
ot_synch = rmfield(ot_synch,'FFf1');
ot_synch = rmfield(ot_synch,'FFf2');

az = cell(1,10);
el = cell(1,10);
az{1} = ot_synch.FF{1};
el{1} = ot_synch.FF{1};

for m = 2:10
    az{m} = nan(size(ot_synch.FF{2},1),21);
    el{m} = nan(size(ot_synch.FF{2},1),17);
    for i = 1:size(ot_synch.FF{m},1)
        [az{m}(i,:),el{m}(i,:)] = SpRF2HorVer(ot_synch.FF{m}(i,:));
    end
end

ot_synch.az = az;
ot_synch.el = el;
ot_synch = rmfield(ot_synch,'FF');

clearvars -except ot_synch

cd('F:\Desktop\Data\WorkingFolder')

save ot_synch
%% More Munging
% Check different normalization methods and if the non-sensitive stimuli
% are omitted or transformed into 0's
%   see "synch_varyingparameters.mat" for the data with different
%   combinations of these parameters

load('ot_synch')
% load('ot_synch_sensboth')
% load('ot_synch_sensflank')
% load('ot_synch_sensfr')
filetypes = fieldnames(ot_synch);

% % set up holders for data
peak = cell(1,length(filetypes));
bw = cell(1,length(filetypes));
auc = cell(1,length(filetypes));
fr = cell(1,length(filetypes));

norm = 1;   % do you want to normalize the data for each cell?

for t = 1:length(filetypes)
    data = ot_synch.(filetypes{t});
    
    fr{t} = nan(size(data{3}));
    for i = 1:length(fr{t}(:))
        fr{t}(i) = sqrt(data{3}(i)*data{4}(i));
    end
    
    % check "normalization" for which type of normalization being used
    peak{t} = normalization(data{2},norm,1);
    bw{t} = normalization(data{8},norm,1);
    auc{t} = normalization(data{10},norm,1);
    fr{t} = normalization(fr{t},norm,1);
%     chck = peak{t} <= 0;
%     correct = nan;
%     peak{t}(chck) = correct;
%     bw{t}(chck) = correct;
%     auc{t}(chck) = correct;
%     fr{t}(chck) = correct;
%     chck = data{6} == 0;
%     correct = 0;
%     peak{t}(chck) = correct;
%     bw{t}(chck) = correct;
%     auc{t}(chck) = correct;
%     chck = data{7} == 0;
%     correct = nan;
%     peak{t}(chck) = correct;
%     bw{t}(chck) = correct;
%     auc{t}(chck) = correct;
%     fr{t}(chck) = correct;

    clear chck i
end

clear norm t data ot_synch filetypes correct

% % correlations

corr_full = nan(3,length(fr)*2);
corr_cells = cell(1,length(fr));
val = cell(1,length(fr));
p = [1,3,5,7,9,11;2,4,6,8,10,12];

for t = 1:length(fr)
    corr_cells{t} = nan(size(fr{t},1),3);
    val{t} = cell(1,6);
    for m = 1:3
        switch m
            case 1
                data = peak{t};
            case 2
                data = bw{t};
            case 3
                data = auc{t};
        end
        for i = 1:size(data,1)
            if sum(isfinite(data(i,:))) > 2
                c = corr(fr{t}(i,:)',data(i,:)','rows','pairwise');
                c = 0.5*log((1+c)/(1-c));       % z-transformation
                corr_cells{t}(i,m) = c;
            end
        end
        [corr_full(m,p(1,t)),corr_full(m,p(2,t))] = corr(fr{t}(:),data(:),'rows','pairwise');
        val{t}{2*m-1} = fr{t}(:);
        val{t}{2*m} = data(:);
        figure(t)
        subplot(3,1,m)
        scatter(fr{t}(:),data(:),2,'k','fill')
    end
end

for t = 1:length(val)
    m = max(cellfun(@length,val{t}));
    for i = 1:length(val{t})
        val{t}{i} = [val{t}{i}; nan(m - length(val{t}{i}),1)];
    end
    val{t} = cell2mat(val{t});
end

clear t p m data i c

%% binning

x = 0:0.1:1;
val = cell(length(fr),3);
out = cell(length(fr),3);

for m = 1:3
    for t = 1:length(fr)
        switch m
            case 1
                data = peak{t}(:);
            case 2
                data = bw{t}(:);
            case 3
                data = auc{t}(:);
        end
        
        [~,idx] = histc(fr{t}(:),x);
        out{t,m} = zeros(3,length(x));
        
        for h = 1:length(x)
            i = idx == h;
            val{t,m}{h} = data(i);
            out{t,m}(1,h) = nanmedian(data(i));
            out{t,m}(2,h) = nanmean(data(i));
            out{t,m}(3,h) = nanstd(data(i));
        end
        
        clear i h data idx
        
    end
    
end

for m = 1:3
    for t = 1:length(fr)
        ma = max(cellfun(@length,val{t,m}));
        for v = 1:length(val{t,m})
            val{t,m}{v} = [val{t,m}{v};nan(ma-length(val{t,m}{v}),1)];
        end
        val{t,m} = cell2mat(val{t,m});
    end
end
clear m t x ma v
%%
m = 1;
for t = 1:length(fr)
    figure(t)
    subplot(2,1,1)
    errorbar(out{t,m}(1,:),out{t,m}(3,:))
    subplot(2,1,2)
    errorbar(out{t,m}(2,:),out{t,m}(3,:))
end

clear t m

%%
clear all
load('synch_varyingparameters.mat')

list = who;
data = nan(size(list,1),4);

for i = 1:length(list)
    eval(['d = ' list{i} ';'])
    data(i,1) = d(1,1);
    data(i,2) = d(1,3);
    data(i,3) = d(1,5);
    data(i,4) = d(1,7);
end

clearvars -except data*

