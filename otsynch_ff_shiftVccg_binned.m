load('ot_synch_ff_shift.mat')
ff = ot_synch.ff;
clear ot_synch

correct = nan(size(ff{11}{1}));
normalize = [1,1];

chck = ff{6};
chck = chck == 0;
chck = find(chck == 1);
for i = 1:length(chck)
    ff{11}{chck(i)} = correct;
    ff{12}{chck(i)} = correct;
    ff{13}{chck(i)} = correct;
end

GM = sqrt(ff{3} .* ff{4});
GM = normalization(GM,normalize(1));

edges = linspace(0,1,11);

[~,~,idx] = histcounts(GM,edges);

CCG = cell(size(GM,1),max(unique(idx)));
SH = cell(size(GM,1),max(unique(idx)));
COR = cell(size(GM,1),max(unique(idx)));
gm = nan(size(GM,1),max(unique(idx)));

for n = 1:size(GM,1)
    for f = 1:max(unique(idx))
        d = cell2mat(ff{11}(n,idx(n,:) == f));
        d(isnan(d)) = 0;
        CCG{n,f} = sum(d,2)/size(d,2);
        d = cell2mat(ff{12}(n,idx(n,:) == f));
        d(isnan(d)) = 0;
        SH{n,f} = sum(d,2)/size(d,2);
        d = cell2mat(ff{13}(n,idx(n,:) == f));
        d(isnan(d)) = 0;
        COR{n,f} = sum(d,2)/size(d,2);
        d = GM(n,idx(n,:) == f);
        gm(n,f) = nanmean(d);
    end
end

clear edges idx n f d ff GM chck correct

binNum = 199; int = 10;
% binNum = 39; int = 5;

ccg = nan(size(CCG));
sh = nan(size(SH));
cor = nan(size(COR));

for n = 1:size(ccg,1)
    for sp = 1:size(ccg,2)
        if ~isempty(CCG{n,sp})
            ccg(n,sp) = sum(CCG{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        end
        if ~isempty(SH{n,sp})
            sh(n,sp) = sum(SH{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        end
        if ~isempty(COR{n,sp})
            cor(n,sp) = sum(COR{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        end
    end
end

clear binNum int n sp 

% % normalization and organization

tmp = ccg;
tmp(tmp == 0) = nan;
rat = sh ./ tmp;
dif = tmp - sh;
clear tmp

ccg = normalization(ccg,normalize(2));
sh = normalization(sh,normalize(2));
cor = normalization(cor,normalize(2));
sh(~isfinite(sh)) = 0;
ccg(~isfinite(ccg)) = 0;
cor(~isfinite(cor)) = 0;
output{1} = gm;
output{2} = ccg;
output{3} = sh;
output{4} = cor;

clear CCG SH COR

% % plotting
rangex = [0,1,11];
rangey = [0,1,11];
cmap = flipud(gray(64));

figure(1)

for m = 1:3
    data = [output{1}(:),output{m+1}(:)];
    [mat,~,~] = hist2D(data,rangex,rangey);
    subplot(3,2,m*2-1)
    imagesc(mat)
    colormap(cmap)
    colorbar
    axis square
    subplot(3,2,m*2)
    scatter(output{1}(:),output{m+1}(:),2,'k','fill')
    axis square
end

% % correlation analysis
corr_cell = cell(1,3);
corr_full = nan(1,3);

for m = 2:4
    corr_full(1,m-1) = corr(output{1}(:),output{m}(:),'rows','pairwise');
    corr_cell{1,m-1} = nan(size(output{1},1),1);
    for i = 1:size(output{1},1)
        if sum(isfinite(output{m}(i,:))) > 2
            c = corr(output{1}(i,:)',output{m}(i,:)','rows','pairwise');
%             corr_cell{m-1}(i) = 0.5*log((1+c)/(1-c));
            corr_cell{m-1}(i) = c;
        end
    end
end

corr_cell = cell2mat(corr_cell);
% x = [output{1}(:),output{2}(:),output{3}(:),output{4}(:)];

clear i rangex rangey cmap m mat sh gm cor ccg data c normalize

