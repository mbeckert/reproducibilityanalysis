%% normalized heatmaps (NOT USING ANYMORE)

names = {'Normalized Geometric Mean Firing Rate','Normalized Synchrony'};
figure(13)

for t = 1%:4
    if t == 1
        load('f:\desktop\WorkingFolder\ot_synch_ff.mat')
        gm = GM;
        syn = cor;
        gm = normalization(gm,1);
        syn = normalization(syn,1);
    else
        fol = 'f:\desktop\Data\Array\analysis\corr\';
        load([fol 'data_compile_norm'], 'plotdata_synch', 'name_synch');
        gm = plotdata_synch{1}(:,1);
        syn = plotdata_synch{1}(:,t);
    end
    
    subplot(2,2,t)
    
    a = gm(:);
    b = syn(:);
    
    [cc, pp] = scatterheatmapplot(a,b, names);
    
end

%% Compare nearby and distant

fol = 'f:\desktop\Data\Array\analysis\corr\';
load([fol 'data_compile'], 'max_synch');
distant = max_synch{1};

load('ot_synch_ff.mat');
nearby = [max(GM,[],2), max(ccg,[],2), max(sh,[],2), max(cor,[],2)];

clear GM ccg sh cor fol max_synch

figure(14)

x = 1:2;
labels = {'nearby','distant'};
titles = {'ccg','sh','cor'};

for i = 2:4
    y = {nearby(:,i), distant(:,i)};
    subplot(1,3,i-1)
    boxplot2_points(x,y)
    xlim([0 3])
    xticks([1,2])
    xticklabels(labels)
    title(titles{i-1})
end

clear x y i

%% Regression Plots!!!!

load('f:\desktop\WorkingFolder\synchony\ot_synch_ff.mat')
cmpl = flipud(gray);

if exist('idx','var')
    ccg(idx) = nan;
    sh(idx) = nan;
    cor(idx) = nan;
end

figure

for t = 1:3
    switch t
        case 1
            b = ccg(:);
        case 2
            b = sh(:);
        case 3
            b = cor(:);
    end
    a = GM(:);
    subplot(1,3,t)
    
    [mat,~,~] = hist2D([a,b], [0,max(a),7], [0,max(b),6]);
    imagesc(mat)
    
    ax1 = gca;
    axis square
    ax1_pos = ax1.Position;
    ax2 = axes('Position',ax1_pos,...
        'XAxisLocation','top',...
        'YAxisLocation','right',...
        'Color','none');
    
    xlabel('Geometric mean firing rate')
    ylabel('Synchrony')
    
    colormap(cmpl)
    hold on
    scatter(a, b, 'Parent', ax2, 2, 'r')
    
    axis square
    idx = isnan(a);
    a(idx) = []; b(idx) = [];
    idx = isnan(b);
    a(idx) = []; b(idx) = [];
    p = polyfit(a, b, 1);
    v = polyval(p,a);
    plot(a, v, 'b', 'LineWidth', 3)
    [cc, pp] = corr(a,b,'rows','pairwise');
    title([num2str(cc) ' ---- ' num2str(pp)])

end