%% get ratio of shift from ccg; requires running 'shiftVccg'
load('otsynch_ff_shiftVccg.mat')

output = cell(1,3);
normalize = [1,0];

gm = normalization(gm,normalize(1));
output{1} = normalization(ccg,normalize(2));
output{2} = normalization(sh,normalize(2));
output{3} = normalization(cor,normalize(2));

[~,~,idx] = histcounts(gm,0:0.1:1);

clear sh gm cor ccg normalize

binnum = max(unique(idx));

data = cell(1,binnum);

for i = 1:binnum
    c = output{1}(idx == i);
    s = output{2}(idx == i);
    data{i} = s ./ c;
    data{i} = data{i}(isfinite(data{i}));
end

m = max(cell2mat(cellfun(@length,data,'UniformOutput',0)));
for i = 1:binnum
    data{i} = [data{i}; nan(m - length(data{i}),1)];
end

data = cell2mat(data);
% data(data > 1) = 1;
x = data(:,5:end);
x = x(:);

clear i c s binnum m idx

%% plot summed ccgs; requires running 'otsynch_ff'
load('ot_synch_ff_shift.mat')
ff = ot_synch.ff;
clear ot_synch

% binNum = 39;
binNum = 199;

sh = zeros(binNum,1);
ccg = zeros(binNum,1);
cor = zeros(binNum,1);

for n = 1:size(ff{11},1)
    for s = 1:size(ff{11},2)
        if length(ff{11}{n,s}) == sum(isfinite(ff{11}{n,s}))
            ccg = ccg + ff{11}{n,s};
            sh = sh + ff{12}{n,s};
            cor = cor + ff{13}{n,s};
        end
    end
end

% x = linspace(-100,100,39);

plot(ccg,'g');
hold on
plot(sh,'r');
plot(cor,'k');
hold off

clear n s x

%% normalize each cell by the max of the standard ccg

load('ot_synch_ff_shift.mat')
ff = ot_synch.ff;
clear ot_synch

gm = sqrt(ff{3} .* ff{4});
CCG = ff{11};
SH = ff{12};
COR = ff{13};

for n = 1:size(CCG,1)
    m = max(cellfun(@max,CCG(n,:)));
    CCG(n,:) = cellfun(@(x) x/m, CCG(n,:),'UniformOutput',0);
    SH(n,:) = cellfun(@(x) x/m, SH(n,:),'UniformOutput',0);
    COR(n,:) = cellfun(@(x) x/m, COR(n,:),'UniformOutput',0);
end

% binNum = 39; int = 5;
binNum = 199; int = 10;

ccg = nan(size(ff{2}));
sh = nan(size(ff{2}));
cor = nan(size(ff{2}));

for n = 1:size(ccg,1)
    for sp = 1:size(ccg,2)
        ccg(n,sp) = sum(CCG{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        sh(n,sp) = sum(SH{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        cor(n,sp) = sum(COR{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
    end
end

correct = nan;
chck = ff{5};
chck = chck == 0;
ccg(chck) = correct; sh(chck) = correct; cor(chck) = correct;
chck = ff{6};
chck = chck == 0;
ccg(chck) = correct; sh(chck) = correct; cor(chck) = correct;

clear ff binNum n sp chck correct int COR SH CCG m

save otsynch_ff_shiftVccg_normccg









