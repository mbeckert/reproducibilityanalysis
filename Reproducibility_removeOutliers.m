outlier_tally = nan(3,6);

for t = 1:6
    switch t
        case 1
            data = az;
        case 2
            data = el;
        case 3
            data = itd;
        case 4
            data = ild;
        case 5
            data = otAZ;
        case 6
            data = otEL;
    end
    
    outcount = 0;
    l = size(data,2);
    
    for p = 1:4
    
    qRange = quantile(data(p,:),[0.25,0.5,0.75]);
    iqr = qRange(3)-qRange(1);
    outlierRange = [qRange(1)-1.5*iqr qRange(3)+1.5*iqr];
    
    outlier_idx = data(p,:) < outlierRange(1) | data(p,:) > outlierRange(2);
    
    data(:,outlier_idx) = [];
    
    outcount = outcount + sum(outlier_idx);
    
    end
    
    outlier_tally(1,t) = outcount;
    outlier_tally(2,t) = outcount/l;
    
    clear outcount l qRange iqr outlierRange outlier_idx p
    
    switch t
        case 1
            az_trim = data;
        case 2
            el_trim = data;
        case 3
            itd_trim = data;
        case 4
            ild_trim = data;
        case 5
            otAZ_trim = data;
        case 6
            otEL_trim = data;
    end
    
end

clear t data