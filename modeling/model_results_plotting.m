cd('f:\desktop\WorkingFolder\model')

%% plot average curves

count = 0;

for ty = 1:2
    switch ty
        case 1
            type = 'ILD';
        case 2
            type = 'ITD';
    end
    
    for n = 1:2
        count = count + 1;
        
        for t = 1:5
            switch t
                case 1
                    file = [type 'm.mat'];
                case 2
                    file = [type 'c3.mat'];
                case 3
                    file = [type 'c6.mat'];
                case 4
                    file = [type 'c9.mat'];
                case 5
                    file = [type 'c12.mat'];
            end
            
            load(file)
            
            if n == 1
                data = cellfun(@(x) x/max(x), data, 'UniformOutput', 0);
            end
            
            figure(count)
            subplot(2,3,t)
            nNeurons = size(data{1},2);
            c = linspace(-75,75,nNeurons);
            
            in = cell2mat(data(:,1));
            out = cell2mat(data(:,2));
            
            errorbar(c, nanmean(in), nanstd(in), 'k')
%             plot(c, nanmean(in), 'k')
            hold on
            errorbar(c, nanmean(out), nanstd(out), 'r')
%             plot(c, nanmean(out), 'r')
            xlabel('azimuth')
            ylabel('firing rate')
            title(file)
            axis square 
            
        end
    end
end

clear t in out c file type

figure(5)
for n = 1:2
    load('inout.mat')
    if n == 1
        data = cellfun(@(x) x/max(x), data, 'UniformOutput', 0);
    end
    c = linspace(1,50,500);
    in = cell2mat(data(:,1));
    out = cell2mat(data(:,2));
    
    subplot(1,2,n)
    errorbar(c, mean(in), std(in), 'k')
    hold on
    errorbar(c, mean(out), std(out), 'r')
    xlabel('input rate')
    ylabel('output rate')
    title('input/ouput')
    axis square
    xlim([0 25])
    
end

%% run some quick analysis

inout = cell(2,4);
sps = cell(1,4);
hw = cell(2,4);

for ty = 1:2
    switch ty
        case 1
            type = 'ITD';
        case 2
            type = 'ILD';
    end
    
    for t = 1:5
        switch t
            case 1
                file = [type 'm.mat'];
            case 2
                file = [type 'c3.mat'];
            case 3
                file = [type 'c6.mat'];
            case 4
                file = [type 'c9.mat'];
            case 5
                file = [type 'c12.mat'];
        end
        
        load(file)
        
        io = cellfun(@max, data);
        inout{ty,t} = io(:,1) - io(:,2);
        
        h = cellfun(@(x) x > max(x)/2, data, 'UniformOutput', 0);
        h = cellfun(@(x) find(x(round(length(x)/2):end) == 0, 1, 'first'), h, 'UniformOutput', 0);
        h = cell2mat(h);
        
        hw{ty,t} = h(:,2) - h(:,1);
        
        if ty == 1
%             s = cellfun(@(x) 1 - max(x(350:end))/max(x), data, 'UniformOutput', 0);
            s = cellfun(@(x) max(x(350:end)), data, 'UniformOutput', 0);
            s = cell2mat(s);
            sps{1,t} = s(:,2)./s(:,1);
        end
        
    end
end

clear ty t io h s data file type

%% plot analysis data

labels = {'model', 'c3', 'c6', 'c9', 'c12'};
type = {'ITD', 'ILD'};


for ty = 1:2
    
    figure(1)
    subplot(1,2,ty)
    boxplot2_points(1:size(hw,2), hw(ty,:))
    title(['half-width ' type{ty}])
    xticks(1:length(labels))
    xticklabels(labels)
    xlim([0 size(hw,2)+1])
    axis square
    
    figure(2)
    subplot(1,2,ty)
    boxplot2_points(1:size(inout,2), inout(ty,:))
    title(['inout ' type{ty}])
    xticks(1:length(labels))
    xticklabels(labels)
    xlim([0 size(inout,2)+1])
    axis square
end

figure(3)
boxplot2_points(1:size(sps,2), sps)
xlim([0 size(sps,2)+1])
title('side-peak suppression ITD')
xticks(1:length(labels))
xticklabels(labels)
axis square

clear ty type labels
