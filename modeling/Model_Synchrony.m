r_range = 5:5:100;
data = zeros(2,length(r_range));
out = zeros(2,length(r_range));
data(1,:) = r_range;

for REPS = 1:100
    
    R = 1;
    while R <= length(r_range)
        check = 0;
        while check < 1
            try
                Model_ReproVsJitter_test
                model_spikes_1 = model_spikes;
                Model_ReproVsJitter_test
                model_spikes_2 = model_spikes;
                check = 1;
            catch
                check = 0;
            end
        end
        
        if check == 1
            
            model_spikes_1 = cellfun(@(x) x/1000,model_spikes_1, 'UniformOutput',0);
            model_spikes_2 = cellfun(@(x) x/1000,model_spikes_2, 'UniformOutput',0);
            reps = 100;
            nN = size(model_spikes_1,3)/reps;
            Window = sort([-0.1 0.1]);
            bin = 0.001;
            binWidth = bin;
            binNum = length(Window(1):binWidth:Window(2))-2;
            dur = T/1000;
            
            clear T
            
            outSYNCH = cell(size(model_spikes_1,1),size(model_spikes_1,2));
            
            for fr = 1:size(model_spikes_1,1)
                for re = 1:size(model_spikes_1,2)
                    
                    unit_1 = cell(nN,reps);
                    unit_2 = cell(nN,reps);
                    for i = 1:nN
                        offset = reps*i-reps;
                        unit_1(i,:) = squeeze(model_spikes_1(fr,re,1+offset:offset+reps))';
                        unit_1(i,:) = squeeze(model_spikes_2(fr,re,1+offset:offset+reps))';
                    end
                    fr_matrix = cellfun(@length,unit_1,'UniformOutput',0);
                    fr_matrix = cell2mat(fr_matrix);
                    list = combnk(1:nN,2);
                    
                    outSYNCH{fr,re} = cell(size(list,1),10);
                    CCG = zeros(1,binNum);
                    shiftCCG = zeros(1,binNum);
                    
                    for pair = 1:size(list,1)
                        for trial = 1:size(unit_1,2)
                            for sp = 1:length(unit_1{list(pair,1),trial})
                                times = unit_1{list(pair,1),trial}(sp) - unit_1{list(pair,2),trial};
                                his=hist(times,Window(1):binWidth:Window(2)); %% Here is where you can change binwidth
                                CCG=CCG+his(2:end-1);
                                times = unit_1{list(pair,1),trial}(sp) - unit_2{list(pair,2),trial};
                                his=hist(times,Window(1):binWidth:Window(2));
                                shiftCCG=shiftCCG+his(2:end-1);
                            end
                        end
                        
                        rate1=mean(fr_matrix(list(pair,1),:))/dur;
                        rate2=mean(fr_matrix(list(pair,2),:))/dur;
                        
                        SmCCG=(smooth(CCG)/sqrt(rate1*rate2))/binNum;
                        SmShiftCCG=(smooth(shiftCCG)/sqrt(rate1*rate2))/binNum;
                        
                        SmCCGCorrected=SmCCG-SmShiftCCG;
                        
                        flankS=std([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                        flankM=mean([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                        
                        PEAK=SmCCGCorrected(round(binNum/2));
                        check = PEAK>flankM+5*flankS;
                        
                        positive=(SmCCGCorrected>=(PEAK/2));
                        pos=diff(positive);
                        less=pos(1:round(binNum/2));
                        more=pos(round(binNum/2):end);
                        Low=find(less==1,1,'last');
                        High=find(more==-1,1,'first');
                        Low=length(less)-Low;
                        bandPEAK=High+Low;
                        
                        if isempty(bandPEAK)
                            bandPEAK=nan;
                            sumband=nan;
                        else
                            sumband=sum(SmCCGCorrected(length(less)-Low:length(less)+High))/bandPEAK;
                        end
                        
                        outSYNCH{fr,re}{pair,1}=PEAK;
                        outSYNCH{fr,re}{pair,2}=rate1;
                        outSYNCH{fr,re}{pair,3}=rate2;
                        outSYNCH{fr,re}{pair,4}=check;
                        outSYNCH{fr,re}{pair,5}=bandPEAK;
                        outSYNCH{fr,re}{pair,6}=sum(SmCCGCorrected);
                        outSYNCH{fr,re}{pair,7}=sumband;
                        outSYNCH{fr,re}{pair,8}=SmCCG;
                        outSYNCH{fr,re}{pair,9}=SmShiftCCG;
                        outSYNCH{fr,re}{pair,10}=SmCCGCorrected;
                    end
                    
                end
            end
            
            peak = zeros(size(outSYNCH));
            bw = zeros(size(outSYNCH));
            in = zeros(size(outSYNCH));
            
            for fr = 1:size(outSYNCH,1)
                for re = 1:size(outSYNCH,2)
                    %         idx = cell2mat(outSYNCH{fr,re}(:,4)) ~= 0;
                    peak(fr,re) = mean(cell2mat(outSYNCH{fr,re}(:,1)));
                    bw(fr,re) = mean(cell2mat(outSYNCH{fr,re}(:,5)));
                    in(fr,re) = mean(cell2mat(outSYNCH{fr,re}(:,7)));
                end
            end
            
        end
        data(2,R) = in(1,1);
        R = R + 1;
    end
    
    out = out + data;
    
end

out = out/REPS;

clearvars -except out
