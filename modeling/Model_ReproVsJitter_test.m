
%% Simulation parameters

%Time duration (ms)
T=500;

%Spike counts to test
rate= [5:5:50];
Nr=length(rate);

%Jitter values to test
jit=1:20;
Nj=length(jit);

%Number of repeats for each "neuron"
Nt=1000;


%% Generate a Poisson spike train

mrate=1/(max(rate)*1000/T); %max rate

spike_time=[];
n=0;
k=0;

while n<T
    ISI=exprnd(mrate)*1000;
    k=k+1;
    spike_time(k)=n+ISI;
    n=n+ISI;
end
    
%% Generate spikes for different rates and jitter levels
Nsp=length(spike_time);

model_spikes=cell(Nr,Nj,Nt);
for n=1:Nr
    for m=1:Nj
        for k=1:Nt
            %Shuffle the spikes
            in=randperm(Nsp);
            %Get desired number of spikes
            spt=spike_time(in(1:rate(n)));
            %jitter the spikes
            spt=spt+(rand(size(spt))-.5)*jit(m);

            model_spikes{n,m,k,:}=sort(spt);
        end
    end
end

%% Plot rasters for each rate and jitter level
% figure(1);clf;
% 
% for n=1:Nr
%     for m=1:Nj
%         sp=squeeze(model_spikes(n,m,:,:));
%         subplot(Nr,Nj,(n-1)*Nj+m)
%         bsRaster(sp,[0 T])
%         if n==Nr && m==3
%             xlabel('Jitter Increasing Left to Right','fontsize',15)
%         end
%         if n==3 && m==1
%             ylabel('Rate Increasing Top to Bottom','fontsize',15)
%         end
%     end
% end
% 



