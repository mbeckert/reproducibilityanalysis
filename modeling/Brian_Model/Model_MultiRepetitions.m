reps = 50;
for type = 2
    for c = 6 %2:2:10
        for sw = 7
            
        for iter = 1:20
            
            results = nan(reps,2*type);
            
            for rep = 1:reps
                tic
                disp(num2str(rep))
                if type == 2
                    [Rin,Rout] = BiophysModelReproducibility_Mike_2(c);
                    clearvars -except reps results rep Rin Rout iter type c sw S SW s
                    Rin = norm01(Rin);
                    Rout = norm01(Rout');
                    results(rep,1) = max(Rin(1:50))/max(Rin);
                    results(rep,2) = max(Rout(1:50))/max(Rout);
                    results(rep,3) = sum(Rin(50:150) > max(Rin)/2);
                    results(rep,4) = sum(Rout(50:150) > max(Rout)/2);
                elseif type == 1
                    [Rin,Rout] = BiophysModelReproducibility_Mike_ILD_2(c);
                    clearvars -except reps results rep Rin Rout iter type c sw
                    Rin = norm01(Rin);
                    Rout = norm01(Rout');
                    results(rep,1) = sum(Rin > max(Rin)/2);
                    results(rep,2) = sum(Rout > max(Rout)/2);
                end
                
                toc
            end
            
            clear r Rin Rout rep
            
            save(['model_' num2str(type) '_' num2str(iter)],'results');
        end
        end
    end
end
clear iter reps type c

%% compile data into a single file

reps = 25;

cd('f:\desktop\WorkingFolder\model_fr')

files = ls;
files = files(3:end,:);

dataout = nan(size(files,1)*reps,4);

for f = 1:size(files,1)
    load(files(f,:))
    L = find(isnan(dataout),1,'first');
    dataout(L:L+reps-1,:) = results;
end

clear reps files f results L

cd('F:\Desktop\WorkingFolder')