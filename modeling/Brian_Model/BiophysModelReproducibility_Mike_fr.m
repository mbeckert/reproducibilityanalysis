function [Rin,Rout,rp] = BiophysModelReproducibility_Mike_fr(sw,C,s)

%% Simulation parameters

%Time duration (ms)
T=500;
Ts=1/48.7;
t=0:Ts:T;
Lt=length(t);

bins = 0:0.001:T/1000;
bin = 0.001;
maxWidth = 100;

%Number of trials for calibrating reproducibility
Nt=200;

sf=.3;nf=.5; %max frequency for signal and noise (kHz)

%% Network

N=200;
c=linspace(-75,75,N);

% sw=7;
W=zeros(N,N);
for n=1:N
    W(n,:)=exp(-.5*((c(n)-c)/sw).^2) * s;
end

%% synapse

tau_syn=.1;
amp_syn=.2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Rate Calibration
Amp=[linspace(0,10,20) linspace(12,70,20)];
La=length(Amp);

rate=zeros(1,La);
%% Signal
s=genSignal(T,Ts,1,2,2*pi*sf);

%% Get rates
for i=1:La

I=s/sqrt(mean(s.^2))*Amp(i);

%%
[~,rate(i),~]=genLIFspikesCurr(I, Ts);

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reproducibility Calibration
SNR=[linspace(.1,5,20) linspace(5.5,20,20)];
Ls=length(SNR);

rms=10;
s=s/sqrt(mean(s.^2))*rms;

rep=zeros(1,Ls);

for i=1:Ls


I=zeros(Nt,Lt);

for n=1:Nt

noise=genSignal(T,Ts,rms/SNR(i),2,2*pi*nf);

I(n,:)=s+noise;

end

%%

[~,fr,sp]=genLIFspikesCurr(I, Ts);
%%
spikes=cell(1,Nt);
for n=1:Nt
    spikes{n}=sp(n,1:fr(n));
end

[~,~,~,~,~,~,rep(i)] = reproducibility(spikes,bins,bin,maxWidth);

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% Desired ICcl response properties


% r=normpdf(c,-5,10)*sqrt(2*pi*10^2)*50+28*(normpdf(c,55,10)*sqrt(2*pi*10^2)+normpdf(c,-65,10)*sqrt(2*pi*10^2));
r=normpdf(c,-5,10)*sqrt(2*pi*10^2)*50+20*(normpdf(c,55,10)*sqrt(2*pi*10^2)+normpdf(c,-65,10)*sqrt(2*pi*10^2));
r=round(r*1.5);

for condition= C

    rp=ones(1,N)*(0.1*C);
%     rp = r/max(r)*(0.1*C);
    
%     switch condition
%         case 1
%             rp=.6-abs(c)/max(c)*.55;
%         case 2
%             rp=ones(1,N)*.6; 
%         case 3
%             rp = r/max(r)*0.6;
%         case 4
%             rp=ones(1,N)*1;
%         case 5
%             rp=ones(1,N)*0;
%     end

% figure(condition);clf;
% switch condition
%     case 1
%         subplot(311)    
%         title('Direction-dependent Reproducibility','fontsize',18)
%     case 2
%         subplot(311)    
%         title('Constant Reproducibility','fontsize',18)
%     case 3
%         subplot(311)
%         title('FR-dependent Reproducibility','fontsize',18)
% end
% 
% subplot(321)
% plot(c,r,'-o','linewidth',1.5)
% set(gca,'box','off','fontsize',12)
% xlabel('Preferred direction','fontsize',15)
% ylabel('Desired Num. Spikes','fontsize',15)
% 
% 
% subplot(322)
% plot(c,rp,'-o','linewidth',1.5)
% set(gca,'box','off','fontsize',12)
% xlabel('Preferred direction','fontsize',15)
% ylabel('Desired Reprod.','fontsize',15)


%% Find necessary amplitude, SNR, RMS

a=zeros(1,N);
for n=1:N
    [~,i]=min(abs(r(n)-rate));
    a(n)=Amp(i);
end

snr=zeros(1,N);
for n=1:N
    [~,i]=min(abs(rp(n)-rep));
    snr(n)=SNR(i);
end

rms=a./sqrt(1+1./(snr.^2));



%% Get population response

Rin=zeros(1,N);

spikes=cell(N,1);

for n=1:N

sig=s/sqrt(mean(s.^2))*rms(n);

noise=genSignal(T,Ts,rms(n)/snr(n),2,2*pi*nf);

I=sig+noise;

[~,Rin(n),sp]=genLIFspikesCurr(I, Ts);

spikes{n}=sp(1:Rin(n));


end


%convert input spikes to 1's and 0's
input=zeros(N,Lt);

for n=1:N
    input(n,:)=doPSTH(spikes(n),0,T,Ts);
end

% Get input conductance
g=zeros(N,Lt);
for n=2:Lt
    g(:,n)=g(:,n-1)+Ts/tau_syn*(-g(:,n-1)+input(:,n-1));
end

%Get output spikes
[~,Rout,~]=genLIFspikesSynch(W*g*amp_syn, Ts);


% figure(condition);
% subplot(312);
% plot(c,Rin,'k-s',c,Rout,'r-o','linewidth',1.5)
% title('Input and Ouput Rates','fontsize',18)
% legend('Input','Output')
% axis([-90 90 0 max(Rin)*1.05])
% set(gca,'box','off','fontsize',12)
% xlabel('Preferred direction','fontsize',15)
% ylabel('Response','fontsize',15)
% 
% subplot(313);
% plot(c,Rin/max(Rin),'k-s',c,Rout/max(Rout),'r-o','linewidth',1.5)
% axis([-90 90 0 1.05])
% set(gca,'box','off','fontsize',12)
% xlabel('Preferred direction','fontsize',15)
% ylabel('Normalized response','fontsize',15)
end
end