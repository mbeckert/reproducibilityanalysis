load('f:\desktop\Data\Analysis\ReproducibilityPaper\model_results_ot.mat')

%% convert the halfwidth measurments from "cells" to ITD and ILD

f = 80/200;
ILD_flat = ILD_flat * f;
ILD_fr = ILD_fr * f;
ILD_1 = ILD_1 * f;
ILD_0 = ILD_0 * f;

f = 600/200;
ITD_flat(:,3:4) = ITD_flat(:,3:4) * f;
ITD_fr(:,3:4) = ITD_fr(:,3:4) * f;
ITD_1(:,3:4) = ITD_1(:,3:4) * f;
ITD_0(:,3:4) = ITD_0(:,3:4) * f;

clear f
%% calculate halfwidth changes (percent difference between fr and flat)

d = nan(500,8);
for t = 1:8
    switch t
        case 1
            data = ITD_0(1:500,3:4);
        case 2
            data = ITD_flat(1:500,3:4);
        case 3
            data = ITD_1(1:500,3:4);
        case 4
            data = ITD_fr(1:500,3:4);
        case 5
            data = ILD_0(1:500,:);
        case 6
            data = ILD_flat(1:500,:);
        case 7
            data = ILD_1(1:500,:);
        case 8
            data = ILD_fr(1:500,:);
    end
    
    d(:,t) = 1 - (data(:,2)./data(:,1));
    
end

clear data t
%% calculate sps change (percent difference between sps [1-sidepeakheight] for fr and flat)

d = nan(1000,2);

for t = 1:2
    switch t
        case 1
            data = ITD_flat(:,1:2);
        case 2
            data = ITD_fr(:,1:2);
    end
    
    d(:,t) = (1-data(:,2)) - (1-data(:,1));
    
end

clear t

%% for varying constant levels of synchrony

data = cell(1,2);

for t = 1:2
    data{t} = cell(1,10);
    for s = 1:10
        load(['model_' num2str(t) '_' num2str(s) '_1.mat'])
        data{t}{s} = results;
    end
end

clear results t s

itd_bw = nan(100,10);
ild_bw = nan(100,10);
itd_sps = nan(100,10);
for s = 1:10
    itd_bw(:,s) = 1 - data{2}{s}(:,4)./data{2}{s}(:,3);
    ild_bw(:,s) = 1 - data{1}{s}(:,2)./data{1}{s}(:,1);
    itd_sps(:,s) = (1 - data{2}{s}(:,2)) - (1 - data{2}{s}(:,1));
end


clear s

%% for varying levels of direction-dependent synchrony

bw = cell(1,2);

for t = 1:2
    switch t
        case 1
            data = ITD;
            f = 600/200;
            sps = nan(length(data{1}),length(data));
        case 2
            data = ILD;
            f = 80/200;
    end
    
    bw{t} = nan(length(data{1}),length(data));
    
    for s = 1:length(data)
        if t == 1
            d = data{s}(:,3:4);
        elseif t == 2
            d = data{s};
        end
        
        d = d * f;
        
        bw{t}(:,s) = (1-d(:,2)) - (1-d(:,1));
        
        if t == 1
            d = data{s}(:,1:2);
            sps(:,s) = (1-d(:,2)) - (1-d(:,1));
        end
    end
end

clear d data f s t

%% compile for varying synaptic weights

cd('f:\desktop\WorkingFolder\model_varyingSWwithS_constant');
files = ls;
files = files(3:end,:);

fac = 600/200;

data = cell(1,size(files,1));

for f = 1:size(files,1)
    load(files(f,:))
    data{f} = [results(:,1:2), results(:,3:4)*fac];
end
clear files results sw f fac

bw = cellfun(@(x) 1 - (x(:,4)./x(:,3)), data, 'UniformOutput',0);
sps = cellfun(@(x) (1-x(:,2)) - (1-x(:,1)), data, 'UniformOutput',0);

bw = cell2mat(bw);
sps = cell2mat(sps);

cd('F:\Desktop\WorkingFolder');
