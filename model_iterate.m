n = 300;

for c = 2:3
    
data = cell(n,2);

for i = 1:n
    tic
    disp(num2str(i))
    [data{i,1},data{i,2}] = model_inputoutputfunction(c);
    toc
end
if c == 1
    save inout2 data
elseif c == 2
    save ITDmodel2 data
elseif c == 3
    save ILDmodel2 data
end

end
%%
n = 400;

for repro = 0.9:0.3:1.2
for c = 2:3
    
data = cell(n,2);

for i = 1:n
    tic
    disp(num2str(i))
    [data{i,1},data{i,2}] = model_inputoutputfunction_constant(c,repro);
    toc
end
if c == 1
    save inout data
elseif c == 2
    save(['ITDconstant' num2str(repro*10) '3.mat'], 'data')
elseif c == 3
    save(['ILDconstant' num2str(repro*10) '3.mat'], 'data')
end
end
end

%%

n = 400;

for repro = 0.3:0.3:0.6
for c = 2:3
    
data = cell(n,2);

for i = 1:n
    tic
    disp(num2str(i))
    [data{i,1},data{i,2}] = model_inputoutputfunction_constant(c,repro);
    toc
end
if c == 1
    save inout data
elseif c == 2
    save(['ITDconstant' num2str(repro*10) '2.mat'], 'data')
elseif c == 3
    save(['ILDconstant' num2str(repro*10) '2.mat'], 'data')
end
end
end
%%
clear
load data

Rin = cell2mat(data(:,1));
Rout = cell2mat(data(:,2));
errorbar(mean(Rout),std(Rout));
hold on
errorbar(mean(Rout),std(Rout));

