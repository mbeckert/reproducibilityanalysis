addpath('C:\Users\mbeck\Desktop\Code\other');
%% Extract the raw data for each pair
cd('f:\desktop\Data\Analysis\ReproducibilityPaper\working')
load synchdata_may2017
out = cell(200,1);

pattern1 = '';          % To remove
pattern2 = 'FF.';    % To keep

filetypes = fieldnames(synch);

idx = regexpi(filetypes,pattern1);
idx = cellfun(@isempty,idx);
filetypes = filetypes(idx);

idx = regexpi(filetypes,pattern2);
idx = ~cellfun(@isempty,idx);
filetypes = filetypes(idx);

for d = 1:6
    for s = 1:4
        for u1 = 1:7
            for u2 = 1:8
                
                holder = nan(4,144,length(filetypes));
                check = 0;
                
                for f = 1:length(filetypes)
                    i1 = find(synch.(filetypes{f}){1}(:,1) == d);
                    i2 = find(synch.(filetypes{f}){1}(:,2) == s);
                    i3 = find(synch.(filetypes{f}){1}(:,3) == u1);
                    i4 = find(synch.(filetypes{f}){1}(:,4) == u2);
                    idx = intersect(intersect(intersect(i1,i2),i3),i4);
                    
                    if ~isempty(idx)
                        check = 1;
                        holder(1,:,f) = sqrt(synch.(filetypes{f}){3}(idx,:).*synch.(filetypes{f}){4}(idx,:));
                        holder(2,:,f) = synch.(filetypes{f}){2}(idx,:);
                        holder(3,:,f) = synch.(filetypes{f}){6}(idx,:);
                        holder(4,:,f) = synch.(filetypes{f}){8}(idx,:);
                        sens = find(synch.(filetypes{f}){5}(idx,:) == 0);
                        holder(2:4,sens,f)=nan;
                    end
                end     % f
                
                if check == 1
                    holder = nanmean(holder,3);
                    L = find(cellfun(@isempty,out),1,'first');
                    out{L} = holder;
                end     % check
                
            end         % u2
        end             % u1
    end                 % s
end                     % d

L = find(cellfun(@isempty,out),1,'first');
out(L:end) = [];

clearvars -except out synch

out = cellfun(@norm01,out,'UniformOutput',0);
cd('F:\Desktop\Data\WorkingFolder')

% % transform to Azimuth & Elevation

cd('f:\desktop\Data\Analysis')
load('SpeakerIndex.mat','SpeakerIndex');
[B,I]=sortrows(SpeakerIndex(:,1));
[b,i]=sortrows(SpeakerIndex(:,2));
H=hist(B,-100:10:100);
H=cumsum(H);
h=hist(b,-80:10:80);
h=cumsum(h);
clear B b SpeakerIndex

Azimuth = cell(size(out));
Elevation = cell(size(out));

for p = 1:length(out)
    
    unitA=out{p}(:,I);
    unitE=out{p}(:,i);
    
    Az = nan(4,21);
    El = nan(4,17);
    
    Az(:,1) = nanmean(unitA(:,1:H(1)),2);
    El(:,1) = nanmean(unitE(:,1:h(1)),2);
    
    for point = 2:length(H)
        Az(:,point)=nanmean(unitA(:,H(point-1):H(point)),2);
    end
    for point = 2:length(h)
        El(:,point)=nanmean(unitE(:,h(point-1):h(point)),2);
    end
    
    Azimuth{p}=Az;
    Elevation{p}=El;
    
end

clearvars -except synch out Azimuth Elevation

% Azimuth = cellfun(@norm01,Azimuth,'UniformOutput',0);
% Elevation = cellfun(@norm01,Elevation,'UniformOutput',0);

% %
out = cell2mat(out');
Azimuth = cell2mat(Azimuth');
Elevation = cell2mat(Elevation');
out = out';
Azimuth = Azimuth';
Elevation = Elevation';

cd('F:\Desktop\Data\WorkingFolder')

%%
c = nan(2,3);
p = nan(2,3);
for i = 2:4
    [c(1,i-1),p(1,i-1)] = corr(in(:,1),in(:,i),'rows','pairwise');
    [c(2,i-1),p(2,i-1)] = corr(out(:,1),out(:,i),'rows','pairwise');
%     [c(3,i-1),p(3,i-1)] = corr(out(:,1),out(:,i),'rows','pairwise');
end

clear i

%%

d = out;
range = 0:0.05:0.5;

hisM = nan(3,length(range));
hisS = nan(3,length(range));
N = nan(1,11);
[~,I]=histc(d(:,1),range);

for bin = 1:length(range)
    idx=find(I==bin);
    hisM(1,bin) = nanmean(d(idx,2));
    hisM(2,bin) = nanmean(d(idx,3));
    hisM(3,bin) = nanmean(d(idx,4));
    hisS(1,bin) = nanstd(d(idx,2))/sqrt(length(idx));
    hisS(2,bin) = nanstd(d(idx,3))/sqrt(length(idx));
    hisS(3,bin) = nanstd(d(idx,4))/sqrt(length(idx));
    N(bin) = length(idx);
end

clear d 

d(:,1) = hisM(1,:)';
d(:,2) = hisS(1,:)';
d(:,3) = hisM(2,:)';
d(:,4) = hisS(2,:)';
d(:,5) = hisM(3,:)';
d(:,6) = hisS(3,:)';
% clear d I bin idx