load('f:\desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted.mat','OT');
data = OT; clear OT

Window = sort([-0.1 0.1]);
binWidth = 0.005;
binNum = length(Window(1):binWidth:Window(2))-2;

dur = 0.15;
pattern = 'frozen.|b.';                   % These are to remove
pattern2 = '.';                   % These are to keep

D=[];

for date=1:size(data,2)
    for site = 1:length(data{date})
        list=combnk(2:length(data{date}{site}),2);
        for pair = 1:size(list,1)
            
            disp(['date: ' num2str(date) '; site: ' num2str(site) '; pair: ' num2str(pair)]);
            
            unit1 = data{date}{site}{list(pair,1)};
            unit2 = data{date}{site}{list(pair,2)};
            filetypes1 = fieldnames(unit1);
            filetypes2 = fieldnames(unit2);
            filetypes = intersect(filetypes1,filetypes2);
            idx = regexpi(filetypes,pattern);
            idx = cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            idx = regexpi(filetypes,pattern2);
            idx = ~cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            
            clear filetypes1 filetypes2 idx
            
            for file = 1:length(filetypes)
                
                CCG = zeros(length(unit1.(filetypes{file}).depvar),binNum);
                shiftCCG = zeros(length(unit1.(filetypes{file}).depvar),binNum);
                
                for depvar = 1:length(unit1.(filetypes{file}).depvar)

%                     if unit1.(filetypes{file}).sensitive(depvar) && unit2.(filetypes{file}).sensitive(depvar)
                        for trial = 1:unit1.(filetypes{file}).reps
                            if ~isempty(unit1.(filetypes{file}).spikes_times{depvar,trial})
                                for spike = 1:length(unit1.(filetypes{file}).spikes_times{depvar,trial})
                                    
                                    times = unit1.([filetypes{file}]).spikes_times{depvar,trial}(spike) - unit2.([filetypes{file}]).spikes_times{depvar,trial};
                                    his=hist(times,Window(1):binWidth:Window(2)); %% Here is where you can change binwidth
                                    CCG(depvar,:)=CCG(depvar,:)+his(2:end-1);
                                    if trial==unit1.([filetypes{file}]).reps
                                        times = unit1.([filetypes{file}]).spikes_times{depvar,trial}(spike) - unit2.([filetypes{file}]).spikes_times{depvar,1};
                                    else
                                        times = unit1.([filetypes{file}]).spikes_times{depvar,trial}(spike) - unit2.([filetypes{file}]).spikes_times{depvar,trial+1};
                                    end
                                    his=hist(times,Window(1):binWidth:Window(2));
                                    shiftCCG(depvar,:)=shiftCCG(depvar,:)+his(2:end-1);
                                    
                                end
                                
                            end
                        end % for "trial"
                        
%                     end
                end     % for "depvar" the first time
                
                clear his spike times
                
                holder = cell(1,13);
                holder{1} = [date,site,list(pair,1),list(pair,2)];
                for i = 2:10
                    holder{i} = nan(1,length(unit1.([filetypes{file}]).depvar));
                end
                for i = 11:13
                    holder{i}=cell(1,length(unit1.([filetypes{file}]).depvar));
                end
                
                m = mean(unit1.(filetypes{file}).spikes_count,2);
                b = mean(unit1.(filetypes{file}).base_count(:));
                s = std(unit1.(filetypes{file}).base_count(:));
                c = b+2*s;
                chck1 = m >= c;
                m = mean(unit2.(filetypes{file}).spikes_count,2);
                b = mean(unit2.(filetypes{file}).base_count(:));
                s = std(unit2.(filetypes{file}).base_count(:));
                c = b+2*s;
                chck2 = m >= c;
                check2 = chck1 | chck2;
                holder{7} = check2;
                
                clear m b s c chck* check2
                
                for depvar = 1:length(unit1.([filetypes{file}]).depvar)
                    
                    rate1=mean(unit1.([filetypes{file}]).spikes_count(depvar,:))/dur;
                    rate2=mean(unit2.([filetypes{file}]).spikes_count(depvar,:))/dur;
                    
                    SmCCG=(smooth(CCG(depvar,:))/sqrt(rate1*rate2))/binNum;
                    SmShiftCCG=(smooth(shiftCCG(depvar,:))/sqrt(rate1*rate2))/binNum;
                    
                    SmCCGCorrected=SmCCG-SmShiftCCG;
                    
                    flankS=std([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                    flankM=mean([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                    
                    PEAK=SmCCGCorrected(round(binNum/2));
                    check1 = PEAK>flankM+5*flankS;
                    check2 = unit1.(filetypes{file}).sensitive(depvar)*unit2.(filetypes{file}).sensitive(depvar);
                    check = check1 * check2;
                    positive=(SmCCGCorrected>=(PEAK/2));
                    pos=diff(positive);
                    less=pos(1:round(binNum/2));
                    more=pos(round(binNum/2):end);
                    Low=find(less==1,1,'last');
                    High=find(more==-1,1,'first');
                    Low=length(less)-Low;
                    bandPEAK=High+Low;
                    
                    if isempty(bandPEAK)
                        bandPEAK=nan;
                        sumband=nan;
                    else
                        sumband=sum(SmCCGCorrected(length(less)-Low:length(less)+High))/bandPEAK;
                    end
                    
                    holder{2}(1,depvar)=PEAK;
                    holder{3}(1,depvar)=rate1;
                    holder{4}(1,depvar)=rate2;
                    holder{5}(1,depvar)=check;
                    holder{6}(1,depvar)=check1;
%                     holder{7}(1,depvar)=check2;
                    holder{8}(1,depvar)=bandPEAK;
                    holder{9}(1,depvar)=sum(SmCCGCorrected);
                    holder{10}(1,depvar)=sumband;
                    holder{11}{1,depvar}=SmCCG;
                    holder{12}{1,depvar}=SmShiftCCG;
                    holder{13}{1,depvar}=SmCCGCorrected;
                    
                    
                    clear positive pos less more Low High bandPEAK
                    
                end     % for "depvar" the second time
                
                if isfield(D,([filetypes{file}]))
                    for i = 1:10
                        if size(D.(filetypes{file}){i},2) > size(holder{i},2)
                            padding = nan(1,size(D.(filetypes{file}){i},2)-size(holder{i},2));
                            D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i},padding];
                        elseif size(D.(filetypes{file}){i},2) < size(holder{i},2)
                            padding = nan(size(D.(filetypes{file}){i},1),size(holder{i},2)-size(D.(filetypes{file}){i},2));
                            D.([filetypes{file}]){i}=[D.([filetypes{file}]){i},padding;holder{i}];
                        else
                            D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i}];
                        end
                    end
                    for i = 11:13
                        if size(D.(filetypes{file}){i},2) > size(holder{i},2)
                            padding = cell(1,size(D.(filetypes{file}){i},2)-size(holder{i},2));
                            D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i},padding];
                        elseif size(D.(filetypes{file}){i},2) < size(holder{i},2)
                            padding = cell(size(D.(filetypes{file}){i},1),size(holder{i},2)-size(D.(filetypes{file}){i},2));
                            D.([filetypes{file}]){i}=[D.([filetypes{file}]){i},padding;holder{i}];
                        else
                            D.([filetypes{file}]){i}=[D.([filetypes{file}]){i};holder{i}];
                        end
                    end
                else
                    for i = 1:length(holder)
                        D.([filetypes{file}]){i}=holder{i};
                    end
                end
                
                clear holder depvar rate* i PEAK check SmCCG curves CORR trial flank* sumband SmShiftCCG shiftCCG SmCCGCorrected CCG
                
            end         % for "file"
            
            clear filetypes
            
        end             % for "pair"
    end                 % for "site"
end                     % for "date"

ot_synch = D;

clearvars -except ot_synch

cd('F:\Desktop\WorkingFolder')

save ot_synch
