function [Rin, Rout] = model_inputoutputfunction(cond)
%% Constants

T = 500;
Ts = 1/48.7;
t = 0:Ts:T;
Lt = length(t);

bins = 0:0.001:T/1000;
bin = 0.005;
maxWidth = 100;

Nt = 200;

sf = 0.1;
nf = 0.1;

N = 500;

c=linspace(-75,75,N);

sw = 7;
W=zeros(N,N);
for n=1:N
    W(n,:)=exp(-.5*((c(n)-c)/sw).^2);
end

tau_syn=.1;
amp_syn=.2;

%% Rate Calibration

Amp = linspace(0,2.5,100);

La=length(Amp);

rate=zeros(1,La);

s=genSignal(T,Ts,1,2,2*pi*sf);

for i=1:La
    
    I=s/sqrt(mean(s.^2))*Amp(i);
    [~,rate(i),~]=genLIFspikesCurr(I, Ts);
    
end

%% Reproducibility Calibration

SNR = [linspace(0.01,1,50) linspace(1.01,10,50)];

Ls=length(SNR);

rms=1;
s=s/sqrt(mean(s.^2))*rms;

rep=zeros(1,Ls);

for i=1:Ls
    
    I=zeros(Nt,Lt);
    
    for n=1:Nt
        
        noise=genSignal(T,Ts,rms/SNR(i),2,2*pi*nf);
        I(n,:)=s+noise;
        
    end
    
    [~,fr,sp]=genLIFspikesCurr(I, Ts);
    spikes=cell(1,Nt);
    
    for n=1:Nt
        spikes{n}=sp(n,1:fr(n));
    end
    
    [~,rep(i),~,~,~,~,~] = reproducibility(spikes,bins,bin,maxWidth);
    
end

%%

r = normpdf(c,-5,10)*sqrt(2*pi*10^2)*20 ...
    +10*(normpdf(c,55,10)*sqrt(2*pi*10^2) ...
    +normpdf(c,-65,10)*sqrt(2*pi*10^2));

rp = r/max(r) * 1.5;

%% Find necessary amplitude, SNR, RMS

a=zeros(1,N);
for n=1:N
    [~,i]=min(abs(r(n)-rate));
    a(n)=Amp(i);
end

snr=zeros(1,N);
for n=1:N
    [~,i]=min(abs(rp(n)-rep));
    snr(n)=SNR(i);
end

rms=a./sqrt(1+1./(snr.^2));

%% Get population response

Rin=zeros(1,N);

spikes=cell(N,1);
Vm_in=nan(N,length(I));

for n=1:N

sig=s/sqrt(mean(s.^2))*rms(n);

noise=genSignal(T,Ts,rms(n)/snr(n),2,2*pi*nf);

I=sig+noise;

[Vm_in(n,:),Rin(n),sp(n,:)]=genLIFspikesCurr(I, Ts);

spikes{n}=sp(1:Rin(n));

end

%convert input spikes to 1's and 0's
input=zeros(N,Lt);

for n=1:N
    input(n,:)=doPSTH(spikes(n),0,T,Ts);
end

% Get input conductance
g=zeros(N,Lt);
for n=2:Lt
    g(:,n)=g(:,n-1)+Ts/tau_syn*(-g(:,n-1)+input(:,n-1));
end

%Get output spikes
[Vm_out,Rout,sp_out]=genLIFspikesSynch(W*g*amp_syn, Ts);
Rout = Rout';

