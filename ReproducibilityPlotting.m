
% find appropriate file to load data from
load('C:\Users\PenaLab\Desktop\FreeField\ICls\Data\ReproducibilitySummary.mat')

%% 
%   Correct the ITD and ILD such that the location where FR is best is 0,
%   and all other values are relative to this location. 

d=ITD;
s=site_itd;
data=d;
for i=1:max(s);
    x=d(s==i,:);
    best=find(x(:,2)==1,1,'first');
    data(s==i,3)=x(:,3)-x(best,3);
end

figure(2)
subplot(2,1,1)
scatter(data(:,3),data(:,1));
title('reproducibility vs cue')
subplot(2,1,2)
scatter(data(:,3),data(:,2));
title('firing rate vs cue')

corrected_itd=data;
