load('C:\Users\mbeck\Desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted_2.mat')
data = OT;
clear AAr FL OT
%%
pattern1 = '';
pattern2 = 'FF.';

fr = nan(200,1);

for d = 1:size(data,2)
    for s = 1:size(data{d},1)
        for u = 2:size(data{d}{s},1)
            filetypes = fieldnames(data{d}{s}{u});
            
            idx = regexpi(filetypes,pattern1);
            idx = cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            
            idx = regexpi(filetypes,pattern2);
            idx = ~cellfun(@isempty,idx);
            filetypes = filetypes(idx);

            if ~isempty(filetypes)
                
                m = 0;
                
                for f = 1:length(filetypes)
                    curve = mean(data{d}{s}{u}.(filetypes{f}).spikes_count,2);
                    if max(curve) > m
                        m = max(curve);
                    end     % m
                end         % f
                
                L = find(isnan(fr),1,'first');
                fr(L) = m;
            end             % filestypes
        end                 % u
    end                     % s
end                         % d

L = find(isnan(fr),1,'first');
fr(L:end) = [];

clearvars -except data fr

fr = fr/0.15;

