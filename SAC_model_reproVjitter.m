CURVE = cell(size(model_spikes,1),size(model_spikes,2));
Mean = cell(size(model_spikes,1),size(model_spikes,2));
SACcurve = cell(size(model_spikes,1),size(model_spikes,2));
SACnorm = cell(size(model_spikes,1),size(model_spikes,2));

binWidth=0.05;
maxBin=20;

for r=1:size(model_spikes,1)
    for m=1:size(model_spikes,2)
        
        TrialData = squeeze(model_spikes(r,m,:))';
        
        CURVE{r,m}=zeros([1 size(TrialData,1)]);
        SACcurve{r,m}=cell([1 size(TrialData,1)]);
        SACnorm{r,m}=cell([1 size(TrialData,1)]);
        
        for h=1:size(TrialData,1)
            spikes=TrialData(h,:);
            spikeWin=[0 100];
            duration = 500;
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Run computeSAC - originally written by Bjorn
            
            % Make sure spikeWin is sorted in the correct order.
            spikeWin=sort(spikeWin);
            
            % Pre-allocate some arrays.
            rates=zeros(size(spikes));
            histax=-maxBin:binWidth:maxBin;
            sac=zeros(size(histax));
            
            % Now the work...
            for n=1:length(spikes)
                
                % For the current trial, get the spikes, filter out any outside the
                % spike window, and then compute a mean rate for use in the normalizing
                % factor later.
                thisTrial=spikes{n};
                thisTrial=thisTrial(thisTrial>spikeWin(1)&thisTrial<spikeWin(2));
                rates(n)=length(thisTrial)./(diff(spikeWin)./1000);
                
                % Now, for each trial which we're NOT working on, find all the spikes
                % in the spike window.
                idx=1:length(spikes);idx=idx(idx~=n);
                restSpikes=[spikes{idx}];
                restSpikes=restSpikes(restSpikes>spikeWin(1)&...
                    restSpikes<spikeWin(2));
                
                % Then, for each spike in the current trial, subtract it from the times
                % of all spikes from all other trials, bin them up, and keep track of
                % them in the running histogram.
                for t=1:length(thisTrial);
                    rs=restSpikes-thisTrial(t);
                    rs=rs(abs(rs)<maxBin);
                    tmp=hist(rs,histax);
                    sac=sac+tmp;
                end
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            sac=sac(2:end-1);
            
            % % Get SAC values as described by Joris (2006)
%             nf=length(spikes)*(length(spikes)-1)*mean(rates).^2*...
%                 (diff(spikeWin)./1000)*(binWidth./1000);
%             SACcurve{r,m}{1,h} = sac;
%             SACnorm{r,m}{1,h} = sac/nf;

            % % Get SAC values as written by Bertrand Fontaine via email
            sac = sac/(binWidth/1000)/(duration/1000)/length(spikes)/(length(spikes)-1);
            SACcurve{r,m}{1,h} = sac;
            SACnorm{r,m}{1,h} = (sac-mean(rates).^2)*(binWidth/1000)/mean(rates);
            
            % % Get SAC values as purely described by Brette (2012)
%             sac = sac/(binWidth/1000)/length(spikes);
%             SACcurve{r,m}{1,h} = sac;
%             SACnorm{r,m}{1,h} = (sac-mean(rates).^2)*(binWidth/1000)/mean(rates);


            mid = round(length(sac)/2);
            int = round(3/binWidth);
%             CURVE{r,m}(1,h)=sum(SACnorm{r,m}{1,h}(mid-int:mid+int))/((mid+int)-(mid-int)); 
            CURVE{r,m}(1,h) = sum(SACnorm{r,m}{1,h});
            Mean{r,m}(1,h)=mean(rates);
        end
        
        
    end
end

CURVE = cell2mat(CURVE);
Mean = cell2mat(Mean);

clearvars -except CURVE Mean SACnorm SACcurve model_spikes icls param az el itd ild out
