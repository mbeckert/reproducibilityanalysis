fol = 'F:\Desktop\WorkingFolder\synchony\';
file = 'ot_synch_ff.mat';
load([fol file])
%%
for i = 1:size(GM,1)
    figure(1)
    m = find(max(GM(i,:)) == GM(i,:), 1, 'first');
    plot(-99:99, smooth(CCG{i,m}), '--k')
    hold
    plot(-99:99, smooth(SH{i,m}), 'r', 'LineWidth', 2)
    plot(-99:99, smooth(COR{i,m}), 'b')
    title(num2str(i))
    xlabel('Lag (ms)')
    ylabel('Coincidences')
    pause
    close
end