%% extract ccg data. be mindful of what binwidth was used!
% requires running 'otsynch_ff' you can find the binwidth there

load('ot_synch_ff_shift.mat')
ff = ot_synch.ff;
clear ot_synch

% binNum = 39; int = 5;
binNum = 199; int = 10;

gm = sqrt(ff{3} .* ff{4});
% gm = normalization(gm,1);

ccg = nan(size(ff{2}));
sh = nan(size(ff{2}));
cor = nan(size(ff{2}));

for n = 1:size(ccg,1)
    for sp = 1:size(ccg,2)
        ccg(n,sp) = sum(ff{11}{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        sh(n,sp) = sum(ff{12}{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        cor(n,sp) = sum(ff{13}{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
    end
end

correct = nan;
chck = ff{5};
chck = chck == 0;
ccg(chck) = correct; sh(chck) = correct; cor(chck) = correct;
chck = ff{6};
chck = chck == 0;
ccg(chck) = correct; sh(chck) = correct; cor(chck) = correct;

clear ff binNum n sp chck correct int

save otsynch_ff_shiftVccg

%% general analysis (normalize, run correlations, and plot)
% continuation from the above cell

load('otsynch_ff_shiftVccg.mat')

output = cell(1,4);
normalize = [1,1];

output{1} = normalization(gm,normalize(1));
output{2} = normalization(ccg,normalize(2));
output{3} = normalization(sh,normalize(2));
output{4} = normalization(cor,normalize(2));

clear normalize ccg cor sh gm

% % Run correlations

corr_cell = cell(1,3);
corr_full = nan(1,3);

for m = 2:4
    [~,corr_full(1,m-1)] = corr(output{1}(:),output{m}(:),'rows','pairwise');
    corr_cell{1,m-1} = nan(size(output{1},1),1);
    for i = 1:size(output{1},1)
        if sum(isfinite(output{m}(i,:))) > 2
            c = corr(output{1}(i,:)',output{m}(i,:)','rows','pairwise');
%             corr_cell{m-1}(i) = 0.5*log((1+c)/(1-c));
            corr_cell{m-1}(i) = c;
        end
    end
end

corr_cell = cell2mat(corr_cell);

clear m i c

% % scatter plots for visualization

rangex = [0,1,11];
rangey = [0,1,11];
cmap = flipud(gray(64));

figure(1)

for m = 1:3
    data = [output{1}(:),output{m+1}(:)];
    [mat,~,~] = hist2D(data,rangex,rangey);
    subplot(3,2,m*2-1)
    imagesc(mat)
    colormap(cmap)
    colorbar
    axis square
    subplot(3,2,m*2)
    scatter(output{1}(:),output{m+1}(:),2,'k','fill')
    axis square
end

clear range* m data mat cmap

%% generate ccgs at varying gms

load('ot_synch_ff_shift.mat')
ff = ot_synch.ff;

gm = normalization(sqrt(ff{3} .* ff{4}),1);
data{1} = ff{11};
data{2} = ff{12};
data{3} = ff{13};

clear ot_synch ff

[~,~,idx] = histcounts(gm,0:0.1:1);
out = cell(1,3);

for t = 1:3
    out{t} = cell(length(unique(idx)),1);
    for i = 0:length(unique(idx))-1
        r = data{t}(idx == i);
        r = cellfun(@transpose,r,'UniformOutput',0);
        r = cell2mat(r);
        r(isnan(r)) = 0;
        out{t}{i+1} = sum(r)/size(r,1);
    end
    out{t} = cell2mat(out{t});
    out{t}(~isfinite(out{t})) = 0;
    subplot(1,3,t)
    imagesc(out{t})
    axis square
    colorbar
end

clear gm data idx t i r ff

%% alternative plotting
x = linspace(-100,100,39);
figure(2)

for i = 1:size(out{1},1)
    subplot(1,size(out{1},1),i)
    plot(x,out{1}(i,:),'--g','LineWidth',1)
    hold on
    plot(x,out{2}(i,:),'--r','LineWidth',1)
    plot(x,out{3}(i,:),'k','LineWidth',1)
    hold off
    axis square
end

figure(3)
plot(x,out{1}(7,:),'--g','LineWidth',1)
hold on
plot(x,out{2}(7,:),'--r','LineWidth',1)
plot(x,out{3}(7,:),'k','LineWidth',1)
hold off
axis square
clear x i