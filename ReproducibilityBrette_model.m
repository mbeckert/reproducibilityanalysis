%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set parameters in Model_ReproVsJitter_test to create the matrix of spike
% trains, and "reproducibility" to modify how reproducibility is calculated
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Model_ReproVsJitter_test

clearvars -except model_spikes rate jit m r count

outREP = cell(1,size(model_spikes,1));
outMEAN = cell(1,size(model_spikes,2));
outHW = cell(1,size(model_spikes,1));

bins = 0:0.001:0.50;
bin = 0.001;
maxWidth = 100;

for fr = 1:size(model_spikes,1)
    for re = 1:size(model_spikes,2)
    
    TrialData = squeeze(model_spikes(fr,re,:))';
    [~,outREP{fr,re},outMEAN{fr,re},~,~,outHW{fr,re}] = reproducibility(TrialData,bins,bin,maxWidth);
    
    end
end

outREP = cell2mat(outREP);
outHW = cell2mat(outHW);
outMEAN = cell2mat(outMEAN);

figure(2)
imagesc(outREP)
x = cellfun(@str2num,xticklabels);
y = cellfun(@str2num,yticklabels);
yticklabels(rate(y));
xticklabels(jit(x));
xlabel('Jitter')
ylabel('Firing Rate')
h = colorbar;
ylabel(h,'Reproducibility')

figure(3)
imagesc(outHW)
x = cellfun(@str2num,xticklabels);
y = cellfun(@str2num,yticklabels);
yticklabels(rate(y));
xticklabels(jit(x));
xlabel('Jitter')
ylabel('Firing Rate')
h = colorbar;
ylabel(h,'Half-width')

figure(4)
subplot(2,2,1)
plot(rate,outREP)
title('FR vs REP')
xlabel('Firing Rate')
ylabel('Reproducibility')
subplot(2,2,2)
plot(jit,outREP')
title('jitter vs REP')
xlabel('jitter')
ylabel('Reproducibility')
subplot(2,2,3)
plot(rate,outHW)
title('FR vs HW')
xlabel('Firing Rate')
ylabel('Half-width')
subplot(2,2,4)
plot(jit,outHW')
title('jitter vs HW')
xlabel('jitter')
ylabel('Half-width')


clear bins bin maxWidth fr re TrialData h x y
