CURVE = cell(size(model_spikes,1),size(model_spikes,2));
Mean = cell(size(model_spikes,1),size(model_spikes,2));
SACcurve = cell(size(model_spikes,1),size(model_spikes,2));
SACnorm = cell(size(model_spikes,1),size(model_spikes,2));

binWidth=0.05;
maxBin=20;

spikeWin=[0 100];
duration = 100;
x = -5:1:100;

for r=1:size(model_spikes,1)
    for m=1:size(model_spikes,2)
        
        TrialData = squeeze(model_spikes(r,m,:))';
        his = zeros(1,length(x));
        rates = nan(1,length(x));
        
        for i = 1:length(TrialData)
            h = hist(TrialData{i},x);
            his = his + h;
            rates(i) = length(TrialData{i});
        end
        
        sac = nan(1,size(his,2));
        
        sac(1) = mean(his.*his);
        
        for i = 2:size(his,2)
            sac(i) = mean(his(2:end-i+2).*his(i:end));
        end
        
        sac = [fliplr(sac), sac];
        
        % % Get SAC values as described by Joris (2006)
%         nf=length(spikes)*(length(spikes)-1)*mean(rates).^2*...
%             (diff(spikeWin)./1000)*(binWidth./1000);
%         SACcurve{r,m}{1,h} = sac;
%         SACnorm{r,m}{1,h} = sac/nf;
        
        % % Get SAC values as written by Bertrand Fontaine via email
%         sac = sac/(binWidth/1000)/(duration/1000)/length(spikes)/(length(spikes)-1);
%         SACcurve{r,m}{1,h} = sac;
%         SACnorm{r,m}{1,h} = (sac-mean(rates).^2)*(binWidth/1000)/mean(rates);
        
        % % Get SAC values as purely described by Brette (2012)
        sac = sac/(binWidth/1000)/length(TrialData);
        SACcurve{r,m} = sac;
        SACnorm{r,m} = (sac-mean(rates).^2)*(binWidth/1000)/mean(rates);
        
        mid = round(length(sac)/2);
%         int = round(3/binWidth);
%         CURVE{r,m}(1,h)=sum(SACnorm{r,m}{1,h}(mid-int:mid+int))/((mid+int)-(mid-int));
        CURVE{r,m} = sum(SACnorm{r,m});
        
        Mean{r,m}=mean(rates);
        
    end
end

CURVE = cell2mat(CURVE);
Mean = cell2mat(Mean);