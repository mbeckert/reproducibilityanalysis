load('ot_synch_ff_shift_nonsmooth.mat')
ff = ot_synch.ff;
clear ot_synch

correct = nan(size(ff{11}{1}));
normalize = [1,1];

chck = ff{6};
chck = chck == 0;
chck = find(chck == 1);
for i = 1:length(chck)
    ff{11}{chck(i)} = correct;
    ff{12}{chck(i)} = correct;
    ff{13}{chck(i)} = correct;
end

GM = sqrt(ff{3} .* ff{4});
GM = normalization(GM,normalize(1));

edges = linspace(0,1,11);

[~,~,idx] = histcounts(GM,edges);

CCG = cell(size(GM,1),max(unique(idx)));
SH = cell(size(GM,1),max(unique(idx)));
COR = cell(size(GM,1),max(unique(idx)));
gm = nan(size(GM,1),max(unique(idx)));

for n = 1:size(GM,1)
    for f = 1:max(unique(idx))
        d = cell2mat(ff{11}(n,idx(n,:) == f));
        d(isnan(d)) = 0;
        CCG{n,f} = sum(d,2)/size(d,2);
        d = cell2mat(ff{12}(n,idx(n,:) == f));
        d(isnan(d)) = 0;
        SH{n,f} = sum(d,2)/size(d,2);
        d = cell2mat(ff{13}(n,idx(n,:) == f));
        d(isnan(d)) = 0;
        COR{n,f} = sum(d,2)/size(d,2);
        d = GM(n,idx(n,:) == f);
        gm(n,f) = nanmean(d);
    end
end

% for n = 1:size(CCG,1)
%     for sp = 1:size(CCG,2)
%         m = max([CCG{n,sp};SH{n,sp}]);
%         CCG{n,sp} = CCG{n,sp}/m;
%         SH{n,sp} = SH{n,sp}/m;
%     end
% end

clear edges idx n f d ff GM chck correct

binNum = 199; int = 10;
% binNum = 39; int = 5;

ccg = nan(size(CCG));
sh = nan(size(SH));
cor = nan(size(COR));

for n = 1:size(ccg,1)
    for sp = 1:size(ccg,2)
        if ~isempty(CCG{n,sp})
            ccg(n,sp) = sum(CCG{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        end
        if ~isempty(SH{n,sp})
            sh(n,sp) = sum(SH{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        end
        if ~isempty(COR{n,sp})
            cor(n,sp) = sum(COR{n,sp}(round(binNum/2) - int:round(binNum/2) + int));
        end
    end
end

clear binNum int n sp

% % normalization and organization

tmp = ccg;
idx = tmp == 0;
rat = sh ./ tmp;
dif = tmp - sh;
rat(idx) = 1;
clear tmp idx

ccg = normalization(ccg,normalize(2));
sh = normalization(sh,normalize(2));
cor = normalization(cor,normalize(2));
sh(~isfinite(sh)) = 0;
ccg(~isfinite(ccg)) = 0;
cor(~isfinite(cor)) = 0;
output{1} = gm;
output{2} = ccg;
output{3} = sh;
output{4} = cor;

clear i normalize 

%%

inv = nan(size(CCG));
% r = find(rat > 1);
% load('chck.mat')
% idx = find(isnan(chck));
% c = nan(size(CCG));
% 
% for i = 1:length(r)
%     if ~isempty(CCG{r(i)}) && ~isempty(SH{r(i)})
%         c(i) = corr(CCG{r(i)},SH{r(i)});
%     end
% end

for i = 1:size(inv,1)*size(inv,2)
% for i = 1:length(idx)
%     if sum(CCG{i}) > 0 && sum(SH{i}) > 0
    if ~isempty(CCG{i}) && ~isempty(SH{i})
        plot(-99:99,CCG{i},'--g')
        hold on
        plot(-99:99,SH{i},'--r')
        plot(-99:99,COR{i},'k')
        title(num2str(i))
%         title([num2str(idx(i)) ' - ' num2str(rat(idx(i))) ' - ' num2str(dif(idx(i)))])
        hold off
%         check = input('anti?- 1 yes, 0 no == ');
        check = input('missing spikes? - 1 yes, (0) no == ');
        if isempty(check)
            inv(i) = 0;
        else
            inv(i) = check;
        end
    end
end

%%
load('chck.mat');
load('inv.mat')

inv(isnan(inv)) = 0;
inv = logical(inv);

a = find(ccg == 0);
chck(a) = 1;
b = find(sh == 0);
chck(b) = 0;
idx = intersect(a,b);

chck(idx) = nan;
ccg(idx) = nan;
sh(idx) = nan;
cor(idx) = nan;
rat(idx) = nan;
dif(idx) = nan;

clear a b check i idx

a = rat(inv);
