
%% Bin data for plotting and statistics

val = cell(1,6);
out = cell(1,6);
X = nan(1,6);

for t = 1:6
    switch t
        case 1
            data = az_trim;
        case 2
            data = el_trim;
        case 3
            data = itd_trim;
        case 4
            data = ild_trim;
        case 5
            data = otAZ_trim;
        case 6
            data = otEL_trim;
    end
    X(t) = ceil(max(data(2,:)));
    x = 0:5:X(t); % for raw values
%     x = 0:0.1:1;    % for normalized values
    val{t} = cell(1,length(x));
    
[~,idx] = histc(data(2,:),x);
out{t} = zeros(3,length(x));

for h = 1:length(x)
    i = idx == h;
    val{t}{h} = data(1,i);
    out{t}(1,h) = nanmedian(data(1,i));
    out{t}(2,h) = nanmean(data(1,i));
    out{t}(3,h) = nanstd(data(1,i));
end

clear i h data idx

end

clear t x

close all

for t = 1:6
    figure(t)
    subplot(2,1,1)
    errorbar(out{t}(1,:),out{t}(3,:))
%     plot(out{t}(1,:))
    subplot(2,1,2)
    errorbar(out{t}(2,:),out{t}(3,:))
%     plot(out{t}(2,:))
%     subplot(3,1,3)
%     plot(out{t}(3,:))
end
    
clear t

for t = 1:6
    m = max(cell2mat(cellfun(@length,val{t},'UniformOutput',0)));
    for i = 1:length(val{t})
        val{t}{i} = [val{t}{i}'; nan(m-length(val{t}{i}),1)];
    end
    val{t} = cell2mat(val{t});
end

clear t m i

%% Performs the same function as the initial plotting in ReproducibiliityAnalysis
% This is here just for convenience when looking at the data
% comment out when not needed

% close all
% subplot(3,2,1)
% scatter(az(2,:),az(1,:),2,'fill')
% subplot(3,2,2)
% scatter(el(2,:),el(1,:),2,'fill')
% subplot(3,2,3)
% scatter(itd(2,:),itd(1,:),2,'fill')
% subplot(3,2,4)
% scatter(ild(2,:),ild(1,:),2,'fill')
% subplot(3,2,5)
% scatter(otAZ(2,:),otAZ(1,:),2,'fill')
% subplot(3,2,6)
% scatter(otEL(2,:),otEL(1,:),2,'fill')