% function [indexer,CURVE,rate]=varyingSAC3
clear all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% "indexer" holds the itd and iid values for each file for later
% organization. ITD are rows, ILD are columns.
% "CURVE" holds the integral values for each ABI in each file. ABI are
% rows, files are columns
% "rate" is the mean evoked firing rate for the file. Organized the same as
% "CURVE"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% input the XDPHYS file names here
files={'023.01.2.0.abi.mat','023.01.2.1.abi.mat','023.01.2.2.abi.mat','023.01.2.3.abi.mat','023.01.2.4.abi.mat','023.01.2.5.abi.mat','023.01.2.6.abi.mat','023.01.2.7.abi.mat','023.01.2.8.abi.mat','023.01.2.9.abi.mat','023.01.2.10.abi.mat','023.01.2.11.abi.mat','023.01.2.12.abi.mat','023.01.2.13.abi.mat','023.01.2.14.abi.mat','023.01.2.15.abi.mat'};
% files={'023.01.2.7.abi.mat'};

% Set up some variables. Change row index to match the number of parameters
% that were presented
count=0;
indexer=zeros([2 length(files)]);
CURVE=zeros([4 length(files)]);
rate=zeros([4 length(files)]);

% Actual Work
for j=1:length(files);
    filestr=files{j};
    count=count+1;
    
    data=BEDSload(filestr);      % Load the XDPHYS file
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %NOTE: we need the stimulus duration
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    [epoch,duration,delay]=BEDSgetEpoch(data);
    
    spikes=BEDSgetSpikes(data);
    depvar=BEDSgetDepvar(data);
    indexer(1,count)=data.trial(1,1).stim.itd;      % Fill in Indexer
    indexer(2,count)=data.trial(1,1).stim.iid;
    
    uniDV=unique(depvar);
    uniDV = setdiff(uniDV,6666);
    holder=cell(round(length(spikes)/length(uniDV)),length(uniDV));
    
    % Organize the data by depvar
    for i=1:length(uniDV);
        co=find(depvar==uniDV(i)); %find the index of a given variable value
        holder(1:length(spikes(co')),i)=spikes(co');
    end
    
    for h=1:length(uniDV);
        spikes=holder(:,h);
        spikeWin=[50 500];
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Run computeSAC
        
        % Set some defaults.
        % binWidth=0.2;
        % binWidth=0.1
        binWidth=50e-3;
        % binWidth=20e-3;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% NOTE: you should take the window where the SAAC is above
        %%% baseline. This should correspond to the time scale of the
        %%% spike train. I took 4ms
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        maxBin=4;
        % warnopts(assignopts(who,varargin));
        
        % Make sure spikeWin is sorted in the correct order.
        spikeWin=sort(spikeWin);
        
        % Pre-allocate some arrays.
        rates=zeros(size(spikes));
        histax=-maxBin:binWidth:maxBin;
        sac=zeros(size(histax));
        % Now the work...
        for n=1:length(spikes)
            
            % For the current trial, get the spikes, filter out any outside the
            % spike window, and then compute a mean rate for use in the normalizing
            % factor later.
            thisTrial=spikes{n};
            thisTrial=thisTrial(thisTrial>spikeWin(1)&thisTrial<spikeWin(2));
            rates(n)=length(thisTrial)./(diff(spikeWin)./1000);
            
            % Now, for each trial which we're NOT working on, find all the spikes
            % in the spike window.
            idx=1:length(spikes);idx=idx(idx~=n);
            restSpikes=[spikes{idx}];
            restSpikes=restSpikes(restSpikes>spikeWin(1)&...
                restSpikes<spikeWin(2));
            
            % Then, for each spike in the current trial, subtract it from the times
            % of all spikes from all other trials, bin them up, and keep track of
            % them in the running histogram.
            for t=1:length(thisTrial);
                rs=restSpikes-thisTrial(t);
                rs=rs(abs(rs)<maxBin);
                tmp=hist(rs,histax);
                sac=sac+tmp;
            end
        end
        
        % % Finally, compute the normalizing factor.
        % nf=length(spikes)*(length(spikes)-1)*mean(rates).^2*...
        %     (diff(spikeWin)./1000)*(binWidth./1000);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Calculate the reliability (integral of the SAC)
%         CURVE(h,j)=sum(sac(150:450)-mean(rates).^2)*binWidth/1000/mean(rates);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %note: the first problem is that you are using ms as units.
        %Everything should be converted to seconds (hence the /1000).
        %Also, there is actually a normalization to do. If you check
        %Romain's Plos comput. biol. paper he talks about mean cross-correlogram so we have to
        %first normalized by the number of loops. Then normalize by the
        %duration and the bin size. 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        sac = sac/(binWidth/1000)/(duration/1000)/length(spikes)/(length(spikes)-1);
        CURVE(h,j)=sum(sac-mean(rates).^2)*(binWidth/1000)/mean(rates);
        rate(h,j)=mean(rates);
        
    end
end

