load('blacklist.mat')
output = cell(57,1);

Window = sort([-0.1 0.1]);
binWidth = 0.001;
binNum = length(Window(1):binWidth:Window(2))-2;
norminc = 0:0.1:1;

dur = 0.15;
pattern = 'frozen.|b.|i.';                   % These are to remove
pattern2 = '.';                   % These are to keep
f = 'ff';

D=[];

for g = 1:length(output)
    tic
disp(['ITERATION: ' num2str(g)])

data = createsyntheticspiketimes;

for date=1:size(data,2)
    for site = 1:length(data{date})
        list=combnk(2:length(data{date}{site}),2);
        for pair = 1:size(list,1)
            
%             disp(['date: ' num2str(date) '; site: ' num2str(site) '; unit1: ' num2str(list(pair,1)) '; unit2: ' num2str(list(pair,2))]);
            
            unit1 = list(pair,1); unit2 = list(pair,2);
            chck1 = blacklist(:,1) == date;
            chck2 = blacklist(:,2) == site;
            chck3 = blacklist(:,3) == unit1;
            chck4 = blacklist(:,3) == unit2;
            check = chck1 .* chck2 .* (chck3 + chck4);
            
            if sum(check) > 0
                pattern2 = 'FFf1';  
            elseif sum(check) == 0
                pattern2 = '.';
            end
            
            unit1 = data{date}{site}{list(pair,1)};
            unit2 = data{date}{site}{list(pair,2)};
            filetypes1 = fieldnames(unit1);
            filetypes2 = fieldnames(unit2);
            filetypes = intersect(filetypes1,filetypes2);
            idx = regexpi(filetypes,pattern);
            idx = cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            idx = regexpi(filetypes,pattern2);
            idx = ~cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            
            clear filetypes1 filetypes2 idx chck* check
            
            if ~isempty(filetypes)
                st_u1 = []; st_u2 = []; sc_u1 = []; sc_u2 = []; bc_u1 = []; bc_u2 = [];
                for file = 1:length(filetypes)
                    st_u1 = [st_u1,unit1.(filetypes{file}).spikes_times];
                    st_u2 = [st_u2,unit2.(filetypes{file}).spikes_times];
                    sc_u1 = [sc_u1,unit1.(filetypes{file}).spikes_count];
                    sc_u2 = [sc_u2,unit2.(filetypes{file}).spikes_count];
                    bc_u1 = [bc_u1,unit1.(filetypes{file}).sensitive];
                    bc_u2 = [bc_u2,unit2.(filetypes{file}).sensitive];
                end
                
                br1 = sum(bc_u1,2);
                br2 = sum(bc_u2,2);
                br1 = br1 > 0;
                br2 = br2 > 0;
                depvar = size(sc_u1,1);
                reps = size(sc_u1,2);
                
                CCG = zeros(depvar,binNum);
                shiftCCG = zeros(depvar,binNum);
                
                for t = 1:depvar
                    for r = 1:reps
                        if ~isempty(st_u1{t,r})
                            for spike = 1:length(st_u1{t,r})
                                times = st_u1{t,r}(spike) - st_u2{t,r};
                                his=hist(times,Window(1):binWidth:Window(2)); %% Here is where you can change binwidth
                                CCG(t,:)=CCG(t,:)+his(2:end-1);
                                
                                % shuffle using the shifted trial number
%                                 if t == size(sc_u1,1)
%                                     times = st_u1{t,r}(spike) - (rand(size(st_u2{1})) * dur);
%                                 else
%                                     times = st_u1{t,r}(spike) - (rand(size(st_u2{t+1,r})) * dur);
%                                 end
                                
                                % shuffle using the same trial number
%                                 times = st_u1{t,r}(spike) - (rand(size(st_u2{t,r})) * dur);
                                
                                % regular shift
                                if t==size(sc_u1,1)
                                    times = st_u1{t,r}(spike) - st_u2{1};
                                else
                                    times = st_u1{t,r}(spike) - st_u2{t+1,r};
                                end
                                
                                his=hist(times,Window(1):binWidth:Window(2));
                                shiftCCG(t,:)=shiftCCG(t,:)+his(2:end-1);
                            end
                        end
                    end
                end % for "t"
                
                clear his spike times
                
                holder = cell(1,13);
                holder{1} = [date,site,list(pair,1),list(pair,2)];
                for i = 2:10
                    holder{i} = nan(1,depvar);
                end
                for i = 11:13
                    holder{i}=cell(1,depvar);
                end
                
                for t = 1:depvar
                    
                    check2 = br1(t) .* br2(t);
                    
                    rate1=mean(sc_u1(t,:)/dur);
                    rate2=mean(sc_u2(t,:)/dur);
                    
                    SmCCG=(smooth(CCG(t,:))/sqrt(rate1*rate2))/binNum;
                    SmShiftCCG=(smooth(shiftCCG(t,:))/sqrt(rate1*rate2))/binNum;
                    
                    SmCCGCorrected=SmCCG-SmShiftCCG;
                    
                    flankS=std([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                    flankM=mean([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                    center = SmCCGCorrected(round(binNum/2) - 15:round(binNum/2) + 15);
                    
                    PEAK=SmCCGCorrected(round(binNum/2));
                    %                     check1 = max(center)>flankM+5*flankS;
                    check1 = PEAK>flankM+5*flankS;
                    positive=(SmCCGCorrected>=(PEAK/2));
                    pos=diff(positive);
                    less=pos(1:round(binNum/2));
                    more=pos(round(binNum/2):end);
                    Low=find(less==1,1,'last');
                    High=find(more==-1,1,'first');
                    Low=length(less)-Low;
                    bandPEAK=High+Low;
                    
                    if isempty(bandPEAK)
                        bandPEAK=nan;
                        sumband=nan;
                    else
                        sumband=sum(SmCCGCorrected(length(less)-Low:length(less)+High))/bandPEAK;
                    end
                    
                    holder{2}(1,t)=PEAK;
                    holder{3}(1,t)=rate1;
                    holder{4}(1,t)=rate2;
                    holder{5}(1,t)=check1;
                    holder{6}(1,t)=check2;
                    %                     holder{7}(1,i)=check;
                    holder{8}(1,t)=bandPEAK;
                    holder{9}(1,t)=sum(center);
                    holder{10}(1,t)=sumband;
                    holder{11}{1,t}=SmCCG;
                    holder{12}{1,t}=SmShiftCCG;
                    holder{13}{1,t}=SmCCGCorrected;
                    
                    clear positive pos less more Low High bandPEAK sens1 sens2
                    
                end     % for "i" the second time
                
                if isfield(D,f)
                    for i = 1:10
                        if size(D.(f){i},2) > size(holder{i},2)
                            padding = nan(1,size(D.(f){i},2)-size(holder{i},2));
                            D.(f){i}=[D.(f){i};holder{i},padding];
                        elseif size(D.(f){i},2) < size(holder{i},2)
                            padding = nan(size(D.(f){i},1),size(holder{i},2)-size(D.(f){i},2));
                            D.(f){i}=[D.(f){i},padding;holder{i}];
                        else
                            D.(f){i}=[D.(f){i};holder{i}];
                        end
                    end
                    for i = 11:13
                        if size(D.(f){i},2) > size(holder{i},2)
                            padding = cell(1,size(D.(f){i},2)-size(holder{i},2));
                            D.(f){i}=[D.(f){i};holder{i},padding];
                        elseif size(D.(f){i},2) < size(holder{i},2)
                            padding = cell(size(D.(f){i},1),size(holder{i},2)-size(D.(f){i},2));
                            D.(f){i}=[D.(f){i},padding;holder{i}];
                        else
                            D.(f){i}=[D.(f){i};holder{i}];
                        end
                    end
                else
                    for i = 1:length(holder)
                        D.(f){i}=holder{i};
                    end
                end
                
                clear holder depvar rate* i PEAK check SmCCG curves CORR trial flank* sumband SmShiftCCG shiftCCG SmCCGCorrected CCG
                
            end         % for "file"
            
            clear filetypes
            
        end             % for "pair"
    end                 % for "site"
end                     % for "date"

output{g} = D.ff;
clear D
D=[];
toc

end

clearvars -except output

cd('F:\Desktop\WorkingFolder')

save syntheticOT_3

%% 
cd('F:\Desktop\WorkingFolder\synthetic')

data = cell(1,57);

for it = 1:57
    eval(['load(''synth_' num2str(it) '.mat'')' ]);
    ff = d;
    
c = cell(1,length(ff{1}));
m = cell(1,length(ff{1}));
h = cell(1,length(ff{1}));
p = cell(1,length(ff{1}));
for i = 1:length(ff{1})
    c{i} = ff{9}(i,:);
    m{i} = sqrt((ff{3}(i,:) .* ff{4}(i,:)));
    h{i} = ff{8}(i,:);
    p{i} = ff{2}(i,:);
    chck = ff{5}(i,:);
    chck = chck == 0;
    correct = nan;
    h{i}(chck) = correct;
    p{i}(chck) = correct;
    c{i}(chck) = correct;
    chck = ff{6}(i,:);
    chck = chck == 0;
    correct = nan;
    h{i}(chck) = correct;
    p{i}(chck) = correct;
    c{i}(chck) = correct;
end

data{4,it} = c;
data{1,it} = m;
data{3,it} = h;
data{2,it} = p;

end

% data = cell2mat(data,'UniformOutput',0);
cd('F:\Desktop\WorkingFolder')

clear i c m h p az el chck correct ff it d

% %

% for m = 1:4
%     for it = 1:57
%         data{m,it} = cellfun(@(x) normalization(x,1),data{m,it},'UniformOutput',0);
%     end
% end

for m = 1:4
    for it = 1:57
        data{m,it} = cell2mat(data{m,it});
    end
end        
        
data = cell2mat(data);

[N,EDGES,BIN] = histcounts(data(1,:));

binned = cell(1,length(N));

for b = 1:length(N)
    binned{b} = data(4,BIN == b);
end

clear m it b N

n = sum(isfinite(data(4,:)));
p = quantile(data(4,:),0.95);

save synthetic_KEY