
epoch = 5000;
rep = 100;
trials = 100;

bins = 0:0.001:0.5;
bin = 0.001;
maxWidth = 20;

fr = 10:10:1000;
data1 = nan(length(fr),rep);
data2 = nan(length(fr),rep);
data3 = nan(length(fr),rep);

for r = 1:rep
for fr = 10:10:1000
    sp = cell(1,rep);
    for t = 1:trials
        sp{t} = sort(rand(1,fr)*epoch);
    end
    [~,data1(fr/10,r),~,~,~,data2(fr/10,r),data3(fr/10,r)] = reproducibility(sp,bins,bin,maxWidth);
end
end

clearvars -except data*