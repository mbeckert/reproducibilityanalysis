% function [CURVE,Index,Mean,HIST,HistCorr,SACcurve,SACnorm]=varyingSAC_FF_3 %#ok<STOUT>

cd('C:\Users\mbeck\Desktop\Data\ICls\Data\RawData\FF')

FILES = ls;
FILES = FILES(3:end,:);

Index = cell(1,length(FILES));
HIST = cell(1,length(FILES));
HistCorr = HIST;
CURVE = HIST;
Mean = HIST;
SACcurve = HIST;
SACnorm = HIST;

for file=1:size(FILES,1)
    
    
    %%%%%% Load Data
    load(FILES(file,:))
    disp(num2str(file))
    % TrialData(:,425:500)={NaN};
    % TrialData(:,1:200)={NaN};
    % TrialData(:,290:400)={NaN};
    
    if ~exist('spikecountmatrix','var');
        spikecountmatrix=cellfun(@length,TrialData);
    end
    
    Index{file}=Indexer;
    RasterCheck{file}=zeros([1 size(TrialData,1)]);
    
    %%%%%% Plot PSTH for each Speaker then run correlation on each pair
    
    holder=zeros([1 500]);
    for ii=1:size(TrialData,1);
        %     figure(ii);
        for i=1:size(TrialData,2)
            
            h=hist(TrialData{ii,i},[1:500]);
            holder=holder+h;
            clear h;
        end
        holder(1:50)=0;
        eval(['HIST{file}.spkr' num2str(ii) '=holder;']);
        holder=zeros([1 500]);
    end
    
    % Number of permutations of units
    list=combnk(1:size(TrialData,1),2);
    
    % Preparing the final output
    % Col 1&2 are the units of the correlation
    % Remaining Col are for each freq tested
    HistCorr{file}=zeros([size(list,1) 4]);
    HistCorr{file}(:,1)=list(:,1);
    HistCorr{file}(:,2)=list(:,2);
    
    % Calculate the Correlations and input them into the FINAL matrix
    for k=1:size(list,1);
        eval(['[CORR,p]=corrcoef(HIST{file}.spkr' num2str(list(k,1)) '(50:end),HIST{file}.spkr' num2str(list(k,2)) '(50:end));']);
        HistCorr{file}(k,3)=CORR(1,2);
        HistCorr{file}(k,4)=p(1,2);
    end
    
    clear k holder i ii h

    %%%%%% Actual Work
    
    CHECK = 1;
    PARAM=[0.05,20];
    
    while CHECK==1;
        
        running = 0;
        
        CURVE{file}=zeros([1 size(TrialData,1)]);
        % rate=zeros([1 size(TrialData,1)]);
        SACcurve{file}=cell([1 size(TrialData,1)]);
        SACnorm{file}=cell([1 size(TrialData,1)]);
        
        binWidth=PARAM(1);
        maxBin=PARAM(2);
        
        for h=1:size(TrialData,1)
            spikes=TrialData(h,:); %#ok<COLND>
            spikeWin=[50 500];
            duration = 450;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Run computeSAC
            
            % Make sure spikeWin is sorted in the correct order.
            spikeWin=sort(spikeWin);
            
            % Pre-allocate some arrays.
            rates=zeros(size(spikes));
            histax=-maxBin:binWidth:maxBin;
            sac=zeros(size(histax));
            
            % Now the work...
            for n=1:length(spikes)
                
                % For the current trial, get the spikes, filter out any outside the
                % spike window, and then compute a mean rate for use in the normalizing
                % factor later.
                thisTrial=spikes{n};
                thisTrial=thisTrial(thisTrial>spikeWin(1)&thisTrial<spikeWin(2));
                rates(n)=length(thisTrial)./(diff(spikeWin)./1000);
                
                % Now, for each trial which we're NOT working on, find all the spikes
                % in the spike window.
                idx=1:length(spikes);idx=idx(idx~=n);
                restSpikes=[spikes{idx}];
                restSpikes=restSpikes(restSpikes>spikeWin(1)&...
                    restSpikes<spikeWin(2));
                
                % Then, for each spike in the current trial, subtract it from the times
                % of all spikes from all other trials, bin them up, and keep track of
                % them in the running histogram.
                for t=1:length(thisTrial);
                    rs=restSpikes-thisTrial(t);
                    rs=rs(abs(rs)<maxBin);
                    tmp=hist(rs,histax);
                    sac=sac+tmp;
                end
            end

            sac=sac(2:end-1);

            % % Get SAC values as described by Joris (2006)
%             nf=length(spikes)*(length(spikes)-1)*mean(rates).^2*...
%                 (diff(spikeWin)./1000)*(binWidth./1000);
%             SACcurve{file}{1,h} = sac;
%             SACnorm{file}{1,h} = sac/nf;

            % % Get SAC values as written by Bertrand Fontaine via email
%             sac = sac/(binWidth/1000)/(duration/1000)/length(spikes)/(length(spikes)-1);
%             SACcurve{file}{1,h} = sac;
%             SACnorm{file}{1,h} = (sac-mean(rates).^2)*(binWidth/1000)/mean(rates);
            
            % % Get SAC values as purely described by Brette (2012)
%             sac = sac/(binWidth/1000)/length(spikes);
%             SACcurve{file}{1,h} = sac;
%             SACnorm{file}{1,h} = (sac-mean(rates).^2)*(binWidth/1000)/mean(rates);

            % % Get SAC values through a combined method
%             sac = sac/((binWidth/1000)*(duration/1000))/(length(spikes)*(length(spikes)-1));
%             SACcurve{file}{1,h} = sac;
%             SACnorm{file}{1,h} = (sac-mean(rates).^2)/mean(rates);
                     

            % % Get SAC values without normalization
            sac = sac/(binWidth/1000)/length(spikes);
            SACcurve{file}{1,h} = sac;
            SACnorm{file}{1,h} = sac;

%             mid = round(length(sac)/2);
%             int = round(3/binWidth);
            CURVE{file}(1,h)=sum(SACnorm{file}{1,h});
            
            Mean{file}(1,h)=mean(rates);
        end
        
        CHECK=0;
        
    end
end

clearvars -except Index HIST HistCorr CURVE Mean SACcurve SACnorm

cd('C:\Users\mbeck\Desktop\Data\Analysis\ReproducibilityPaper\working')