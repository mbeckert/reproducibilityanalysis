% Run "otsynch_ff" and then "reformatsynch"

%% Plot data

cd('f:\desktop\WorkingFolder')

cmpl = flipud(gray);
output = nan(3,2);

names = {'Normalized Geometric Mean Firing Rate','Normalized Synchrony'};

for n = 1%:2
    figure(n+10)
    count = 0;
    
    for t = 1:2
        
        clear data
        
        if n == 1
            tt = '_norm';
        else
            tt = [];
        end
        
        switch t
            case 1
                load(['ot_synch_ff' tt '.mat']);
            case 2
                load(['ot_synch_ff_base' tt '.mat']);
        end
        
        data = cell(3,1);
        data{1} = ccg;
        data{2} = sh;
        data{3} = cor;
        
        for m = 1:3
            
            count = count + 1;
            subplot(2,3,count)
            a = GM(:);
            b = data{m}(:);
            
            if n == 1
                [cc, pp] = scatterheatmapplot(a,b, names);
            else
                scatter(a,b, 5, 'fill')
                cc = corr(a,b, 'rows', 'pairwise');
            end
            output(m,t) = cc;
            
        end
        
    end
end

clear colors GM data t m
    
    %% rearrange to be put into GraphPad
    
    evoke = load('ot_synch_ff.mat');
    base = load('ot_synch_ff_base.mat');
    
    i = 'cor';
    
    gm = [evoke.GM(:); base.GM(:)];
    ev = [evoke.(i)(:); nan(length(base.GM(:)),1)];
    ba = [nan(length(evoke.GM(:)),1); base.(i)(:)];
    
    out = [gm,ev,ba];