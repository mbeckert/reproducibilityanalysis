function [CURVE,Indexer,Mean,HIST,HistCorr]=varyingSAC_FF %#ok<STOUT>

%% Load Data
load('C:\Users\PenaLab\Desktop\FreeField\ICls\Feb17-2015\021-2015-0217-03-FFFrozen2-data.mat')
% TrialData(:,425:500)={NaN};
% TrialData(:,1:200)={NaN};
% TrialData(:,250:end)={NaN};

%% Plot Rasters of the Trials

XX=0; YY=1;
for ii=1:size(TrialData,1);
    XX=XX+1;
    FigHandle = figure(ii);
    set(FigHandle, 'OuterPosition', [XX*380-379, 850-(YY*350+80-(350+(80*YY))), 380, 350]);
    clf;hold on;
    for hh=1:length(TrialData);
        if not(isempty(TrialData{ii,hh}));
            scatter(TrialData{ii,hh},(ones(1,length(TrialData{ii,hh}))*hh),8,'fill','k');
        end
    end
    hold off
    if XX==5;
        XX=0;
        YY=YY+1;
    end
end

pause

%% Plot PSTH for each Speaker then run correlation on each pair

holder=zeros([1 500]);
for ii=1:size(TrialData,1);
    figure(ii);
    for i=1:500;

        h=hist(TrialData{ii,i},[1:500]);
        holder=holder+h;
        clear h;
    end
    bar(holder(50:end))
    eval(['HIST.spkr' num2str(ii) '=holder;']);
    holder=zeros([1 500]);
end

% Number of permutations of units
list=combnk(1:size(TrialData,1),2);

% Preparing the final output
% Col 1&2 are the units of the correlation
% Remaining Col are for each freq tested
HistCorr=zeros([size(list,1) 4]);
HistCorr(:,1)=list(:,1);
HistCorr(:,2)=list(:,2);

% Calculate the Correlations and input them into the FINAL matrix
for k=1:size(list,1);
        eval(['[CORR,p]=corrcoef(HIST.spkr' num2str(list(k,1)) '(50:end),HIST.spkr' num2str(list(k,2)) '(50:end));']);
        HistCorr(k,3)=CORR(1,2);
        HistCorr(k,4)=p(1,2);
end

clear k holder i ii h

pause

%% get mean evoked FR from spikecountmatrix

Mean=mean(spikecountmatrix,2);
Mean(isnan(Mean(:,1)))=[];
Mean=Mean';

%% Actual Work

CURVE=zeros([1 size(TrialData,1)]);
PLOT=cell([1 size(TrialData,1)]);

for h=1:size(TrialData,1);
    spikes=TrialData(h,:); %#ok<COLND>
    spikeWin=[50 500];
    duration = 450;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run computeSAC

% Set some defaults.
% binWidth=0.2;
binWidth=0.1;
% binWidth=50e-3;
% binWidth=20e-3;
maxBin=10;
% warnopts(assignopts(who,varargin));

% Make sure spikeWin is sorted in the correct order.
spikeWin=sort(spikeWin);

% Pre-allocate some arrays.
rates=zeros(size(spikes));
histax=-maxBin:binWidth:maxBin;
sac=zeros(size(histax));

% Now the work...
for n=1:length(spikes)
    
    % For the current trial, get the spikes, filter out any outside the
    % spike window, and then compute a mean rate for use in the normalizing
    % factor later.
    thisTrial=spikes{n};
    thisTrial=thisTrial(thisTrial>spikeWin(1)&thisTrial<spikeWin(2));
    rates(n)=length(thisTrial)./(diff(spikeWin)./1000);
    
    % Now, for each trial which we're NOT working on, find all the spikes
    % in the spike window.
    idx=1:length(spikes);idx=idx(idx~=n);
    restSpikes=[spikes{idx}];
    restSpikes=restSpikes(restSpikes>spikeWin(1)&...
        restSpikes<spikeWin(2));
    
    % Then, for each spike in the current trial, subtract it from the times
    % of all spikes from all other trials, bin them up, and keep track of
    % them in the running histogram.
    for t=1:length(thisTrial);
        rs=restSpikes-thisTrial(t);
        rs=rs(abs(rs)<maxBin);
        tmp=hist(rs,histax);
        sac=sac+tmp;
    end
end

% % Finally, compute the normalizing factor.
% nf=length(spikes)*(length(spikes)-1)*mean(rates).^2*...
%     (diff(spikeWin)./1000)*(binWidth./1000);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate the reliability (integral of the SAC)
sac = sac/(binWidth/1000)/(duration/1000)/length(spikes)/(length(spikes)-1);
PLOT{1,h}=(sac-mean(rates).^2)*binWidth/mean(rates);
figure(h); plot(PLOT{1,h});
CURVE(1,h)=sum(sac-mean(rates).^2)*binWidth/mean(rates);

% save 006-2015-0107-03-FFFrozen-analysis

end
