function [CURVE,Index,Mean,HIST,HistCorr,RasterCheck]=varyingSAC_FF_smallwindow %#ok<STOUT>
FILES={
'006-2015-0107-01-FFFrozen2-data.mat'; 
'006-2015-0107-02-FFFrozen-data.mat';  
'006-2015-0107-03-FFFrozen-data.mat';  
'006-2015-0211-01-FFFrozen3-data3.mat';
'006-2015-0211-02-FFFrozen-data.mat';  
'006-2015-0219-01-FFFrozen1-data.mat'; 
'006-2015-0219-01-FFFrozen2-data.mat'; 
'006-2015-0219-02-FFFrozen1-data.mat'; 
'006-2015-0219-02-FFFrozen2-data.mat'; 
'006-2015-0219-02-FFFrozen3-data.mat'; 
'006-2015-0219-03-FFFrozen1-data.mat'; 
'006-2015-0219-03-FFFrozen2-data.mat'; 
'006-2015-0302-01-FFFrozen1-data.mat'; 
'006-2015-0302-01-FFFrozen2-data2.mat';
'006-2015-0302-02-FFFrozen1-data.mat'; 
'006-2015-0302-02-FFFrozen2-data2.mat';
'006-2015-0302-03-FFFrozen1-data.mat'; 
'006-2015-0302-03-FFFrozen2-data3.mat';
'021-2014-1208-06-Frozen-data.mat';    
'021-2014-1208-07-Frozen-data.mat';    
'021-2014-1223-01-FFFrozen2-data.mat'; 
'021-2014-1223-01-FFFrozen3-data.mat'; 
'021-2014-1223-02-FFFrozen-data.mat';  
'021-2015-0114-01-FFFrozen-data2.mat'; 
'021-2015-0114-02-FFFrozen-data2.mat'; 
'021-2015-0114-03-FFFrozen-data2.mat'; 
'021-2015-0114-04-FFFrozen-data2.mat'; 
'021-2015-0209-01-FFFrozen-data.mat';  
'021-2015-0209-02-FFFrozen-data.mat';  
'021-2015-0209-03-FFFrozen-data2.mat'; 
'021-2015-0217-01-FFFrozen1-data.mat'; 
'021-2015-0217-01-FFFrozen2-data.mat'; 
'021-2015-0217-02-FFFrozen1-data.mat'; 
'021-2015-0217-02-FFFrozen2-data.mat'; 
'021-2015-0217-02-FFFrozen3-data.mat'; 
'021-2015-0217-03-FFFrozen1-data.mat'; 
'021-2015-0217-03-FFFrozen2-data.mat'; 
'021-2015-0226-01-FFFrozen1-data2.mat';
'021-2015-0226-02-FFFrozen1-data2.mat';
'021-2015-0226-02-FFFrozen2-data2.mat';
'021-2015-0226-03-FFFrozen1-data2.mat';
'023-2015-0213-01-FFFrozen1-data.mat'; 
'023-2015-0213-02-FFFrozen1-data.mat'; 
'023-2015-0213-02-FFFrozen2-data.mat'; 
'023-2015-0213-03-FFFrozen1-data.mat'; 
'023-2015-0213-03-FFFrozen2-data.mat'; 
'023-2015-0213-04-FFFrozen1-data.mat'; 
'023-2015-0213-04-FFFrozen2-data.mat'; 
'023-2015-0213-04-FFFrozen3-data2.mat';
'023-2015-0223-01-FFFrozen1-data.mat'; 
'023-2015-0223-01-FFFrozen2-data.mat'; 
'023-2015-0223-02-FFFrozen1-data.mat'; 
'023-2015-0223-02-FFFrozen2-data2.mat';
'023-2015-0223-03-FFFrozen1-data.mat'; 
'023-2015-0223-03-FFFrozen2-data2.mat';
'023-2015-0305-01-FFFrozen1-data.mat'; 
'023-2015-0305-01-FFFrozen2-data3.mat';
'023-2015-0305-02-FFFrozen1-data.mat'; 
'023-2015-0305-02-FFFrozen2-data3.mat';
'023-2015-0305-03-FFFrozen1-data.mat'; 
'023-2015-0305-03-FFFrozen2-data3.mat';
'023-2015-0331-01-FFFrozen-data1.mat'; 
'023-2015-0331-02-FFFrozen-data1.mat'; 
'023-2015-0331-03-FFFrozen-data1.mat'};

for file=1:length(FILES);
    
    
%% Load Data
load(FILES{file})
disp(num2str(file))
% TrialData(:,425:500)={NaN};
% TrialData(:,1:200)={NaN};
% TrialData(:,290:400)={NaN};

if ~exist('spikecountmatrix','var');
    spikecountmatrix=cellfun(@length,TrialData);
end

Index{file}=Indexer;
RasterCheck{file}=zeros([1 size(TrialData,1)]);

%% Plot Rasters of the Trials

% XX=0; YY=1;
RC = 2;
for ii=1:size(TrialData,1);
% %     XX=XX+1;
% %     FigHandle = figure(ii);
% %     set(FigHandle, 'OuterPosition', [XX*380-379, 850-(YY*350+80-(350+(80*YY))), 380, 350]);
% %     clf;
% figure(1)
% % x=cell2mat(TrialData(ii,:));
% % C=cellfun(@length,TrialData(ii,:));
% % G=[1,C];
% % idx=cumsum(G);
% % y=nan(1,length(x));
% % for i=1:length(TrialData);
% %     y(idx(i):idx(i+1))=i;
% % end
% % scatter(x,y,8,'fill','k');
%     hold on;
%     for hh=1:length(TrialData);
%         if not(isempty(TrialData{ii,hh}));
%             scatter(TrialData{ii,hh},(ones(1,length(TrialData{ii,hh}))*hh),8,'fill','k');
%         end
%     end
%     hold off
% %     if XX==5;
% %         XX=0;
% %         YY=YY+1;
% %     end
%     RC=input('How does the raster look? no patterning - 0; too few spikes - 1; {meets criteria - 2} ... ');
%     if isempty(RC);
%         RC=2;
%     end
    RasterCheck{file}(ii)=RC;
    close all
end

% check=input('Do the rasters show patterning? = {yes-1} no-0');
% if isempty(check);
%     check=1;
% end
% if check == 0;
%     error('terminated analysis')
% end

% pause

%% Plot PSTH for each Speaker then run correlation on each pair

holder=zeros([1 500]);
for ii=1:size(TrialData,1);
%     figure(ii);
    for i=1:size(TrialData,2)

        h=hist(TrialData{ii,i},[1:500]);
        holder=holder+h;
        clear h;
    end
    holder(1:50)=0;
%     bar(holder)
    eval(['HIST{file}.spkr' num2str(ii) '=holder;']);
    holder=zeros([1 500]);
%     axis([0 500 0 70]);
end

% Number of permutations of units
list=combnk(1:size(TrialData,1),2);

% Preparing the final output
% Col 1&2 are the units of the correlation
% Remaining Col are for each freq tested
HistCorr{file}=zeros([size(list,1) 4]);
HistCorr{file}(:,1)=list(:,1);
HistCorr{file}(:,2)=list(:,2);

% Calculate the Correlations and input them into the FINAL matrix
for k=1:size(list,1);
        eval(['[CORR,p]=corrcoef(HIST{file}.spkr' num2str(list(k,1)) '(50:end),HIST{file}.spkr' num2str(list(k,2)) '(50:end));']);
        HistCorr{file}(k,3)=CORR(1,2);
        HistCorr{file}(k,4)=p(1,2);
end

clear k holder i ii h

% pause

%% get mean evoked FR from spikecountmatrix

Mean{file}=nanmean(spikecountmatrix,2);
Mean{file}(isnan(Mean{file}(:,1)))=[];
Mean{file}=Mean{file}';

%% Actual Work

CHECK = 1;
PARAM=[0.5,20];     % these are default binWidths and maxBin respectively, see commented bit in the Run computeSAC section for some accepted values

while CHECK==1;

%     running=input(['Change Parameters? Current binWidth = ' num2str(PARAM(1)) '; maxBin = ' num2str(PARAM(2)) ': yes-1 {no-0}']);
%     if isempty(running);
%         running=0;
%     end
%     if running == 1;
%         PARAM(1)=input('Input SAC defaults: [binWidth]');
%     end
running = 0;
    
CURVE{file}=zeros([1 size(TrialData,1)]);
% rate=zeros([1 size(TrialData,1)]);
PLOT=cell([1 size(TrialData,1)]);

binWidth=PARAM(1);
maxBin=PARAM(2);

for h=1:size(TrialData,1);
    spikes=TrialData(h,:); %#ok<COLND>
    spikeWin=[50 500];
    duration = 45;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run computeSAC

% Set some defaults.
% binWidth=0.5;
% binWidth=0.2;
% binWidth=0.1;
% binWidth=50e-3;
% binWidth=20e-3;
% maxBin=100;
% maxBin=50;
% maxBin=20;
% maxBin=10;
% warnopts(assignopts(who,varargin));

% Make sure spikeWin is sorted in the correct order.
spikeWin=sort(spikeWin);

% Pre-allocate some arrays.
rates=zeros(size(spikes));
histax=-maxBin:binWidth:maxBin;
sac=zeros(size(histax));

% Now the work...
for n=1:length(spikes)
    
    % For the current trial, get the spikes, filter out any outside the
    % spike window, and then compute a mean rate for use in the normalizing
    % factor later.
    thisTrial=spikes{n};
    thisTrial=thisTrial(thisTrial>spikeWin(1)&thisTrial<spikeWin(2));
    rates(n)=length(thisTrial)./(diff(spikeWin)./1000);
    
    % Now, for each trial which we're NOT working on, find all the spikes
    % in the spike window.
    idx=1:length(spikes);idx=idx(idx~=n);
    restSpikes=[spikes{idx}];
    restSpikes=restSpikes(restSpikes>spikeWin(1)&...
        restSpikes<spikeWin(2));
    
    % Then, for each spike in the current trial, subtract it from the times
    % of all spikes from all other trials, bin them up, and keep track of
    % them in the running histogram.
    for t=1:length(thisTrial);
        rs=restSpikes-thisTrial(t);
        rs=rs(abs(rs)<maxBin);
        tmp=hist(rs,histax);
        sac=sac+tmp;
    end
end

sac=sac(2:end-1);

% % Finally, compute the normalizing factor.
% nf=length(spikes)*(length(spikes)-1)*mean(rates).^2*...
%     (diff(spikeWin)./1000)*(binWidth./1000);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mid = round(length(sac)/2);
int = round(1/binWidth); 

% Calculate the reliability (integral of the SAC)
sac = sac/length(spikes)/(length(spikes)-1);
% PLOT{1,h}=(sac-mean(rates).^2)*(binWidth/1000)/mean(rates);
% figure(h); plot(PLOT{1,h});
CURVE{file}(1,h)=sum(sac(mid-int:int+mid)-mean(rates).^2)*(binWidth/1000)/mean(rates);
% rate(1,j)=mean(rates);

% save 006-2015-0107-03-FFFrozen-analysis

end

CURVE{file}

CHECK=0;
% check=input('Run again? = yes-1 {no-0}');
% if isempty(check);
%     check=0;
% end
% 
% if check==0;
%     CHECK=0;
% end

end
end