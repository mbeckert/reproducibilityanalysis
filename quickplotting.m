for i = 1:length(SACcurve)
    figure(1)
    hold on
    for j = 1:length(SACcurve{i})
        cc = hsv(12);
        plot(SACcurve{i}{j},'color',cc(j,:));
    end
    hold off
    figure(2)
    hold on
    for j = 1:length(SACnorm{i})
        cc = hsv(12);
        plot(SACnorm{i}{j},'color',cc(j,:));
    end
    pause
    close all
end

%%
reps = cell(size(spikes));

for i = 1:length(spikes)
    reps{i} = ones(size(spikes{i})).*i;
end

x = cell2mat(spikes);
y = cell2mat(reps);

scatter(x,y,2,'fill')