%% Probability method but always comparing to the original template
% This is similar to the physiological data of comparing the response at
% the best speaker to across all other speakers in free field stimulation

dur = 1;
reps = 200;
q = 10;

n_spikes = 1:1:100;
n_prob = 0.01:0.05:1;

euc = nan(length(n_prob), length(n_spikes));
vp = nan(length(n_prob), length(n_spikes));
sync = nan(length(n_prob), length(n_spikes));
spt = cell(length(n_prob), length(n_spikes));

for n = 1:length(n_spikes)
    sp_tmp = poissonspikes(n_spikes(n), dur, 0.0001)';
    
    for p = 1:length(n_prob)
        disp(['n = ' num2str(n) '; p = ' num2str(p)])
        tic
        
        
        vt = cell(1, reps);
        for r = 1: reps
            vt{r} = rand(length(sp_tmp), 1);
        end
        
        spt{p, n} = cellfun(@(x) sp_tmp(n_prob(p) >= x), vt, 'UniformOutput', 0);
        
        holder = nan(reps, 1);
        holder2 = nan(reps, 1);
        holder3 = nan(reps, 1);
        
        for r = 1:reps
            holder(r) = euclidean_distance(sp_tmp, spt{p, n}{r}, 0:0.001:dur);
            holder2(r) = VPdistance(sp_tmp, spt{p, n}{r}, q);
            [holder3(r), ~] = Event_Sync(sp_tmp, spt{p, n}{r});
        end
        
        euc(p, n) = nanmean(holder);
        vp(p, n) = nanmean(holder2);
        sync(p, n) = nanmean(holder3);
        
    end
    toc
end

clear dur reps q n sp_tmp p vt r holder*

% save DistanceTest_prob_Vtemplate