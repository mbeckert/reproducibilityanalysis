
dur = 1000;

Window = sort([-0.1 0.1]);
binWidth = 0.001;
binNum = 199;
int = 10;

reps = 500;
n_spikes = 1:1:50;

spikes1 = cell(reps, length(n_spikes));
spikes2 = cell(reps, length(n_spikes));
y = cell(reps, length(n_spikes));
synch = nan(4,length(n_spikes));

for n = 1:length(n_spikes)
    spikes1(:, n) = cellfun(@(x) rand([1,n_spikes(n)]), spikes1(:, n), 'UniformOutput', 0);
    spikes2(:, n) = cellfun(@(x) rand([1,n_spikes(n)]), spikes2(:, n), 'UniformOutput', 0);
end
for r = 1:reps
    for n = 1:length(n_spikes)
        y(r, n) = cellfun(@(x) ones(1, n_spikes(n)) * r, y(r, n), 'UniformOutput', 0);
    end
end

for n = 1:length(n_spikes)
    
    [CCG, SH, COR, ~, ~, ...
        ~, ~, synch(1,n)] = ...
        measuresynch(spikes1(:,n), ...
        spikes2(:,n), binWidth, Window, dur);
    
    synch(2,n) = sum(CCG(round(binNum/2) - int:round(binNum/2) + int));
    synch(3,n) = sum(SH(round(binNum/2) - int:round(binNum/2) + int));
    synch(4,n) = sum(COR(round(binNum/2) - int:round(binNum/2) + int));
    
end

clear n r CCG SH COR

clear bin binNum bins dur int Window binWidth

%%

co = corrcoef(synch');

figure

subplot(1,4,1)
plot(synch(1,:),synch(2,:))
axis square
xlabel('gm fr')
ylabel('ccg')

subplot(1,4,2)
plot(synch(1,:),synch(3,:))
axis square
xlabel('gm fr')
ylabel('sh')

subplot(1,4,3)
plot(synch(1,:),synch(4,:))
axis square
xlabel('gm fr')
ylabel('cor')

subplot(1,4,4)
imagesc(co)
axis square
xticks(1:size(co,1))
yticks(1:size(co,2))
xticklabels({'gm','ccg','sh','cor'})
yticklabels({'gm','ccg','sh','cor'})
title('correlations')

%%

for i = 1:length(n_spikes)
    subplot(1, length(n_spikes), i)
    
    scatter(cell2mat(spikes1(:,i)'), cell2mat(y(:,i)'), 3, 'b', 'fill')
    hold on
    scatter(cell2mat(spikes2(:,i)'), cell2mat(y(:,i)'), 3, 'r', 'fill')
    xlabel('time')
    ylabel('reps')
    
    title(num2str(n_spikes(i)))
    axis square
end