for R = 1:20

dur = 1;
q = 10;
reps = 100;

n_spikes = 1:5:100;
n_noise = 0.001:0.005:0.05;

euc = cell(length(n_spikes), 1);
vp = cell(length(n_spikes), 1);
es = cell(length(n_spikes), 1);
spikes1 = cell(length(n_spikes), 1);
spikes2 = cell(length(n_spikes), 1);

for n = 1:length(n_spikes)
    
    tic
    
    euc{n} = nan(length(n_noise));
    vp{n} = nan(length(n_noise));
    es{n} = nan(length(n_noise));    
    spikes1{n} = cell(length(n_noise));
    spikes2{n} = cell(length(n_noise)); 
    
    spt1 = poissonspikes(n_spikes(n), dur, 0.0001)';
    spt2 = poissonspikes(n_spikes(n), dur, 0.0001)';
    
    for p1 = 1:length(n_noise)
        for p2 = 1:length(n_noise)
            
            disp(['n = ' num2str(n) '; p1 = ' num2str(p1) '; p2 = ' num2str(p2)])
            
            sp1 = repmat({spt1}, 1, reps);
            sp1 = cellfun(@(x) (((n_noise(p1) - -n_noise(p1)) * rand([length(spt1), 1])) + -n_noise(p1)) + x, sp1, 'UniformOutput', 0);
            sp2 = repmat({spt2}, 1, reps);
            sp2 = cellfun(@(x) (((n_noise(p2) - -n_noise(p2)) * rand([length(spt2), 1])) + -n_noise(p2)) + x, sp2, 'UniformOutput', 0);
            
            holder1 = nan(reps, 1);
            holder2 = nan(reps, 1);
            holder3 = nan(reps, 1);
            
            for r = 1:reps
                holder1(r) = euclidean_distance(sp1{r}, sp2{r}, 0:0.001:dur);
                holder2(r) = VPdistance(sp1{r}, sp2{r}, q);
                [holder3(r), ~] = Event_Sync(sp1{r}, sp2{r});
            end
            
            euc{n}(p1, p2) = nanmean(holder1);
            vp{n}(p1, p2) = nanmean(holder2);
            es{n}(p1, p2) = nanmean(holder3);
            spikes1{n}{p1, p2} = sp1;
            spikes2{n}{p1, p2} = sp2;
            
        end
    end
    
    toc
    
end

clear dur holder* n p1 p2 q r reps sp1 sp2 spt1 spt2

save(['DistanceTest_jitter_2pat' num2str(R)])

end