duration = 1;
n_spikes = 0.001:0.1:1;
reps = 200;

q = 10;     % scaling factor for cost of time shifting a spike

result = cell(1, length(n_spikes));
distance = cell(1, length(n_spikes));
sync = cell(1, length(n_spikes));
spt_1 = cell(reps, length(n_spikes));
spt_2 = cell(reps, length(n_spikes));

for n = 1:length(n_spikes)
    disp(['n = ' num2str(n)])
    
    result{n} = nan(reps, 1);
    distance{n} = nan(reps, 1);
    sync{n} = nan(reps, 1);
    for r = 1:reps
        
        if rand(1) < n_spikes(n)
            s = 1;
        else
            s = 0;
        end
        spt_1{r, n} = poissonspikes(s, duration, 0.0001)';
        if rand(1) > n_spikes(n)
            s = 1;
        else
            s = 0;
        end
        spt_2{r, n} = poissonspikes(s, duration, 0.0001)';
        spt_2{r, n}(spt_2{r, n} > duration) = duration;
        result{n}(r) = VPdistance_reich(spt_1{r, n}, spt_2{r, n}, q);
        distance{n}(r) = euclidean_distance(spt_1{r, n}, spt_2{r, n}, 0:0.001:duration);
        sync{n}(r) = Event_Sync(spt_1{r, n}, spt_2{r, n});
        
    end
end

fr_1 = cellfun(@length, spt_1);
fr_2 = cellfun(@length, spt_2);

clear duration q n cost r reps
