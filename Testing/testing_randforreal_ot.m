cd('f:\desktop\WorkingFolder\distance\data')
load('FreeField_OT_wDelay','OT')
% load('FreeField_OT_wDelay_unsorted','OT')
data = OT; clear OT
cd('F:\Desktop\WorkingFolder')

data = data_trim(data, 0.1);

sp_count = cell(200,1);
Mean = cell(200,1);

start = 0.1;

count = 1;

for date = 1:length(data)
    for site = 1:length(data{date})
        for unit = 2:length(data{date}{site})
            
            d = data{date}{site}{unit};
            disp(['Date - ' num2str(date) '; Site - ' num2str(site) '; Unit - ' num2str(unit)]);
            filetypes = fieldnames(d);
            idx = regexpi(filetypes,'FFf.');
            filetypes = filetypes(~cellfun(@isempty,idx));
            
            sp_count{count} = cell([length(filetypes) 1]);
            Mean{count} = cell([length(filetypes) 1]);
            
            check = 0;
            
            for file=1:length(filetypes)
                TrialData = d.(filetypes{file}).spikes_times;
                Mean{count}{file} = nanmean(cellfun(@length, TrialData), 2)';
                
                for i = 1:length(TrialData(:))
                    TrialData{i}(TrialData{i} < start) = [];
                end
                
                sp_count{count}{file} = cellfun(@length, TrialData);
                check = check + sum(sum(Mean{count}{file}));
            end
            
            if check > 0
                count = count + 1;
            end
            
        end
    end
end

L = cellfun(@isempty,Mean);
sp_count(L)=[];
Mean(L)=[];

clear check count d date file filetypes i site unit L start TrialData idx

%%

q = 10;
dur = 0.15;
del = 0.05;
start = 0.1;

mod_dist = cell(size(sp_count));

for u = 1:length(sp_count)
    
    disp(num2str(u))
    mod_dist{u} = cell(size(sp_count{u}));
    
    for f = 1:length(sp_count{u})

        TrialData = cell(size(sp_count{u}{f}));
        
        for r = 1:length(TrialData(:))
            TrialData{r} = sort((del + dur - start) .* rand(sp_count{u}{f}(r), 1) + start);
        end
        
        mod_dist{u}{f} = nan(size(TrialData, 2), 1);
        
        idx = ones(size(TrialData, 2));
        idx = logical(tril(idx, -1));
       
        for s = 1:size(TrialData, 1)
            holder = nan(size(TrialData, 2));
            for t1 = 1:size(TrialData, 2)
                for t2 = 1:size(TrialData, 2)
                    if idx(t1, t2) == 1
                        holder(t1, t2) = VPdistance_reich(TrialData{s, t1}, TrialData{s, t2}, q);
                    end
                end
            end
            mod_dist{u}{f}(s) = nanmean(holder(idx));
        end
        
    end
end
        
clear q dur del start u f TrialData r idx s holder t1 t2

for i = 1:length(mod_dist)
    mod_dist{i} = cell2mat(mod_dist{i}');
    Mean{i} = cell2mat(Mean{i});
end
mod_dist = cellfun(@(x) nanmean(x, 2), mod_dist, 'UniformOutput', 0);
Mean = cellfun(@(x) nanmean(x, 1), Mean, 'UniformOutput', 0);

clear i data
%%

