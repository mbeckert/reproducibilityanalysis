duration = 0.1;
n_spikes = 1:1:200;
reps = 100;
poly = 4;

for q = 100     % scaling factor for cost of time shifting a spike
    tic
disp(['----- q = ' num2str(q) ' -----'])
result = cell(1, length(n_spikes));
distance = cell(1, length(n_spikes));
sync = cell(1, length(n_spikes));
spt_1 = cell(reps, length(n_spikes));
spt_2 = cell(reps, length(n_spikes));

for n = 1:length(n_spikes)
    disp(['n = ' num2str(n)])
    
    result{n} = nan(reps, 1);
    distance{n} = nan(reps, 1);
    sync{n} = nan(reps, 1);
    for r = 1:reps
        
        spt_1{r, n} = sort(randi(int16(duration / 0.0001) , [n_spikes(n) 1]) * 0.0001);
        spt_2{r, n} = sort(randi(int16(duration / 0.0001) , [randi(n_spikes(n) * 2) - 1 1]) * 0.0001);

        result{n}(r) = VPdistance_reich(spt_1{r, n}, spt_2{r, n}, q);
        distance{n}(r) = euclidean_distance(spt_1{r, n}, spt_2{r, n}, 0:0.001:duration);
        sync{n}(r) = Event_Sync(spt_1{r, n}, spt_2{r, n});
        
    end
end

fr_2 = cellfun(@length, spt_2) ./ duration;
n_spikes = n_spikes ./ duration;

dis = cell2mat(distance);
re = cell2mat(result);
sy = cell2mat(sync);
gm = nan(size(fr_2));

for i = 1:length(n_spikes)
    gm(:, i) = (fr_2(:, i) + n_spikes(i)) / 2;
end

[~, ~, idx] = histcounts(gm, max(n_spikes));

key = nan(max(idx(:)), 4);
for i = 1:max(idx(:))
    key(i, 1) = nanmedian(gm(idx == i));
    key(i, 2) = nanmedian(re(idx == i));
    key(i, 3) = nanmedian(dis(idx == i));
    key(i, 4) = nanmedian(sy(idx == i));
end

cellplot = cell(max(idx(:)), 4);
for i = 1:max(idx(:))
    cellplot{i, 1} = gm(idx == i);
    cellplot{i, 2} = re(idx == i);
    cellplot{i, 3} = dis(idx == i);
    cellplot{i, 4} = sy(idx == i);
end

idx = isnan(key(:, 1));
key(idx, :) = [];

cfun = cell(3, 1);

for t = 2:4

p = polyfitZero(key(1:35, 1), key(1:35, t), poly);

cfun{t - 1} = p;

end

% clear t p i idx

save(['distance_correction_0_FR_p' num2str(poly) '_c0' num2str(duration * 100) '_q_' num2str(q)])

toc

end
