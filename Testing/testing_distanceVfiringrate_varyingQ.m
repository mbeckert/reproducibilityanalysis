duration = 1;
n_spikes = 1:2:200;
reps = 100;

n_q = linspace(0, 1000, 100);     % scaling factor for cost of time shifting a spike

distance = cell(length(n_q), 1);
spt_1 = cell(reps, length(n_spikes));
spt_2 = cell(reps, length(n_spikes));

disp('generating spike times')
tic
for n = 1:length(n_spikes)
    for r = 1:reps
        
        spt_1{r, n} = poissonspikes(n_spikes(n), duration, 0.0001)';
        spt_2{r, n} = poissonspikes(randi(n_spikes(n) * 2) - 1, duration, 0.0001)';
        spt_2{r, n}(spt_2{r, n} > duration) = duration;
        
    end
end
tic

disp('calculating distance')

for q = 1:length(n_q)
    disp(['q = ' num2str(n_q(q))])
    tic
    distance{q} = nan(reps, length(n_spikes));
    for n = 1:length(n_spikes)
        disp(['spikes = ' num2str(n_spikes(n))])
        
        for r = 1:reps
            distance{q}(r, n) = VPdistance_reich(spt_1{r, n}, spt_2{r, n}, n_q(q));
        end
    end
    toc
end

fr_2 = cellfun(@length, spt_2);

clear duration q n r reps

% save testing_q

%%

load('testing_q.mat')

rm = nan(size(fr_2));
for i = 1:length(n_spikes)
    rm(:, i) = (fr_2(:, i) + n_spikes(i)) / 2;
end

[~, ~, idx] = histcounts(rm, length(n_spikes));

key = nan(max(idx(:)), size(distance, 1));
gm = nan(size(idx, 1), 1);
for i = 1:max(idx(:))
    gm(i, 1) = nanmean(rm(idx == i));
    for j = 1:size(distance, 1)
        key(i, j) = nanmean(distance{j}(idx == i));
    end
end

clear i j idx 
%%

imagesc(key')
xticks(1:size(gm, 1))
xticklabels(num2str(gm))
xlabel('spike count')
yticks(1:length(n_q))
yticklabels(num2str(n_q'))
ylabel('q')

%%

plot(n_spikes, key)
xlabel('spike count')

%%

surf(key)

