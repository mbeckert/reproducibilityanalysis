fol = 'f:\desktop\WorkingFolder\distance\';

figure(4)

load([fol 'DistanceTest_jitter.mat'])

subplot(3,3,1)
imagesc(euc)
xticks(1:size(euc,2))
xticklabels(n_spikes)
xlabel('mean spike count')
yticks(1:size(euc,1))
yticklabels(n_noise)
ylabel('noise')
axis square
colorbar
title('Euclidean Distance')

subplot(3,3,2)
imagesc(vp)
xticks(1:size(vp,2))
xticklabels(n_spikes)
xlabel('mean spike count')
yticks(1:size(vp,1))
yticklabels(n_noise)
ylabel('noise')
axis square
colorbar
title('Victor-Purpura Cost')

subplot(3,3,3)
imagesc(sync)
xticks(1:size(sync,2))
xticklabels(n_spikes)
xlabel('mean spike count')
yticks(1:size(sync,1))
yticklabels(n_noise)
ylabel('noise')
axis square
colorbar
title('Quiroga Event Synchronization')

load([fol 'DistanceTest_prob.mat'])

counts = cell(size(spt));

for r = 1:size(spt, 1)
    for c = 1:size(spt, 2)
        counts{r, c} = cellfun(@length, spt{r, c});
    end
end

counts = cellfun(@mean, counts);
counts = mean(counts);

EUC = cell(1,size(euc, 2));
VP = cell(1, size(vp, 2));
ES = cell(1, size(sync, 2));

for i = 1:size(euc, 2)
    EUC{i} = euc(:, i);
    VP{i} = vp(:, i);
    ES{i} = sync(:, i);
end

subplot(3,3,4)
boxplot2_points(counts, EUC, 4)
xlabel('spike counts')
ylabel('euclidean distance')

subplot(3,3,5)
boxplot2_points(counts, VP, 4)
xlabel('spike counts')
ylabel('victor-purpura cost')

subplot(3,3,6)
boxplot2_points(counts, ES, 4)
xlabel('spike counts')
ylabel('quiroga event synchronization')



load([fol 'DistanceTest_prob_Vtemplate.mat'])

subplot(3,3,7)
imagesc(euc)
xticks(1:size(euc,2))
xticklabels(n_spikes)
xlabel('template spike count')
yticks(1:size(euc,1))
yticklabels(n_noise)
ylabel('train 2 prob')
axis square
colorbar
title('Euclidean Distance')

subplot(3,3,8)
imagesc(vp)
xticks(1:size(vp,2))
xticklabels(n_spikes)
xlabel('template spike count')
yticks(1:size(vp,1))
yticklabels(n_noise)
ylabel('train 2 prob')
axis square
colorbar
title('Victor-Purpura Cost')

subplot(3,3,9)
imagesc(sync)
xticks(1:size(sync,2))
xticklabels(n_spikes)
xlabel('template spike count')
yticks(1:size(sync,1))
yticklabels(n_noise)
ylabel('train 2 prob')
axis square
colorbar
title('Quiroga Event Synchronization')

%% plot scatter to show patterning

y = cell(size(spt));
for r = 1:size(spt, 1)
    for c = 1:size(spt, 2)
        y{r, c} = get_spike_y(spt{r, c});
    end
end

index = reshape(1 : size(spt,1) * size(spt,2), size(spt,1), size(spt,2));
figure(1)
for i = 1:size(spt,1) * size(spt,2)
    subplot(size(spt,1), size(spt,2), index(i));
    scatter(cell2mat(spt{index(i)}(:)), cell2mat(y{index(i)}(:)), 5, 'fill')
    axis([0 1 0 100])
end

clear i y index

%% Plot testing distance against two patterns
% either changing noise with jitter or using a probability to change firing
% rate

fol = 'f:\desktop\WorkingFolder\distance\';
load([fol 'DistanceTest_jitter_2pat.mat'])

[optmin] = find_minimal_subplot_dim(es);

for n = 1:length(n_spikes)
    
    figure(21)
    subplot(optmin(1), optmin(2), n)
    imagesc(euc{n})
    xticks([]); yticks([])
    axis square
    
    figure(22)
    subplot(optmin(1), optmin(2), n)
    imagesc(vp{n})
    xticks([]); yticks([])
    axis square
    
    figure(23)
    subplot(optmin(1), optmin(2), n)
    imagesc(es{n})
    xticks([]); yticks([])
    axis square
    
end

load([fol 'DistanceTest_prob_2pat.mat'])

[optmin] = find_minimal_subplot_dim(es);

for n = 1:length(n_spikes)
    
    figure(24)
    subplot(optmin(1), optmin(2), n)
    imagesc(euc{n})
    xticks([]); yticks([])
    axis square
    
    figure(25)
    subplot(optmin(1), optmin(2), n)
    imagesc(vp{n})
    xticks([]); yticks([])
    axis square
    
    figure(26)
    subplot(optmin(1), optmin(2), n)
    imagesc(es{n})
    xticks([]); yticks([])
    axis square
    
end


%% plot the rasters for the two pattern tests

% load('f:\desktop\WorkingFolder\distance\DistanceTest_jitter_2pat.mat'); n = 27;
load('f:\desktop\WorkingFolder\distance\DistanceTest_prob_2pat.mat'); n = 28;

u = 10;

spt1 = spikes1{u};
spt2 = spikes2{u};

y1 = cell(size(spt1));
y2 = cell(size(spt2));
for r = 1:size(spt1, 1)
    for c = 1:size(spt1, 2)
        y1{r, c} = get_spike_y(spt1{r, c});
        y2{r, c} = get_spike_y(spt2{r, c});
    end
end

index = reshape(1 : size(spt1,1) * size(spt1,2), size(spt1,1), size(spt1,2));
figure(n)
for i = 1:size(spt1,1) * size(spt1,2)
    subplot(size(spt1,1), size(spt1,2), index(i));
    scatter(cell2mat(spt1{index(i)}(:)), cell2mat(y1{index(i)}(:)), 1, 'fill', 'b')
    hold on
    scatter(cell2mat(spt2{index(i)}(:)), cell2mat(y2{index(i)}(:)), 1, 'fill', 'r')
    axis([0 1 0 100])
    xticks([])
    yticks([])
end

clear i y index

%% plot rasters for different iterations of the jitter 2 pattern test
% doing this because unlike the "prob" test, there is no clear trend in the
% distance. This is likley because of how similar overall the two patterns
% are.

fol = 'f:\desktop\WorkingFolder\distance\test_distance_jitter_2pat\';

for i = 1:20
    load([fol 'DistanceTest_jitter_2pat' num2str(i)])
    [optmin] = find_minimal_subplot_dim(es);
    figure(i)
    
    for n = 1:length(es)
    subplot(optmin(1), optmin(2), n)
    imagesc(euc{n})
    xticks([]); yticks([])
    axis square
    end
    
end
    

