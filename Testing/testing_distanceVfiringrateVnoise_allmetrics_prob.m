%% Probability method

dur = 1;
reps = 20;
trials = 100;
q = 10;

n_prob = 0.01:0.05:1;

euc = nan(reps, length(n_prob));
vp = nan(reps, length(n_prob));
sync = nan(reps, length(n_prob));
spt = cell(reps, length(n_prob));

for p = 1:length(n_prob)
    disp(['p = ' num2str(p)])
    tic
    for R = 1:reps      
    
        sp_tmp = poissonspikes(100, dur, 0.0001)';
        
        vt = cell(1, trials);
        for r = 1: trials
            vt{r} = rand(length(sp_tmp), 1);
        end
        
        spt{R, p} = cellfun(@(x) sp_tmp(n_prob(p) >= x), vt, 'UniformOutput', 0);
        
        holder = nan(trials);
        holder2 = nan(trials);
        holder3 = nan(trials);
        
        idx = ones(trials);
        idx = tril(idx, -1);
        
        for t1 = 1:trials
            for t2 = 1:trials
                if idx(t1, t2) == 1
                    holder(t1, t2) = euclidean_distance(spt{R, p}{t1}, spt{R, p}{t2}, 0:0.001:dur);
                    holder2(t1, t2) = VPdistance(spt{R, p}{t1}, spt{R, p}{t2}, q);
                    [holder3(t1, t2), ~] = Event_Sync(spt{R, p}{t1}, spt{R, p}{t2});
                end
            end
        end
        idx = find(idx == 1);
        euc(R, p) = nanmean(holder(idx));
        vp(R, p) = nanmean(holder2(idx));
        sync(R, p) = nanmean(holder3(idx));

    end
    toc
end

clear dur reps q s n idx holder* t1 t2 sp_tmp R

save DistanceTest_prob