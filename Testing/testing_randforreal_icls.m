q = 10;
del = 0.05;
start = 0.1;

for type = 2:3
    switch type
        case 1
            %             cd('f:\desktop\Data\ICls\Data\RawData\FF')
            cd('Z:\Michael Beckert\data\ICls\Data\RawData\FF')
            dur = 0.5;
        case 2
            %             cd('f:\desktop\Data\ICls\Data\RawData\Dichotic\ILD')
            cd('Z:\Michael Beckert\data\ICls\Data\RawData\Dichotic\ILD')
            dur = 0.3;
        case 3
            %             cd('f:\desktop\Data\ICls\Data\RawData\Dichotic\ITD')
            cd('Z:\Michael Beckert\data\ICls\Data\RawData\Dichotic\ITD')
            dur = 0.3;
    end
    
    FILES = ls;
    FILES = FILES(3:end,:);
    
    sp_count = cell(size(FILES, 1), 1);
    Mean = cell(size(FILES, 1), 1);
    Index = cell(size(FILES, 1), 1);
    
    for file=1:size(FILES,1)
        
        if type >1
            load(FILES(file,:), 'curvedata')
            TrialData = curvedata.spike_times;
            clear curvedata
        else
            load(FILES(file,:), 'TrialData', 'Indexer')
            Index{file}=Indexer;
        end
        
        Mean{file} = nanmean(cellfun(@length, TrialData), 2);
        
        TrialData = cellfun(@(x) x/1000, TrialData, 'UniformOutput', 0);
        TrialData = cellfun(@transpose, TrialData, 'UniformOutput', 0);
        
        for i = 1:length(TrialData(:))
            TrialData{i}(TrialData{i} < start) = [];
        end
        
        sp_count{file} = cell2mat(cellfun(@length, TrialData, 'UniformOutput', 0));
        
    end
    
    cd('f:\desktop\WorkingFolder')
    
    mod_dist = cell(size(sp_count));
    
    for u = 1:length(sp_count)
        
        disp(num2str(u))
        
        TrialData = cell(size(sp_count{u}));
        
        for r = 1:length(TrialData(:))
            TrialData{r} = sort((del + dur - start) .* rand(sp_count{u}(r), 1) + start);
        end
        
        mod_dist{u} = nan(size(TrialData, 1), 1);
        
        idx = ones(size(TrialData, 2));
        idx = logical(tril(idx, -1));
        
        for s = 1:size(TrialData, 1)
            holder = nan(size(TrialData, 2));
            for t1 = 1:size(TrialData, 2)
                for t2 = 1:size(TrialData, 2)
                    if idx(t1, t2) == 1
                        holder(t1, t2) = VPdistance_reich(TrialData{s, t1}, TrialData{s, t2}, q);
                    end
                end
            end
            mod_dist{u}(s) = nanmean(holder(idx));
        end
    end

    
    cd('f:\desktop\WorkingFolder')
    
    switch type
        case 1
            save iclsFF_mod mod_dist sp_count Index Mean
            for t = 1:2
                load('iclsFF_mod.mat')
                for i = 1:length(Index)
                    idx = find(Index{i}(t,:) == mode(Index{i}(t,:)));
                    mod_dist{i} = mod_dist{i}(idx);
                    Index{i} = Index{i}(:,idx);
                    Mean{i} = Mean{i}(idx);
                end
                clear i idx
                switch t
                    case 1
                        save iclsAZ_mod mod_dist sp_count Index Mean
                    case 2
                        save iclsEL_mod mod_dist sp_count Index Mean
                end
            end
            clear t
        case 2
            save iclsILD_mod mod_dist sp_count Index Mean
        case 3
            save iclsITD_mod mod_dist sp_count Index Mean
    end
    
    
end