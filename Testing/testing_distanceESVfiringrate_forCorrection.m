duration = 1;
n_spikes = 1:1:100;
reps = 100;

q = 10;     % scaling factor for cost of time shifting a spike

result = cell(1, length(n_spikes));
distance = cell(1, length(n_spikes));
sync = cell(1, length(n_spikes));
spt_1 = cell(reps, length(n_spikes));
spt_2 = cell(reps, length(n_spikes));

for n = 1:length(n_spikes)
    result{n} = nan(reps, 1);
    distance{n} = nan(reps, 1);
    for r = 1:reps
        
        spt_1{r, n} = sort(rand(n_spikes(n), 1)) * duration;
        spt_2{r, n} = sort(rand(randi(n_spikes(n) * 2) - 1, 1)) * duration;
        spt_2{r, n}(spt_2{r, n} < 0.001) = 0.001;
        spt_2{r, n}(spt_2{r, n} > duration) = duration;
        spt_1{r, n}(spt_1{r, n} < 0.001) = 0.001;
        spt_1{r, n}(spt_{r, n} > duration) = duration;
        result{n}(r) = VPdistance_reich(spt_1{r, n}, spt_2{r, n}, q);
        distance{n}(r) = sqrt( sum( (hist(spt_1{r, n}, 0:0.001:duration) - hist(spt_2{r, n}, 0:0.001:duration)) .^2 ) );
        sync{n}(r) = Event_Sync(spt_1{r, n}, spt_2{r, n});
        
    end
end

fr_2 = cellfun(@length, spt_2);

clear duration q n cost r reps

%%

figure; hold on

i = 3;

for ii = 1:size(spt_1, 1)
    figure(1)
    scatter(spt_1{ii, i}, ones(length(spt_1{ii, i}), 1) * 2, 10, 'fill', 'b')
    hold on
    scatter(spt_2{ii, i}, ones(length(spt_2{ii, i}), 1), 10, 'fill', 'r')
    axis([0 1 0 3])
    title(['trial = ' num2str(ii) ...
        '; fr1 = ' num2str(length(spt_1{ii, i})) ...
        '; fr2 = ' num2str(length(spt_2{ii, i})) ...
        '; cost = ' num2str(result{i}(ii))])
    pause
    close 1
end

%%

dis = cell2mat(distance);
re = cell2mat(result);
sy = cell2mat(sync);
gm = nan(size(fr_2));

for i = 1:length(n_spikes)
    %     gm(:, i) = sqrt(fr_2(:, i) * n_spikes(i));
    gm(:, i) = (fr_2(:, i) + n_spikes(i)) / 2;
end

[~, ~, idx] = histcounts(gm, length(n_spikes));

key = nan(max(idx(:)), 3);
for i = 1:max(idx(:))
    key(i, 1) = nanmean(gm(idx == i));
    key(i, 2) = nanmean(re(idx == i));
    key(i, 3) = nanmean(dis(idx == i));
    key(i, 4) = nanmean(sy(idx == i));
end

clear i idx
%%

figure(5)
boxplot2_points(1:length(n_spikes), result, 0.5);
xlim([0, length(n_spikes) + 1])
xticks(1:length(n_spikes))
xticklabels(n_spikes)
xlabel('firing rate (train 1)')
ylabel('vp cost')
hold on
plot([0 100], [0 100], 'k', 'LineWidth', 3)

plot(key(:, 1), key(:, 2), 'g', 'LineWidth', 3)
scatter(nanmean(fr_2), nanmean(re), 50, 'r', 'fill')
xlabel('gm firing rate')
ylabel('vp cost')

figure(6)
boxplot2_points(1:length(n_spikes), distance, 0.5);
xlim([0, length(n_spikes) + 1])
xticks(1:length(n_spikes))
xticklabels(n_spikes)
xlabel('firing rate (train 1)')
ylabel('euc distance')

plot(key(:, 1), key(:, 3), 'g', 'LineWidth', 3)
scatter(nanmean(fr_2), nanmean(dis), 50, 'r', 'fill')
xlabel('gm firing rate')
ylabel('euc distance')

clear re dis i idx

figure(7)
boxplot2_points(1:length(n_spikes), sync, 0.5);
xlim([0, length(n_spikes) + 1])
xticks(1:length(n_spikes))
xticklabels(n_spikes)
xlabel('firing rate (train 1)')
ylabel('euc distance')

plot(key(:, 1), key(:, 4), 'g', 'LineWidth', 3)
scatter(nanmean(fr_2), nanmean(sy), 50, 'r', 'fill')
xlabel('gm firing rate')
ylabel('event synchronization')

clear re dis i idx


