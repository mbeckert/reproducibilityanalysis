duration = 0.1;
n_spikes = 1:1:200;
reps = 100;
poly = 4;

for q = 2000     % scaling factor for cost of time shifting a spike
    tic
disp(['----- q = ' num2str(q) ' -----'])
result = cell(1, length(n_spikes));
distance = cell(1, length(n_spikes));
sync = cell(1, length(n_spikes));
spt_1 = cell(reps, length(n_spikes));
spt_2 = cell(reps, length(n_spikes));

for n = 1:length(n_spikes)
    disp(['n = ' num2str(n)])
    
    result{n} = nan(reps, 1);
    distance{n} = nan(reps, 1);
    sync{n} = nan(reps, 1);
    for r = 1:reps
        
        spt_1{r, n} = sort(randi(int16(duration / 0.0001) , [n_spikes(n) 1]) * 0.0001);
        spt_2{r, n} = sort(randi(int16(duration / 0.0001) , [randi(n_spikes(n) * 2) - 1 1]) * 0.0001);
%         spt_2{r, n} = sort(randi(int16(duration / 0.0001) , [n_spikes(n) 1]) * 0.0001);

        result{n}(r) = VPdistance_reich(spt_1{r, n}, spt_2{r, n}, q);
        distance{n}(r) = euclidean_distance(spt_1{r, n}, spt_2{r, n}, 0:0.001:duration);
        sync{n}(r) = Event_Sync(spt_1{r, n}, spt_2{r, n});
        
    end
end

fr_2 = cellfun(@length, spt_2);

% clear duration q n cost r reps

% % plot rasters of each trial

% figure; hold on
% 
% i = 3;
% 
% for ii = 1:size(spt_1, 1)
%     figure(1)
%     scatter(spt_1{ii, i}, ones(length(spt_1{ii, i}), 1) * 2, 10, 'fill', 'b')
%     hold on
%     scatter(spt_2{ii, i}, ones(length(spt_2{ii, i}), 1), 10, 'fill', 'r')
%     axis([0 1 0 3])
%     title(['trial = ' num2str(ii) ...
%         '; fr1 = ' num2str(length(spt_1{ii, i})) ...
%         '; fr2 = ' num2str(length(spt_2{ii, i})) ...
%         '; cost = ' num2str(result{i}(ii))])
%     pause
%     close 1
% end

% %

% load('f:\desktop\WorkingFolder\distance\data\distance_correction.mat')
% load('distance_correction_q_100.mat')

dis = cell2mat(distance);
re = cell2mat(result);
sy = cell2mat(sync);
gm = nan(size(fr_2));

for i = 1:length(n_spikes)
%         gm(:, i) = sqrt(fr_2(:, i) * n_spikes(i));
    gm(:, i) = (fr_2(:, i) + n_spikes(i)) / 2;
end

[~, ~, idx] = histcounts(gm, length(n_spikes));

key = nan(max(idx(:)), 4);
for i = 1:max(idx(:))
    key(i, 1) = nanmedian(gm(idx == i));
    key(i, 2) = nanmedian(re(idx == i));
    key(i, 3) = nanmedian(dis(idx == i));
    key(i, 4) = nanmedian(sy(idx == i));
end

cellplot = cell(max(idx(:)), 4);
for i = 1:max(idx(:))
    cellplot{i, 1} = gm(idx == i);
    cellplot{i, 2} = re(idx == i);
    cellplot{i, 3} = dis(idx == i);
    cellplot{i, 4} = sy(idx == i);
end

cfun = cell(3, 1);

for t = 2:4
% p = polyfit(key(1:35, 1), key(1:35, t), poly);
p = polyfitZero(key(1:35, 1), key(1:35, t), poly);

% figure(t+2)
% scatter(key(:, 1), key(:, t), 10, 'fill')
% hold on
% plot(key(:, 1), p(1) * key(:, 1) .^ 3 ...
%     + p(2) * key(:, 1) .^ 2 ...
%     + p(3) * key(:, 1) ...
%     + p(4), 'LineWidth', 2)
% plot(key(:, 1), p(1) * key(:, 1) + p(2), 'LineWidth', 2)
% plot(key(:, 1), p(1) * key(:, 1) .^ 4 ...
%     + p(2) * key(:, 1) .^ 3 ...
%     + p(3) * key(:, 1) .^ 2 ...
%     + p(4) * key(:, 1) ...
%     + p(5), 'LineWidth', 2)

cfun{t - 1} = p;

end

% clear t p i idx

save(['distance_correction_0_p' num2str(poly) '_c0' num2str(duration * 100) '_q_' num2str(q)])
% save(['distance_correction_0_p' num2str(poly) '_c0' num2str(duration * 100) '_q_01'])

toc

end
%%

% load('f:\desktop\WorkingFolder\distance\data\distance_correction.mat')
% load('f:\desktop\WorkingFolder\distance\data\distance_correction_q_1.mat')

figure(31)
boxplot2_points(1:length(n_spikes), result, 0.5);
xlim([0, length(n_spikes) + 1])
xticks(1:length(n_spikes))
xticklabels(n_spikes)
xlabel('firing rate (train 1)')
ylabel('vp cost')
hold on
plot([0 100], [0 100], 'k', 'LineWidth', 3)

plot(key(:, 1), key(:, 2), 'g', 'LineWidth', 3)
scatter(nanmean(fr_2), nanmean(re), 50, 'r', 'fill')
xlabel('gm firing rate')
ylabel('vp cost')

figure(22)
boxplot2_points(1:length(n_spikes), distance, 0.5);
xlim([0, length(n_spikes) + 1])
xticks(1:length(n_spikes))
xticklabels(n_spikes)
xlabel('firing rate (train 1)')
ylabel('euc distance')

plot(key(:, 1), key(:, 3), 'g', 'LineWidth', 3)
scatter(nanmean(fr_2), nanmean(dis), 50, 'r', 'fill')
xlabel('gm firing rate')
ylabel('euc distance')

clear re dis i idx

figure(23)
boxplot2_points(1:length(n_spikes), sync, 0.5);
xlim([0, length(n_spikes) + 1])
xticks(1:length(n_spikes))
xticklabels(n_spikes)
xlabel('firing rate (train 1)')
ylabel('euc distance')

plot(key(:, 1), key(:, 4), 'g', 'LineWidth', 3)
scatter(nanmean(fr_2), nanmean(sy), 50, 'r', 'fill')
xlabel('gm firing rate')
ylabel('event synchronization')

clear re dis sy i idx

%%

figure(24)
boxplot2_points(cellfun(@nanmedian, cellplot(:,1)), cellplot(:,2), 0.5);
figure(25)
boxplot2_points(cellfun(@nanmedian, cellplot(:,1)), cellplot(:,3), 0.5);
figure(26)
boxplot2_points(cellfun(@nanmedian, cellplot(:,1)), cellplot(:,4), 0.5);
