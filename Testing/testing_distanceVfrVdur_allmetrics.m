n_duration = 0.1:0.2:2;
n_spikes = 1:1:50;
reps = 500;

q = 10;     % scaling factor for cost of time shifting a spike

vp = cell(length(n_duration), length(n_spikes));
euc = cell(length(n_duration), length(n_spikes));
sync = cell(length(n_duration), length(n_spikes));
spt_1 = cell(length(n_duration), length(n_spikes));
spt_2 = cell(length(n_duration), length(n_spikes));

for d = 1:length(n_duration)
for n = 1:length(n_spikes)
    disp(['d = ' num2str(d) '; n = ' num2str(n)])
    
    vp{d, n} = nan(reps, 1);
    euc{d, n} = nan(reps, 1);
    sync{d, n} = nan(reps, 1);
    spt_1{d, n} = cell(reps, 1);
    spt_2{d, n} = cell(reps, 1);
    
    for r = 1:reps
        
        spt_1{d, n}{r} = poissonspikes(n_spikes(n), n_duration(d), 0.0001)';
        spt_2{d, n}{r} = poissonspikes(randi(n_spikes(n) * 2) - 1, n_duration(d), 0.0001)';
        spt_2{d, n}{r}(spt_2{d, n}{r} > n_duration(d)) = n_duration(d);
        
        vp{d, n}(r) = VPdistance_reich(spt_1{d, n}{r}, spt_2{d, n}{r}, q);
        euc{d, n}(r) = euclidean_distance(spt_1{d, n}{r}, spt_2{d, n}{r}, 0:0.001:n_duration(d));
        sync{d, n}(r) = Event_Sync(spt_1{d, n}{r}, spt_2{d, n}{r});
        
    end
end
end

clear reps q r d n

spc1 = cellfun(@(x) mean(cellfun(@length, x)), spt_1);
spc2 = cellfun(@(x) mean(cellfun(@length, x)), spt_2);

%%

v = cellfun(@mean, vp);
g = sqrt(spc1 .* spc2);

figure(1)

subplot(2, 2, 1)
imagesc(v)
xlabel('fr')
ylabel('duration')
title('vp')

subplot(2, 2, 2)
imagesc(g)
xlabel('fr')
ylabel('duration')
title('gm fr')

subplot(2, 2, 3)
imagesc(spc1)
xlabel('fr')
ylabel('duration')
title('cell 1')

subplot(2, 2, 4)
imagesc(spc2)
xlabel('fr')
ylabel('duration')
title('cell 2')


figure(2)
plot(g', v')
