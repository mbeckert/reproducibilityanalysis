%% Jitter method

dur = 1;
reps = 100;
q = 10;

n_spikes = 1:5:100;
n_noise = 0.001:0.005:0.1;

euc = nan(length(n_noise), length(n_spikes));
vp = nan(length(n_noise), length(n_spikes));
sync = nan(length(n_noise), length(n_spikes));
spt = cell(length(n_noise), length(n_spikes));

for s = 1:length(n_spikes)
    for n = 1:length(n_noise)
        
        disp(['s = ' num2str(s) '; n = ' num2str(n)])
        
        sp_tmp = poissonspikes(n_spikes(s), dur, 0.0001)';
        spt{n, s} = repmat({sp_tmp}, 1, reps);
        spt{n, s} = cellfun(@(x) (((n_noise(n) - -n_noise(n)) * rand([length(sp_tmp), 1])) + -n_noise(n)) + x, spt{n, s}, 'UniformOutput', 0);
        
        holder = nan(reps);
        holder2 = nan(reps);
        holder3 = nan(reps);
        
        idx = ones(reps);
        idx = tril(idx, -1);
        
        for t1 = 1:reps
            for t2 = 1:reps
                if idx(t1, t2) == 1
                    holder(t1, t2) = euclidean_distance(spt{n, s}{t1}, spt{n, s}{t2}, 0:0.001:dur);
                    holder2(t1, t2) = VPdistance(spt{n, s}{t1}, spt{n, s}{t2}, q);
                    [holder3(t1, t2), ~] = Event_Sync(spt{n, s}{t1}, spt{n, s}{t2});
                end
            end
        end
        idx = find(idx == 1);
        euc(n, s) = nanmean(holder(idx));
        vp(n, s) = nanmean(holder2(idx));
        sync(n, s) = nanmean(holder3(idx));
        
    end
end

clear dur reps q s n idx holder* t1 t2 sp_tmp
%%
save DistanceTest_jitter