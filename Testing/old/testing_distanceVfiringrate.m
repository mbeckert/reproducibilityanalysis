reps = 100;
n_spikes = 1:1:100;
bins = 0:0.001:1;

spikes = cell(reps, length(n_spikes));

y = cell(reps, length(n_spikes));
distance = nan(length(n_spikes), 1);

for n = 1:length(n_spikes)
    spikes(:, n) = cellfun(@(x) rand([1,n_spikes(n)]), spikes(:, n), 'UniformOutput', 0);
end
for r = 1:reps
    for n = 1:length(n_spikes)
        y(r, n) = cellfun(@(x) ones(1, n_spikes(n)) * r, y(r, n), 'UniformOutput', 0);
    end
end

spikes_his = cellfun(@(x) hist(x, bins), spikes, 'UniformOutput', 0);

for u = 1:length(n_spikes)
    holder = nan(reps);
    for t1 = 1:reps
        for t2 = 1:reps
            holder(t1, t2) = sqrt( sum( (spikes_his{t1, u} - spikes_his{t2, u}) .^2 ) );
        end
    end
    idx = ones(reps);
    idx = tril(idx, -1);
    idx = idx == 1;
    distance(u) = nanmean(holder(idx));
end

clear u t1 t2 idx n r holder spikes_his bins reps

%%

for i = 1:length(n_spikes)
    subplot(1, length(n_spikes), i)
    
    scatter(cell2mat(spikes(:,i)'), cell2mat(y(:,i)'), 3, 'b', 'fill')

    xlabel('time')
    ylabel('reps')
    
    title(num2str(n_spikes(i)))
    axis square
end

figure
plot(n_spikes, distance)
xlabel('firing rate')
ylabel('distance')