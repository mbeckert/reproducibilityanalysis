% set variables
dur = 1;                        % duration of "stimulus" in seconds
spikebin = 0.000010;            % bin size for spike times in seconds
spikebins = 0:spikebin:dur;     % full time bins

cw = 0.001000;      % coincidence window used for SAC
bins = 0:cw:dur;    % bins for SAC 
maxWidth = 100;      % number of bins used for the SAC later
reps = 100;         % number of repetitions
n_spikes = 1:2:50;  % what firing rates we want to test

% Preallocate some variables
spikes = cell(reps, length(n_spikes));      % spike times
y = cell(reps, length(n_spikes));           % spike times matched with trial number for scatter plots
repro_b = cell(5,length(n_spikes));         % reproducibility output using Brette normalization
repro_j = cell(5,length(n_spikes));         % reproducibility output using Joris normalization

% generate random spike trains for each firing rate for a given number of
% repetitions (converting spike times to milliseconds for input into
% reproducibility script
for n = 1:length(n_spikes)
    spikes(:, n) = cellfun(@(x) rand([1,n_spikes(n)]) * dur * 1000, spikes(:, n), 'UniformOutput', 0);
end

% generate matching trial numbers for quick scatter plotting later (rasters)
for r = 1:reps
    for n = 1:length(n_spikes)
        y(r, n) = cellfun(@(x) ones(1, n_spikes(n)) * r, y(r, n), 'UniformOutput', 0);
    end
end
    
% Calculate reproducibility using both types of normalization
for n = 1:length(n_spikes)
    [~, repro_b{2,n}, repro_b{1,n}, ~, repro_b{5,n}, repro_b{4,n}, repro_b{3,n}] = ...
        reproducibility_brettenorm(spikes(:,n)', bins, cw, maxWidth);
    [~, repro_j{2,n}, repro_j{1,n}, ~, repro_j{5,n}, repro_j{4,n}, repro_j{3,n}] = ...
        reproducibility_jorisnorm(spikes(:,n)', bins, cw, maxWidth);
end

clear n r 

%% A quick plotting of results

names = {'AUC', 'Peak', 'HW'};
figure
for m = 1:length(names)
    
    subplot(2, length(names), m)
    plot(cell2mat(repro_b(1,:)), cell2mat(repro_b(m+1,:)))
    xlabel('firing rate')
    ylabel(names{m})
    axis square
    
    subplot(2, length(names), m+length(names))
    plot(cell2mat(repro_j(1,:)), cell2mat(repro_j(m+1,:)))
    xlabel('firing rate')
    ylabel(names{m})
    axis square
    
end
    

%% Plot each "unit" to ensure the SAC looks appropriate

for i = 1:size(n_spikes,2)
    
    close all

    subplot(3,1,1)
    yy = cell2mat(y(:,i)');
    xx = cell2mat(spikes(:,i)');
    scatter(xx, yy, 5, 'fill')
    title([num2str(repro_b{1,i}) ' --- ' num2str(repro_b{2,i}) newline ...
        num2str(repro_j{1,i}) ' --- ' num2str(repro_j{2,i})])
    
    subplot(3,1,2)
    plot(repro_b{5,i}, 'k', 'LineWidth', 1)
    subplot(3,1,3)
    plot(repro_j{5,i}, 'k', 'LineWidth', 1)
    
    pause
    
end

clear i yy xx
    