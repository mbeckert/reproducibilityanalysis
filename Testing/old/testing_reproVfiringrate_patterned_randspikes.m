% some variables
dur = 1;                        % duration of "stimulus"
spikebin = 0.000050;              % bin size for spikes
spikebins = 0:spikebin:dur;     % full spike bins
reps = 100;                     % number of reps

cw = 0.001000;                     % coincidence window used for SAC
bins = 0:cw:dur;                % bins for SAC
maxWidth = 50;                 % number of bins for SAC

%%%%%%%%%%%%%%%%%%
% These are used for the generation of the spike times.
% A specified number of spikes are randomly drawn from a distribution of
% probabilities where the probability of spiking is higher at particular
% times. This is done by centering several normal distributions around a
% list of times and varying their sigmas. Each gaussian is summed up and
% normalized such that the total probability is equal to 1.
%%%%%%%%%%%%%%%%%%

n_spikes = 1:2:10;              % list of firing rates
var = 0.005;         % list of variability, equal to the sigma of a gaussian
% These are the mus for the gaussians used to generate the spike
% probabilities (arbitrarily assigned, can use any number of points).
pattern = [0.002, 0.01, 0.05, 0.12, 0.2, 0.24, 0.5, 0.61, 0.69, 0.73, 0.8, 0.91];
rand_spikes = n_spikes;             % list of random number of spikes to include

%%%%%%%%%%%%%%%%%%

% generate a probability distribution for each time point in the pattern
% list (pattern) and each combination of variabilities (var)
dist = cell(length(pattern), length(rand_spikes));
for p = 1:length(pattern)
    for v = 1:length(rand_spikes)
        dist{p,v} = normpdf(spikebins, pattern(p), var);
    end
end

% sum and normalize the probability distributions of each point in time 
% for each variability being tested.
prob = cell(1, length(rand_spikes));
for v = 1:length(rand_spikes)
    prob{v} = sum(cell2mat(dist(:, v))) / sum(sum(cell2mat(dist(:, v))));
end

% Now pull random spikes from the distributions for each variability
% n_spikes number of times to generate the spike times
% Note that here the spikes are not sorted in time 
spikes = cell(1, length(rand_spikes));
for v = 1:length(rand_spikes)
    spikes{v} = cell(reps, length(n_spikes));
    
    for r = 1:reps
        for n = 1:length(n_spikes)
            spikes{v}{r, n} = nan(n_spikes(n) + rand_spikes(v),1);
            for s = 1:n_spikes(n)
                p = cumsum([ 0 prob{v}(:)' ./ sum(prob{v}(:)) ]);
                p(end) = 1e3*eps + p(end);
                [~, a] = histc(rand,p);
                spikes{v}{r, n}(s) = spikebins(a) * 1000;
            end
            for s = 1:rand_spikes(v)
                r_spike = ceil(rand * length(spikebins));
                spikes{v}{r, n}(n_spikes(n) + s) = spikebins(r_spike) * 1000;
            end
        end
    end
end

% create a matching matrix which contains the repetition number n_spikes
% number of times to be used to quickly plot the rasters later.
y = cell(1, length(rand_spikes));
for v = 1:length(rand_spikes)
    y{v} = cell(reps, length(n_spikes));
    for r = 1:reps
        for n = 1:length(n_spikes)
            y{v}(r, n) = cellfun(@(x) ones(1, (n_spikes(n)) + rand_spikes(v)) * r, y{v}(r, n), 'UniformOutput', 0);
            
        end
    end
end

% Calculate the reproducibility using the Brette and Joris normalization
% methods.
repro_b = cell(1, length(rand_spikes));
repro_j = cell(1, length(rand_spikes));
for v = 1:length(rand_spikes)
    repro_b{v} = cell(5,length(n_spikes));
    repro_j{v} = cell(5,length(n_spikes));
    for n = 1:length(n_spikes)
        [~, repro_b{v}{2,n}, repro_b{v}{1,n}, ~, repro_b{v}{5,n}, repro_b{v}{4,n}, repro_b{v}{3,n}] = ...
            reproducibility_brettenorm(spikes{v}(:,n)', bins, cw, maxWidth);
        [~, repro_j{v}{2,n}, repro_j{v}{1,n}, ~, repro_j{v}{5,n}, repro_j{v}{4,n}, repro_j{v}{3,n}] = ...
            reproducibility_jorisnorm(spikes{v}(:,n)', bins, cw, maxWidth);
    end
end

clear a n p r s v dist
%% plot every raster for visual inspection

for v = 1:length(rand_spikes)
    figure(v)
    for n = 1:length(n_spikes)
        subplot(3, length(n_spikes), n)
        scatter(cell2mat(spikes{v}(:,n)), cell2mat(y{v}(:,n)')', 2, 'fill')
        axis square
        subplot(3, length(n_spikes), n + length(n_spikes))
        plot(repro_j{v}{5, n})
        axis square
        subplot(3, length(n_spikes), n + length(n_spikes) * 2)
        plot(repro_b{v}{5, n})
        axis square
    end
end

clear v n

%% Plot the probability distributions for all variabilities
figure
hold on
for v = 1:length(var)
    plot(prob{v})
end

clear v

%% rep plotting

for v = 1:length(var)
    for i = 1:size(n_spikes,2)
        
        close all
        
        subplot(3,1,1)
        yy = cell2mat(y{v}(:,i)');
        xx = cell2mat(spikes{v}(:,i));
        scatter(xx, yy, 5, 'fill')
        title([num2str(repro_b{v}{1,i}) ' --- ' num2str(repro_b{v}{2,i}) newline ...
            num2str(repro_j{v}{1,i}) ' --- ' num2str(repro_j{v}{2,i})])
        
        subplot(3,1,2)
        plot(repro_b{v}{5,i}, 'k', 'LineWidth', 1)
        subplot(3,1,3)
        plot(repro_j{v}{5,i}, 'k', 'LineWidth', 1)
        
        pause
        
    end
end

clear i yy xx v

%% surf plots of relationship between variability, firing rate, and reproducibility

names = {'AUC', 'Peak', 'HW'};

figure
for no = 1:2
    switch no
        case 1
            r = repro_b;
        case 2
            r = repro_j;
    end
    
    for m = 1:3
        
        plotdata_sp = nan(size(r,2), size(r{1},2));
        plotdata_rep = nan(size(r,2), size(r{1},2));
        
        for v = 1:size(r,2)
            plotdata_sp(v,:) = cell2mat(r{v}(1,:));
            plotdata_rep(v,:) = cell2mat(r{v}(m + 1,:));
        end
        
        subplot(3,2,no + (m * 2 - 2))
        surf(plotdata_rep)
        axis square
        xlabel('firing rate')
        ylabel('variability')
        zlabel(names{m})
        
    end
end

clear no r m v