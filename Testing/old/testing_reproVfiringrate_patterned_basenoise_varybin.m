% some variables
dur = 1;                        % duration of "stimulus"
spikebin = 0.000050;            % bin size for spikes
spikebins = 0:spikebin:dur;     % full spike bins
reps = 100;                     % number of reps

% coincidence window used for SAC (varying this here)
% cw = [0.000010, 0.000050, 0.000100, 0.000500, 0.001000, 0.005000, 0.010000];
cd = 0.000100;

mw_time = 0.02;
maxWidth = round(mw_time ./ cw);                 % number of bins for SAC
% bins = 0:cw:dur;                % bins for SAC

refract = 1;                    %refractory period in milliseconds

%%%%%%%%%%%%%%%%%%
% These are used for the generation of the spike times.
% A specified number of spikes are randomly drawn from a distribution of
% probabilities where the probability of spiking is higher at particular
% times. This is done by centering several normal distributions around a
% list of times and varying their sigmas. Each gaussian is summed up and
% normalized such that the total probability is equal to 1.
%%%%%%%%%%%%%%%%%%

n_spikes = 5;              % list of firing rates
var = 0.001;         % list of variability, equal to the sigma of a gaussian
% These are the mus for the gaussians used to generate the spike
% probabilities (arbitrarily assigned, can use any number of points).
pattern = [0.002, 0.01, 0.05, 0.12, 0.2, 0.24, 0.5, 0.61, 0.69, 0.73, 0.8, 0.91];
noise = 5;             % list of baseline amount of noise to include

%%%%%%%%%%%%%%%%%%

% generate a probability distribution for each time point in the pattern
% list (pattern) and each combination of variabilities (var)
dist = cell(length(pattern), length(cw));
for p = 1:length(pattern)
    for v = 1:length(cw)
        dist{p,v} = normpdf(spikebins, pattern(p), var);
    end
end

% sum and normalize the probability distributions of each point in time 
% for each variability being tested.
prob = cell(1, length(cw));
for v = 1:length(cw)
    noise_prob = ones(1,length(spikebins)) * noise;
    prob{v} = (sum(cell2mat(dist(:, v))) + noise_prob) / (sum(sum(cell2mat(dist(:, v)))) + sum(noise_prob));
end

% Now pull random spikes from the distributions for each variability
% n_spikes number of times to generate the spike times
% Note that here the spikes are not sorted in time 
spikes = cell(1, length(cw));
for v = 1:length(cw)
    spikes{v} = cell(reps, length(n_spikes));
    
    for r = 1:reps
        for n = 1:length(n_spikes)
            spikes{v}{r, n} = nan(n_spikes(n),1);
            s = 1;
            p = cumsum([ 0 prob{v}(:)' ./ sum(prob{v}(:)) ]);
            p(end) = 1e3*eps + p(end);
            while s <= n_spikes(n)
                [~, a] = histc(rand,p);
                sp = spikebins(a) * 1000;
                if sum(abs(spikes{v}{r, n} - sp) < refract) == 0
                    spikes{v}{r, n}(s) = sp;
                    s = s + 1;
                end
            end
        end
    end
end

% create a matching matrix which contains the repetition number n_spikes
% number of times to be used to quickly plot the rasters later.
y = cell(1, length(cw));
for v = 1:length(cw)
    y{v} = cell(reps, length(n_spikes));
    for r = 1:reps
        for n = 1:length(n_spikes)
            y{v}(r, n) = cellfun(@(x) ones(1, n_spikes(n)) * r, y{v}(r, n), 'UniformOutput', 0);
            
        end
    end
end

% Calculate the reproducibility using the Brette and Joris normalization
% methods.
repro_b = cell(1, length(cw));
repro_j = cell(1, length(cw));
HIST = cell(1, length(cw));

for v = 1:length(cw)
    repro_b{v} = cell(5,length(n_spikes));
    repro_j{v} = cell(5,length(n_spikes));
    HIST{v} = cell(2, length(n_spikes));
    for n = 1:length(n_spikes)
        bins = 0:cw(v):dur; 
        [HIST{v}{1,n}, repro_b{v}{2,n}, repro_b{v}{1,n}, ~, repro_b{v}{5,n}, repro_b{v}{4,n}, repro_b{v}{3,n}] = ...
            reproducibility_brettenorm(spikes{v}(:,n)', bins, cw(v), maxWidth(v));
        [HIST{v}{2,n}, repro_j{v}{2,n}, repro_j{v}{1,n}, ~, repro_j{v}{5,n}, repro_j{v}{4,n}, repro_j{v}{3,n}] = ...
            reproducibility_jorisnorm(spikes{v}(:,n)', bins, cw(v), maxWidth(v));
        disp([num2str(v) ' and ' num2str(n)])
    end
end

clear a n p r s v dist sp
%% plot every raster for visual inspection

for v = 1:length(cw)
    figure(v)
    for n = 1:length(n_spikes)
        subplot(4, length(n_spikes), n)
        scatter(cell2mat(spikes{v}(:,n)), cell2mat(y{v}(:,n)')', 2, 'fill')
        axis square
        subplot(4, length(n_spikes), n + length(n_spikes))
        plot(repro_j{v}{5, n})
        axis square
        subplot(4, length(n_spikes), n + length(n_spikes) * 2)
        plot(repro_b{v}{5, n})
        axis square
        subplot(4, length(n_spikes), n + length(n_spikes) * 3)
        bar(HIST{v}{1, n})
        axis square
    end
    
end

clear v n

%% surf plots of relationship between variability, firing rate, and reproducibility

names = {'AUC', 'Peak', 'HW'};

figure
for no = 1:2
    switch no
        case 1
            r = repro_b;
        case 2
            r = repro_j;
        case 3
            r = repro_o;
    end
    
    for m = 1:2
        
        plotdata_sp = nan(size(r,2), size(r{1},2));
        plotdata_rep = nan(size(r,2), size(r{1},2));
        
        for v = 1:size(r,2)
            plotdata_sp(v,:) = cell2mat(r{v}(1,:));
            plotdata_rep(v,:) = cell2mat(r{v}(m + 1,:));
        end
        
        subplot(3, 3, no + (m * 3 - 3))
        surf(plotdata_rep)
        axis square
        xlabel('firing rate')
        ylabel('variability')
        zlabel(names{m})
        
    end
end

clear no r m v