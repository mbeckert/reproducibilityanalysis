% use this script to generate the curves for the freefield stimuli
% Use "otsynch_binnedGM" first for the dichotic stimuli


%%
load('f:\desktop\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted.mat','OT');
data = OT; clear OT

Window = sort([-0.1 0.1]);
binWidth = 0.001;
binNum = length(Window(1):binWidth:Window(2))-2;
norminc = 0:0.1:1;

dur = 0.15;
pattern = 'frozen.|b.|i.';                   % These are to remove
pattern2 = '.';                   % These are to keep

cd('f:\desktop\Data\Analysis')
load('SpeakerIndex.mat','SpeakerIndex');
[B,~]=sortrows(SpeakerIndex(:,1));
[b,~]=sortrows(SpeakerIndex(:,2));
H=hist(B,-100:10:100);
H=cumsum(H);
h=hist(b,-80:10:80);
h=cumsum(h);
clear B b SpeakerIndex

D=[];

for date=1:size(data,2)
    for site = 1:length(data{date})
        list=combnk(2:length(data{date}{site}),2);
        for pair = 1:size(list,1)
            
            disp(['date: ' num2str(date) '; site: ' num2str(site) '; pair: ' num2str(pair)]);
            
            unit1 = data{date}{site}{list(pair,1)};
            unit2 = data{date}{site}{list(pair,2)};
            filetypes1 = fieldnames(unit1);
            filetypes2 = fieldnames(unit2);
            filetypes = intersect(filetypes1,filetypes2);
            idx = regexpi(filetypes,pattern);
            idx = cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            idx = regexpi(filetypes,pattern2);
            idx = ~cellfun(@isempty,idx);
            filetypes = filetypes(idx);
            
            clear filetypes1 filetypes2 idx
            if ~isempty(filetypes)
            st_u1 = []; st_u2 = []; sc_u1 = []; sc_u2 = []; bc_u1 = []; bc_u2 = [];
            for file = 1:length(filetypes)
                st_u1 = [st_u1,unit1.(filetypes{file}).spikes_times];
                st_u2 = [st_u2,unit2.(filetypes{file}).spikes_times];
                sc_u1 = [sc_u1,unit1.(filetypes{file}).spikes_count];
                sc_u2 = [sc_u2,unit2.(filetypes{file}).spikes_count];
                bc_u1 = [bc_u1,unit1.(filetypes{file}).sensitive];
                bc_u2 = [bc_u2,unit2.(filetypes{file}).sensitive];
            end
%             brate1 = mean(bc_u1/dur,2);
%             bstd1 = std(bc_u1/dur,0,2);
%             brate2 = mean(bc_u2/dur,2);
%             bstd2 = std(bc_u2/dur,0,2);
%             brate1 = mean(bc_u1(:),1);
%             bstd1 = std(bc_u1(:),0,1);
%             brate2 = mean(bc_u2(:),1);
%             bstd2 = std(bc_u2(:),0,1);
%             br1 = brate1 + (2*bstd1);
%             br2 = brate2 + (2*bstd2);
            br1 = sum(bc_u1,2);
            br2 = sum(bc_u2,2);
            br1 = br1 > 0;
            br2 = br2 > 0;
            reps = size(sc_u1,2);
            
            for ax = 1:2
                switch ax
                    case 1
                        idx = [1,H];
                        f = 'az';
                    case 2
                        idx = [1,h];
                        f = 'el';
                end
                
                CCG = zeros(length(idx)-1,binNum);
                shiftCCG = zeros(length(idx)-1,binNum);
                
                for i = 1:length(idx)-1
                    trials = idx(i):idx(i+1);
                    count = 0;

                    for t = 1:length(trials)
                        if br1(trials(t)) * br2(trials(t)) == 1
                            count = count+1;
                        for r = 1:reps
                        if ~isempty(st_u1{trials(t),r})
                            for spike = 1:length(st_u1{trials(t),r})
                                times = st_u1{trials(t),r}(spike) - st_u2{trials(t),r};
                                his=hist(times,Window(1):binWidth:Window(2)); %% Here is where you can change binwidth
                                CCG(i,:)=CCG(i,:)+his(2:end-1);
                                % shuffle using the shifted trial number
%                                 if trials(t)==size(sc_u1,1)
%                                     times = st_u1{trials(t),r}(spike) - (rand(size(st_u2{1})) * dur);
%                                 else
%                                     times = st_u1{trials(t),r}(spike) - (rand(size(st_u2{trials(t)+1,r})) * dur);
%                                 end
                                % shuffle using the same trial number
                                times = st_u1{trials(t),r}(spike) - (rand(size(st_u2{trials(t),r})) * dur);
                                % regular shift
%                                 if trials(t)==size(sc_u1,1)
%                                     times = st_u1{trials(t),r}(spike) - st_u2{1};
%                                 else
%                                     times = st_u1{trials(t),r}(spike) - st_u2{trials(t)+1,r};
%                                 end
                                his=hist(times,Window(1):binWidth:Window(2));
                                shiftCCG(i,:)=shiftCCG(i,:)+his(2:end-1);
                            end
                        end
                        end
                        end
                    end % for "t"
                    CCG(i,:) = CCG(i,:)/count;
                    shiftCCG(i,:) = shiftCCG(i,:)/count;
                end
                
                clear his spike times
                
                holder = cell(1,13);
                holder{1} = [date,site,list(pair,1),list(pair,2)];
                for i = 2:10
                    holder{i} = nan(1,length(idx)-1);
                end
                for i = 11:13
                    holder{i}=cell(1,length(idx)-1);
                end
                
                for i = 1:length(idx)-1
                    
                    trials = idx(i):idx(i+1);
                    
                    rate1 = sc_u1(trials,:);
                    rate2 = sc_u2(trials,:);
                    b1 = br1(trials);
                    b2 = br2(trials);
                    chck = b1 .* b2;
                    if sum(chck) > 0
                        check2 = 1;
                    elseif sum(check2) == 0
                        check2 = 0;
                    end
%                     br1 = brate1(trials) + (2*bstd1(trials));
%                     br2 = brate2(trials) + (2*bstd2(trials));
%                     chck = 0;
%                     for x = 1:size(rate1,1)
%                         if mean(rate1(x,:)) >= br1
%                             chck = chck + 1;
%                         end
%                         if mean(rate2(x,:)) >= br2
%                             chck = chck + 1;
%                         end
%                     end

                    rate1=mean(rate1(:)/dur);
                    rate2=mean(rate2(:)/dur);
                    
%                     if chck > 0
%                         check2 = 1;
%                     elseif chck == 0
%                         check2 = 0;
%                     end
                    
                    SmCCG=(smooth(CCG(i,:))/sqrt(rate1*rate2))/binNum;
                    SmShiftCCG=(smooth(shiftCCG(i,:))/sqrt(rate1*rate2))/binNum;
                    
                    SmCCGCorrected=SmCCG-SmShiftCCG;
                    
                    flankS=std([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                    flankM=mean([SmCCGCorrected(1:round((binNum/5)));SmCCGCorrected(end-round((binNum/5)):end)]);
                    center = SmCCGCorrected(round((binNum/5))+1:end-round((binNum/5))-1);
                    
                    PEAK=SmCCGCorrected(round(binNum/2));
                    check1 = max(center)>flankM+5*flankS;
                    positive=(SmCCGCorrected>=(PEAK/2));
                    pos=diff(positive);
                    less=pos(1:round(binNum/2));
                    more=pos(round(binNum/2):end);
                    Low=find(less==1,1,'last');
                    High=find(more==-1,1,'first');
                    Low=length(less)-Low;
                    bandPEAK=High+Low;
                    
                    if isempty(bandPEAK)
                        bandPEAK=nan;
                        sumband=nan;
                    else
                        sumband=sum(SmCCGCorrected(length(less)-Low:length(less)+High))/bandPEAK;
                    end
                    
                    holder{2}(1,i)=PEAK;
                    holder{3}(1,i)=rate1;
                    holder{4}(1,i)=rate2;
                    holder{5}(1,i)=check1;
                    holder{6}(1,i)=check2;
                    %                     holder{7}(1,i)=check;
                    holder{8}(1,i)=bandPEAK;
                    holder{9}(1,i)=sum(center);
                    holder{10}(1,i)=sumband;
                    holder{11}{1,i}=SmCCG;
                    holder{12}{1,i}=SmShiftCCG;
                    holder{13}{1,i}=SmCCGCorrected;
                    
                    clear positive pos less more Low High bandPEAK sens1 sens2
                    
                end     % for "i" the second time
                
                if isfield(D,f)
                    for i = 1:10
                        if size(D.(f){i},2) > size(holder{i},2)
                            padding = nan(1,size(D.(f){i},2)-size(holder{i},2));
                            D.(f){i}=[D.(f){i};holder{i},padding];
                        elseif size(D.(f){i},2) < size(holder{i},2)
                            padding = nan(size(D.(f){i},1),size(holder{i},2)-size(D.(f){i},2));
                            D.(f){i}=[D.(f){i},padding;holder{i}];
                        else
                            D.(f){i}=[D.(f){i};holder{i}];
                        end
                    end
                    for i = 11:13
                        if size(D.(f){i},2) > size(holder{i},2)
                            padding = cell(1,size(D.(f){i},2)-size(holder{i},2));
                            D.(f){i}=[D.(f){i};holder{i},padding];
                        elseif size(D.(f){i},2) < size(holder{i},2)
                            padding = cell(size(D.(f){i},1),size(holder{i},2)-size(D.(f){i},2));
                            D.(f){i}=[D.(f){i},padding;holder{i}];
                        else
                            D.(f){i}=[D.(f){i};holder{i}];
                        end
                    end
                else
                    for i = 1:length(holder)
                        D.(f){i}=holder{i};
                    end
                end
                
                clear holder depvar rate* i PEAK check SmCCG curves CORR trial flank* sumband SmShiftCCG shiftCCG SmCCGCorrected CCG
                
            end         % for "file"
            
            end
            
            clear filetypes
            
        end             % for "pair"
    end                 % for "site"
end                     % for "date"

ot_synch = D;

clearvars -except ot_synch

cd('F:\Desktop\WorkingFolder')

x.az = ot_synch.az;
x.el = ot_synch.el;
load('ot_synch.mat')
ot_synch.az = x.az;
ot_synch.el = x.el;
clear x
save ot_synch_azel_shuff

%% run correlation analysis

load('ot_synch_azel.mat')
filetypes = fieldnames(ot_synch);

corr_cell = cell(1,length(filetypes));
corr_full = cell(1,length(filetypes));
val = cell(1,length(filetypes));

for file = 1:length(filetypes)
    data = ot_synch.(filetypes{file});
    corr_cell{file} = nan(size(data{1},1),3);
    corr_full{file} = nan(3,2);
    val{file} = cell(1,4);
    figure(file)
    
    for met = 1:3
        switch met
            case 1
                m = 2;
            case 2
                m = 8;
            case 3
                m = 10;
        end
        
        gm = sqrt(data{3}.*data{4});
        d = data{m};
        
        for i = 1:size(gm,1)
            gm(i,:) = normalization(gm(i,:),1);
            d(i,:) = normalization(d(i,:),1);
            if sum(isfinite(d(i,:))) > 2
                corr_cell{file}(i,met)= corr(gm(i,:)',d(i,:)','rows','pairwise');
%                 corr_cell{file}(i,met) = 0.5*log((1+corr_cell{file}(i,met))/(1-corr_cell{file}(i,met)));       % z-transformation
            end
        end
        val{file}{1} = gm(:);
        val{file}{met+1} = d(:);
        [corr_full{file}(met,1),corr_full{file}(met,2)] = corr(gm(:),d(:),'rows','pairwise');
        subplot(3,1,met)
        scatter(gm(:),d(:),2,'k')
        
    end
    
    val{file} = cell2mat(val{file});
    
end

corr_full = cell2mat(corr_full);
clear filetypes file data m gm i met d

%% plot data as a 2D histogram (requires custom-written 'hist2D')

met = 2; % 2: peak, 3: bw, 4: auc

data{1} = [val{5}(:,1),val{5}(:,met)];
data{2} = [val{6}(:,1),val{6}(:,met)];

rangex = [0,1,21];
rangey = [0,1,21];

mat = cell(1,2);

for t = 1:2
    [mat{t},~,~] = hist2D(data{t},rangex,rangey);
end

clear met data range* t

figure(1)
subplot(2,1,1)
imagesc(mat{1})
axis square
subplot(2,1,2)
imagesc(mat{2})
axis square

%% compile data into a format to easily paste into graphpad for scatter
clear d

data = val;
met = 2;    % 2: peak, 3: bw, 4: auc
t = 2;      % 1: dichotic, 2: ff

d(:,1) = [data{2*t-1}(:,1);data{2*t}(:,1)];
d(:,2) = [data{2*t-1}(:,met);nan(length(data{2*t}(:,met)),1)];
d(:,3) = [nan(length(data{2*t-1}(:,met)),1);data{2*t}(:,met)];

clear met t data

%% compile data into a format to easily paste into graphpad for corr cell

clear d

data = corr_cell;
met = 3;    % 1: peak, 2: bw, 3: auc

d{1,1} = data{2}(:,met);
d{1,2} = data{1}(:,met);
d{1,3} = data{3}(:,met);
d{1,4} = data{4}(:,met);

m = max(cellfun(@length,d));
for i = 1:length(d)
    d{i} = [d{i}; nan(m - length(d{i}),1)];
end

d = cell2mat(d);

clear data met t m i