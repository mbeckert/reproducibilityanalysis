cd('f:\desktop\WorkingFolder\synchony')
load('ot_synch_ff_shift.mat')
ff = ot_synch.ff;
clear ot_synch

normalize = [1,1];

GM = sqrt(ff{3} .* ff{4});
GM = normalization(GM,normalize(1));

correct = nan(size(ff{11}{1}));

chck = ff{6};
chck = chck == 0;
chck = find(chck == 1);
for i = 1:length(chck)
    ff{11}{chck(i)} = correct;
    ff{12}{chck(i)} = correct;
    ff{13}{chck(i)} = correct;
end

CCG = ff{11};
SH = ff{12};
COR = ff{13};

binNum = 199; int = 10; half = round(binNum/2);

ccg = nan(size(CCG));
sh = nan(size(SH));
cor = nan(size(COR));

for n = 1:size(ccg,1)
    for sp = 1:size(ccg,2)
        if ~isempty(CCG{n,sp})
            ccg(n,sp) = sum(CCG{n,sp}(half - int:half + int));
        end
        if ~isempty(SH{n,sp})
            sh(n,sp) = sum(SH{n,sp}(half - int:half + int));
        end
        if ~isempty(COR{n,sp})
            cor(n,sp) = sum(COR{n,sp}(half - int:half + int));
        end
    end
end

ccg = normalization(ccg,normalize(2));
sh = normalization(sh,normalize(2));
cor = normalization(cor,normalize(2));

save('ot_synch_ff_norm.mat', 'ccg','sh','cor','GM','CCG','SH','COR')
cd('F:\Desktop\WorkingFolder')
%%

[~,~,histidx] = histcounts(GM(:),0:0.1:1);

ho = cell(3,1);

for t = 1:3
    ho{t} = cell(1,11);
    switch t
        case 1
            data = ccg;
        case 2
            data = sh;
        case 3
            data = cor;
    end
    
for i = 0:10
%     ho{t}(1,i+1) = nanmean(data(histidx == i));
%     ho{t}(2,i+1) = nanstd(data(histidx == i));
    ho{t}{i+1} = data(histidx == i);
end

% figure(t)
% errorbar(ho{t}(1,:),ho{t}(2,:))

m = max(cellfun(@length,ho{t}));
for i = 1:11
    ho{t}{i} = [ho{t}{i};nan(m - length(ho{t}{i}),1)];
end

ho{t} = cell2mat(ho{t});

end

clear t i data histidx



    
    
    
    
    
    