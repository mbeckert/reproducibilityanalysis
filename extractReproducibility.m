
load('ReproducibilityRAW_FF.mat','CURVE','Mean','Index','RasterCheck');
load('ReproducibilityRAW_ILD.mat','CURVEILD','MeanILD','IndexILD','RasterCheckILD');
%%

e=[1:18,21:64];
% e=1:length(CURVE);
Site=[];Rep=[];FR=[];Ind=[];RC=[];
for i=e;
%     Rep=[Rep,CURVE{i}/max(CURVE{i})];
%     FR=[FR,Mean{i}/max(Mean{i})];
    s=ones(1,length(CURVE{i}))*i;
    Site=[Site,s];
    Rep=[Rep,CURVE{i}];
    FR=[FR,Mean{i}];
    Ind=[Ind,Index{i}];
    RC=[RC,RasterCheck{i}];
end
FF(:,1)=Site';
FF(:,3)=Rep';
FF(:,2)=FR';
FF(:,4:5)=Ind';
FF(:,6)=RC';

clear i Rep FR Ind RC Site s 

x=cellfun(@length,CURVE(e));
x=[1,x];
X=cumsum(x);
X=X';

clear x e

% FF(FF(:,5)==0,1)=nan;
% FF(FF(:,5)==1,1)=nan;

countA=1;
countE=1;

Az=[];
El=[];
for i=1:length(X)-1;
    A=unique(FF(X(i):X(i+1)-1,4));
    E=unique(FF(X(i):X(i+1)-1,5));
    for h=1:length(A);
        Az(countA,:)=[i,nanmean(FF(FF(X(i):X(i+1)-1,4)==A(h),2)),nanmean(FF(FF(X(i):X(i+1)-1,4)==A(h),3)),A(h),nanmean(FF(FF(X(i):X(i+1)-1,5)==E(h),6))];
        countA=countA+1;
    end
    for h=1:length(E);
        El(countE,:)=[i,nanmean(FF(FF(X(i):X(i+1)-1,5)==E(h),2)),nanmean(FF(FF(X(i):X(i+1)-1,5)==E(h),3)),E(h),nanmean(FF(FF(X(i):X(i+1)-1,5)==E(h),6))];
        countE=countE+1;
    end
end

clear count* A E i X h

%%
clear Index* CURVE* Mean* X e i x FR Ind Rep countA countE h RasterCheck* RC A E
%%
Ratio=FF(:,1)./FF(:,2);

site=unique(Az(:,4));
distanceRep=nan(1,22);
distanceFR=nan(1,22);
distanceRat=nan(1,22);

for i = 1:length(site);
    R=[Az(Az(:,4)==i,1),Az(Az(:,4)==i,2),Az(Az(:,4)==i,3)/10,Ratio(Az(:,4)==i)];
    steps=cumsum(diff(R(:,3)));
    steps=[0;steps]; steps=steps+1; 
    best=steps(find(R(:,2)==max(R(:,2))));
    steps=steps+11-best(1);
    holder{1}=nan(1,22);
    holder{1}(steps)=R(:,1);
    holder{2}=nan(1,22);
    holder{2}(steps)=R(:,2);
    holder{3}=nan(1,22);
    holder{3}(steps)=R(:,4);
    distanceRep=[distanceRep;holder{1}];
    distanceFR=[distanceFR;holder{2}];
    distanceRat=[distanceRat;holder{3}];
end

clear R site steps best holder Ratio i

%%

for t = 1:5
    
    if t==1; data=FF; end
    if t==2; data=Az; end
    if t==3; data=El; end
    if t==4; data=ILD; end
    if t==5; data=ITD; end
    
    out=[];
    
    for i = 1:max(data(:,1))
        idx=find(data(:,1)==i);
        if ~isempty(idx)
            out(idx,:)=[data(idx,2)/max(data(idx,2)),data(idx,3)/max(data(idx,3))];
        end
    end
    out=[data(:,1),out,data(:,4:end)];
    
    clear i idx
    
    if t==1; FF_norm=out; end
    if t==2; Az_norm=out; end
    if t==3; El_norm=out; end
    if t==4; ILD_norm=out; end
    if t==5; ITD_norm=out; end
    
    clear data out
    
end

clear t
    
%%

for t = 1:5
    
    if t==1; data=FF; DATA=FF_norm; end
    if t==2; data=Az; DATA=Az_norm; end
    if t==3; data=El; DATA=El_norm; end
    if t==4; data=ILD; DATA=ILD_norm; end
    if t==5; data=ITD; DATA=ITD_norm; end
    
    figure(t)
    subplot(1,2,1); scatter(data(:,2),data(:,3));
    subplot(1,2,2); scatter(DATA(:,2),DATA(:,3)); axis([0 1 0 1]);
    title(num2str(t))
    
end

clear t data DATA

%%

out = cell(1,4);

for t = 2:5
    
    out{t-1}=cell(1,2);
    
    if t==2; data=Az; DATA=Az_norm; factor = 10; end
    if t==3; data=El; DATA=El_norm; factor = 10; end
    if t==4; data=ILD; DATA=ILD_norm; factor = 5; end
    if t==5; data=ITD; DATA=ITD_norm; factor = 30; end
    
    for i = 1:max(data(:,1))
        
        d = data(data(:,1)==i,2:4);
        d(:,3)=round(d(:,3)/factor);
        e = DATA(DATA(:,1)==i,3);
        
        best = find(d(:,1)==max(d(:,1)));
        idx = d(:,3) + 21 - d(best(1),3);
        
        temp = nan(2,41);
        temp(1,idx)=d(:,2);
        temp(2,idx)=e;
        
        out{t-1}{1}(i,:)=temp(1,:);
        out{t-1}{2}(i,:)=temp(2,:);
        
    end

    clear i d e temp best idx factor data DATA
    
end

center = out;

clear out t

%%
%% generate histograms based on firing rates

HISTm=cell(2,4);
HISTs=cell(2,4);
HISTn=cell(2,4);
for t = 1:4;
    if t == 1; d = ITD; e = ITD_norm; end
    if t == 2; d = ILD; e = ILD_norm; end
    if t == 3; d = Az; e = Az_norm; end
    if t == 4; d = El; e = El_norm; end

    e(e(:,3)>1,:)=nan;
    e(e(:,3)<0,:)=nan;
    
    HISTm{1,t}=nan(1,11);
    HISTm{2,t}=nan(1,11);
    HISTs{1,t}=nan(1,11);
    HISTs{2,t}=nan(1,11);
    HISTn{1,t}=nan(1,11);
    HISTn{2,t}=nan(1,11);

    [B,I]=histc(e(:,2),0:0.1:1);

    for bin = 1:11
        idx=find(I==bin);
        HISTm{1,t}(bin)=nanmean(d(idx,3));
        HISTm{2,t}(bin)=nanmean(e(idx,3));
        HISTs{1,t}(bin)=nanstd(d(idx,3));
        HISTs{2,t}(bin)=nanstd(e(idx,3));
        HISTn{1,t}(bin)=length(d(idx,3));
        HISTn{2,t}(bin)=length(e(idx,3));
    end

clear B I idx bin d e
end

clear t

%%

for i=1:8;
subplot(2,4,i); plot(HIST{i});
end