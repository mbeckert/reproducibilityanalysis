%% Organize Data

% Munge ICLS FF
load('f:\desktop\Data\Analysis\ReproducibilityPaper\working\iclsFF_brette_7.mat');
data{1}{2} = CURVE;
data{1}{1} = Mean;

% Munge ICLS Dichotic
load('f:\desktop\Data\Analysis\ReproducibilityPaper\working\iclsITD_brette_7.mat');
c = CURVE;
m = Mean;
load('f:\desktop\Data\Analysis\ReproducibilityPaper\working\iclsILD_brette_7.mat');
for i = 1:length(Mean)
    c{i} = [c{i},CURVE{i}];
    m{i} = [m{i},Mean{i}];
end
data{2}{2} = c;
data{2}{1} = m;

clear c m h p CURVE HIST HW i Index Mean PEAK SAC*

% Munge OT FF
load('f:\desktop\Data\Analysis\ReproducibilityPaper\working\otAzEl_brette_5.mat');
c = cell(1,length(Azimuth));
m = cell(1,length(Azimuth));
for i = 1:length(Azimuth)
    c{i} = [Azimuth{1,i},Elevation{1,i}];
    m{i} = [Azimuth{2,i},Elevation{2,i}];
end
data{3}{2} = c;
data{3}{1} = m;
clear Azimuth Elevation i c m h p

cd('F:\Desktop\WorkingFolder')

% save repsyncdata

% % consolidate data

% load('repsyncdata.mat')

output = cell(1,3);
normalize = [1,1];

for t = 1:3
    output{t} = cell(2,length(data{t}{1}));
    for i = 1:length(data{t}{1})
        output{t}{1,i} = normalization(data{t}{1}{i},normalize(1));
        for m = 2
            output{t}{m,i} = normalization(data{t}{m}{i},normalize(2));
        end
    end
end

clear normalize t i m data

% % Run correlations

m_output = output;
m_output{1} = cell2mat(m_output{1});
m_output{2} = cell2mat(m_output{2});
m_output{3} = cell2mat(m_output{3});

corr_cell = cell(length(output),1);
corr_full = nan(size(m_output,2),size(m_output{1},1)-1);

for t = 1:3
    for m = 2
        corr_full(t,m-1) = corr(m_output{t}(1,:)',m_output{t}(m,:)','rows','pairwise');
        corr_cell{t,m-1} = nan(length(output{t}),1);
        for i = 1:length(output{t})
            if sum(isfinite(output{t}{m,i})) > 2
            c = corr(output{t}{1,i}',output{t}{m,i}','rows','pairwise');
%             corr_cell{t,m-1}(i) = 0.5*log((1+c)/(1-c));
            corr_cell{t,m-1}(i) = c;
            end
        end
    end
end

clear t m i c

% % scatter plots for visualization

% rangex = [0,1,11];
% rangey = [0,1,11];
% cmap = flipud(gray(64));
% 
% for t = 1:3
%     for m = 1
%         data = [m_output{t}(1,:)',m_output{t}(m+1,:)'];
%         [mat,~,~] = hist2D(data,rangex,rangey);
%         figure(t)
%         subplot(1,2,m*2-1)
%         imagesc(mat)
%         colormap(cmap)
%         colorbar
%         axis square
%         subplot(1,2,m*2)
%         scatter(m_output{t}(1,:),m_output{t}(m+1,:),2,'k','fill')
%         axis square
%     end
% end

clear range* t m data mat cmap