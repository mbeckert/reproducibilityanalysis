clr = distinguishable_colors(2);

cut = 25;
load('inout.mat')
pl{1} = cell2mat(data(:,1));
pl{2} = cell2mat(data(:,2));
m = cellfun(@mean, pl, 'UniformOutput', 0);
idx = m{1} > cut;
m{1}(idx) = [];
m{2}(idx) = [];
figure(1)
scatter(m{1},m{2}, 10, clr(1,:))
hold on
plot(1:cut,1:cut, 'Color', clr(2,:))
title('input/output')

for t = 1:3
    switch t
        case 1
            load('ITDmodel.mat')
        case 2
            load('ILDmodel.mat')
        case 3
            load('ITDconstant2.mat')
    end
pl{1} = cell2mat(data(:,1));
pl{2} = cell2mat(data(:,2));
m = cellfun(@mean, pl, 'UniformOutput', 0);
s = cellfun(@std, pl, 'UniformOutput', 0);
m{1} = m{1}/max(m{1});
m{2} = m{2}/max(m{2});
s{1} = s{1}/max(m{1});
s{2} = s{2}/max(m{2});

figure(t+1)
hold on
for i = 1:2
%     errorbar(linspace(-75,75,200),m{i}(1:200), s{i}(1:200), 'Color', clr(i,:))
    plot(linspace(-75, 75, 200), m{i}, 'Color', clr(i,:));
end
end

clear pl m s t i data cut clr idx ma