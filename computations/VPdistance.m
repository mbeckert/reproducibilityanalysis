function [cost, spt_1a] = VPdistance(spt_1, spt_2, q)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the Victor-Purpura Distance Metric between 2 spike trains
% (spt_1 and spt_2).
%
% Uses a logical process where we look for spikes in train 1 which are too
% far away from any other spike in train 2 to shift. Deleting these spikes
% and either adding them back or keeping them out (in the case that train 1
% has a higher firing rate than 2), is therefore the cheapest solution.
%
% Then goes through and finds the smallest (cheapest) temporal shifts
% possible. Shifting spikes from smallest to largest ISI under the theshold
% ensures that each shift made is the cheapest. If the situation arises
% where a spike would shift to where another spike has already shifted to
% (which would have been a cheaper move) then find the second best shift.
% If none exist then delete that spike.
%
% Lastly, we identify any missing spikes in train 2 and add these to train
% 1, this step is optimal because if no spike could be shifted to match
% these missing spikes then the cost of addition is cheapest.
%
%
% Input:
%   spt_1 : Original spike train 1. Spike times should be in seconds
%   spt_2 : Second spike train, spt_1 will become spt_2
%   q     : This is the important weight variable which is multiplied by
%           change in time for a temporal shift to calculate the cost
%           associated. If this is very small then time shifts will be
%           relatively cheaper and deletions will be less likely.
%               User should play with this to find an appropriate value
%
% Output:
%   cost   : final cost added up during the process (arbitrary units)
%   spt_1a : the resulting spike train, should always match spt_2
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Version 1: 11/21/18 - Michael Beckert
%
% - Added the catch for the special situation of inputing empty spike
% trains. Originally the program would yield a cost of 1 if the spikes
% trains were empty. Now it should output 0.
%           11/26/18 - MVB
%
% - Fixed error which occured when train 2 had a single spike
%           11/27/18 - MVB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TO DO:
%
%   - Confirm if when a spike shift is going to occur but the spike is
%   already occupied, that it is not possible that the new shift could ever
%   be cheaper than any shift which occured previously. 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



ad = 1;         % cost to add or delete a spikes (should always be 1)
cost = 0;       % initiate cost counter
spt_1a = spt_1; % This is  the spike train we will be editing and tracking

%%%%%%%%%%
% Get all of the ISIs between all spikes in train 1 and train 2. This will
% be used for Steps 1 and 2

isi = nan(length(spt_2), length(spt_1));
for s = 1:length(spt_1)
    isi(:, s) = abs(spt_1(s) - spt_2);
end
clear s

%%%%%%%%%%
% Step 1 is to find spikes in train 1 which are too far away from any spike
% in train 2 and delete them
% Also including a catch for the special situation where there are no
% spikes in some of the spike trains

if ~isempty(spt_1) && isempty(spt_2)
    cost = length(spt_1);
elseif ~isempty(spt_1) && ~isempty(spt_2)
    mincheck = isi * q < ad * 2;
    idx = sum(mincheck, 1) == 0;
    spt_1a(idx) = nan;
    cost = cost + sum(idx);
end

clear idx mincheck

%%%%%%%%%%
% Step 2 is make the smallest temporal shift possible for all remaining
% spikes. Before each shift we confirm that the spike is not already
% "occupied," Meaning two spikes cannot be moved to the same time. If this
% occurs then go through that particular spikes ISIs and find the next
% available candidate shift. If none are possible then delete the spike.

% Get a list of ISIs which are below the cost threshold in ascending order
minlist = unique(min(isi, [], 1));
minlist = sort(minlist(minlist * q < ad * 2));

for i = 1:length(minlist)
    [sp2, sp1] = find(isi == minlist(i), 1, 'first');       % Identify the related spikes in the 2 trains for this minimal ISI
    if sum(unique(spt_1a) == spt_2(sp2)) == 0   %   Confirm no other spike exists at this new time
        spt_1a(sp1) = spt_2(sp2);               %   Shift the spike
        cost = cost + (minlist(i) * q);         %   Add qDeltaT to cost
    else                                        % Here there is already a spike which was shifted for a cheaper cost
        minlist_sub = sort(isi(isi(:,sp1) * q < ad * 2, sp1));  % Find all other ISIs under the threshold for this spike
        flag_move = 0;                          %   Create a flag to denote when a usable shift is found
        ii = 0;
        while flag_move == 0 && ii < length(minlist_sub)
        	ii = ii + 1;
            [sp2, sp1] = find(isi == minlist_sub(ii), 1, 'first');
            if sum(unique(spt_1a) == spt_2(sp2)) == 0   % Check if this new ISI is usable
                spt_1a(sp1) = spt_2(sp2);               %   If so shift it
                cost = cost + (minlist(i) * q);         %   add to the cost
                flag_move = 1;                          %   and mark the flag
            end
        end
        if flag_move == 0                       % If no usuable ISI is found then
            spt_1a(sp1) = nan;                  %   delete the spike
            cost = cost + 1;
        end
        clear flag_move ii
    end
    clear sp1 sp2
end
clear i minlist isi

%%%%%%%%%%%%%%%
% Step 3 is to check for any missing spikes and add them

occ_check = nan(size(spt_2));               % Preallocate
for s = 1:length(occ_check)
    occ_check(s) = sum(spt_1a == spt_2(s)); % Check for identical spike times
end

if sum(occ_check > 1)       % This is quick check that nothing strange happened in Step 2
    error('multiple spikes occupying same time value')
end

add_sp = nan(1000, 1);      % preallocate some space to put additional spikes

for s = 1:length(occ_check)
    if occ_check(s) == 0    % If any spike is missing, add it
        add_sp(find(isnan(add_sp), 1, 'first')) = spt_2(s);
        cost = cost + 1;
    end
end

clear s occ_check

spt_1a = [spt_1a; add_sp];  % Tidy up the new spike train, concatenate the additional spikes
spt_1a = sort(spt_1a(~isnan(spt_1a))); % and remove all nans, sorting the result

clear add_sp

end