% "TrialData" must be in milliseconds (we convert to seconds later)
% "bins" and "bin" must be in seconds
% "maxWidth" is a unitless number of elements to make the SAC

function [HIST,CURVE,Mean,SACcurve,SACnorm,HW,PEAK] = reproducibility_jorisnorm(TrialData,bins,bin,maxWidth)

TrialData = cellfun(@(x) x/1000, TrialData, 'UniformOutput', 0);

m = cellfun(@length, TrialData, 'UniformOutput', 0);
m = cell2mat(m);
m = mean(m,2)/max(bins);
psth = cellfun(@(x) hist(x,bins), TrialData, 'UniformOutput', 0);

N = size(psth,2);
dur = bins(end) - bins(1);

HIST = sum(cell2mat(psth'));
HIST = HIST/N*(max(diff(bins))*1000)*1000;

% HIST = HIST(1:maxWidth); %%%%%

sac=zeros(1,maxWidth);

PSTHnoself=(HIST-1/(bin*N));
PSTHnoself(PSTHnoself < 0) = 0;
sac(1)=mean(HIST.*PSTHnoself)*N/(N-1);

for i = 2:maxWidth
    sac(i)=mean(HIST(1:end-i+1).*HIST(i:end));
end

sacfull=[fliplr(sac(2:end)),sac];

% Joris normalization
nf = N * (N - 1) * m.^2 * dur * bin;
SACcurve = sacfull;
SACnorm = sacfull ./ nf;

% Brette normalization
%     SACcurve = sacfull/bin/dur;
%     SACnorm = ((sacfull/bin/dur)-m.^2)*bin/m;

thresh = (SACcurve(maxWidth) + min(SACcurve))/2;
thresh_check = SACcurve(maxWidth:end) > thresh;
if thresh_check(1) == 0
    HW = nan(size(thresh, 1), 1);
else
    x = (find(thresh_check == 0, 1, 'first') * 2) - 1;
    if isempty(x)
        x = nan;
    end
    HW = x;% * bin;
end

% integral of curve at half width
% point = find(thresh_check == 0, 1, 'first');
% CURVE = sum(SACnorm(maxWidth-point+1:maxWidth+point-1))/(point*2-1);
CURVE = nanmean(SACnorm, 2);

% use a constant width
%     CURVE = sum(SACnorm(maxWidth-16:maxWidth+16));

PEAK = SACnorm(maxWidth)';

Mean = m';

end