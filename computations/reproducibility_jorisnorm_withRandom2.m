% "TrialData" must be in milliseconds (we convert to seconds later)
% "bins" and "bin" must be in seconds
% "maxWidth" is a unitless number of elements to make the SAC

function [HIST,CURVE,Mean,SACcurve,SACnorm,HW,PEAK] = reproducibility_jorisnorm_withRandom2(TrialData,bins,bin,maxWidth)

% Convert spike times to seconds
TrialData = cellfun(@(x) x/1000, TrialData, 'UniformOutput', 0);
RandomData = generate_random_spikes(TrialData, bins(1), bins(end));

% find mean firing rate, used for normalization and output
m = cellfun(@length, TrialData, 'UniformOutput', 0);
m = cell2mat(m);
m = mean(m,2)/max(bins);

% Convert spike times for each trial to vectors of time bins
psth = cellfun(@(x) hist(x,bins), TrialData, 'UniformOutput', 0);
psth_R = cellfun(@(x) hist(x,bins), RandomData, 'UniformOutput', 0);

% number of trials
N = size(psth,2);
% duration of trial
dur = bins(end) - bins(1);

% master histogram
HIST = sum(cell2mat(psth'));
HIST = HIST/N*(max(diff(bins))*1000)*1000;
HIST_R = sum(cell2mat(psth_R'));
HIST_R = HIST_R/N*(max(diff(bins))*1000)*1000;

% Compute SAC as performed by Romain Brette in 2012 paper
sac=zeros(1,maxWidth);

PSTHnoself=(HIST-1/(bin*N));
PSTHnoself(PSTHnoself < 0) = 0;
sac(1)=mean(HIST.*PSTHnoself)*N/(N-1);

sac_R=zeros(1,maxWidth);

PSTHnoself_R=(HIST_R-1/(bin*N));
PSTHnoself_R(PSTHnoself_R < 0) = 0;
sac_R(1)=mean(HIST.*PSTHnoself_R)*N/(N-1);

for i = 2:maxWidth
    sac(i)=mean(HIST(1:end-i+1).*HIST(i:end));
    sac_R(i)=mean(HIST_R(1:end-i+1).*HIST_R(i:end));
end

sacfull=[fliplr(sac(2:end)),sac];
sacfull_R=[fliplr(sac_R(2:end)),sac_R];

% Joris normalization
nf = N * (N - 1) * m^2 * dur * bin;
SACcurve = sacfull - sacfull_R;
SACnorm = (sacfull - sacfull_R) / nf;

% Brette normalization
% SACcurve = (sacfull - sacfull_R)  / bin / dur;
% SACnorm = (((sacfull - sacfull_R)  / bin / dur) - m.^2)*bin/m;

% find half-width
thresh = (SACcurve(maxWidth) + min(SACcurve))/2;
thresh_check = SACcurve(maxWidth:end) > thresh;
if thresh_check(1) == 0
    HW = nan;
else
    x = (find(thresh_check == 0, 1, 'first') * 2) - 1;
    if isempty(x)
        x = nan;
    end
    HW = x;% * bin;
end

% Take area under the curve at the half-widths
% Using this for tests but have also used a constant width to keep the
% variable widths out as a confound in actual data, instead using a
% constant width which matches the median half-width of our data set (MB)
point = find(thresh_check == 0, 1, 'first');
CURVE = sum(SACnorm(maxWidth-point+1:maxWidth+point))/(point*2+1);

% Constant width for AUC (+-16 bins in this case)
%     CURVE = sum(SACnorm(maxWidth-16:maxWidth+16));

% Use the value at the center as the peak
PEAK = SACnorm(maxWidth);

Mean = m';

end