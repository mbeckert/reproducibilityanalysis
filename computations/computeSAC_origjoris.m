function [sac, nf, fr]=computeSAC_origjoris(spikes,spikeWin,binWidth, maxBin)
% computeSAC         Compute a shuffled auto-correlogram.
%
% SAC=computeSAC(SPK,SPKWIN) will compute an unnormalized shuffled 
% auto-correlogram SAC, using the cell array SPK (one element per trial,
% spike times in milliseconds) between the window MIN(SPKWIN) and
% MAX(SPKWIN).
%
% [SAC,NF]=computeSAC(...) does the same, but returns the normalizing
% factor NF such that values of 1 in SAC./NF signify a distribution
% predicted by chance.
%
% computeSAC takes the following variable arguments:
%    'binWidth': bin width of the output histogram (ms; default 50e-3).
%    'maxBin': maximum extent of the output histogram (ms; default 15).
%
% Computation of the SAC and its normalizing factor is as in 
% Joris PX (2003), J Neurosci 23:6345?6350.  Note that while the full SAC
% is return (corresponding to a time axis -maxBin:binWidth:maxBin), it is
% symmetric about zero.

% Set some defaults.
if nargin < 3
    binWidth=50e-3;
end
if nargin < 4
    maxBin=15;
end

% Make sure spikeWin is sorted in the correct order.
spikeWin=sort(spikeWin);

% Pre-allocate some arrays.
rates=zeros(size(spikes));
histax=-maxBin:binWidth:maxBin;
sac=zeros(size(histax));

% Now the work...
for n=1:length(spikes)
    
    % For the current trial, get the spikes, filter out any outside the
    % spike window, and then compute a mean rate for use in the normalizing
    % factor later.
    thisTrial=spikes{n};
    thisTrial=thisTrial(thisTrial>spikeWin(1)&thisTrial<spikeWin(2));
    rates(n)=length(thisTrial)./(diff(spikeWin)./1000);
    
    % Now, for each trial which we're NOT working on, find all the spikes
    % in the spike window.
    idx=1:length(spikes);idx=idx(idx~=n);
    restSpikes=[spikes{idx}];
    restSpikes=restSpikes(restSpikes>spikeWin(1)&...
        restSpikes<spikeWin(2));
    
    % Then, for each spike in the current trial, subtract it from the times
    % of all spikes from all other trials, bin them up, and keep track of
    % them in the running histogram.
    for t=1:length(thisTrial);
        rs=restSpikes-thisTrial(t);
        rs=rs(abs(rs)<maxBin);
        tmp=hist(rs,histax);
        sac=sac+tmp;
    end
end

% Finally, compute the normalizing factor.
nf=length(spikes)*(length(spikes)-1)*mean(rates).^2*...
    (diff(spikeWin)./1000)*(binWidth./1000);

fr = mean(rates);
        