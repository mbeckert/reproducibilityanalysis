
%% Bin data for plotting and statistics

met = 4; % met is the metric you want to use, 
         % either 1 for reproducibility 
         % or 3 for HW
         % or 4 for PEAK

val = cell(1,6);
out = cell(1,6);
% X = nan(1,6);

for t = 1:6
    switch t
        case 1
            data = iclsAZ;%_trim;
        case 2
            data = iclsEL;%_trim;
        case 3
            data = iclsITD;%_trim;
        case 4
            data = iclsILD;%_trim;
        case 5
            data = otAZ;%_trim;
        case 6
            data = otEL;%_trim;
    end
    
%     data = cell2mat(data);
    
%     X(t) = ceil(max(data(2,:)));
%     x = linspace(0,X(t),10); % for raw values
    x = 0:0.2:1;    % for normalized values
%     x = -4:0.1:4;
    val{t} = cell(1,length(x));
    
[~,idx] = histc(data(2,:),x);
out{t} = zeros(3,length(x));

for h = 1:length(x)
    i = idx == h;
    val{t}{h} = data(met,i);
    out{t}(1,h) = nanmedian(data(met,i));
    out{t}(2,h) = nanmean(data(met,i));
    out{t}(3,h) = nanstd(data(met,i));
end

clear i h data idx

end

clear t x

close all

for t = 1:6
    figure(t)
    subplot(2,1,1)
    errorbar(out{t}(1,:),out{t}(3,:))
%     plot(out{t}(1,:))
    subplot(2,1,2)
    errorbar(out{t}(2,:),out{t}(3,:))
%     plot(out{t}(2,:))
%     subplot(3,1,3)
%     plot(out{t}(3,:))
end
    
clear t

for t = 1:6
    m = max(cell2mat(cellfun(@length,val{t},'UniformOutput',0)));
    for i = 1:length(val{t})
        val{t}{i} = [val{t}{i}'; nan(m-length(val{t}{i}),1)];
    end
    val{t} = cell2mat(val{t});
end

clear t m i

statistics = cell(1,6);

for t = 1:6
    figure(t)
    [~,~,stats] = kruskalwallis(val{t});
%     pause
    statistics{t} = multcompare(stats);    
end

clear t stats met X

chck = cellfun(@(x) x(:,6)<0.05, statistics,'UniformOutput',0);
chck = cellfun(@sum,chck);

%% Performs the same function as the initial plotting in ReproducibiliityAnalysis
% This is here just for convenience when looking at the data
% comment out when not needed

% close all
% subplot(3,2,1)
% scatter(az(2,:),az(1,:),2,'fill')
% subplot(3,2,2)
% scatter(el(2,:),el(1,:),2,'fill')
% subplot(3,2,3)
% scatter(itd(2,:),itd(1,:),2,'fill')
% subplot(3,2,4)
% scatter(ild(2,:),ild(1,:),2,'fill')
% subplot(3,2,5)
% scatter(otAZ(2,:),otAZ(1,:),2,'fill')
% subplot(3,2,6)
% scatter(otEL(2,:),otEL(1,:),2,'fill')