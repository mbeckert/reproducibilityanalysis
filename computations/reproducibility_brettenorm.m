% "TrialData" must be in milliseconds (we convert to seconds later)
% "bins" and "bin" must be in seconds
% "maxWidth" is a unitless number of elements to make the SAC

function [HIST,CURVE,Mean,SACcurve,SACnorm,HW,PEAK] = reproducibility_brettenorm(TrialData,bins,bin,maxWidth)

% Convert spike times to seconds
TrialData = cellfun(@(x) x/1000, TrialData, 'UniformOutput', 0);

% find mean firing rate, used for normalization and output
m = cellfun(@length, TrialData, 'UniformOutput', 0);
m = cell2mat(m);
m = mean(m,2)/max(bins);

% Convert spike times for each trial to vectors of time bins
psth = cellfun(@(x) hist(x,bins), TrialData, 'UniformOutput', 0);

% number of trials
N = size(psth,2);
% duration of trial
dur = bins(end) - bins(1);

% master histogram
HIST = sum(cell2mat(psth'));
HIST = HIST/N*(max(diff(bins))*1000)*1000;

% HIST = HIST(1:maxWidth); %%%%%

% Compute SAC as performed by Romain Brette in 2012 paper
sac=zeros(1,maxWidth);

PSTHnoself=(HIST-1/(bin*N));
PSTHnoself(PSTHnoself < 0) = 0;
sac(1)=mean(HIST.*PSTHnoself)*N/(N-1);

for i = 2:maxWidth
    sac(i)=mean(HIST(1:end-i+1).*HIST(i:end));
end

sacfull=[fliplr(sac(2:end)),sac];

% Joris normalization
%     nf = N * (N - 1) * m^2 * dur * bin;
%     SACcurve = sacfull;
%     SACnorm = sacfull/nf;

% Brette normalization
% SACcurve = sacfull/bin/dur;
% SACnorm = ((sacfull/bin/dur)-m.^2)*bin/m;
SACcurve = sacfull;
SACnorm = ((sacfull)-m.^2)*bin/m;

% find half-width
thresh = (SACcurve(maxWidth) + min(SACcurve))/2;
thresh_check = SACcurve(maxWidth:end) > thresh;
if thresh_check(1) == 0
    HW = nan;
else
    x = (find(thresh_check == 0, 1, 'first') * 2) - 1;
    if isempty(x)
        x = nan;
    end
    HW = x;% * bin;
end

% Take area under the curve at the half-widths
% Using this for tests but have also used a constant width to keep the
% variable widths out as a confound in actual data, instead using a
% constant width which matches the median half-width of our data set (MB)

% point = find(thresh_check == 0, 1, 'first');
% point = 4;

% CURVE = sum(SACnorm(maxWidth-point+1:maxWidth+point))/(point*2+1);
% CURVE = (sum(sacfull(maxWidth-point+1:maxWidth+point-1)) - m .^ 2) * bin / m;
CURVE = (sum(sacfull - m .^ 2)) * bin / m;
% CURVE = sum(SACnorm(maxWidth-point+1:maxWidth+point));

% Constant width for AUC (+-16 bins in this case)
%     CURVE = sum(SACnorm(maxWidth-16:maxWidth+16));

% Use the value at the center as the peak
PEAK = SACnorm(maxWidth);

Mean = m';

end