function [nf,CURVE,Mean,SACcurve,SACnorm,HW,PEAK]  = reproducibility_bjorn(spikes, spikeWin, binWidth, maxBin)
% computeSAC         Compute a shuffled auto-correlogram.
%
% SAC=computeSAC(SPK,SPKWIN) will compute an unnormalized shuffled 
% auto-correlogram SAC, using the cell array SPK (one element per trial,
% spike times in milliseconds) between the window MIN(SPKWIN) and
% MAX(SPKWIN).
%
% [SAC,NF]=computeSAC(...) does the same, but returns the normalizing
% factor NF such that values of 1 in SAC./NF signify a distribution
% predicted by chance.
%
% computeSAC takes the following variable arguments:
%    'binWidth': bin width of the output histogram
%    'maxBin': maximum extent of the output histogram
%
% Computation of the SAC and its normalizing factor is as in 
% Joris PX (2003), J Neurosci 23:6345?6350.  Note that while the full SAC
% is return (corresponding to a time axis -maxBin:binWidth:maxBin), it is
% symmetric about zero.

% all times in seconds.
% ex: binWidth = 0.001 (1ms)
%     maxBin = 0.100
%     spikeWin = [0 0.5];

% Make sure spikeWin is sorted in the correct order.
spikeWin=sort(spikeWin);

% Pre-allocate some arrays.
rates=zeros(size(spikes));
histax = linspace(-maxBin*binWidth, maxBin*binWidth, maxBin*2+1);
sac=zeros(size(histax));

% Now the work...
for n=1:length(spikes)
    
    % For the current trial, get the spikes, filter out any outside the
    % spike window, and then compute a mean rate for use in the normalizing
    % factor later.
    thisTrial=spikes{n};
    thisTrial=thisTrial(thisTrial>spikeWin(1)&thisTrial<spikeWin(2));
    rates(n)=length(thisTrial)./(diff(spikeWin)./1000);
    
    % Now, for each trial which we're NOT working on, find all the spikes
    % in the spike window.
    idx=1:length(spikes);idx=idx(idx~=n);
    restSpikes=[spikes{idx}];
    restSpikes=restSpikes(restSpikes>spikeWin(1)&...
        restSpikes<spikeWin(2));
    
    % Then, for each spike in the current trial, subtract it from the times
    % of all spikes from all other trials, bin them up, and keep track of
    % them in the running histogram.
    for t=1:length(thisTrial);
        rs=restSpikes-thisTrial(t);
        rs=rs(abs(rs)<maxBin);
        tmp=hist(rs,histax);
        sac=sac+tmp;
    end
end

% Finally, compute the normalizing factor.
nf=length(spikes)*(length(spikes)-1)*mean(rates).^2*...
    diff(spikeWin)*binWidth;

% Joris normalization
%     nf = N * (N - 1) * m^2 * dur * bin;
%     SACcurve = sacfull;
%     SACnorm = sacfull/nf;

% Brette normalization
% SACcurve = sacfull/bin/dur;
% SACnorm = ((sacfull/bin/dur)-m.^2)*bin/m;
SACcurve = sac;
SACnorm = ((sac)-mean(rates).^2)*binWidth/mean(rates);

% find half-width
thresh = (SACcurve(maxBin) + min(SACcurve))/2;
thresh_check = SACcurve(maxBin:end) > thresh;
if thresh_check(1) == 0
    HW = nan;
else
    x = (find(thresh_check == 0, 1, 'first') * 2) - 1;
    if isempty(x)
        x = nan;
    end
    HW = x;% * bin;
end

% Take area under the curve at the half-widths
% Using this for tests but have also used a constant width to keep the
% variable widths out as a confound in actual data, instead using a
% constant width which matches the median half-width of our data set (MB)

point = find(thresh_check == 0, 1, 'first');
% point = 4;

CURVE = sum(SACnorm(maxBin-point+1:maxBin+point))/(point*2+1);
% CURVE = sum(SACnorm(maxBin-point+1:maxBin+point));

% Constant width for AUC (+-16 bins in this case)
%     CURVE = sum(SACnorm(maxBin-16:maxBin+16));

% Use the value at the center as the peak
PEAK = SACnorm(maxBin);

Mean = mean(rates);

        