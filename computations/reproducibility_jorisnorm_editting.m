% "TrialData" must be in milliseconds (we convert to seconds later)
% "bins" and "bin" must be in seconds
% "maxWidth" is a unitless number of elements to make the SAC

function [HIST,CURVE,Mean,SACcurve,SACnorm,HW,PEAK] = reproducibility_jorisnorm_editting(TrialData,bins,bin,maxWidth)

TrialData = cellfun(@(x) x/1000, TrialData, 'UniformOutput', 0);

m = cellfun(@length, TrialData, 'UniformOutput', 0);
m = cell2mat(m);
m = mean(m,2)/max(bins);
psth = cellfun(@(x) hist(x,bins), TrialData, 'UniformOutput', 0);

N = size(psth,2);
dur = bins(end) - bins(1);

HIST = sum(cell2mat(psth'));
HIST = HIST/N*(max(diff(bins))*1000)*1000;

[sacfull, ~] = reproducibility_bjorn ... 
    (TrialData, [bins(1) bins(end)], bin, maxWidth * bin);

% Joris normalization
nf = N * (N - 1) * m^2 * dur * bin;
SACcurve = sacfull;
SACnorm = sacfull / nf;

% Brette normalization
%     SACcurve = sacfull/bin/dur;
%     SACnorm = ((sacfull/bin/dur)-m.^2)*bin/m;

thresh = (SACcurve(maxWidth+1) + min(SACcurve))/2;
thresh_check = SACcurve(maxWidth + 1:end) > thresh;
if thresh_check(1) == 0
    HW = nan;
else
    x = (find(thresh_check == 1, 1, 'last') * 2) - 1;
    if isempty(x)
        x = nan;
    end
    HW = x * bin;
end

% integral of curve at half width
point = find(thresh_check == 1, 1, 'last');
CURVE = sum(SACnorm(maxWidth-point+1:maxWidth+point))/(point*2+1);

% use a constant width
%     CURVE = sum(SACnorm(maxWidth-16:maxWidth+16));

PEAK = SACnorm(maxWidth);

Mean = m';

end