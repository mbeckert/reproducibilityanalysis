function [cost, spt_1a] = VPdistance_perm(spt_1, spt_2, q)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculates the Victor-Purpura Distance Metric between 2 spike trains
% (spt_1 and spt_2).
%
% Uses a logical process where we look for spikes in train 1 which are too
% far away from any other spike in train 2 to shift. Deleting these spikes
% and either adding them back or keeping them out (in the case that train 1
% has a higher firing rate than 2), is therefore the cheapest solution.
%
% Then goes through and finds the smallest (cheapest) temporal shifts
% possible. Shifting spikes from smallest to largest ISI under the theshold
% ensures that each shift made is the cheapest. If the situation arises
% where a spike would shift to where another spike has already shifted to
% (which would have been a cheaper move) then find the second best shift.
% If none exist then delete that spike.
%
% Lastly, we identify any missing spikes in train 2 and add these to train
% 1, this step is optimal because if no spike could be shifted to match
% these missing spikes then the cost of addition is cheapest.
%
%
% Input:
%   spt_1 : Original spike train 1. Spike times should be in seconds
%   spt_2 : Second spike train, spt_1 will become spt_2
%   q     : This is the important weight variable which is multiplied by
%           change in time for a temporal shift to calculate the cost
%           associated. If this is very small then time shifts will be
%           relatively cheaper and deletions will be less likely.
%               User should play with this to find an appropriate value
%
% Output:
%   cost   : final cost added up during the process (arbitrary units)
%   spt_1a : the resulting spike train, should always match spt_2
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Version 2: 11/27/18 - Michael Beckert
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   TO DO:
%
% - Update comments/description
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if length(spt_2) < length(spt_1)
    x = spt_1; spt_1 = spt_2; spt_2 = x;
    clear x
end

ad = 1;              

isi = cell(1, length(spt_1));
for s = 1:length(spt_1)
    isi{s} = abs(spt_1(s) - spt_2');
end

isi_list = combvec(isi{:});
if length(spt_2) == 1
    isi_list = isi_list';
end
isi_list(isi_list * q >= ad * 2) = 2;

isi_opt = sum(isi_list, 1);
[~, isi_opt] = sort(isi_opt);
isi_list = isi_list(:, isi_opt);

isi = cell2mat(isi')';

if isempty(spt_1) && isempty(spt_2)
    success_flag = 1;
    cost = 0;
    spt_1a = [];
elseif isempty(spt_1)
    cost = length(spt_2);
    success_flag = 1;
    spt_1a = [];
elseif isempty(spt_2)
    cost = length(spt_1);
    success_flag = 1;
    spt_1a = [];
else
    success_flag = 0;
    cost = 0;
    fail_flag = 0;
    it = 1;
end

while success_flag == 0
    
    if it > size(isi_list, 2)
        error('unsolvable train')
    end
    
    spt_1a = spt_1; 
    idx_del = isi_list(:, it) == 2;
    spt_1a(idx_del) = NaN;
    cost = cost + (sum(idx_del) * ad);
    
    for s = 1:size(isi_list, 1)
        if ~isnan(spt_1a(s))
            [sp2, ~] = find(isi(:, s) == isi_list(s, it), 1, 'first');
            if sum(unique(spt_1a) == spt_2(sp2)) == 0
                spt_1a(s) = spt_2(sp2);
                cost = cost + (isi_list(s, it) * q);
            else
                fail_flag = 1;
            end
        end
    end
    
    if fail_flag == 0
        success_flag = 1;
    elseif fail_flag == 1
        success_flag = 0;
        fail_flag = 0;
        it = it + 1;
        cost = 0;
    end
    
end

occ_check = nan(size(spt_2)); 
for s = 1:length(occ_check)
    occ_check(s) = sum(spt_1a == spt_2(s));
end

if sum(occ_check > 1)
    error('multiple spikes occupying same time value')
end

add_sp = nan(1000, 1);

for s = 1:length(occ_check)
    if occ_check(s) == 0
        add_sp(find(isnan(add_sp), 1, 'first')) = spt_2(s);
        cost = cost + ad;
    end
end

spt_1a = [spt_1a; add_sp];
spt_1a = sort(spt_1a(~isnan(spt_1a)));

clear add_sp ad
