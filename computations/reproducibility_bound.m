% "TrialData" must be in milliseconds (we convert to seconds later)
% "bins" and "bin" must be in seconds
% "maxWidth" is a unitless number of elements to make the SAC

function [HIST,CURVE,Mean,SACcurve,SACnorm,HW,PEAK] = reproducibility_bound(TrialData,bins,bin,maxWidth,peakwidth)
    
    TrialData = cellfun(@(x) x/1000, TrialData, 'UniformOutput', 0);
    m = cellfun(@length, TrialData, 'UniformOutput', 0);
    m = cell2mat(m);
    m = mean(m,2)/max(bins);
    psth = cellfun(@(x) hist(x,bins), TrialData, 'UniformOutput', 0);
    
    N = size(psth,2);
    
    HIST = zeros(size(psth,1),length(bins));
    
    for h = 1:size(psth,1)
        for i = 1:N
            HIST(h,:) = HIST(h,:) + psth{h,i};
        end
    end
    
    HIST = HIST/N*(max(diff(bins))*1000)*1000;
    
    SACcurve = cell(1,size(psth,1));
    SACnorm = cell(1,size(psth,1));
    CURVE = nan(1,size(psth,1));
    HW = nan(1,size(psth,1));
    PEAK = nan(1,size(psth,1));
    
    for sp = 1:size(psth,1)
        
        sac=zeros(1,maxWidth);
        PSTHnoself=(HIST(sp,:)-1/(bin*N));
        PSTHnoself(PSTHnoself < 0) = 0;
        sac(1)=mean(HIST(sp,:).*PSTHnoself)*N/(N-1);
        
        for i = 2:maxWidth
            sac(i)=mean(HIST(sp,1:end-i+1).*HIST(sp,i:end));
        end
        
        sacfull=[fliplr(sac(2:end)),sac];
        
        nf=N*(N-1)*m(sp).^2*...
            diff([bins(1), bins(end)]) * bin;
        SACcurve{sp} = sacfull;
        SACnorm{sp} = sacfull/nf;
    
%         SACcurve{sp} = sacfull/bin/N;
%         SACnorm{sp} = ((sacfull/bin/N)-m(sp).^2)*bin/m(sp);
        
        thresh = SACcurve{sp}(maxWidth)/2;
        thresh_check = SACcurve{sp}(maxWidth:end) > thresh;
        if thresh_check(1) == 0
            HW(sp) = nan;
        else
            x = (find(thresh_check == 0, 1, 'first') * 2) - 1;
            if isempty(x)
                x = nan;
            end
            HW(sp) = x;% * bin;
        end
        
%         point = find(thresh_check == 0, 1, 'first');
        
%         CURVE(sp) = sum(SACnorm{sp}(maxWidth-point:maxWidth+point))/(point*2+1);
%         CURVE(sp) = sum(SACnorm{sp})/(length(SACnorm{sp}));
        CURVE(sp) = sum(SACnorm{sp}(maxWidth-peakwidth:maxWidth+peakwidth));
        PEAK(sp) = SACnorm{sp}(maxWidth);
        
    end
    
    Mean = m';
    
end