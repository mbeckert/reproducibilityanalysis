% "TrialData" must be in milliseconds (we convert to seconds later)
% "bins" and "bin" must be in seconds
% "maxWidth" is a unitless number of elements to make the SAC

function [HIST,CURVE,Mean,SACcurve,SACnorm,HW,PEAK] = reproducibility_brettenorm_withRandom(TrialData,bins,bin,maxWidth)

TrialData = cellfun(@(x) x/1000, TrialData, 'UniformOutput', 0);
RandomData = generate_random_spikes(TrialData, bins(1), bins(end));

m = cellfun(@length, TrialData, 'UniformOutput', 0);
m = cell2mat(m);
m = mean(m,2)/max(bins);
psth = cellfun(@(x) hist(x,bins), TrialData, 'UniformOutput', 0);
psth_R = cellfun(@(x) hist(x,bins), RandomData, 'UniformOutput', 0);

N = size(psth,2);

HIST = zeros(size(psth,1),length(bins));
HIST_R = zeros(size(psth_R,1),length(bins));

for h = 1:size(psth,1)
    for i = 1:N
        HIST(h,:) = HIST(h,:) + psth{h,i};
        HIST_R(h,:) = HIST_R(h,:) + psth_R{h,i};
    end
end

HIST = HIST/N*(max(diff(bins))*1000)*1000;
HIST_R = HIST_R/N*(max(diff(bins))*1000)*1000;

SACcurve = cell(1,size(psth,1));
SACnorm = cell(1,size(psth,1));
CURVE = nan(1,size(psth,1));
HW = nan(1,size(psth,1));
PEAK = nan(1,size(psth,1));

for sp = 1:size(psth,1)
    
    sac=zeros(1,maxWidth);
    PSTHnoself=(HIST(sp,:)-1/(bin*N));
    PSTHnoself(PSTHnoself < 0) = 0;
    sac(1)=mean(HIST(sp,:).*PSTHnoself)*N/(N-1);
    
    sac_R = zeros(1,maxWidth);
    PSTHnoself_R = (HIST_R(sp,:)-1/(bin*N));
    PSTHnoself_R(PSTHnoself_R < 0) = 0;
    sac_R(1)=mean(HIST_R(sp,:).*PSTHnoself_R)*N/(N-1);
    
    for i = 2:maxWidth
        sac(i)=mean(HIST(sp,1:end-i+1).*HIST(sp,i:end));
        sac_R(i)=mean(HIST_R(sp,1:end-i+1).*HIST_R(sp,i:end));
    end
    
    sacfull=[fliplr(sac(2:end)),sac] / bin/ N;
    sacfull_R=[fliplr(sac_R(2:end)),sac_R] / bin/ N;
    
    % Joris normalization
%     nf=N*(N-1)*m(sp).^2*...
%         diff([bins(1), bins(end)]) * bin;
%     SACcurve{sp} = sacfull;
%     SACnorm{sp} = sacfull/nf;
    
    % Brette normalization
    SACcurve{sp} = sacfull - sacfull_R;
    SACnorm{sp} = (((sacfull - sacfull_R) /bin/N)-m(sp).^2)*bin/m(sp);
    
    thresh = SACcurve{sp}(maxWidth)/2;
    thresh_check = SACcurve{sp}(maxWidth:end) > thresh;
    if thresh_check(1) == 0
        HW(sp) = nan;
    else
        x = (find(thresh_check == 0, 1, 'first') * 2) - 1;
        if isempty(x)
            x = nan;
        end
        HW(sp) = x;% * bin;
    end
    
    %         point = find(thresh_check == 0, 1, 'first');
    
    %         CURVE(sp) = sum(SACnorm{sp}(maxWidth-point:maxWidth+point))/(point*2+1);
    %         CURVE(sp) = sum(SACnorm{sp})/(length(SACnorm{sp}));
    CURVE(sp) = sum(SACnorm{sp}(maxWidth-16:maxWidth+16));
    PEAK(sp) = SACnorm{sp}(maxWidth);
    
end

Mean = m';

end