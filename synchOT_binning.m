% cd('C:\Users\mbeck\Desktop\Data\Analysis\ReproducibilityPaper\working')
% load SynchOT_checksidepeaks
% 
% el = cell2mat(Elevation')';

d = cell(3,11);
x = 0:0.1:1;

% data = [in;out];
% data = el;
data=data';

[~,bins] = histc(data(:,1),x);

for i = 1:length(x)
    idx = bins == i;
    d{1,i} = data(idx,2);
    d{2,i} = data(idx,3);
    d{3,i} = data(idx,4);
end

m = max(max(cell2mat(cellfun(@length,d,'UniformOutput',0))));
for t = 1:3
    for i = 1:length(d)
        d{t,i} = [d{t,i}; nan(m-length(d{t,i}),1)];
    end
end

clear x data i bins 

x1 = cell2mat(d(1,:));
x2 = cell2mat(d(2,:));
x3 = cell2mat(d(3,:));
