load('D:\Data\Analysis\Data\FreeFieldRawDATA_notsidecorrected_FFffomitted_2.mat','OT')
data = OT;
clear OT
%

output = cell(200,2);

pattern = '';                   % These are to remove
pattern2 = 'it.';                   % These are to keep

for date = 1:length(data)
    for site = 1:length(data{date})
        for unit = 2:length(data{date}{site})
            
            d = data{date}{site}{unit};
            
            files = fieldnames(d);
            
            idx = regexpi(files,pattern);
            idx = cellfun(@isempty,idx);
            files = files(idx);
            
            idx = regexpi(files,pattern2);
            idx = ~cellfun(@isempty,idx);
            files = files(idx);
            
            clear idx
            
            if length(files) > 1
                error('multiple filetypes obtained')
            elseif length(files) == 1
                L = find(cellfun(@isempty,output(:,1)),1,'first');
                output{L,1} = cellfun(@transpose,d.(files{1}).spikes_times,'UniformOutput',0);
                output{L,2} = d.(files{1}).depvar;
            end
            
        end         % unit
    end             % site
end                 % date

L = find(cellfun(@isempty,output(:,1)),1,'first');
output(L:end,:) = [];

ot = output;

clear date site unit pattern* filetypes L output d files data

%

output = cell(200,2);

pattern = '.dat';                   % These are to remove
pattern2 = '.-itd.';                   % These are to keep

cd('D:\Data\ICls')
fol = ls;
fol = fol(5:end,:);

for fo = 1:size(fol,1)
    
    cd(['D:\Data\ICls\' fol(fo,:)]);
    files = ls;
    chck = zeros(size(files,1),1);
    
    for fi = 3:size(files,1)
        
        out = regexpi(files(fi,:),pattern);
        in = regexpi(files(fi,:),pattern2);
        if isempty(out) && ~isempty(in)
            chck(fi) = 1;
        end
        
    end         % fi
    chck = chck == 1;
    files = files(chck,:);
    
    for fi = 1:size(files,1)
        load(files(fi,:))
        L = find(cellfun(@isempty,output(:,1)),1,'first');
        output{L,1} = curvedata.spike_times;
        output{L,2} = curvedata.depvars_sort(:,1);
    end         % fi
    
end             % fo

L = find(cellfun(@isempty,output(:,1)),1,'first');
output(L:end,:) = [];

icls = output;

clear pattern* f* chck in curve* L out*

cd('D:\Data\WorkingFolder')